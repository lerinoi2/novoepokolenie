<?php

namespace app\models;

use phpDocumentor\Reflection\Types\Null_;
use Yii;

/**
 * This is the model class for table "{{%camp_order}}".
 *
 * @property integer $id
 * @property integer $camp_id
 * @property integer $camp_shift_id
 * @property integer $booking_end
 * @property integer $entity
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $discount
 * @property boolean $active
 * @property integer $count
 * @property string $token
 * @property integer $create_date
 * @property integer $status
 * @property Camps $camp
 * @property CampsShift $campShift
 */
class WaitingList extends \yii\db\ActiveRecord
{
    const STATUS_NO_ORDER = 0;
    const STATUS_ORDER_CREATE = 1;
    const STATUS_ORDER_END = 2;
    const STATUS_ORDER_PAYED = 3;
    const STATUS_NOT_ACTIVE = 4;

    public static function getStatusesArray()
    {
        return array(
            self::STATUS_NO_ORDER => 'Без брони',
            self::STATUS_ORDER_CREATE => 'Бронь создана',
            self::STATUS_ORDER_END => 'Бронь закончилась',
            self::STATUS_ORDER_PAYED => 'Бронь оплачена',
            self::STATUS_NOT_ACTIVE => 'Не активен',
        );
    }

    public static function tableName()
    {
        return '{{%waiting_list}}';
    }

    public function rules()
    {
        return [
            [['camp_shift_id'], 'required'],
            [['camp_shift_id', 'send_date', 'status', 'order_id', 'date_order_create'], 'integer'],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
            [['camp_shift_id'], 'exist', 'skipOnError' => true, 'targetClass' => CampsShift::className(), 'targetAttribute' => ['camp_shift_id' => 'id']],
            [['name', 'phone', 'email', 'camp_shift_id'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'camp_shift_id' => 'Camp Shift ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'email' => 'Email',
        ];
    }

    public function getCampShift()
    {
        return $this->hasOne(CampsShift::className(), ['id' => 'camp_shift_id']);
    }

    public function getTodayCreate()
    {
        $dayscount = ($this->date_order_create - Yii::$app->formatter->asTimestamp(date("d.M.y"))) / (60 * 60 * 24);
        if ($dayscount == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getStatusString()
    {
        return self::getStatusesArray()[$this->status];
    }

    public function CreateOrder($add = 1)
    {
        if ($this->status == self::STATUS_NO_ORDER) {
            $order = new CampOrder();
            $order->name = $this->name;
            $order->phone = $this->phone;
            $order->email = $this->email;
            $order->count = 1;
            $order->discount = 0;
            $order->camp_shift_id = $this->camp_shift_id;
            $order->camp_id = $this->campShift->camp->id;
            $order->entity = CampOrder::ENTITY_FIZ;
            $now = date("d.M.y");
            if($add == 1){
                $add1 = date("d.M.y", strtotime($now . "+1 day"));
            }else{
                $add1 = date("d.M.y", strtotime($now . "+2 days"));
            }
            $order->booking_end = Yii::$app->formatter->asTimestamp($add1);
            $order->booking_end_string = $add1;

            $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            $count = mb_strlen($chars);
            for ($i = 0, $res = ''; $i < 30; $i++) {
                $ii = rand(0, $count - 1);
                $res .= mb_substr($chars, $ii, 1);
            }
            $order->token = $res;
            $order->price = CampsShift::getCurrentPrice($order->camp_shift_id);

            if ($order->save()) {
                $this->order_id = $order->id;
                $voucher = new Vouchers();
                $voucher->order_id = $order->id;
                $voucher->way = Vouchers::TYPE_CAMP;
                $voucher->save();
                $this->save();
                try {
                    Yii::$app->mailer->compose('@app/mail/CreateOrderwl', ['order' => $order])
                        ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                        ->setTo($order->email)
                        ->setSubject("Забронирована путёвка в лагерь \"Новое поколение\"")
                        ->send();
                } catch (\Exception $exception) {
                    echo $exception;
                }
            } else {
                $this->order_id = -1;
            }
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!$this->isNewRecord) {
                if ($this->status == self::STATUS_NO_ORDER && $this->order_id && $this->order_id > 0) {
                    $this->status = self::STATUS_ORDER_CREATE;
                    $this->date_order_create = Yii::$app->formatter->asTimestamp(date("d.M.y"));
                }
            }
            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }
}

?>