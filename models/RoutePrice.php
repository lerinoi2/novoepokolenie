<?php

namespace app\models;

use Yii;

class RoutePrice extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%route_price}}';
    }

    public function rules()
    {
        return [
            [['route_id', 'price', 'date_start'], 'required'],
            [['route_id'], 'integer'],
            [['price'], 'double'],
            [['route_id'], 'exist', 'skipOnError' => true, 'targetClass' => Routes::className(), 'targetAttribute' => ['route_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'route_id' => 'Номер маршрута',
            'price' => 'Стоимость',
            'date_start' => 'Дата',
        ];
    }

    public function getRoute()
    {
        return $this->hasOne(Routes::className(), ['id' => 'route_id']);
    }

    public function getByRouteId($id)
    {
        return RoutePrice::find()->where(['route_id' => $id])->all();
    }
}
