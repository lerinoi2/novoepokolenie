<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%vouchers}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $birth
 * @property integer $order_id
 * @property integer $way
 * @property string $voucher_number
 * @property string $representative_name
 * @property string $representative_phone
 * @property string $representative_address
 * @property string $who_created
 * @property integer $gender
 * @property integer $squad
 * @property integer $room
 *
 * @property Room $room0
 */
class Vouchers extends \yii\db\ActiveRecord
{

    public $birth_string;
    public $party_tmp;

    const TYPE_CAMP = 1;
    const TYPE_ROUTE = 2;


    public function fields()
    {
        $fields = parent::fields();
        array_push($fields, 'partyid');
        return $fields;
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vouchers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['birth', 'order_id', 'way', 'gender', 'squad', 'room'], 'integer'],
            [['name', 'voucher_number', 'representative_name', 'representative_phone', 'representative_address', 'who_created', 'birth_string'], 'string', 'max' => 255],
            [['room'], 'exist', 'skipOnError' => true, 'targetClass' => Room::className(), 'targetAttribute' => ['room' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'birth' => 'Birth',
            'order_id' => 'Order ID',
            'way' => 'Way',
            'voucher_number' => 'Voucher Number',
            'representative_name' => 'Representative Name',
            'representative_phone' => 'Representative Phone',
            'representative_address' => 'Representative Address',
            'who_created' => 'Who Created',
            'gender' => 'Gender',
            'squad' => 'Squad',
            'room' => 'Room',
        ];
    }

    public function convertDate()
    {
        if ($this->birth == null) {
            $this->birth_string = "";
        } else {
            $this->birth_string = Yii::$app->formatter->asDate($this->birth, "dd.MM.y");
        }
    }

    public function convertDateForXlsx()
    {
        if ($this->birth == null) {
            $this->birth_string = "";
        } else {
            $this->birth_string = Yii::$app->formatter->asDate($this->birth, "php:Y-m-d");
        }
    }

    public function getOrderName()
    {
//        $order = CampOrder::findOne($this->order_id);
//        return $order->name ?: "";
        return "";
    }

    public function getYearOld()
    {
        if ($this->birth != 0) {
            $now = new \DateTime();
            $birthday = new \DateTime($this->birthstring);
            $datedDiff = $now->diff($birthday);

            return $datedDiff->format('%y');
        }
        return 0;
    }

    public function getShiftId()
    {
//        $order = CampOrder::findOne($this->order_id);
//
//        return $order->camp_shift_id;
        return "";
    }

    public function getParty()
    {
        $parties = self::getParties();
        $shiftlist = ShiftList::find()->where(['voucher_id' => $this->id])->one();
        if ($shiftlist) {
            return $parties[$shiftlist->party];
        }
        return 'Не установленно';
    }

    public function getPartyid()
    {
        $shiftlist = ShiftList::find()->where(['voucher_id' => $this->id])->one();
        if ($shiftlist) {
            return $shiftlist->party;
        }
        return -1;
    }

    public function getParties()
    {
        $arr = array();
        for ($i = 1; $i <= 17; $i++) {
            $arr[$i] = $i . ' отряд';
        }

        return $arr;
    }

    public static function getVouchersForShiftList($shift = 'all', $ageform = null, $ageto = null)
    {
        $vouchers = self::find()->where(['not', ['voucher_number' => '']])->andWhere(['way' => self::TYPE_CAMP]);
        $vouchers = $vouchers->orderBy('birth');
        $vouchers = $vouchers->all();
        if ($shift !== 'all') {
            $shift = json_decode($shift);
            foreach ($vouchers as $key => $voucher) {
                if ($voucher->shiftid != $shift) {
                    unset($vouchers[$key]);
                }
            }
        }
        if ($ageform != null) {
            foreach ($vouchers as $key => $voucher) {
                if ($voucher->yearold < $ageform) {
                    unset($vouchers[$key]);
                }
            }
        }
        if ($ageto != null) {
            foreach ($vouchers as $key => $voucher) {
                if ($voucher->yearold > $ageto) {
                    unset($vouchers[$key]);
                }
            }
        }
        return $vouchers;
    }

    public function getBirthstring()
    {
        if ($this->birth != 0) {
            return Yii::$app->formatter->asDate($this->birth, "dd.MM.y");
        }
        return 'Не установленно';
    }

    public function getOrder()
    {
        return $this->hasOne(CampOrder::className(), ['id' => 'order_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->birth = Yii::$app->formatter->asTimestamp($this->birth_string);
            $this->who_created = Yii::$app->user->identity->name;
            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }
}
