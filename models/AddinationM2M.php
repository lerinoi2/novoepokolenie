<?php
namespace app\models;

use phpDocumentor\Reflection\Types\Null_;
use Yii;

/**
 * This is the model class for table "{{%camp_order}}".
 *
 * @property integer $id
 * @property integer $camp_id
 * @property integer $camp_shift_id
 * @property integer $booking_end
 * @property integer $entity
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $discount
 * @property boolean $active
 * @property integer $count
 * @property string $token
 * @property integer $create_date
 * @property integer $status
 * @property Camps $camp
 * @property CampsShift $campShift
 */

class AddinationM2M extends \yii\db\ActiveRecord{


    public static function tableName()
    {
        return '{{%addinationM2M}}';
    }

    public function rules()
    {
        return [
            [['addination_id', 'route_order_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
        ];
    }

    public function getAddination()
    {
        return $this->hasOne(Addination::className(), ['id' => 'addination_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }
}
?>