<?php

namespace app\models;

use Yii;

class CampPrice extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%camp_price}}';
    }

    public function rules()
    {
        return [
            [['camp_id', 'price', 'date_start'], 'required'],
            [['camp_id'], 'integer'],
            [['price'], 'double'],
            [['camp_id'], 'exist', 'skipOnError' => true, 'targetClass' => CampsShift::className(), 'targetAttribute' => ['camp_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'route_id' => 'Номер смены',
            'price' => 'Стоимость',
            'date_start' => 'Дата',
        ];
    }

    public function getCamp()
    {
        return $this->hasOne(CampsShift::className(), ['id' => 'camp_id']);
    }

    public function getByCampId($id)
    {
        return CampPrice::find()->where(['camp_id' => $id])->all();
    }
}
