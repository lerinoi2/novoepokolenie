<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Routes;

/**
 * RoutesSearch represents the model behind the search form of `app\models\Routes`.
 */
class RoutesSearch extends Routes
{
    public $date_from;
    public $date_to;
    public $d_f;
    public $d_t;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'date_start', 'date_end', 'status', 'price', 'places'], 'integer'],
            [['description', 'link', 'name'], 'safe'],
            [['date_from', 'date_to', 'd_f', 'd_t'], 'date', 'format' => 'php:d.m.Y'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Routes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'status' => $this->status,
            'price' => $this->price,
            'places' => $this->places,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['>=', Routes::tableName() . '.date_start', $this->date_from ? strtotime($this->date_from . ' 00:00:00') : null])
            ->andFilterWhere(['<=', Routes::tableName() . '.date_start', $this->date_to ? strtotime($this->date_to . ' 23:59:59') : null])
            ->andFilterWhere(['>=', Routes::tableName() . '.date_end', $this->d_f ? strtotime($this->d_f . ' 00:00:00') : null])
            ->andFilterWhere(['<=', Routes::tableName() . '.date_end', $this->d_t ? strtotime($this->d_t . ' 23:59:59') : null])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
