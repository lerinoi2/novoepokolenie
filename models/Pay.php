<?php

namespace app\models;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "np_pay".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $way
 * @property integer $sum
 * @property integer $date
 * @property boolean $payed
 * @property string sberbank_id
 */
class Pay extends ActiveRecord
{
    public $date_string;

    //Тип платежа.
    const TYPE_ROUTE = 0;
    const TYPE_CAMP = 1;

    public static function getTypesArray()
    {
        return array(
            self::TYPE_ROUTE => 'Маршрут',
            self::TYPE_CAMP => 'Лагерь'
        );
    }

    // Способ оплаты
    const PAY_TYPE_CASH = 0;
    const PAY_TYPE_NON_CASH = 1;
    const PAY_TYPE_CARD = 2;

    public static function getPayTypesArray()
    {
        return array(
            self::PAY_TYPE_CASH => 'Наличные',
            self::PAY_TYPE_NON_CASH => 'Безналичный перевод',
            self::PAY_TYPE_CARD => 'Оплата по карте',
        );
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pay}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'way', 'date', 'type'], 'integer'],
            [['payed'], 'boolean'],
            [['sum'], 'double'],
            [['date_string', 'sberbank_id', 'person'], 'string'],
            [['frompaid'], 'string','enableClientValidation' => false]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'way' => 'Way',
            'sum' => 'Sum',
            'date' => 'Date',
        ];
    }

    public function convertDate()
    {
        $this->date_string = Yii::$app->formatter->asDate($this->date, "dd.MM.y");
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                if ($this->date_string) {
                    $this->date = Yii::$app->formatter->asTimestamp($this->date_string);
                } else {
                    $now = date("d.M.y");
                    $this->date = Yii::$app->formatter->asTimestamp($now);
                }
            }
            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->way == self::TYPE_CAMP) {
            $order = CampOrder::findOne($this->order_id);
            $order->RefreshStatus();
            $order->save();
        }
    }

    public function getDateString(){
        try {
            return Yii::$app->formatter->asDate($this->date, "dd.MM.y");
        } catch (InvalidConfigException $e) {
            return '';
        }
    }

    public function getPayType(){
        $types = Pay::getPayTypesArray();
        return $types[$this->type];
    }
}
