<?php
namespace app\models;

use phpDocumentor\Reflection\Types\Null_;
use Yii;

/**
 * This is the model class for table "{{%camp_order}}".
 *
 */

class Addination extends \yii\db\ActiveRecord{


    public static function tableName()
    {
        return '{{%addination}}';
    }

    public function rules()
    {
        return [
            [['name', 'period'], 'string', 'max' => 255],
            [['price'], 'integer'],
            [['name', 'price'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'perion' => 'Период',
        ];
    }

    public function getNameWithPrice(){
        return $this->name . ' - ' . $this->price . ' руб.';
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }
}
?>