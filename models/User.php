<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%users}}".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property integer $role
 * @property string $name
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

    public $auth_token;
    public $new_password;
    const ROLE_ADMIN = 1;
    const ROLE_ACCOUNTANT = 2;
    const ROLE_MANAGER = 3;
    const ROLE_MARKETING = 4;
    public $salt;

    public static function getRolesArray()
    {
        return array(
            self::ROLE_ADMIN => 'Директор',
            self::ROLE_ACCOUNTANT => 'Бухгалтер',
            self::ROLE_MANAGER => 'Менеджер',
            self::ROLE_MARKETING => 'Маркетолог',
        );
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'role'], 'required'],
            [['role'], 'integer'],
            [['login', 'password', 'name', 'new_password'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'password' => 'Password',
            'role' => 'Role',
            'name' => 'Name',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_token;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_token === $authKey;
    }

    public static function findByUsername($username){
        return static::findOne(['login' => $username]);
    }

    public function validatePassword($password){
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function setPassword($password){
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }


    public function beforeSave($insert)
    {
        if($insert)
        {
            if($this->new_password) {
                $this->setPassword($this->password);
            }
        }

        return parent::beforeSave($insert);
    }

}
