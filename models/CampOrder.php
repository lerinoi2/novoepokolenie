<?php

namespace app\models;

use phpDocumentor\Reflection\Types\Null_;
use Yii;
use yii\db\Expression;
use app\models\Vouchers;

/**
 * This is the model class for table "{{%camp_order}}".
 *
 * @property integer $id
 * @property integer $camp_id
 * @property integer $camp_shift_id
 * @property integer $booking_end
 * @property integer $entity
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $discount
 * @property boolean $active
 * @property integer $count
 * @property string $token
 * @property integer $create_date
 * @property integer $status
 * @property Camps $camp
 * @property CampsShift $campShift
 */
class CampOrder extends \yii\db\ActiveRecord
{
    const ENTITY_FIZ = 0;
    const ENTITY_ORG = 1;

    const STATUS_NOT_PAYED = 0;
    const STATUS_PREPAYED = 1;
    const STATUS_PAYED = 2;
    const STATUS_COMPLETE = 3;

    public $booking_end_string;
    public $create_date_string;

    public static function getEntityesArray()
    {
        return array(
            self::ENTITY_FIZ => 'Физическое лицо',
            self::ENTITY_ORG => 'Юридическое лицо'
        );
    }

    public static function getStatusArray()
    {
        return array(
            self::STATUS_NOT_PAYED => 'Не оплачен',
            self::STATUS_PREPAYED => 'Предоплата',
            self::STATUS_PAYED => 'Оплачен',
            self::STATUS_COMPLETE => 'Завершен'
        );
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%camp_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['camp_id', 'camp_shift_id','entity', 'name', 'phone', 'email', 'discount', 'count'], 'required'],
            [['camp_id', 'camp_shift_id', 'booking_end', 'entity', 'count', 'create_date', 'status'], 'integer'],
            [['active'], 'boolean'],
            [['price'], 'double'],
            [['name', 'phone', 'email', 'booking_end_string', 'token', 'create_date_string'], 'string', 'max' => 255],
            [['camp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Camps::className(), 'targetAttribute' => ['camp_id' => 'id']],
            [['camp_shift_id'], 'exist', 'skipOnError' => true, 'targetClass' => CampsShift::className(), 'targetAttribute' => ['camp_shift_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер заказа',
            'camp_id' => 'Camp ID',
            'camp_shift_id' => 'Camp Shift ID',
            'booking_end' => 'Booking End',
            'entity' => 'Тип плательщика',
            'name' => 'ФИО \ Компания',
            'phone' => 'Телефон',
            'email' => 'Email',
            'discount' => 'Скидка (%)',
            'create_date' => 'Дата заказа',
            'status' => 'Статус',
            'count' => 'Кол-во путёвок',
            'price' => 'Сумма заказа'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCamp()
    {
        return $this->hasOne(Camps::className(), ['id' => 'camp_id']);
    }

    public function getFio()
    {
        return $this->hasOne(Vouchers::className(), ['order_id' => 'id']);
    }

    public function getBirth(){
        return $this->hasOne(Vouchers::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampShift()
    {
        return $this->hasOne(CampsShift::className(), ['id' => 'camp_shift_id']);
    }

    public function getCampShiftName($id){
        return CampsShift::findOne($id)['name'];
    }

    public function allShift(){
        $res = CampsShift::find()->select(["id", "name"])->asArray()->all();
        foreach ($res as $q) {
            $result[$q['id']] = $q['name'];
        }
        return $result;
    }

    public function getEnd($status, $booking_end)
    {
        if ($status != self::STATUS_NOT_PAYED) {
            return 'Не учитывается';
        } else {
            $el = ($booking_end - Yii::$app->formatter->asTimestamp(date("d.M.y"))) / (60 * 60 * 24);
            return [
                $el > 0 ? $el : 0,
                Yii::$app->formatter->asDate($booking_end)
            ];
        }
    }

    public function getVouchers()
    {
        return $this->hasMany(Vouchers::className(), ['order_id' => 'id'])->where(['way' => Vouchers::TYPE_CAMP]);
    }

    public function getPays()
    {
        return $this->hasMany(Pay::className(), ['order_id' => 'id'])->where(['way' => Pay::TYPE_CAMP]);
    }

    public function getPaysPayed()
    {
        return $this->hasMany(Pay::className(), ['order_id' => 'id'])->where(['way' => Pay::TYPE_CAMP, 'payed' => true]);
    }

    public function getSum()
    {
        return $this->price * $this->count * ((100 - $this->discount) / 100);
    }

    public function getDayForEnd()
    {
        if ($this->status != self::STATUS_NOT_PAYED) {
            return 'Не учитывается';
        } else {
            return ($this->booking_end - Yii::$app->formatter->asTimestamp(date("d.M.y"))) / (60 * 60 * 24);
        }
    }

    public function getRemainder()
    {
        $payed = 0;
        foreach ($this->pays as $el) {
            if ($el->sberbank_id && $el->payed) {
                $payed += $el->sum;
            } elseif (!$el->sberbank_id) {
                $payed += $el->sum;
            }
        }
        return $this->sum - $payed;
    }

    public function getSumpayed()
    {
        $payed = 0;
        foreach ($this->pays as $el) {
            if ($el->sberbank_id && $el->payed) {
                $payed += $el->sum;
            } elseif (!$el->sberbank_id) {
                $payed += $el->sum;
            }
        }
        return $payed;
    }

    public function getDiscountPersent()
    {
        return ((100 - $this->discount) / 100);
    }

    public function getCountPlacesPayed()
    {
        $payed = 0;
        foreach ($this->pays as $el) {
            if ($el->sberbank_id && $el->payed) {
                $payed += $el->sum;
            } elseif (!$el->sberbank_id) {
                $payed += $el->sum;
            }
        }
        $price = ($this->price == 0)?CampsShift::getCurrentPrice($this->camp_shift_id):$this->price;
        return floor($payed / ($price * $this->getDiscountPersent()));
    }

    public function getCountVouchersWithNumber()
    {
        $count = 0;
        foreach ($this->vouchers as $el) {
            if ($el->voucher_number) {
                $count++;
            }
        }
        return $count;
    }

    public function convertDate()
    {
        $this->booking_end_string = Yii::$app->formatter->asDate($this->booking_end, "dd.MM.y");
        $this->create_date_string = yii::$app->formatter->asDatetime($this->create_date);
    }

    public function convertDateForXlsx()
    {
        $this->booking_end_string = Yii::$app->formatter->asDate($this->booking_end, "php:Y-m-d");
        $this->create_date_string = yii::$app->formatter->asDate($this->create_date, "php:Y-m-d");
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->RefreshStatus();
            if ($this->isNewRecord) {
                if ($this->booking_end_string) {
                    if ($this->entity == 1){
                        $d = explode('.', $this->booking_end_string);
                        $tomorrow  = date("d.m.Y g:i", mktime(9, 00, 0, $d[1], $d[0] + 1,$d[2]));
                        $q = strtotime($tomorrow);
                        $this->booking_end = $q;
                    } else {
                        $this->booking_end = Yii::$app->formatter->asTimestamp($this->booking_end_string);
                    }
                } else {
                    if (!$this->booking_end) {
                        $now = date("d.M.y");
                        $add15 = date("d.M.y", strtotime($now . "+15 days"));
                        $this->booking_end = Yii::$app->formatter->asTimestamp($add15);
                    }
                    //Если дата не утсновлена, то окончание бронирования закончится через 15 дней.
                }
                $this->status = self::STATUS_NOT_PAYED;
                if ($this->create_date == null) {
                    $now = new \DateTime('now', new \DateTimeZone('Asia/Yekaterinburg'));
                    $this->create_date = yii::$app->formatter->asTimestamp($now);
                }
            } else {
                if ($this->booking_end_string) {
                    $this->booking_end = Yii::$app->formatter->asTimestamp($this->booking_end_string);
                }
            }
            $this->discount = abs($this->discount);
            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }

    public static function getPayedOrder()
    {
        $camps = CampOrder::find()->where(['active' => true])->all();
        foreach ($camps as $key => $el) {
            $pay = Pay::find()->where(['way' => Pay::TYPE_CAMP, 'order_id' => $el->id, 'payed' => true])->all();
            if (count($pay) == 0) {
                unset($camps[$key]);
            }
            unset($pay);
        }
        return $camps;
    }

    public static function getNotPayed()
    {
        $orders = CampOrder::find()->where(['active' => true])
            ->andWhere(['or', ['status' => self::STATUS_PREPAYED], ['status' => self::STATUS_NOT_PAYED]])
            ->all();
        return $orders;
    }

    public static function getPayed($id = 0)
    {
        $camps = CampOrder::find()->where(['active' => true, 'camp_shift_id' => $id])
            ->andWhere(['or', ['status' => self::STATUS_PREPAYED], ['status' => self::STATUS_PAYED], ['status' => self::STATUS_COMPLETE]])
            ->all();
        $count = 0;
        foreach ($camps as $el) {
            if ($el->status == self::STATUS_PREPAYED) {
                $count += $el->countPlacesPayed;
            } else {
                $count += $el->count;
            }
            
        }
        return $count;
    }

    public static function getForVouchers()
    {
        $orders = CampOrder::find()->where(['active' => true])
            ->andWhere(['or', ['status' => self::STATUS_PREPAYED], ['status' => self::STATUS_PAYED]])
            ->all();
        return $orders;
    }

    public static function getAllowed($id = 0)
    {
        $camps = self::MultipleSearchAndFilter('all', 'all', '['.$id.']', "0", "0", '1');
        $shift = CampsShift::findOne($id);
        $count = 0;
        foreach ($camps as $el) {
            if ($el->count != null) {
                $count += $el->count;
            }
        }
        return $shift->places - $count;
    }

    public static function getBooking($id = 0)
    {
        $orders = self::MultipleSearchAndFilter('[0,1]', 'all', '['.$id.']', "0", "0");
        $count = 0;
        foreach ($orders as $key => $el) {
            if ($el->active == true) {
                $count += ($el->count - $el->CountPlacesPayed);
            }
        }

        return $count;
    }

    public static function getOrderForNewBooking($search = '')
    {
        $orders = array();
        $payed = array();
        $counts = array();
        $pays = Pay::find()->where(['way' => Pay::TYPE_CAMP])->all();
        foreach ($pays as $pay) {
            if ($search === '') {
                $order = CampOrder::find()->where(['and', ['id' => $pay->order_id], ['not', ['status' => self::STATUS_COMPLETE]], ['not', ['status' => self::STATUS_NOT_PAYED]]])->one();
            } else {
                $order = CampOrder::find()
                    ->where(['or', ['like', 'name', $search], ['like', 'phone', $search], ['like', 'email', $search], ['like', 'id', $search]])
                    ->andWhere(['id' => $pay->order_id])
                    ->andWhere(['active' => true])
                    ->andWhere(['not', ['status' => self::STATUS_COMPLETE]])
                    ->andWhere(['not', ['status' => self::STATUS_NOT_PAYED]])
                    ->one();
            }
            if ($order != null && !in_array($order, $orders)) {
                $orders[] = $order;
            }
            $shift = CampsShift::findOne($order->camp_shift_id);
            if ($order != null && $payed[$order->id]) {
                $payed[$order->id] += $pay->sum;
                $counts[$order->id] = $payed[$order->id] / ($order->price * ((100 - $order->discount) / 100));
            } else {
                $payed[$order->id] = $pay->sum;
                if ($shift) {
                    $counts[$order->id] = $payed[$order->id] / ($order->price * ((100 - $order->discount) / 100));
                }
            }
        }
        foreach ($orders as $key => $el) {
            $el->campShift->convertDate();
            $el->convertDate();
            $data[$key]['order'] = $el;
            $data[$key]['voucher'] = Vouchers::find()->where(['order_id' => $el->id, 'way' => Pay::TYPE_CAMP])->all();
            foreach ($data[$key]['voucher'] as $voucher) {
                $voucher->convertDate();
            }
            $data[$key]['day_count'] = ($el->booking_end - Yii::$app->formatter->asTimestamp(date("d.M.y"))) / (60 * 60 * 24);
            $data[$key]['pays'] = Pay::find()->where(['order_id' => $el->id, 'way' => Pay::TYPE_CAMP])->all();
            $sumCount = 0;
            foreach ($data[$key]['pays'] as $pay) {
                if ($pay->sberbank_id && $pay->payed) {
                    $sumCount += $pay->sum;
                } elseif (!$pay->sberbank_id && $pay->sum > 0) {
                    $sumCount += $pay->sum;
                }
            }
            $data[$key]['sum'] = $el->count * $el->price * ((100 - $el->discount) / 100);
            $data[$key]['remainder'] = $data[$key]['sum'] - $sumCount;
            $data[$key]['payedcount'] = $counts[$el->id];
        }
        return $data;
    }

    public static function withJoin()
    {
        return self::find()->innerJoinWith('vouchers')->innerJoinWith('campShift');
    }

    public static function SearchAndFilters($search = null, $status = 'all', $entity = 'all', $shift = 'all', $onlyactive = 1, $type = null)
    {
        $orders = self::withJoin();
        if ($search != null) {
            $orders = $orders
                ->where(['like', Vouchers::tableName() . '.name', $search])
                ->orWhere(['like', Vouchers::tableName() . '.voucher_number', $search])
                ->orWhere(['like', self::tableName() . '.name', $search])
                ->orWhere(['like', self::tableName() . '.id', $search])
                ->orWhere(['like', self::tableName() . '.email', $search])
                ->orWhere(['like', self::tableName() . '.phone', $search]);
        }

        switch ($type) {
            case 'payed':
                if ($status !== 'all') {
                    $orders = $orders->andWhere([self::tableName() . '.status' => $status, self::tableName() . '.active' => true]);
                } else {
                    $orders = $orders->andWhere([self::tableName() . '.active' => true])
                        ->andWhere(['or', [self::tableName() . '.status' => self::STATUS_PREPAYED], [self::tableName() . '.status' => self::STATUS_PAYED]]);
                }
                break;
            case 'notpayed':
                if ($status !== 'all') {
                    $orders = $orders->andWhere([self::tableName() . '.status' => $status, self::tableName() . '.active' => true]);
                } else {
                    $orders = $orders->andWhere([self::tableName() . '.active' => true])
                        ->andWhere(['or', [self::tableName() . '.status' => self::STATUS_PREPAYED], [self::tableName() . '.status' => self::STATUS_NOT_PAYED]]);
                }
                break;
            case 'archive':
                $orders = $orders->andWhere([self::tableName() . '.active' => true])
                    ->andWhere([self::tableName() . '.status' => self::STATUS_COMPLETE]);
                break;
            case null:
            default:
                if ($status !== 'all') {
                    if ($status == -1) {
                        $orders = $orders->andWhere([self::tableName() . '.active' => false]);
                    } else {
                        $orders = $orders->andWhere([self::tableName() . '.status' => $status, self::tableName() . '.active' => true]);
                    }
                }
                break;
        }

        if ($entity !== 'all') {
            $orders = $orders->andWhere([self::tableName() . '.entity' => $entity]);
        }
        if ($shift !== 'all') {
            $orders = $orders->andWhere([CampsShift::tableName() . '.id' => $shift]);
        }
        if ($onlyactive == 1) {
            $orders = $orders->andWhere([self::tableName() . '.active' => true]);
        }

        $orders = $orders->orderBy(['id' => SORT_DESC]);

        return $orders->all();
    }

    public static function MultipleSearchAndFilter($status = "all", $entity = "all", $shift = "all", $date_start = null, $date_end = null, $active = 'all')
    {
        $orders = self::withJoin();

        if ($status !== 'all') {
            $status = json_decode($status);
            if (in_array(-1, $status)) {
                $orders = $orders->andWhere(['in', self::tableName() . '.status', $status])->orWhere([self::tableName() . '.active' => false]);
            } else {
                $orders = $orders->andWhere(['in', self::tableName() . '.status', $status])->andWhere([self::tableName() . '.active' => true]);
            }
        }
        if ($entity !== 'all') {
            $entity = json_decode($entity);
            $orders = $orders->andWhere(['in', self::tableName() . '.entity', $entity]);
        }
        if ($shift !== 'all') {
            $shift = json_decode($shift);
            $orders = $orders->andWhere(['in', CampsShift::tableName() . '.id', $shift]);
        }
        if ($date_start === $date_end && $date_start != null && $date_start > 0 && $date_end != null && $date_end > 0) {
            $orders = $orders->andWhere(['>=', self::tableName() . '.create_date', $date_start])->andWhere(['<=', self::tableName() . '.create_date', $date_start + 86400]);
        } else {
            if ($date_start != null && $date_start > 0) {
                $orders = $orders->andWhere(['>=', self::tableName() . '.create_date', $date_start]);
            }
            if ($date_end != null && $date_end > 0) {
                $orders = $orders->andWhere(['<=', self::tableName() . '.create_date', $date_end + 86400]);
            }
        }
        if ($active !== 'all') {
            $orders = $orders->andWhere(['in', self::tableName() . '.active', '1']);
        }
        return $orders->all();
    }

    public function RefreshStatus()
    {
        if ($this != null) {
            if ($this->sumpayed == 0) {
                $this->status = self::STATUS_NOT_PAYED;
            }
            if ($this->sumpayed > 0) {
                $this->status = self::STATUS_PREPAYED;
            }
            if ($this->remainder == 0) {
                $this->status = self::STATUS_PAYED;
            }
            if ($this->CountPlacesPayed == $this->count && $this->CountVouchersWithNumber == $this->CountPlacesPayed && $this->status == self::STATUS_PAYED) {
                $this->status = self::STATUS_COMPLETE;
            }
        }
    }
}