<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "{{%room}}".
 *
 * @property integer $id
 * @property string $number
 * @property string $count
 * @property integer $home
 * @property integer $gender
 *
 */
class Room extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%room}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['home', 'gender', 'count'], 'integer'],
            [['number'], 'string', 'max' => 11],
            [['home'], 'exist', 'skipOnError' => true, 'targetClass' => Home::className(), 'targetAttribute' => ['home' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'count' => 'Count',
            'home' => 'Home',
            'gender' => 'Gender',
        ];
    }

    public static function getRoomForId($id, $home_id)
    {
        $homes = ArrayHelper::map(self::findAll(['camp_shift' => $id, 'home' => $home_id]), 'id', 'number');
        return $homes;
    }
}
