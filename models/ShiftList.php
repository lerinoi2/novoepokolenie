<?php

namespace app\models;

use phpDocumentor\Reflection\Types\Null_;
use Yii;

/**
 * This is the model class for table "{{%camp_order}}".
 *
 * @property integer $id
 * @property integer $voucher_id
 * @property integer $camp_shift_id
 */
class ShiftList extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shift_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['camp_shift_id', 'voucher_id', 'party'], 'required'],
            [['voucher_id', 'camp_shift_id', 'party'], 'integer'],
            [['camp_shift_id'], 'exist', 'skipOnError' => true, 'targetClass' => CampsShift::className(), 'targetAttribute' => ['camp_shift_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'voucher_id' => 'Voucher ID',
            'camp_shift_id' => 'Camp Shift ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampShift()
    {
        return $this->hasOne(CampsShift::className(), ['id' => 'camp_shift_id']);
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }
}