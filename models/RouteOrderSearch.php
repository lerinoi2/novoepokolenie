<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RouteOrder;

/**
 * RouteOrderSearch represents the model behind the search form of `app\models\RouteOrder`.
 */
class RouteOrderSearch extends RouteOrder
{

    public $date_from;
    public $date_to;
    public $date_f;
    public $date_t;
    public $date_start_1;
    public $date_start_2;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'route_id', 'booking_end', 'entity', 'count', 'status', 'active', 'class', 'count_adults', 'count_managers', 'transport', 'abode', 'create_date', 'date_start_contract', 'time_start_contract', 'date_end_contract', 'time_end_contract', 'price'], 'integer'],
            [['name', 'phone', 'email', 'discount', 'school'], 'safe'],
            [['date_from', 'date_to', 'date_f', 'date_t', 'date_start_1', 'date_start_2'], 'date', 'format' => 'php:d.m.Y'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RouteOrder::find();

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['createdate'] = [
            'asc' => [RouteOrder::tableName() . '.create_date' => SORT_ASC],
            'desc' => [RouteOrder::tableName() . '.create_date' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'RouteOrder' => $this->create_date,
//            'route' => $this->id,
            'route_id' => $this->route_id,
            'booking_end' => $this->booking_end,
            'entity' => $this->entity,
            'count' => $this->count,
            'status' => $this->status,
            'active' => $this->active,
            'class' => $this->class,
            'count_adults' => $this->count_adults,
            'count_managers' => $this->count_managers,
            'transport' => $this->transport,
            'abode' => $this->abode,

            'create_date' => $this->create_date,

            'date_start_contract' => $this->date_start_contract,
            'time_start_contract' => $this->time_start_contract,
            'date_end_contract' => $this->date_end_contract,
            'time_end_contract' => $this->time_end_contract,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])

//            ->andFilterWhere(['like', 'route', $this->id])

            ->andFilterWhere(['>=', RouteOrder::tableName() . '.create_date', $this->date_from ? strtotime($this->date_from . ' 00:00:00') : null])
            ->andFilterWhere(['<=', RouteOrder::tableName() . '.create_date', $this->date_to ? strtotime($this->date_to . ' 23:59:59') : null])

            ->andFilterWhere(['>=', RouteOrder::tableName() . '.date_start_contract', $this->date_start_1 ? strtotime($this->date_start_1 . ' 00:00:00') : null])
            ->andFilterWhere(['<=', RouteOrder::tableName() . '.date_start_contract', $this->date_start_2 ? strtotime($this->date_start_2 . ' 23:59:59') : null])

            ->andFilterWhere(['>=', RouteOrder::tableName() . '.booking_end', $this->date_f ? strtotime($this->date_f . ' 00:00:00') : null])
            ->andFilterWhere(['<=', RouteOrder::tableName() . '.booking_end', $this->date_t ? strtotime($this->date_t . ' 23:59:59') : null])

            ->andFilterWhere(['like', 'discount', $this->discount])
            ->andFilterWhere(['like', 'school', $this->school]);

        return $dataProvider;
    }
}
