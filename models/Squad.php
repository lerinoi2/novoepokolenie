<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%squad}}".
 *
 * @property integer $id
 * @property integer $number
 * @property string $age_from
 * @property string $age_to
 * @property string $rooms
 * @property integer $camp_shift
 * @property integer $home
 *
 * @property Home $home0
 * @property CampsShift $campShift
 */
class Squad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%squad}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'camp_shift', 'home'], 'required'],
            [['number', 'camp_shift', 'home'], 'integer'],
            [['rooms'], 'string', 'max' => 255],
            [['home'], 'exist', 'skipOnError' => true, 'targetClass' => Home::className(), 'targetAttribute' => ['home' => 'id']],
            [['camp_shift'], 'exist', 'skipOnError' => true, 'targetClass' => CampsShift::className(), 'targetAttribute' => ['camp_shift' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'rooms' => 'Rooms',
            'camp_shift' => 'Camp Shift',
            'home' => 'Home',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHome0()
    {
        return $this->hasOne(Home::className(), ['id' => 'home']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampShift()
    {
        return $this->hasOne(CampsShift::className(), ['id' => 'camp_shift']);
    }
}
