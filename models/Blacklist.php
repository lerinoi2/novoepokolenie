<?php
namespace app\models;

use phpDocumentor\Reflection\Types\Null_;
use Yii;

/**
 * This is the model class for table "{{%camp_order}}".
 *
 * @property integer $id
 * @property integer $camp_id
 * @property integer $camp_shift_id
 * @property integer $booking_end
 * @property integer $entity
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $discount
 * @property boolean $active
 * @property integer $count
 * @property string $token
 * @property integer $create_date
 * @property integer $status
 * @property Camps $camp
 * @property CampsShift $campShift
 */

class Blacklist extends \yii\db\ActiveRecord{

    public $birth_string;

    public static function tableName()
    {
        return '{{%blacklist}}';
    }

    public function rules()
    {
        return [
            [['birth'], 'integer'],
            [['name','birth_string'], 'string', 'max' => 255],
            [['name', 'birth_string'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'camp_shift_id' => 'Camp Shift ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'email' => 'Email',
        ];
    }

    public function convertDate()
    {
        if($this->birth == null){
            $this->birth_string = "";
        }else{
            $this->birth_string = Yii::$app->formatter->asDate($this->birth, "dd.MM.y");
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->birth = Yii::$app->formatter->asTimestamp($this->birth_string);
            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }
}
?>