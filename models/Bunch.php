<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%bunch}}".
 *
 * @property integer $id
 * @property string $kids
 *
 * @property Squad[] $squads
 */
class Bunch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bunch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kids'], 'required'],
            [['kids'], 'string', 'max' => 255],
            [['camp_shift'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kids' => 'Kids',
            'camp_shift' => 'camp_shift',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSquads()
    {
        return $this->hasMany(Squad::className(), ['bunch' => 'id']);
    }
}
