<?php
/**
 * Created by PhpStorm.
 * User: Amf1k
 * Date: 06.12.2017
 * Time: 17:25
 */

namespace app\models;

use function date;
use function explode;
use Faker\Provider\DateTime;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "contractinfo".
 *
 * @property integer $id
 * @property integer $route_order_id
 * @property string $fio
 * @property string $pasportdata
 * @property integer $datestart
 * @property integer $dateend
 * @property integer $timestart
 * @property integer $timeend
 * @property integer $price
 * @property integer $breakfast
 * @property integer $lunch
 * @property integer $dinner
 * @property integer $transport
 * @property integer $abode
 */

class ContractInfo extends ActiveRecord
{

    public function rules()
    {
        return [
            [['route_order_id', 'fio', 'pasportdata', 'price', 'lunch', 'breakfast', 'dinner', 'transport', 'abode'], 'required'],
            [['route_order_id', 'lunch', 'breakfast', 'dinner'], 'integer'],
            [['price'], 'double'],
            [['fio', 'pasportdata', 'transport', 'abode'], 'string', 'max' => 255],
            [['route_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => RouteOrder::className(), 'targetAttribute' => ['route_order_id' => 'id']],
        ];
    }

    public static function tableName()
    {
        return 'contractinfo';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'route_order_id' => 'Route Order ID',
            'fio' => 'Fio',
            'pasportdata' => 'Pasportdata',
            'price' => 'Сумма заказа',
            'lunch' => 'Обед',
            'breakfast' => 'Завтрак',
            'dinner' => 'Ужин',
            'transport' => 'Транспорт (доставка)',
            'abode' => 'Проживание, размещение (корпус и № комнат)',
        ];
    }

    public function getRouteOrder()
    {
        return $this->hasOne(RouteOrder::className(), ['route_order_id' => 'id']);
    }
}