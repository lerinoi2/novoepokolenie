<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "{{%home}}".
 *
 * @property integer $id
 * @property string $camp_shift
 * @property string $homes
 */
class Home extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%home}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['camp_shift', 'home'], 'required'],
            [['camp_shift', 'home'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'camp_shift' => 'id смены',
            'home' => 'Корпусы',
        ];
    }

    public static function getHomeForId($id)
    {
        $homes = ArrayHelper::map(self::findAll(['camp_shift' => $id]), 'id', 'home');
        return $homes;
    }
}
