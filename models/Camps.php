<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%camps}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 *
 * @property CampsShift[] $campsShifts
 * @property Content[] $contents
 */
class Camps extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    const STATUS_DISABLE = 0;
    const STATUS_ACTIVE = 1;

    public function getStatusesArray()
    {
        return array(
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DISABLE => 'Не активен'
        );
    }

    public static function tableName()
    {
        return '{{%camps}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['status','name'],'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampsShifts()
    {
        return $this->hasMany(CampsShift::className(), ['camp_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents()
    {
        return $this->hasMany(Content::className(), ['camp_id' => 'id']);
    }
}
