<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "np_routes".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_start
 * @property integer $date_end
 * @property string $description
 * @property integer $status
 * @property integer $price
 * @property integer $places
 * @property string $link
 *
 * @property RouteOrder[] $routeOrders
 */
class Upload extends Model
{
    public $file;
    public $type;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'doc'],
            [['type'], 'integer']
        ];
    }
    
}