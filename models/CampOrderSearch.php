<?php

namespace app\models;

use app\models\CampOrder;
use app\models\Vouchers;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CampOrderSearch represents the model behind the search form about `app\models\CampOrder`.
 */
class CampOrderSearch extends CampOrder
{
    public $date_from;
    public $date_to;
    public $fio;
    public $birth;
    public $d_f;
    public $d_t;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'camp_id', 'camp_shift_id', 'booking_end', 'entity', 'count', 'active', 'create_date', 'status', 'price', 'birth'], 'integer'],
            [['name', 'phone', 'email', 'discount', 'token', 'fio'], 'safe'],
            [['date_from', 'date_to', 'd_f', 'd_t'], 'date', 'format' => 'php:d.m.Y'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CampOrder::withJoin();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $dataProvider->sort->attributes['fio'] = [
            'asc' => [Vouchers::tableName() . '.name' => SORT_ASC],
            'desc' => [Vouchers::tableName() . '.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['birth'] = [
            'asc' => [Vouchers::tableName() . '.birth' => SORT_ASC],
            'desc' => [Vouchers::tableName() . '.birth' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->status == 0) {
            $query->andFilterWhere([
                CampOrder::tableName() . '.id' => $this->id,
                CampOrder::tableName() . '.camp_id' => $this->camp_id,
                CampOrder::tableName() . '.camp_shift_id' => $this->camp_shift_id,
                CampOrder::tableName() . '.booking_end' => $this->booking_end,
                CampOrder::tableName() . '.entity' => $this->entity,
                CampOrder::tableName() . '.count' => $this->count,
                CampOrder::tableName() . '.active' => 1,
                CampOrder::tableName() . '.create_date' => $this->create_date,
                CampOrder::tableName() . '.status' => $this->status,
                CampOrder::tableName() . '.price' => $this->price,
            ]);
        }
        elseif ($this->status == 4) {
            $query->andFilterWhere([
                CampOrder::tableName() . '.id' => $this->id,
                CampOrder::tableName() . '.camp_id' => $this->camp_id,
                CampOrder::tableName() . '.camp_shift_id' => $this->camp_shift_id,
                CampOrder::tableName() . '.booking_end' => $this->booking_end,
                CampOrder::tableName() . '.entity' => $this->entity,
                CampOrder::tableName() . '.count' => $this->count,
                CampOrder::tableName() . '.active' => 0,
                CampOrder::tableName() . '.create_date' => $this->create_date,
                CampOrder::tableName() . '.status' => 0,
                CampOrder::tableName() . '.price' => $this->price,
            ]);
        }
        else {
            $query->andFilterWhere([
                CampOrder::tableName() . '.id' => $this->id,
                CampOrder::tableName() . '.camp_id' => $this->camp_id,
                CampOrder::tableName() . '.camp_shift_id' => $this->camp_shift_id,
                CampOrder::tableName() . '.booking_end' => $this->booking_end,
                CampOrder::tableName() . '.entity' => $this->entity,
                CampOrder::tableName() . '.count' => $this->count,
                CampOrder::tableName() . '.active' => $this->active,
                CampOrder::tableName() . '.create_date' => $this->create_date,
                CampOrder::tableName() . '.status' => $this->status,
                CampOrder::tableName() . '.price' => $this->price,
            ]);
        }

        $query->andFilterWhere(['like', CampOrder::tableName() . '.name', $this->name])
            ->andFilterWhere(['like', CampOrder::tableName() . '.phone', $this->phone])
            ->andFilterWhere(['like', CampOrder::tableName() . '.email', $this->email])
            ->andFilterWhere(['like', CampOrder::tableName() . '.discount', $this->discount])
            ->andFilterWhere(['like', CampOrder::tableName() . '.token', $this->token])
            ->andFilterWhere(['>=', CampOrder::tableName() . '.create_date', $this->date_from ? strtotime($this->date_from . ' 00:00:00') : null])
            ->andFilterWhere(['<=', CampOrder::tableName() . '.create_date', $this->date_to ? strtotime($this->date_to . ' 23:59:59') : null])
            ->andFilterWhere(['like', Vouchers::tableName() . '.name', $this->fio])
            ->andFilterWhere(['>=', Vouchers::tableName() . '.birth', $this->d_f ? strtotime($this->d_f . ' 00:00:00') : null])
            ->andFilterWhere(['<=', Vouchers::tableName() . '.birth', $this->d_t ? strtotime($this->d_t . ' 23:59:59') : null]);

        return $dataProvider;
    }
}
