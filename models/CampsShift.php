<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%camps_shift}}".
 *
 * @property integer $id
 * @property integer $camp_id
 * @property string $name
 * @property integer $date_start
 * @property integer $date_end
 * @property integer $date_start_sale
 * @property integer $sort
 * @property integer $status
 * @property integer $conditions
 * @property integer $price
 * @property integer $places
 * @property string $description
 *
 * @property Camps $camp
 * @property Content[] $contents
 */
class CampsShift extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    //Благоустройство
    const CONDITIONS_WELL = 0;
    const CONDITIONS_NOT_WELL = 1;
    const CONDITIONS_TENTS = 2;
    const SAN_WEL = 3;
    const SPEC = 4;

    //Статусы
    const STATUS_DISABLE = 0;
    const STATUS_ACTIVE = 1;

    public $date_start_string;
    public $date_end_string;
    public $date_start_sale_string;


    public $booking;
    public $payed;
    public $allowed;

    public static function getStatusesArray()
    {
        return array(
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DISABLE => 'Не активен'
        );
    }

    public static function getConditionsArray()
    {
        return array(
            self::CONDITIONS_WELL => 'Загородный лагерь отдыха и оздоровления детей',
            self::CONDITIONS_NOT_WELL => 'Лагерь досуга и отдыха',
            self::CONDITIONS_TENTS => 'Детский лагерь палаточного типа',
            self::SAN_WEL => 'Санаторно-оздоровительный детский лагерь',
            self::SPEC => 'Детский специализированный (профильный) лагерь',
        );
    }

    public static function getCurrentPrice($id)
    {
        $camp = static::find()->where([static::tableName() . '.id' => $id])->joinWith(['prices'])->one();
        $camp->convertDate();
        $lastPriceDate = array('date_start' => 0);
        foreach ($camp['prices'] as $price) {
            if ($price['date_start'] > $lastPriceDate['date_start']) {
                $lastPriceDate = $price;
            }
        }
        $camp->price = $lastPriceDate['price'];
        return $camp->price;
    }

    public static function getCampsShift($order = false, $where = false, $andWhere = false)
    {
        $return = static::find()->joinWith(['prices']);
        $return->orderBy(['date_end' => SORT_DESC]);
        if ($order) {
            $return->orderBy($order);
        }
        if ($where) {
            $return->where($where);
        }
        if ($andWhere) {
            $return->andWhere($andWhere);
        }
        $return = $return->all();
        foreach ($return as $camp) {
            $camp->convertDate();
            $lastPriceDate = array('date_start' => 0);
            foreach ($camp['prices'] as $price) {
                if ($price['date_start'] > $lastPriceDate['date_start']) {
                    $lastPriceDate = $price;
                }
            }
            $camp->price = $lastPriceDate['price'];
        }
        return $return;
    }

    public static function tableName()
    {
        return '{{%camps_shift}}';
    }

    public static function getVouchersParty()
    {
        $data = array();
        $shifts = self::find()->where(['status' => self::STATUS_ACTIVE])->all();
        $vouchers = Vouchers::find()->where(['way' => Vouchers::TYPE_CAMP])->andWhere(['not', ['voucher_number' => '']])->with('order')->all();
        foreach ($shifts as $el) {
            $data[$el['id']]['shift']['namewithdate'] = $el->namewithdate;
            foreach ($vouchers as $voucher) {
                if ($voucher->order->camp_shift_id == $el['id']) {
                    $data[$el['id']]['shift']['data'][$voucher->partyid][] = $voucher;
                }
            }
        }
        return $data;
    }

    public function fields()
    {
        $fields = parent::fields();
        array_push($fields, 'booking');
        array_push($fields, 'payed');
        array_push($fields, 'allowed');
        array_push($fields, 'date_start_string');
        array_push($fields, 'date_end_string');
        array_push($fields, 'camp_id');
        array_push($fields, 'namewithdate');
        return $fields;
    }

    public function convertDateForXlsx()
    {
        $this->date_start_string = Yii::$app->formatter->asDate($this->date_start, "php:Y-m-d");
        $this->date_end_string = Yii::$app->formatter->asDate($this->date_end, "php:Y-m-d");
        $this->date_start_sale_string = Yii::$app->formatter->asDate($this->date_start_sale, "php:Y-m-d");
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['camp_id'], 'required'],
            [['camp_id', 'date_start', 'date_end', 'sort', 'conditions', 'places', 'status', 'date_start_sale', 'booking', 'payed'], 'integer'],
            [['name', 'description', 'date_start_string', 'date_end_string', 'date_start_sale_string'], 'string', 'max' => 255],
            [['price'], 'double'],
            [['camp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Camps::className(), 'targetAttribute' => ['camp_id' => 'id']],
            [['camp_id', 'sort', 'conditions', 'places', 'status', 'name', 'description', 'date_start_string', 'date_end_string'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'camp_id' => 'Camp ID',
            'name' => 'Name',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'sort' => 'Sort',
            'status' => 'Status',
            'conditions' => 'Conditions',
            'price' => 'Price',
            'places' => 'Places',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCamp()
    {
        return $this->hasOne(Camps::className(), ['id' => 'camp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents()
    {
        return $this->hasMany(Content::className(), ['camp_shift' => 'id']);
    }

    public function getNameWithDate()
    {
        $this->convertDate();
        return $this->name . ' (' . $this->date_start_string . ' - ' . $this->date_end_string . ')';
    }

    public function convertDate()
    {
        $this->date_start_string = Yii::$app->formatter->asDate($this->date_start, "dd.MM.y");
        $this->date_end_string = Yii::$app->formatter->asDate($this->date_end, "dd.MM.y");
        $this->date_start_sale_string = Yii::$app->formatter->asDate($this->date_start_sale, "dd.MM.y");
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->date_start = Yii::$app->formatter->asTimestamp($this->date_start_string);
            $this->date_end = Yii::$app->formatter->asTimestamp($this->date_end_string);
            $this->date_start_sale = Yii::$app->formatter->asTimestamp($this->date_start_sale_string);
            return parent::beforeSave($insert);
        }
        else {
            return false;
        }
    }

    public function getAllowedPlaces()
    {
        $orders = CampOrder::find()->where(['active' => true, 'camp_shift_id' => $this->id])->all();
        $count = 0;
        foreach ($orders as $order) {
            $count += $order->count;
        }
        return $this->places - $count;
    }

    public function getPayedPlaces()
    {
        $orders = CampOrder::find()->where(['active' => true, 'camp_shift_id' => $this->id])->andWhere(['or', ['status' => CampOrder::STATUS_PAYED, 'status' => CampOrder::STATUS_PREPAYED, 'status' => CampOrder::STATUS_COMPLETE]])->all();
        $count = 0;
        foreach ($orders as $el) {
            $count += $el->CountPlacesPayed;
        }
        return $count;
    }

    public function getBookingPlaces()
    {
        $orders = CampOrder::find()->select(['SUM(count)'])->where(['active' => true, 'camp_shift_id' => $this->id, 'status' => CampOrder::STATUS_NOT_PAYED])->all();
        return $orders - $this->payedplaces;
    }

    public function getPrices()
    {
        return $this->hasMany(CampPrice::className(), ['camp_id' => 'id'])->where(['<=', CampPrice::tableName() . '.`date_start`', time()]);
    }
}
