<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "np_routes".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_start
 * @property integer $date_end
 * @property string $description
 * @property integer $status
 * @property integer $price
 * @property integer $places
 * @property string $link
 *
 * @property RouteOrder[] $routeOrders
 */
class Routes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DISABLE = 0;
    const STATUS_ACTIVE = 1;

    public $date_start_string;
    public $date_end_string;
    public $file;

    
    public function getStatusesArray()
    {
        return array(
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DISABLE => 'Не активен'
        );
    }

    public function fields()
    {
        $fields = parent::fields();
        array_push($fields, 'allowedplaces');
        return $fields;
    }

    public function convertDate()
    {
        $this->date_start_string = Yii::$app->formatter->asDate($this->date_start, "dd.MM.y");
        $this->date_end_string = Yii::$app->formatter->asDate($this->date_end, "dd.MM.y");
    }

    public static function tableName()
    {
        return '{{%routes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_start', 'date_end', 'status', 'price', 'places'], 'integer'],
            [['name', 'description', 'link'], 'string', 'max' => 255],
            [['date_start_string', 'date_end_string'], 'string'],
            [['date_start_string', 'date_end_string', 'status', 'price', 'places', 'description', 'link'], 'required'],
            [['file'], 'file'],
            [['filename'], 'string'],
        ];
    }

    public function validateDate($attribute, $params)
    {
        if (!$this->hasErrors()) {

        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'date_start' => 'Дата начала',
            'date_end' => 'Дата конца',
            'description' => 'Description',
            'status' => 'Статус',
            'price' => 'Цена',
            'places' => 'Мест',
            'link' => 'Ссылка',
            'allowedplaces' => 'Доступно мест',
            'file' => 'Программа маршрута'
        ];
    }

    public function upload()
    {
        $file_name = explode('.', $this->file->name);
        $name = time();
        $this->file->saveAs('data/programs_marsh/' . $name . '.' . $file_name[1]);
        $this->filename = $name . '.' . $this->file->extension;
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRouteOrders()
    {
        return $this->hasMany(RouteOrder::className(), ['route_id' => 'id']);
    }
    
    public static function getRoutes()
    {
        // $return = static::find()->joinWith(['prices'])->all();
        $return = static::find()->all();
        foreach ($return as $route) {
            $route->convertDate();
        //     $lastPriceDate = array('date_start' => 0);
        //     foreach ($route['prices'] as $price) {
        //         if ($price['date_start'] > $lastPriceDate['date_start']) {
        //             $lastPriceDate = $price;
        //         }
        //     }
        //     $route->price = $lastPriceDate['price'];
        }
        return $return;
    }

    public function getBookingplaces()
    {
        $orders = RouteOrder::find()->where(['active' => true, 'route_id' => $this->id])->all();
        $count = 0;
        foreach ($orders as $el) {
            $count += $el->count + $el->count_adults + $el->count_managers;
        }

        return $this->places > $count;
    }

    public function getAllowedplaces()
    {
        $orders = RouteOrder::find()->where(['route_id' => $this->id])->all();
        $count = 0;
        foreach ($orders as $el) {
            $count += $el->count + $el->count_adults + $el->count_managers;
        }

        return $this->places - $count;
    }

    public function getAllowedplaces_new($id, $places)
    {
        $orders = RouteOrder::find()->where(['route_id' => $id])->all();
        $count = 0;
        foreach ($orders as $el) {
            $count += $el->count + $el->count_adults + $el->count_managers;
        }

        return $places - $count;
    }

    public static function getAllowed()
    {
        $routes = self::find()->where(['status' => self::STATUS_ACTIVE])->all();
        foreach ($routes as $key => $el) {
            if ($el->allowedplaces <= 0) {
                unset($routes[$key]);
            }
        }
        return $routes;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->date_start = Yii::$app->formatter->asTimestamp($this->date_start_string);
            $this->date_end = Yii::$app->formatter->asTimestamp($this->date_end_string);
            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }

    public function getDateStart()
    {
        return Yii::$app->formatter->asDate($this->date_start, "dd.MM.y");
    }

    public function getDateEnd()
    {
        return Yii::$app->formatter->asDate($this->date_end, "dd.MM.y");
    }

    public function getPrices()
    {
        return $this->hasMany(RoutePrice::className(), ['route_id' => 'id'])->where(['<=',RoutePrice::tableName().'.`date_start`', time()]);
    }

}
