<?php

namespace app\models;

use function count;
use function intval;
use function substr;
use Yii;
use yii\base\Model;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\base\ErrorException;
use app\models\ContractInfo;


/**
 * This is the model class for table "np_route_order".
 *
 * @property integer $id
 * @property integer $route_id
 * @property integer $booking_end
 * @property integer $entity
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $discount
 * @property string $count
 * @property integer $create_date
 * @property integer $status
 * @property boolean $active
 * @property string $school
 * @property integer $class
 * @property integer $count_adults
 * @property integer $count_managers
 * @property boolean $transport
 * @property boolean $abode
 * @property integer $date_start_contract
 * @property integer $date_end_contract
 * @property integer $time_start_contract
 * @property integer $time_end_contract
 *
 * @property Routes $route
 * @property ContractInfo $contract
 */
class RouteOrder extends ActiveRecord
{
    private $_datestartcontract;
    private $_timestartcontract;
    private $_dateendcontract;
    private $_timeendcontract;

    /**
     * @inheritdoc
     */
    public $addination;
    public $file;

    const ENTITY_FIZ = 0;
    const ENTITY_ORG = 1;

    const STATUS_NOT_PAYED = 0;
    const STATUS_PREPAYED = 1;
    const STATUS_PAYED = 2;
    const STATUS_COMPLETE = 3;
    const STATUS_MODERATE = 4;
    const STATUS_NOT_CONFIRM = 5;

    public $create_date_string;
    public $booking_end_string;

    public static function getEntityesArray()
    {
        return array(
            self::ENTITY_FIZ => 'Физическое лицо',
            self::ENTITY_ORG => 'Юридическое лицо'
        );
    }

    public static function getStatusArray()
    {
        return array(
            self::STATUS_NOT_PAYED => 'Не оплачен',
            self::STATUS_PREPAYED => 'Предоплата',
            self::STATUS_PAYED => 'Оплачен',
            self::STATUS_COMPLETE => 'Завершен',
            self::STATUS_MODERATE => 'На модерации',
            self::STATUS_NOT_CONFIRM => 'Не подтверждён',
        );
    }

    public static function tableName()
    {
        return '{{%route_order}}';
    }

    public static function reload()
    {
        // $arr = self::getNotPayed();
        $arr = self::find()->all();
        foreach ($arr as $routeOrder) {
            if ($routeOrder->endsum && $routeOrder->endsum <= $routeOrder->sumpayed) {
                $routeOrder->status = self::STATUS_PAYED;
            } elseif ($routeOrder->sumpayed > 0) {
                $routeOrder->status = self::STATUS_PREPAYED;
            } else {
                $routeOrder->status = self::STATUS_NOT_PAYED;
            }
            $routeOrder->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['route_id', 'entity', 'count', 'count_adults', 'count_managers', 'transport', 'abode'], 'required'],
            [['route_id', 'booking_end', 'entity', 'count', 'create_date', 'status', 'count_adults', 'count_managers', 'date_start_contract', 'date_end_contract', 'time_start_contract', 'time_end_contract'], 'integer'],
            [['active', 'transport', 'abode'], 'boolean'],
            [['name', 'phone', 'email', 'discount', 'school', 'class', 'BookingEnd', 'DateStartContact', 'DateEndContact', 'TimeStartContact', 'TimeEndContact'], 'string', 'max' => 255],
            [['route_id'], 'exist', 'skipOnError' => true, 'targetClass' => Routes::className(), 'targetAttribute' => ['route_id' => 'id']],
            ['addination', 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'create_date' => 'Дата заказа',
            'id' => 'Номер заказа',
            'addinations' => 'Комментарий',
            'route_id' => 'Route ID',
            'Route' => 'Маршрут',
            'booking_end' => 'Booking End',
            'entity' => 'Тип плательщика',
            'name' => 'Заказчик',
            'phone' => 'Телефон',
            'email' => 'Email',
            'discount' => 'Скидка (%)',
            'addination' => 'addination',
            'BookingEnd' => 'Дата окончания брони',
            'DateStartContact' => 'Дата заезда',
            'DateEndContact' => 'Дата выезда',
            'TimeStartContact' => 'Время заезда',
            'TimeEndContact' => 'Время выезда',
            'transportstring' => 'Транспорт',
            'daterange' => 'Дата заезда-выезда',
            'SchoolClass' => 'Школа - класс',
            'places' => 'Количество (общее)',
            'count' => 'Дети',
            'count_adults' => 'Взрослые',
            'count_managers' => 'Руководитель',
            'date_start_contract' => 'Дата заезда',
            'status' => 'Статус'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(Routes::className(), ['id' => 'route_id']);
    }

    public function getContract()
    {
        return $this->hasOne(ContractInfo::className(), ['route_order_id' => 'id']);
    }

    public function getVouchers()
    {
        return $this->hasMany(Vouchers::className(), ['order_id' => 'id'])->where(['way' => Vouchers::TYPE_ROUTE]);
    }

    public function getPays()
    {
        return $this->hasMany(Pay::className(), ['order_id' => 'id'])->where(['way' => Pay::TYPE_ROUTE]);
    }

    public function getSum()
    {
        return $this->route->price * ($this->count + $this->count_adults) * ((100 - $this->discount) / 100);
    }

    public function getDayForEnd()
    {
        return ($this->booking_end - Yii::$app->formatter->asTimestamp(date("d.M.y"))) / (60 * 60 * 24);
    }

    public function getRemainder()
    {
        $payed = 0;
        foreach ($this->pays as $el) {
            if ($el->sberbank_id && $el->payed) {
                $payed += $el->sum;
            } elseif (!$el->sberbank_id) {
                $payed += $el->sum;
            }
        }
        try {
            return $this->endsum - $payed;
        } catch (ErrorException $e) {
            if ($payed == 0) {
                return '';
            } else {
                return 'Сумма заказа не установлена';
            }
        }

    }

    public function getSumpayed()
    {
        $payed = 0;
        foreach ($this->pays as $el) {
            if ($el->sberbank_id && $el->payed) {
                $payed += $el->sum;
            } elseif (!$el->sberbank_id) {
                $payed += $el->sum;
            }
        }
        return $payed;
    }

    public function getDiscountPersent()
    {
        return ((100 - $this->discount) / 100);
    }

    public function getCountPlacesPayed()
    {
        $payed = 0;
        foreach ($this->pays as $el) {
            if ($el->sberbank_id && $el->payed) {
                $payed += $el->sum;
            } elseif (!$el->sberbank_id) {
                $payed += $el->sum;
            }
        }
        return round($payed / ($this->route->price * $this->getDiscountPersent()), 0, PHP_ROUND_HALF_DOWN);
    }

    public static function getPayed()
    {
        return RouteOrder::find()
            ->where(['active' => true])
            ->andWhere(['or', ['status' => self::STATUS_PREPAYED], ['status' => self::STATUS_PAYED]])
            ->all();
    }

    public static function getNotPayed()
    {
        return RouteOrder::find()
            ->where(['active' => true])
            ->andWhere(['or', ['status' => self::STATUS_PREPAYED], ['status' => self::STATUS_NOT_PAYED]])
            ->all();
    }

    public function getPlaces()
    {
        return $this->count + $this->count_adults + $this->count_managers;
    }

    public function getCreatedateT()
    {
        return Yii::$app->formatter->asDate($this->create_date, "dd.MM.y");
    }

    public function convertDate()
    {
        $this->booking_end_string = Yii::$app->formatter->asDate($this->booking_end, "dd.MM.y");
        $this->create_date_string = yii::$app->formatter->asDatetime($this->create_date);
    }

    public function getAddinationList()
    {
        $q  = ArrayHelper::map(Addination::find()->all(), 'id', 'namewithprice');
        foreach ($q as $key => $value) {
            $ar[$key] = $value;
        }
        return $ar;
    }

    public function getEndSum()
    {
//        $price = intval($this->contract->price);
//        return $price > 0 ? $price : '';
        // return $price;
        return "";
    }

    public function getAddinations()
    {
        $m2m = AddinationM2M::find()->where(['route_order_id' => $this->id])->all();
        $addsum = array();
        foreach ($m2m as $m) {
            $addsum[] = $m->addination;
        }
        return $addsum;
    }

    public function beforeSave($insert)
    {   
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                if (!$this->booking_end) {
                    $now = date("d.M.y");
                    $add15 = date("d.M.y", strtotime($now . "+15 days"));
                    $this->booking_end = Yii::$app->formatter->asTimestamp($add15);
                    // $this->status = self::STATUS_MODERATE;
                }
                if (!$this->status) {
                    // $this->status = self::STATUS_MODERATE;
                    $this->status = self::STATUS_NOT_PAYED;
                }
                $this->active = true;
                if (!$this->create_date) {
                    $now = new \DateTime('now', new \DateTimeZone('Asia/Yekaterinburg'));
                    $this->create_date = yii::$app->formatter->asTimestamp($now);
                }
            }
            $this->discount = abs($this->discount);
            return parent::beforeSave($insert);
        } else {
            return false;
        }
    }

    public function getAddination()
    {
        $arr = array();
        $m2m = AddinationM2M::find()->where(['route_order_id' => $this->id])->all();
        foreach ($m2m as $key => $m) {
            $arr[] = $m->addination_id;
        }
        return $arr;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $m2m = AddinationM2M::find()->where(['route_order_id' => $this->id])->all();
        foreach ($m2m as $m) {
            $m->delete();
        }
        if ($this->addination) {
            foreach ($this->addination as $el) {
                if (AddinationM2M::find()->where(['route_order_id' => $this->id, 'addination_id' => $el])->one() == null) {
                    $add = new AddinationM2M();
                    $add->route_order_id = $this->id;
                    $add->addination_id = $el;
                    $add->save();
                }
            }
        }
    }

    public function getPriceString($price)
    {
        return self::ucfirst_utf8(self::num2str($price));
    }

    public function PricingStringForDay($day = 1)
    {
        $price = $this->endsum * $day;

        return self::ucfirst_utf8(self::num2str($price));
    }

    function num2str($num)
    {
        $nul = 'ноль';
        $ten = array(
            array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
            array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        );
        $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
        $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
        $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
        $unit = array( // Units
            array('копейка', 'копейки', 'копеек', 1),
            array('рубль', 'рубля', 'рублей', 0),
            array('тысяча', 'тысячи', 'тысяч', 1),
            array('миллион', 'миллиона', 'миллионов', 0),
            array('миллиард', 'милиарда', 'миллиардов', 0),
        );
        //
        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
                else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk > 1) $out[] = $this->morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
            } //foreach
        } else $out[] = $nul;
        $out[] = $this->morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

    /**
     * Склоняем словоформу
     * @ author runcore
     */
    function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) return $f5;
        $n = $n % 10;
        if ($n > 1 && $n < 5) return $f2;
        if ($n == 1) return $f1;
        return $f5;
    }

    function ucfirst_utf8($stri)
    {
        if ($stri{0} >= "\xc3")
            return (($stri{1} >= "\xa0") ?
                    ($stri{0} . chr(ord($stri{1}) - 32)) :
                    ($stri{0} . $stri{1})) . substr($stri, 2);
        else return ucfirst($stri);
    }

    function getDops()
    {
        $dops = '';
        foreach ($this->addinations as $addination) {
            $dops .= $addination->name;
            $dops .= ', ';
        }
        return substr($dops, 0, strlen($dops) - 2);
    }

    function getBookingEnd()
    {
        return $this->booking_end ? Yii::$app->formatter->asDate($this->booking_end, "dd.MM.y") : '';
    }

    function setBookingEnd($value)
    {
        $this->booking_end = Yii::$app->formatter->asTimestamp($value);
    }

    function getDateStartContact()
    {
        return $this->date_start_contract ? Yii::$app->formatter->asDate($this->date_start_contract, "dd.MM.y") : '';
    }

    function setDateStartContact($value)
    {
        $this->date_start_contract = Yii::$app->formatter->asTimestamp($value);
    }

    function getDateEndContact()
    {
        return $this->date_end_contract ? Yii::$app->formatter->asDate($this->date_end_contract, "dd.MM.y") : '';
    }

    function setDateEndContact($value)
    {
        $this->date_end_contract = Yii::$app->formatter->asTimestamp($value);
    }

    function getTimeStartContact()
    {
        return $this->time_start_contract ? Yii::$app->formatter->asDate($this->time_start_contract, "php:H:i") : '12:00';
    }

    function setTimeStartContact($value)
    {
        $time = explode(":", $value);
        $now = new \DateTime();
        $now->setTime($time[0], $time[1]);
        $this->time_start_contract = Yii::$app->formatter->asTimestamp($now);
    }

    function getTimeEndContact()
    {
        return $this->time_end_contract ? Yii::$app->formatter->asDate($this->time_end_contract, "php:H:i") : '11:00';
    }

    function setTimeEndContact($value)
    {
        $time = explode(":", $value);
        $now = new \DateTime();
        $now->setTime($time[0], $time[1]);
        $this->time_end_contract = Yii::$app->formatter->asTimestamp($now);
    }

    function getTransportString()
    {
        $arr = array(
            0 => 'Свой транспорт',
            1 => 'Нужен транспорт'
        );
        return $arr[$this->transport];
    }

    function getDateRange()
    {
        return $this->DateStartContact . ' - ' . $this->DateEndContact;
    }

    function getSchoolClass()
    {
        return $this->school . ' / ' . $this->class;
    }


    public static function SearchAndFilter($search = "", $onlyactive = false, $date_start = null, $date_end = null)
    {
        $orders = self::find();

        if ($search) {
            $orders = $orders
                ->where(['like', self::tableName().'.name', $search])
                ->orWhere(['like', self::tableName().'.phone', $search])
//                ->orWhere(['like', ContractInfo::tableName().'.route_order_id', $search])
                ->orWhere(['like', self::tableName().'.email', $search])
                ->orWhere(['like', self::tableName().'.school', $search])
                ->orWhere(['like', self::tableName().'.id', $search])
                ->orWhere(['like', self::tableName().'.class', $search]);
        }

        if ($onlyactive) {
            $orders = $orders->andWhere([self::tableName().'.active' => true]);
        }

        if ($date_start || $date_end) {
            $date_start = $date_start ? yii::$app->formatter->asTimestamp($date_start) : 0;
            $date_end = $date_end ? yii::$app->formatter->asTimestamp($date_end) : 0;
        }

        if ($date_start === $date_end && $date_start != null && $date_start > 0 && $date_end != null && $date_end > 0) {
            $orders = $orders->andWhere(['>=', self::tableName().'.date_start_contract', $date_start])->andWhere(['<=', self::tableName().'.date_end_contract', $date_start]);
        } else {
            if ($date_start != null && $date_start > 0) {
                $orders = $orders->andWhere(['>=', self::tableName().'.date_start_contract', $date_start]);
            }
            if ($date_end != null && $date_end > 0) {
                $orders = $orders->andWhere(['<=', self::tableName().'.date_end_contract', $date_end]);
            }
        }

        return $orders->all();
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    function getCreateDate(){
//        try{
//            $date = yii::$app->formatter->asDatetime($this->create_date);
//        }catch (Exception $e){
//            $date = 'Произошла ошибка';
//        }
//        return $this->create_date;
    }

    function getAllroute(){
        $res = Routes::find()->select(["id", "name"])->asArray()->all();
        foreach ($res as $q) {
            $result[$q['id']] = $q['name'];
        }
        return $result;
    }
}
