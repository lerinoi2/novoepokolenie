<?php

namespace app\modules\adminer\controllers;

use yii\web\Controller;

class AdminerController extends Controller
{
    public $enableCsrfValidation = false;
    public $layout = false;
    public $defaultAction = 'adminer';

    public function actionAdminer()
    {
        @ob_start();
        session_start();
        return $this->render('adminer');
    }
}
