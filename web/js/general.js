function setSettings() {
    var data = JSON.parse(localStorage.getItem('listSettings'));
    if (data != null) {
        for (var i = 0; i < data.length; i++) {
            $("input[data-col-id=" + data[i] + "]").attr('checked', false);
            $('[data-col=' + data[i] + ']').hide();
        }
    }
}

const SORT_ASC = 4;
const SORT_DESC = 3;

$(document).ready(function () {

    $('form').on('beforeSubmit', function()
    {
        var $form = $(this);
        var $submit = $form.find(':submit');
        $submit.prop('disabled', true);
        $submit.html('<span class="fa fa-spin fa-spinner"></span> Processing...');
    });

    $('#pay').click(function (ev) {
        ev.preventDefault();
        var orderid = $('#number_order').val();
            $.ajax({
                url: 'http://novoepokolenie.testck.ru/api/createmarsh',
                type: "POST",
                data: {"order_id": orderid},
                success: function (data) {
                    var resData = JSON.parse(data);
                    if (resData.error == 3) {
                        //console.log('Ошибка оплаты');
                    } else {
                        window.location.href = resData;
                    }
                },
            });
        // }
    })


    $('#sendContract').click(function (ev) {
        ev.preventDefault();
        var id = location.pathname.slice(location.pathname.lastIndexOf('/') + 1, location.pathname.length);
        $.post('/route_order/sendcontract/' + id, $('#form2').serialize());
    })
    $('#saveContract, #saveContractAndStay').click(function (ev) {
        ev.preventDefault();
        var id = location.pathname.slice(location.pathname.lastIndexOf('/') + 1, location.pathname.length);
        $.post($(this).attr('href'), $('#form2').serialize());
    })
    $(document).on('click', '.deleteRoutePrice', function (ev) {
        ev.preventDefault();
        if (confirm('Вы уверены, что хотите удалить безвозвратно?')) {
            $.post($(this).attr('href'), function (data) {
                $(".ajaxTable").html(data);
            });
        }
    })
    $(document).on('submit', '#addRoutePrice', function (ev) {
        ev.preventDefault();
        $.post('/route_price/create/?', $(this).serialize(), function (data) {
            $(".ajaxTable").html(data);
        });
    })
    $(document).on('click', '.deleteCampPrice', function (ev) {
        ev.preventDefault();
        if (confirm('Вы уверены, что хотите удалить безвозвратно?')) {
            $.post($(this).attr('href'), function (data) {
                $(".ajaxTable").html(data);
            });
        }
    })
    $(document).on('submit', '#addCampPrice', function (ev) {
        ev.preventDefault();
        $.post('/camp_price/create/?', $(this).serialize(), function (data) {
            $(".ajaxTable").html(data);
        });
    })
    $(document).on('click', '.moderateOrder', function (ev) {
        ev.preventDefault();
        $.post($(this).attr('href'), 'comment=' + $('#comment').val());
    })
    $(document).on('click', '._saveVoucher', function (ev) {
        ev.preventDefault();
        var attr = $(this).data();
        $.post('/camp_order/voucher_save/' + attr.id, false, function (data) {
            $('#vouchers-' + attr.key + '-voucher_number').val(data);
        });
        $(this).addClass('disabled');
    })
    // Взято из setSettings()
    $(document).on('click', '#editlist', function (ev) {
        ev.preventDefault();
        $('#editlist').parent().find('.items').toggle(250);
    });
    $("tr[data-id]").click(function () {
        $(this).find('.lurking table').toggle();
    })
    $("input[data-col-id]").change(function () {
        if ($(this).is(':checked')) {
            $('[data-col=' + $(this).attr('data-col-id') + ']').show();
            var data = JSON.parse(localStorage.getItem('listSettings'));
            if (data !== null) {
                if (data.indexOf($(this).attr('data-col-id')) != -1) {
                    data.splice(data.indexOf($(this).attr('data-col-id')), 1);
                    localStorage.setItem('listSettings', JSON.stringify(data));
                }
            }
        } else {
            var data = JSON.parse(localStorage.getItem('listSettings'));
            if (data === null) {
                localStorage.setItem('listSettings', JSON.stringify([$(this).attr('data-col-id')]));
            } else {
                if (data.indexOf($(this).attr('data-col-id')) == -1) {
                    data.push($(this).attr('data-col-id'));
                    localStorage.setItem('listSettings', JSON.stringify(data));
                }
            }
            $('[data-col=' + $(this).attr('data-col-id') + ']').hide();
        }
    })
    // Взято из setSettings() - end
    $('#routeorder-datestartcontact').datepicker({
        format: "dd.mm.yyyy"
    });
    $('#routeorder-dateendcontact').datepicker({
        format: "dd.mm.yyyy"
    });
    $('#date_start_price').datepicker({
        format: "dd.mm.yyyy"
    });
    $('#date_start').datepicker({
        format: "dd.mm.yyyy"
    });
    $('#date_end').datepicker({
        format: "dd.mm.yyyy"
    });
    $('#date_start_sale').datepicker({
        format: "dd.mm.yyyy"
    });
    $('#booking_end').datepicker({
        format: "dd.mm.yyyy"
    });
    $('#pay-date_string').datepicker({
        format: "dd.mm.yyyy"
    });
    $('textarea').froalaEditor();
    $('#phone').mask('+7 (999) 999-99-99', {
        placeholder: "+7 (___) ___-__-__"
    });
    $('.phoneMask').mask('+7 (999) 999-99-99', {
        placeholder: "+7 (___) ___-__-__"
    });
    $('[id*="birth_string"]').datepicker({
        format: "dd.mm.yyyy"
    });
    $('#count_voucher').on('change', function () {
        var count = $(this).val();
        var container = $('#voucher-container');
        var html = '';
        var current = container.find('.row').length;
        for (var i = current; i < count; i++) {
            html +=
                '<hr>' +
                '<div class="row">' +
                '    <div class="form-group field-vouchers-' + i + '-id">' +
                '        <input type="hidden" id="vouchers-' + i + '-id" class="form-control" name="Vouchers[' + i + '][id]">' +
                '        <p class="help-block help-block-error"></p>' +
                '    </div>' +
                '    <div class="col-md-4">' +
                '        <div class="form-group field-vouchers-' + i + '-name">' +
                '            <label class="control-label" for="vouchers-' + i + '-name">Имя ребенка</label><input type="text" id="vouchers-' + i + '-name" class="form-control" name="Vouchers[' + i + '][name]">' +
                '            <p class="help-block help-block-error"></p>' +
                '        </div>' +
                '    </div>' +
                '    <div class="col-md-3">' +
                '        <div class="form-group field-vouchers-' + i + '-birth_string">' +
                '            <label class="control-label" for="vouchers-' + i + '-birth_string">День рождения</label><input type="text" id="vouchers-' + i + '-birth_string" class="form-control" name="Vouchers[' + i + '][birth_string]">' +
                '            <p class="help-block help-block-error"></p>' +
                '        </div>' +
                '    </div>' +
                '    <div class="col-md-4">' +
                '        <div class="form-group field-vouchers-' + i + '-voucher_number">' +
                '            <label class="control-label" for="vouchers-' + i + '-voucher_number">Номер путёвки</label><input type="text" id="vouchers-' + i + '-voucher_number" class="form-control" name="Vouchers[' + i + '][voucher_number]" readonly="">' +
                '            <p class="help-block help-block-error"></p>' +
                '        </div>' +
                '    </div>' +
                '    <div class="col-md-1">' +
                '    </div>' +
                '    <div class="col-md-4">' +
                '        <div class="form-group field-vouchers-' + i + '-representative_name">' +
                '            <label class="control-label" for="vouchers-' + i + '-representative_name">Законный представитель (ФИО)</label><input type="text" id="vouchers-' + i + '-representative_name" class="form-control" name="Vouchers[' + i + '][representative_name]">' +
                '            <p class="help-block help-block-error"></p>' +
                '        </div>' +
                '    </div>' +
                '    <div class="col-md-3">' +
                '        <div class="form-group field-vouchers-' + i + '-representative_phone">' +
                '            <label class="control-label" for="vouchers-' + i + '-representative_phone">Телефон</label><input type="phone" id="vouchers-' + i + '-representative_phone" class="form-control phoneMask" name="Vouchers[' + i + '][representative_phone]" placeholder="+7 (___) ___-__-__" maxlength="18">' +
                '            <p class="help-block help-block-error"></p>' +
                '        </div>' +
                '    </div>' +
                '    <div class="col-md-4">' +
                '        <div class="form-group field-vouchers-' + i + '-representative_address">' +
                '            <label class="control-label" for="vouchers-' + i + '-representative_address">Адрес</label><input type="text" id="vouchers-' + i + '-representative_address" class="form-control" name="Vouchers[' + i + '][representative_address]">' +
                '            <p class="help-block help-block-error"></p>' +
                '        </div>' +
                '    </div>' +
                '    <div class="col-md-1">' +
                '    </div>' +
                '<div class="col-md-4">\n' +
                '<div class="form-group field-vouchers-' + i + '-gender has-success">\n' +
                '<label class="control-label" for="vouchers-' + i + '-gender">Пол ребенка</label><select id="vouchers-' + i + '-gender" class="form-control" name="Vouchers[' + i + '][gender]" aria-invalid="false">\n' +
                '<option value="0">Женский</option>\n' +
                '<option value="1" selected="">Мужской</option>\n' +
                '</select>\n' +
                '<p class="help-block help-block-error"></p>\n' +
                '</div>                            </div>' +
                '</div>';
        }
        $(html).appendTo(container);
        $('[id*="birth_string"]').datepicker({
            format: "dd.mm.yyyy"
        });
        $('.phoneMask').mask('+7 (999) 999-99-99', {
            placeholder: "+7 (___) ___-__-__"
        });

        if (count < 1) {
            container.html('');
            container.parent().find('h2').hide();
        }
    });
    setSettings();
    $('#search').submit(function (ev) {
        ev.preventDefault();
        $('#search').find('button').attr('disabled', true);
        var str = $(this).find('#filter_search').val();
        var status = $(this).find('#filter_status').val();
        var entity = $(this).find('#filter_entity').val();
        var shift = $(this).find('#filter_shift').val();
        var active = $(this).find('#filter_active').is(':checked') ? 1 : 0;
        if (str === undefined) {
            str = "";
        }
        $.get(location.pathname, {
            search: str,
            stat: status,
            entity: entity,
            shift: shift,
            onlyactive: active
        }).done(function (data) {
            $('#content').html(data);
            $('#search').find('button').attr('disabled', false);
            setSettings();
        });
    });
    $('#search').on("click", "button[type='reset']", function (ev) {
        ev.preventDefault();
        $('#inlineFormInputGroup').val('');
        $.get(location.pathname, {search: ''}).done(function (data) {
            $('#content').html(data);
            $('#search').find('button').attr('disabled', false);
            setSettings();
        });
    });
    $('#route_search').submit(function (ev) {
        ev.preventDefault();
        $(this).find('button').attr('disabled', true);
        var str = $(this).find('#searchStr').val();
        var active = $(this).find('#active').is(':checked') ? 1 : 0;
        var datestart = $(this).find('#date_start').val();
        var dateend = $(this).find('#date_end').val();
        if (str === undefined) {
            str = "";
        }
        $.get(location.pathname, {
            search: str,
            onlyactive: active,
            date_start: datestart ? datestart : null,
            date_end: dateend ? dateend : null
        }).done(function (data) {
            $('#content').html(data);
            $('#route_search').find('button').attr('disabled', false);
            setSettings();
        });
    });
    $('#route_search').on("click", "button[type='reset']", function (ev) {
        ev.preventDefault();
        $('#inlineFormInputGroup').val('');
        $.get(location.pathname, {search: ''}).done(function (data) {
            $('#content').html(data);
            $('#route_search').find('button').attr('disabled', false);
            setSettings();
        });
    });
    $('#generate').click(function (ev) {
        ev.preventDefault();
        var status = [];
        $('#filter_status option:selected').each(function (index, data) {
            status.push($(data).val());
        })
        var entity = [];
        $('#filter_entity option:selected').each(function (index, data) {
            entity.push($(data).val());
        })
        var shift = [];
        $('#filter_shift option:selected').each(function (index, data) {
            shift.push($(data).val());
        })
        var datestart = $('#date_start').val();
        var dateend = $('#date_end').val();
        var link = location.href + '/generate?status=' + (status == 'all' ? 'all' : JSON.stringify(status))
            + '&entity=' + (entity == 'all' ? 'all' : JSON.stringify(entity))
            + '&shift=' + (shift == 'all' ? 'all' : JSON.stringify(shift))
            + '&date_start=' + (datestart ? datestart : null)
            + '&date_end=' + (dateend ? dateend : null);
        var win = window.open(link, '_blank');
        $.get(location.pathname + '/generate', {
            status: status == 'all' ? 'all' : JSON.stringify(status),
            entity: entity == 'all' ? 'all' : JSON.stringify(entity),
            shift: shift == 'all' ? 'all' : JSON.stringify(shift),
            date_start: datestart ? datestart : null,
            date_end: dateend ? dateend : null
        });
    });
    $('#report').submit(function (ev) {
        ev.preventDefault();
        $('#search').find('button').attr('disabled', true);
        var status = [];
        $(this).find('#filter_status option:selected').each(function (index, data) {
            status.push($(data).val());
        })
        var entity = [];
        $(this).find('#filter_entity option:selected').each(function (index, data) {
            entity.push($(data).val());
        })
        var shift = [];
        $(this).find('#filter_shift option:selected').each(function (index, data) {
            shift.push($(data).val());
        })
        var datestart = $(this).find('#date_start').val();
        var dateend = $(this).find('#date_end').val();
        $.get(location.pathname, {
            status: status == 'all' ? 'all' : JSON.stringify(status),
            entity: entity == 'all' ? 'all' : JSON.stringify(entity),
            shift: shift == 'all' ? 'all' : JSON.stringify(shift),
            date_start: datestart ? datestart : null,
            date_end: dateend ? dateend : null
        }).done(function (data) {
            $('#content').html(data);
            $('#search').find('button').attr('disabled', false);
        });
    });
    $('#smsStats').submit(function (ev) {
        ev.preventDefault();
        $('#search').find('button').attr('disabled', true);
        var datestart = $(this).find('#date_start').val();
        var dateend = $(this).find('#date_end').val();
        $.get(location.pathname, {
            date_start: datestart ? datestart : null,
            date_end: dateend ? dateend : null
        }).done(function (data) {
            $('#content').html(data);
            $('#search').find('button').attr('disabled', false);
        });
    });
    $('#mailing').submit(function (ev) {
        $('.container_loading').show();
        ev.preventDefault();
        $('#search').find('button').attr('disabled', true);
        var status = [];
        $(this).find('#filter_status option:selected').each(function (index, data) {
            status.push($(data).val());
        })
        var entity = [];
        $(this).find('#filter_entity option:selected').each(function (index, data) {
            entity.push($(data).val());
        })
        var shift = [];
        $(this).find('#filter_shift option:selected').each(function (index, data) {
            shift.push($(data).val());
        })
        var mailtheme = $('#mail_theme').val();
        var temp_id = $(this).find('#temp_id').val();
        $.get(location.pathname, {
            status: status == 'all' ? 'all' : JSON.stringify(status),
            entity: entity == 'all' ? 'all' : JSON.stringify(entity),
            shift: shift == 'all' ? 'all' : JSON.stringify(shift),
            theme: mailtheme,
            temp_id: temp_id
        }).done(function (data) {
            var dataJson = JSON.parse(data);
            if (dataJson.ERROR) {
                switch (dataJson.ERROR) {
                    case 0:
                        alert('Ошибка.');
                        break;
                    case 1:
                        alert('Тема не должна быть пустой!');
                        break;
                    case 2:
                        alert('Не правильно выбран шаблон!');
                        break;
                    case 3:
                        alert('Адресная книга в SendPulse не создалась!');
                        break;
                }
                $('.container_loading').hide();
            } else if (dataJson.error_code == 791) {
                alert('Превышен лимит отправки писем, попробуйте позже.');
                $('.container_loading').hide();
            } else if (dataJson.error_code == 709) {
                var timerId = setInterval(function () {
                    $.get('/mailing/sendbytemplate', {
                        temp_id: dataJson.temp_id,
                        book_id: dataJson.book_id,
                        theme: dataJson.theme
                    })
                        .done(function (data) {
                            var tmpdata = JSON.parse(data);
                            if (tmpdata.status == 1) {
                                clearInterval(timerId);
                                alert('Письма успешно отправлены!');
                                $('.container_loading').hide();
                            }
                        })
                }, 5000);
            } else {
                alert('Письма успешно отправлены!');
                $('.container_loading').hide();
            }

        })
    });
    $('#sendsms').submit(function (ev) {
        ev.preventDefault();
        $('#search').find('button').attr('disabled', true);
        var status = [];
        $(this).find('#filter_status option:selected').each(function (index, data) {
            status.push($(data).val());
        })
        var entity = [];
        $(this).find('#filter_entity option:selected').each(function (index, data) {
            entity.push($(data).val());
        })
        var shift = [];
        $(this).find('#filter_shift option:selected').each(function (index, data) {
            shift.push($(data).val());
        })
        var body = $('#sms_body').val();
        $.get(location.pathname, {
            status: status == 'all' ? 'all' : JSON.stringify(status),
            entity: entity == 'all' ? 'all' : JSON.stringify(entity),
            shift: shift == 'all' ? 'all' : JSON.stringify(shift),
            body: body,
        }).done(function (data) {
            alert(data);
        });
    });
    $('#vouchers').submit(function (ev) {
        ev.preventDefault();
        $('#search').find('button').attr('disabled', true);
        var shift = $(this).find('#filter_shift option:selected').val();
        var ageform = $('#age_from').val();
        var ageto = $('#age_to').val();
        $.get(location.pathname, {
            shift: shift == 'all' ? 'all' : JSON.stringify(shift),
            ageform: ageform,
            ageto: ageto,
        }).done(function (data) {
            $('#content').html(data);
            $('#search').find('button').attr('disabled', false);
        });
    });
    $('.create-voucher').submit(function (ev) {
        ev.preventDefault();
        let check = [];
        let num = $('input[id*=-voucher_number]');
        $(num).each(function (index, data) {
            let vouch = $(data).val()
            if (vouch) {
                let parent_1 = $(data).parent(),
                    parent_2 = $(parent_1).parent(),
                    parent = $(parent_2).parent();
                $(parent).find('input').each(function (index, data) {
                    let input_val = $(data).val();
                    if (!input_val) {
                        if (!$(data).attr('data-address')) {
                            $(data).prop('required', true);
                            $(data).addClass('border-danger');
                            check.push(1);
                        }
                    } else {
                        $(data).removeClass('border-danger');
                        check.push(0);
                    }
                })
            }
        })
        if ($.inArray(1, check) == -1) {
            $(this).unbind('submit').submit()
        }
    });

    $('.add-home').click(function (e) {
        e.preventDefault();
        $('.item').last().after('                    <div class="item">\n' +
            '                        <div style="display: inline-block">Номер корпуса:</div>\n' +
            '                        <div style="display: inline-block">\n' +
            '                            <input type="text">\n' +
            '                        </div>\n' +
            '                    </div>');
    });
    $('.save-home').click(function (e) {
        e.preventDefault();
        let arrayIds = [];
        $('#content input').each(function (ind, elem) {
            arrayIds.push($(elem).val());
        });
        $.get(location.pathname + '/save', {
            arrayIds: JSON.stringify(arrayIds),
            id: $('#content').data('id')
        }).done(function (data) {
            location.reload();
        });
    });
    $('.remove-home').click(function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        $(this).closest('.item').remove();
        $.get(location.pathname + '/remove', {
            id_home: id,
        });
    });
    $('.add-room').click(function (e) {
        e.preventDefault();

            $('.item').last().after('<div class="item">\n' +
                '                        <div class="numbers">\n' +
                '                            <div class="inlime">Номер комнаты:</div>\n' +
                '                            <input type="text">\n' +
                '                        </div>\n' +
                '                        <div class="counts">\n' +
                '                            <div class="inlime">Количество мест:</div>\n' +
                '                            <input type="text">\n' +
                '                        </div>\n' +
                '                    </div>');
    });
    $('.save-room').click(function (e) {
        e.preventDefault();
        let arData = [];
        $('.item').each(function (ind, el) {
            if (ind === 0){
                return true;
            }
            let num = $(el).find('.numbers input').val(),
                count = $(el).find('.counts input').val(),
                gender = $(el).find('.genders select').val();
            arData.push({'num': num, 'count': count, 'gender': gender})
        })
        $.get(location.pathname + '/save', {
            arData: JSON.stringify(arData),
            id: $('#content').data('id'),
            home_id: $('#content').data('home')
        }).done(function (data) {
            location.reload();
        });
    });
    $('.remove-room').click(function (e) {
        e.preventDefault();
        let num_room = $(this).parent().data('id');
        $(this).closest('.item').remove();
        $.get(location.pathname + '/remove', {
            num_room: num_room,
        });
    });
    $('.add-squad').click(function (e) {
        e.preventDefault();
        $.get(location.pathname + '/get', {
            id: $('#content').data('id'),
        }).done(function (data) {
            $('.item').last().after(data);
        });
    });
    $('.remove-squad').click(function (e) {
        e.preventDefault();
        let id = $(this).closest('.item').data('squad');
        $(this).closest('.item').remove();
        $.get(location.pathname + '/remove', {
            squad_id: id,
        });
    });
    $(document).on("click", ".save-squad", function (e) {
        e.preventDefault();
        let arrNumbers = [],
            arrAgeFrom = [],
            arrAgeTo = [],
            arrHome = [],
            error = 0, c = 0;
        $('.number input').each(function (ind, elem) {
            if ($(elem).val().length > 0) {
                arrNumbers.push($(elem).val());
            } else {
                $(elem).css('border', '1px solid red');
                error = 1;
            }
        });
        $('.age_from input').each(function (ind, elem) {
            if ($(elem).val().length > 0) {
                arrAgeFrom.push($(elem).val());
            } else {
                $(elem).css('border', '1px solid red');
                error = 1;
            }
        });
        $('.age_to input').each(function (ind, elem) {
            if ($(elem).val().length > 0) {
                arrAgeTo.push($(elem).val());
            } else {
                $(elem).css('border', '1px solid red');
                error = 1;
            }
        });
        $('.home option').each(function (ind, elem) {
            if (elem.selected) {
                arrHome.push($(elem).val());
            }
        });
        if (error == 0) {
            $.get(location.pathname + '/save', {
                id: $('#content').data('id'),
                arrNumbers: JSON.stringify(arrNumbers),
                arrAgeFrom: JSON.stringify(arrAgeFrom),
                arrAgeTo: JSON.stringify(arrAgeTo),
                arrHome: JSON.stringify(arrHome),
            }).done(function (data) {
                location.reload();
            });
        }
    });
    $('.save-gender').click(function (e) {
        e.preventDefault();
        let idq = $(this).data('id'),
            gender = $(this).parent().prev().find('option:selected').val();
        $.get(location.pathname + '/save', {
            idq: idq,
            gender: gender,
        });

        $(this).remove();
    });
    $('.checkbox-bunch').click(function () {
        let id = $(this).parent().parent().data('id'),
            count = 0;
        $('.checkbox-bunch').each(function (i, e) {
            if ($(this).is(":checked")) {
                count++;
            }
        });
        $('.checkbox-bunch').each(function (i, e) {
            if (count == 5) {
                if (!$(this).is(":checked")) {
                    $(e).attr('disabled', 'true');
                }
            } else {
                $(e).removeAttr('disabled');
            }
        });
        if (count > 1) {
            $(this).parent().parent().parent().parent().parent().find('a.btn').removeClass('dis-btn');
        } else {
            $(this).parent().parent().parent().parent().parent().find('a.btn').addClass('dis-btn');
        }
    });
    $('.create-bunch').click(function (e) {
        e.preventDefault();
        let arIds = [];
        $('.checkbox-bunch').each(function (i, e) {
            if ($(e).is(":checked")) {
                arIds.push($(e).data('id'))
            }
        });
        $.get(location.pathname + '/save', {
            arIds: JSON.stringify(arIds),
        }).done(function (data) {
            location.reload();
        });
    });

    let all = $('.all').data('all');
    let current = $('.all').data('currnet');
    $('.all').kumaGauge({
        value: current,
        min: 0,
        max: all,
        animationSpeed: 1000,
        showNeedle: false,
        label: {
            display: true,
            left: 0,
            right: all,
            fontSize: 15,
        }
    });

    let b_all = $('.boys').data('all');
    let b_current = $('.boys').data('currnet');
    $('.boys').kumaGauge({
        value: b_current,
        min: 0,
        max: b_all,
        animationSpeed: 1000,
        showNeedle: false,
        label: {
            display: true,
            left: 0,
            right: b_all,
            fontSize: 15,
        }
    });
    let g_all = $('.girls').data('all');
    let g_current = $('.girls').data('currnet');
    $('.girls').kumaGauge({
        value: g_current,
        min: 0,
        max: g_all,
        animationSpeed: 1000,
        showNeedle: false,
        label: {
            display: true,
            left: 0,
            right: g_all,
            fontSize: 15,
        }
    });
    let qqqq_all = $('.qqqq').data('all');
    let qqqq_current = $('.qqqq').data('currnet');
    $('.qqqq').kumaGauge({
        value: qqqq_current,
        min: 0,
        max: qqqq_all,
        animationSpeed: 1000,
        showNeedle: false,
        label: {
            display: true,
            left: 0,
            right: qqqq_all,
            fontSize: 15,
        }
    });
    $('.all-print').click(function (e) {
        e.preventDefault();
        $.get(location.pathname + '/print', {})
            .done(function (data) {
                console.log(data)
            });
    });
    let nb_all = $('.n-b').data('all');
    let nb_current = $('.n-b').data('currnet');
    $('.n-b').kumaGauge({
        value: nb_current,
        min: 0,
        max: nb_all,
        animationSpeed: 1000,
        showNeedle: false,
        label: {
            display: true,
            left: 0,
            right: nb_all,
            fontSize: 15,
        }
    });
    let ng_all = $('.n-g').data('all');
    let ng_current = $('.n-g').data('currnet');
    $('.n-g').kumaGauge({
        value: ng_current,
        min: 0,
        max: ng_all,
        animationSpeed: 1000,
        showNeedle: false,
        label: {
            display: true,
            left: 0,
            right: ng_all,
            fontSize: 15,
        }
    });
    $('.remove-kid').click(function (e) {
        e.preventDefault();
        let a = $(this).data('id'),
            b = $(this).data('bunch');
        $.get(location.pathname + '/remove', {
            a: a,
            b: b,
        }).done(function (data) {
            location.reload();
        });
    });
    $('.reform-all').click(function (e) {
        e.preventDefault();
        $.get(location.pathname + '/reform', {}).done(function (data) {
            location.reload();
        });
    });
    $(document).on('click', '.edit', function (e) {
        e.preventDefault();
        let parent = $(this).parent().parent(),
            inp = parent.find('input'),
            sel = parent.find('select');
        if ($(this).attr('data-edit') == '0') {
            $(this).text('Cохранить');
            $(this).attr('data-edit', '1');
            inp.each(function (i, el) {
                if (i > 0) {
                    $(el).removeAttr('disabled');
                }
            });
            sel.each(function (i, el) {
                $(el).removeAttr('disabled');
            });
        } else {
            $(this).text('Редактировать');
            $(this).attr('data-edit', '0');
            inp.each(function (i, el) {
                $(el).attr('disabled', 'true');
            });
            sel.each(function (i, el) {
                $(el).attr('disabled', 'true');
            });
            let num = parent.find('.number input').val(),
                from = parent.find('.age_from input').val(),
                to = parent.find('.age_to input').val(),
                home = parent.find(".home option:selected").val(),
                rooms = [];
            parent.find('.rooms select option').each(function (i, e) {
                if (e.selected) {
                    rooms.push($(e).val());
                }
            });
            $.get(location.pathname + '/saveone', {
                num: num,
                from: from,
                to: to,
                home: home,
                rooms: JSON.stringify(rooms)
            }).done(function (data) {
                location.reload();
            });
        }
    });

    $(".rmpay").click(function (e) {
        e.preventDefault();
        let link = $(this).attr("href");
        $.get(location.origin + link, {}).done(function (data) {
            location.reload();
        });
    })
});

function changeNumber(target) {
    let value = $(target).val();
    if (value != "") {

    }
}

if (location.pathname.indexOf('/camp_order/voucher/create/') >= 0) {
    if (countpayed == countwithnumber) {
        $('[id*=-voucher_number]').attr('readonly', true);
    }
    let vouchersnumber = [];
    let vouchers = $('[id*=-voucher_number]');
    for (i in vouchers) {
        if (vouchers[i].value != "" && vouchers[i].value != undefined) {
            vouchersnumber.push(vouchers[i]);
        }
    }
    if (countpayed == 0) {
        $('[id*=-voucher_number]').attr('readonly', true);
    } else {
        $('[id*=-voucher_number]').keyup(function (ev) {
            let inarr = false;
            if (vouchersnumber.length == 0) {
                vouchersnumber.push(ev.target);
            }
            for (i in vouchersnumber) {
                if ($(vouchersnumber[i]).is($(ev.target))) {
                    if ($(ev.target).val() == "") {
                        vouchersnumber.splice(i, 1);
                    } else {
                        inarr = true;
                    }
                } else {
                    vouchersnumber.push(ev.target);
                    inarr = true;
                }
            }
            countwithnumber = vouchersnumber.length;
            if (countwithnumber < 2) {
                $('[id*=-voucher_number]').attr('readonly', true);
                $(ev.target).attr('readonly', false);
            }
            if (countwithnumber == 0) {
                $('[id*=-voucher_number]').attr('readonly', false);
            }
            if (countpayed == vouchers.length) {
                $('[id*=-voucher_number]').attr('readonly', false);
            }
        })
    }
}