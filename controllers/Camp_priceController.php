<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\CampPrice;

class Camp_priceController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }
    
    public function actionCreate()
    {
        $model = new CampPrice();
        if ($model->load(Yii::$app->request->post())) {
            $model->date_start = Yii::$app->formatter->asTimestamp($model->date_start);
            $model->save();
        }
        return $this->renderPartial('/camp_price/index', ['prices' => CampPrice::getByCampId($model->camp_id), 'price' => $model, 'camp_id' => $model->camp_id]);
    }

    public function actionDelete($id){
        $model = CampPrice::findOne($id);
        $camp_id = $model->camp_id;
        if ($model != NULL) {
            $model->delete();
        }
        $price = new CampPrice();
        return $this->renderPartial('/camp_price/index', ['prices' => CampPrice::getByCampId($camp_id), 'price' => $price, 'camp_id' => $camp_id]);
    }
}
