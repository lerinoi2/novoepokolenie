<?php

namespace app\controllers;

use app\models\Bunch;
use app\models\CampOrder;
use app\models\Camps;
use app\models\CampsShift;
use app\models\Home;
use app\models\Room;
use app\models\Squad;
use app\models\Vouchers;
use kartik\mpdf\Pdf;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;


class SquadController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    //поставить пол index
    public function actionSetgender()
    {
        $shifts = ArrayHelper::map(CampsShift::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'namewithdate');
        return $this->render('setgender', ['shifts' => $shifts]);
    }

    //поставить пол - список детей для смены
    public function actionSetgendersquad($id)
    {
        $shift = CampsShift::findOne(['id' => $id]);
        $arKids = [];
        $orders = CampOrder::SearchAndFilters(null, 'all', 'all', $id, 1);
        foreach ($orders as $val) {
            foreach ($val->vouchers as $value) {
                if (is_null($value->gender)) {
                    $arKids[] = array(
                        'id' => $value->id,
                        'name' => $value->name,
                    );
                }
            }
        }
        return $this->render('setgendersquad', ['kids' => $arKids, 'shift' => $shift]);
    }

    //поставить пол - сохранить пол
    public function actionSavegender($id, $idq, $gender)
    {
        $voucher = Vouchers::find()->where(['id' => $idq])->one();
        $voucher->gender = $gender;
        $voucher->birth_string = gmdate("Y-m-d", $voucher->birth);
        $voucher->save();
    }

    //создать корпус - выбор смены
    public function actionHome()
    {
        $shifts = ArrayHelper::map(CampsShift::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'namewithdate');
        return $this->render('home', ['shifts' => $shifts]);
    }

    //создать корпус - создание копусов для смены
    public function actionHomeedit($id)
    {
        $shift = CampsShift::findOne(['id' => $id]);
        $homes = Home::getHomeForId($id);
        return $this->render('edit', ['homes' => $homes, 'id' => $id, 'shift' => $shift]);
    }

    //создать корпус - сохранение корпуса
    public function actionSavehome($arrayIds, $id)
    {
        $homes = Home::getHomeForId($id);
        $arIds = json_decode($arrayIds);
        foreach ($arIds as $val) {
            if (!in_array($val, $homes)) {
                $model = new Home();
                $model->camp_shift = $id;
                $model->home = $val;
                $model->save();
            }
        }
        return $this->render('edit', ['homes' => $homes, 'id' => $id]);
    }

    //создать корпус - удаление корпуса
    public function actionRemovehome($id, $id_home)
    {
        $rooms = Room::find()->where(['home' => $id_home])->all();
        foreach ($rooms as $val) {
            $kids = Vouchers::find()->where(['room' => $val->id])->all();
            if ($kids) {
                foreach ($kids as $value) {
                    $kid = Vouchers::findOne(['id' => $value->id]);
                    $kid->squad = null;
                    $kid->room = null;
                    $kid->birth_string = gmdate("Y-m-d", $kid->birth);
                    $kid->save();
                }
            }
            $room = Room::findOne(['id' => $val->id]);
            $room->delete();
        }
        $model = Home::find()->where(['id' => $id_home, 'camp_shift' => $id])->one();
        $model->delete();
    }

    //список комнат для корпуса
    public function actionEditroom($id, $home_id)
    {
        $shift = CampsShift::findOne(['id' => $id]);
        $rooms = Room::find()->where(['camp_shift' => $id, 'home' => $home_id])->all();
        return $this->render('room_edit', ['rooms' => $rooms, 'id' => $id, 'home_id' => $home_id, 'shift' => $shift]);
    }

    //создание комнат
    public function actionSaveroom($arData, $id, $home_id)
    {
        $arData = json_decode($arData, true);
        foreach ($arData as $key => $value) {
            $model = Room::findOne(['camp_shift' => $id, 'home' => $home_id, 'number' => $value['num']]);
            if (!$model->id) {
                $room = new Room();
                $room->number = $value['num'];
                $room->count = $value['count'];
                $room->gender = $value['gender'];
                $room->home = $home_id;
                $room->camp_shift = $id;
                $room->save();
            }
            else {
                $model->number = $value['num'];
                $model->count = $value['count'];
                $model->gender = $value['gender'];
                $model->home = $home_id;
                $model->camp_shift = $id;
                $model->save();
            }
        }

        return true;
    }

    //удаление комнат
    public function actionRemoveroom($num_room)
    {
        $kids = Vouchers::find()->where(['room' => $num_room])->all();
        if ($kids) {
            foreach ($kids as $value) {
                $kid = Vouchers::findOne(['id' => $value->id]);
                $kid->squad = null;
                $kid->room = null;
                $kid->birth_string = gmdate("Y-m-d", $kid->birth);
                $kid->save();
            }
        }
        $model = Room::findOne(['id' => $num_room]);
        $model->delete();
    }

    //создать отряд - список смен
    public function actionSquad()
    {
        $shifts = ArrayHelper::map(CampsShift::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'namewithdate');
        return $this->render('squad', ['shifts' => $shifts]);
    }

    //создать отряд - список отрядов
    public function actionSquadedit($id)
    {
        $shift = CampsShift::findOne(['id' => $id]);
        $squads = Squad::find()->where(['camp_shift' => $id])->all();
        $homes = Home::find()->where(['camp_shift' => $id])->all();
        $count = [];
        $rooms = Room::find()->where(['camp_shift' => $id])->all();
        foreach ($squads as $key => $squad) {
            usort($squads, function ($a, $b) {
                return ($b->number - $a->number);
            });
        }
        foreach ($squads as $squad) {
            if ($squad->rooms && $squad->rooms !== '[]') {
                $ar_rooms = json_decode($squad->rooms, true);
                foreach ($ar_rooms as $room) {
                    $r = Room::find()->where(['id' => $room])->asArray()->one();
                    $count[$squad->id] += $r['count'];
                }
            }
        }

        $orders = CampOrder::SearchAndFilters(null, 'all', 'all', $id, 1);
        foreach ($orders as $val) {
            foreach ($val->vouchers as $value) {
                if (!is_null($value->voucher_number) and (!is_null($value->gender))) {
                    if ($value->gender == 0) {
                        $arKids['girls'][] = array(
                            'age' => $this->calculate_age(gmdate("Y-m-d", $value->birth))
                        );
                    }
                    else {
                        $arKids['boys'][] = array(
                            'age' => $this->calculate_age(gmdate("Y-m-d", $value->birth))
                        );
                    }
                }
            }
        }
        usort($arKids['girls'], function ($a, $b) {
            return ($a['age'] - $b['age']);
        });
        usort($arKids['boys'], function ($a, $b) {
            return ($a['age'] - $b['age']);
        });

        $girls_age = array_count_values(array_column($arKids['girls'], 'age'));
        $boys_age = array_count_values(array_column($arKids['boys'], 'age'));

        $arKids = array();
        $arKids['boys'] = $boys_age;
        $arKids['girls'] = $girls_age;

        return $this->render('squad_edit', [
            'squads' => $squads,
            'id' => $id,
            'homes' => $homes,
            'shift' => $shift,
            'kids' => $arKids,
            'count' => $count,
            'rooms' => $rooms
        ]);
    }

    //создать отряд - создать отряд
    function calculate_age($birthday)
    {
        $birthday_timestamp = strtotime($birthday);
        $age = date('Y') - date('Y', $birthday_timestamp);
        if (date('md', $birthday_timestamp) > date('md')) {
            $age--;
        }
        return $age;
    }

    //создать отряд - сохранить отряды
    public function actionGetsquad($id)
    {
        $homes = Home::find()->where(['camp_shift' => $id])->all();
        $html = "<div class='item'>
                        <div class='number'>
                            <div class='text'>Номер отряда:</div>
                            <div style='display: inline-block'><input type='text' class='form-control'></div>
                        </div>
                        <div class='home'>
                            <div class='text'>Корпус:</div>
                            <div style='display: inline-block'>
                                <select class='select-home' style='min-width: 100px'>
                                ";
        $html2 = '';
        foreach ($homes as $k => $val) {
            $html2 .= "<option value='" . $val->id . "'>" . $val->home . "</option>";
        }
        $html3 = "</select>
                            </div>
                        </div>";
        $html5 = "
                    </div>";
        return $html . $html2 . $html3 . $html5;
    }

    //создать отряд - удалить отряд
    public function actionSavesquad($id, $arrNumbers, $arrAgeFrom, $arrAgeTo, $arrHome)
    {
        $arNum = json_decode($arrNumbers);
        $arFrom = json_decode($arrAgeFrom);
        $arTo = json_decode($arrAgeTo);
        $arHome = json_decode($arrHome);
        foreach ($arNum as $key => $value) {
            $model = Squad::findOne(['camp_shift' => $id, 'number' => $value, 'home' => $arHome[$key]]);
            if (!$model->id) {
                $squad = new Squad();
                $squad->number = $value;
                $squad->camp_shift = $id;
                $squad->form = 1;
                $squad->home = $arHome[$key];
                $squad->save();
            }
            else {
                $model->number = $value;
                $model->camp_shift = $id;
                $model->form = 1;
                $model->home = $arHome[$key];
                $model->save();
            }
        }
    }

    //создать отряд - формирование / переформирование отрядов
    public function actionRemovesquad($squad_id)
    {
        $kids = Vouchers::find()->where(['squad' => $squad_id])->all();
        if ($kids) {
            foreach ($kids as $value) {
                $kid = Vouchers::findOne(['id' => $value->id]);
                $kid->squad = null;
                $kid->room = null;
                $kid->birth_string = gmdate("Y-m-d", $kid->birth);
                $kid->save();
            }
        }
        $squad = Squad::findOne(['id' => $squad_id]);
        $arBunch = json_decode($squad->bunch);
        foreach ($arBunch as $val) {
            $bunch = Bunch::findOne(['id' => $val]);
            $bunch->delete();
        }
        $squad->delete();
    }

    //формирование отрядов
    public function actionReformsquad($id)
    {
        //отряды от мелких к большим
        $squads = Squad::find()->where(['camp_shift' => $id])->asArray()->all();
        foreach ($squads as $key => $squad) {
            usort($squads, function ($a, $b) {
                return ($b['number'] - $a['number']);
            });
        }

        //очистка уже распределенных
        foreach ($squads as $squad) {
            $kids = Vouchers::find()->where(['squad' => $squad['id']])->all();
            foreach ($kids as $kid) {
                $model = Vouchers::findOne(['id' => $kid->id]);
                $model->room = null;
                $model->birth_string = gmdate("Y-m-d", $model->birth);
                $model->squad = null;
                $model->save();
            }
        }


        //получение детей в связках
        $arBunch = [];
        $bunch_kids = Bunch::find()->where(['camp_shift' => $id])->asArray()->all();
        foreach ($bunch_kids as $bunch_kid) {
            $arBunch[] = json_decode($bunch_kid['kids'], true);
        }
        $arKids = [];
        foreach ($arBunch as $key => $bunch) {
            foreach ($bunch as $kid) {
                $kid = Vouchers::find()->where(['id' => $kid])->asArray()->one();
                $arKids[$kid['branch']][] = array(
                    'age' => $this->calculate_age(gmdate("Y-m-d", $kid['birth'])),
                    'id' => $kid['id'],
                    'birth' => $kid['birth'],
                    'gender' => $kid['gender'],
                );
            }
        }



        foreach ($arKids as $key => $arKid) {
            usort($arKids[$key], function ($a, $b) {
                return ($a['age'] - $b['age']);
            });
        }

        $reverse_squad = array_reverse($squads);
        //распределение связанных детей по отрядам
        foreach ($arKids as $k => $kids) {
            foreach ($reverse_squad as $key => $squad) {
                $first = $kids[0];
                if ($first['age'] >= $squad['age_from'] && $first['age'] <= $squad['age_to']) {
                    $rooms = json_decode($squad['rooms'], true);
                    foreach ($rooms as $room) {
                        $r = Room::find()->where(['id' => $room])->asArray()->one();
                        $count = Vouchers::find()->where(['room' => $room])->asArray()->count();
                        foreach ($kids as $c => $kid) {
                            if (($r['count'] > $count) && ($r['gender'] == $kid['gender'])) {
                                $k = Vouchers::findOne(['id' => $kid['id']]);
                                $k->squad = $squad['id'];
                                $k->birth_string = gmdate("Y-m-d", $kid['birth']);
                                $k->room = $r['id'];
                                $k->save();
                                unset($kids[$c]);
                            }
                            $count++;
                        }
                    }
                }
            }
        }
        $arKids = [];
        $rooms = [];
        $arBunch = [];
        $r = [];
        $count = 0;

        //распределение детей без связи
        $orders = CampOrder::SearchAndFilters(null, 'all', 'all', $id, 1);
        foreach ($orders as $val) {
            foreach ($val->vouchers as $value) {
                if (!is_null($value->voucher_number) and (!is_null($value->gender)) and (is_null($value->squad))) {
                    $arKids[] = array(
                        'age' => $this->calculate_age(gmdate("Y-m-d", $value->birth)),
                        'id' => $value->id,
                        'birth' => $value->birth,
                        'gender' => $value->gender
                    );
                }
            }
        }
        foreach ($arKids as $key => $arKid) {
            usort($arKids, function ($a, $b) {
                return ($b['birth'] - $a['birth']);
            });
        }

        foreach ($squads as $key => $squad) {
            $rooms = json_decode($squad['rooms'], true);
            foreach ($rooms as $room) {
                $r = Room::find()->where(['id' => $room])->asArray()->one();
                $count = Vouchers::find()->where(['room' => $room])->asArray()->count();
                foreach ($arKids as $c => $kid) {
                    if (($r['count'] > $count) && $kid['age'] >= $squad['age_from'] && $kid['age'] <= $squad['age_to'] && $r['gender'] == $kid['gender']) {
                        $k = Vouchers::findOne(['id' => $kid['id']]);
                        $k->squad = $squad['id'];
                        $k->birth_string = gmdate("Y-m-d", $kid['birth']);
                        $k->room = $r['id'];
                        $k->save();

                        $arKids = array_filter($arKids, function ($e) use ($kid) {
                            return $e['id'] !== $kid['id'];
                        });
                        $count++;
                    }
                }
            }
        }

        return true;
    }

    //сохранить отредактированный отряд
    public function actionSavesquadone($id, $num, $from, $to, $home, $rooms)
    {
        $model = Squad::findOne(['camp_shift' => $id, 'number' => $num]);
        $model->camp_shift = $id;
        $model->home = $home;
        $model->age_from = $from;
        $model->age_to = $to;
        $model->rooms = $rooms ?: '[]';
        $model->save();
    }

    //просмотр сформированного отряда
    public function actionFromsquadview($id, $squad_id)
    {
        $s = Squad::findOne(['id' => $squad_id]);
        $arKids = array();
        $orders = CampOrder::SearchAndFilters(null, 'all', 'all', 'all', 1);
        foreach ($orders as $val) {
            if ($val->camp_shift_id == $id) {
                foreach ($val->vouchers as $value) {
                    if ($value->squad == $squad_id) {
                        $room = Room::find()->where(['id' => $value->room])->one();
                        $arKids[] = array(
                            'name' => $value->name,
                            'age' => $this->calculate_age(gmdate("Y-m-d", $value->birth)),
                            'number' => $value->voucher_number,
                            'room' => $room->number
                        );
                    }
                }
            }
        }

        usort($arKids, function ($a, $b) {
            return ($a['room'] - $b['room']);
        });

        return $this->render('formview', ['kids' => $arKids, 'squad' => $s]);
    }

    //создать связь
    public function actionBunch($id)
    {
        $arKids = [];
        $z = [];
        $bunch = Bunch::find()->all();
        if ($bunch) {
            foreach ($bunch as $val) {
                $q = json_decode($val->kids, true);
                foreach ($q as $v) {
                    $z[] = $v;
                }
            }
        }
        $orders = CampOrder::SearchAndFilters(null, 'all', 'all', $id, 1);
        foreach ($orders as $val) {
            foreach ($val->vouchers as $value) {
                if (($value->voucher_number !== null) && ($value->gender !== null)) {
                    if (!in_array($value->id, $z)) {
                        $squad = Squad::findOne(['id' => $value->squad]);
                        $arKids[] = array(
                            'id' => $value->id,
                            'name' => $value->name,
                            'squad' => $squad->number,
                            'age' => $this->calculate_age(gmdate("Y-m-d", $value->birth))
                        );
                    }
                }
            }
        }

        usort($arKids, function ($a, $b) {
            return ($a['age'] - $b['age']);
        });

        return $this->render('bunch', ['kids' => $arKids]);
    }

    //сохранить связь
    public function actionBunchsave($id, $arIds)
    {
        $arIds = json_decode($arIds, true);
        $bunch = new Bunch();
        $bunch->kids = json_encode($arIds);
        $bunch->camp_shift = $id;
        $bunch->save();
        $idbunch = $bunch->getPrimaryKey();

        foreach ($arIds as $key => $val) {
            $voucher = Vouchers::find()->where(['id' => $val])->one();
            $voucher->branch = $idbunch;
            $voucher->birth_string = gmdate("Y-m-d", $voucher->birth);
            $voucher->save();
        }
        return true;
    }

    //список связей
    public function actionBunchview($id)
    {
        $arKids = array();
        $orders = CampOrder::SearchAndFilters(null, 'all', 'all', $id, 1);
        foreach ($orders as $val) {
            foreach ($val->vouchers as $value) {
                if ($value->branch !== 0) {
                    $arKids[] = array(
                        'name' => $value->name,
                        'bunch' => $value->branch,
                        'age' => $this->calculate_age(gmdate("Y-m-d", $value->birth)),
                    );

                }
            }
        }

        if (!$arKids) {
            $this->redirect('/squad/edit/' . $id);
            return false;
        }

        usort($arKids, function ($a, $b) {
            return ($a['bunch'] - $b['bunch']);
        });

        return $this->render('viewbunch', ['kids' => $arKids, 'id' => $id]);
    }

    //просморт связи
    public function actionBunchviewedit($id, $bunch_id)
    {
        $arKids = array();
        $orders = CampOrder::SearchAndFilters(null, 'all', 'all', $id, 1);
        foreach ($orders as $val) {
            foreach ($val->vouchers as $value) {
                if ($value->branch == $bunch_id) {
                    $arKids[] = array(
                        'id' => $value->id,
                        'name' => $value->name,
                        'bunch' => $value->branch,
                        'age' => $this->calculate_age(gmdate("Y-m-d", $value->birth)),
                    );

                }
            }
        }

        if (!$arKids) {
            $this->redirect('/squad/edit/' . $id . '/bunchview');
            return false;
        }

        usort($arKids, function ($a, $b) {
            return ($a['age'] - $b['age']);
        });

        return $this->render('bunchviewedit', ['kids' => $arKids, 'id' => $id, 'bunch' => $bunch_id]);
    }

    //удалить человека из связи
    public function actionBunchviremove($id, $bunch_id, $a, $b)
    {
        $arr = array();
        $bunch = Bunch::find()->where(['id' => $b])->one();
        $arIds = json_decode($bunch->kids);
        foreach ($arIds as $v) {
            if ($v != $a) {
                $arr[] = $v;
            }
        }
        $bunch->kids = json_encode($arr);
        $bunch->save();

        $voucher = Vouchers::find()->where(['id' => $a])->one();
        $voucher->branch = null;
        $voucher->birth_string = gmdate("Y-m-d", $voucher->birth);
        $voucher->save();
    }

    public function actionView()
    {
        $shifts = ArrayHelper::map(CampsShift::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'namewithdate');
        return $this->render('view', ['shifts' => $shifts]);
    }

    public function actionViewall($id)
    {
        $squads = array_reverse(Squad::find()->where(['camp_shift' => $id])->all());
        $homes = Home::find()->where(['camp_shift' => $id])->all();
        $rooms = Room::find()->where(['camp_shift' => $id])->all();
        $count = 0;
        $current = 0;
        $boys = 0;
        $girls = 0;
        $n = 0;
        $a = 0;
        $n_b = 0;
        $n_g = 0;
        $a_b = 0;
        $a_g = 0;
        $arrGirls = array();
        $arrBoys = array();

        foreach ($squads as $squad) {
            $vouchers = Vouchers::find()->where(['squad' => $squad->id])->asArray()->all();
            foreach ($vouchers as $voucher) {
                if ($voucher['gender'] == '0') {
                    $arrGirls[$squad->number][] = array(
                        'room' => $voucher['room'],
                        'id' => $voucher['id']
                    );
                }
                elseif ($voucher['gender'] == '1') {
                    $arrBoys[$squad->number][] = array(
                        'room' => $voucher['room'],
                        'id' => $voucher['id']
                    );
                }
            }
        }

        foreach ($squads as $key => $squad) {
            usort($squads, function ($a, $b) {
                return ($b->number - $a->number);
            });
        }


        foreach ($rooms as $key => $val) {
            $count = $count + $val->count;
            $tmp = Vouchers::find()->where(['room' => $val->id])->all();
            foreach ($tmp as $v) {
                if ($v->gender == '1') {
                    $boys++;
                }
                elseif ($v->gender == '0') {
                    $girls++;
                }
                $current++;
            }
        }
        $arBoys = array(
            'current' => $boys,
            'count' => $boys
        );
        $arGirls = array(
            'current' => $girls,
            'count' => $girls
        );

        $orders = CampOrder::SearchAndFilters(null, 'all', 'all', $id, 1);
        foreach ($orders as $val) {
            foreach ($val->vouchers as $value) {
                if ($value->voucher_number) {
                    if (!is_null($value->gender)) {
                        if (is_null($value->squad)) {
                            if ($value->gender == '1') {
                                $n_b++;
                            }
                            elseif ($value->gender == '0') {
                                $n_g++;
                            }
                            $n++;
                        }
                        if ($value->gender == '1') {
                            $a_b++;
                        }
                        elseif ($value->gender == '0') {
                            $a_g++;
                        }
                        $a++;
                    }
                }
            }
        }

        $arN = array($n, $a);

        $gen = array('boys' => array($n_b, $a_b), 'girls' => array($n_g, $a_g));

        return $this->render('viewall', ['squads' => $squads, 'id' => $id, 'homes' => $homes, 'rooms' => $rooms,
            'all' => $count, 'current' => $current, 'boys' => $arBoys, 'girls' => $arGirls, 'q' => $arN, 'gen' => $gen,
            'arrGirls' => $arrGirls, 'arrBoys' => $arrBoys]);
    }

    public function actionViewsquad($id, $squad_id)
    {
        $arKids = array();
        $all = 0;
        $a_b = 0;
        $c_b = 0;
        $a_g = 0;
        $c_g = 0;
        $orders = CampOrder::SearchAndFilters(null, 'all', 'all', $id, 1);
        foreach ($orders as $val) {
            foreach ($val->vouchers as $value) {
                if ($value->squad == $squad_id) {
                    $home = Room::find()->where(['id' => $value->room])->one();
                    $arKids[] = array(
                        'name' => $value->name,
                        'age' => $this->calculate_age(gmdate("Y-m-d", $value->birth)),
                        'room' => $home->number,
                    );
                    if ($value->gender == '1') {
                        $c_b++;
                    }
                    elseif ($value->gender == '0') {
                        $c_g++;
                    }

                }
            }
        }
        $arRooms = json_decode(Squad::find()->where(['id' => $squad_id])->one()->rooms);
        foreach ($arRooms as $value) {
            $tmp = Room::find()->where(['id' => $value])->one();
            $all = $all + $tmp->count;
            if ($tmp->gender == '1') {
                $a_b = $a_b + $tmp->count;
            }
            elseif ($tmp->gender == '0') {
                $a_g = $a_g + $tmp->count;
            }
        }
        $boys = array(
            'count' => $a_b,
            'current' => $c_b
        );
        $girls = array(
            'count' => $a_g,
            'current' => $c_g
        );

        usort($arKids, function ($a, $b) {
            return ($a['room'] - $b['room']);
        });

        return $this->render('viewsquad', ['kids' => $arKids, 'all' => $all, 'current' => count($arKids), 'boys' => $boys,
            'girls' => $girls, 'id' => $id, 'squad' => $squad_id]);
    }

    public function actionViewallprint($id)
    {
        $orders = CampOrder::SearchAndFilters(null, 'all', 'all', $id, 1);
        foreach ($orders as $val) {
            foreach ($val->vouchers as $value) {
                if (!is_null($value->squad)) {
                    $home = Room::find()->where(['id' => $value->room])->one();
                    $sq = Squad::find()->where(['id' => $value->squad])->one();
                    $h = Home::find()->where(['id' => $home->home])->one();
                    $arKids[] = array(
                        'name' => $value->name,
                        'age' => $this->calculate_age(gmdate("Y-m-d", $value->birth)),
                        'room' => $home->number,
                        'home' => $h->home,
                        'squad' => $sq->number
                    );
                }
            }
        }


        usort($arKids, function ($a, $b) {
            return ($a['squad'] - $b['squad']);
        });

        $shifts = CampsShift::find()->where(['id' => $id])->one();

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('printall', ['kids' => $arKids, 'shift' => $shifts]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'marginLeft' => 0,
            'marginRight' => 0,
            'marginTop' => 0,
            'marginBottom' => 0,
            'marginHeader' => 0,
            'marginFooter' => 0,
            'options' => [
                'title' => 'Путёвка',
                'subject' => 'Путёвка'
            ],
        ]);
        return $pdf->render();
    }

    public function actionViewsquadprint($id, $squad_id)
    {
        $orders = CampOrder::SearchAndFilters(null, 'all', 'all', $id, 1);
        foreach ($orders as $val) {
            foreach ($val->vouchers as $value) {
                if (!is_null($value->squad) and ($value->squad == $squad_id)) {
                    $room = Room::find()->where(['id' => $value->room])->one();
                    $home = Home::find()->where(['id' => $room->home])->one();
                    $sq = Squad::find()->where(['id' => $value->squad])->one();
                    $arKids[] = array(
                        'name' => $value->name,
                        'age' => $this->calculate_age(gmdate("Y-m-d", $value->birth)),
                        'room' => $room->number,
                        'home' => $home->home,
                        'squad' => $sq->number
                    );
                }
            }
        }

        $shifts = CampsShift::find()->where(['id' => $id])->one();

        usort($arKids, function ($a, $b) {
            return ($a['room'] - $b['room']);
        });

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('printall', ['kids' => $arKids, 'shift' => $shifts]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'marginLeft' => 0,
            'marginRight' => 0,
            'marginTop' => 0,
            'marginBottom' => 0,
            'marginHeader' => 0,
            'marginFooter' => 0,
            'options' => [
                'title' => 'Путёвка',
                'subject' => 'Путёвка'
            ],
        ]);
        return $pdf->render();
    }

    public function actionViewsquadprintgender($id, $squad_id, $gender)
    {
        $orders = CampOrder::SearchAndFilters(null, 'all', 'all', $id, 1);
        foreach ($orders as $val) {
            foreach ($val->vouchers as $value) {
                if (!is_null($value->squad) and ($value->squad == $squad_id) and ($value->gender == $gender)) {
                    $room = Room::find()->where(['id' => $value->room])->one();
                    $home = Home::find()->where(['id' => $room->home])->one();
                    $sq = Squad::find()->where(['id' => $value->squad])->one();
                    $arKids[] = array(
                        'name' => $value->name,
                        'age' => $this->calculate_age(gmdate("Y-m-d", $value->birth)),
                        'room' => $room->number,
                        'home' => $home->home,
                        'squad' => $sq->number
                    );
                }
            }
        }

        $pol = $gender == '1' ? 'Мальчики' : 'Девочки';

        $shifts = CampsShift::find()->where(['id' => $id])->one();
        usort($arKids, function ($a, $b) {
            return ($a['room'] - $b['room']);
        });
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('printall', ['kids' => $arKids, 'shift' => $shifts, 'squad' => $pol]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'marginLeft' => 0,
            'marginRight' => 0,
            'marginTop' => 0,
            'marginBottom' => 0,
            'marginHeader' => 0,
            'marginFooter' => 0,
            'options' => [
                'title' => 'Путёвка',
                'subject' => 'Путёвка'
            ],
        ]);
        return $pdf->render();
    }
}
