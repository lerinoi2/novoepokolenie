<?php

namespace app\controllers;

use Yii;
use app\models\Routes;
use app\models\RoutesSearch;
use app\models\RoutePrice;
use yii\web\Controller;
use yii\web\UploadedFile;
use app\models\UploadForm;

class RoutesController extends Controller
{

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }

    public function actionIndex()
    {
        $searchModel = new RoutesSearch();
        $status = Routes::getStatusesArray();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

//        $status = Routes::getStatusesArray();
//        $routes = Routes::getRoutes();
//        return $this->render('index', ['routes' => $routes,'status'=>$status]);
    }

    public function actionCreate()
    {
        $model = new Routes();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->file) {
                $model->file = UploadedFile::getInstance($model, 'file');
                $model->upload();
            }
            $model->save();

            return $this->redirect(array('routes/index', 'id' => $model->id));
        }
        $status = $model->getStatusesArray();
        return $this->render('create', ['model' => $model, 'status' => $status]);
    }

    public function actionEdit($id)
    {
        $model = Routes::findOne($id);
        // $price = new RoutePrice();
        // $priceTable = $this->renderPartial('/route_price/index', ['prices' => RoutePrice::getByRouteId($model->id), 'route_id' => $id]);
        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file) {
                $model->upload();
            }
            $model->save();
            return $this->redirect(array('routes/edit/'.$id));
        }
        if ($model != NULL) {
            $model->convertDate();
            $status = $model->getStatusesArray();
            return $this->render('edit', ['model' => $model, 'status' => $status]);
            // return $this->render('edit', ['model' => $model, 'status' => $status, 'priceTable' => $priceTable, 'price'=>$price]);
        } else {
            throw new HttpException(404, 'Not Found');
        }
    }

    public function actionDelete($id){
        $model = Routes::findOne($id);
        if ($model != NULL) {
            $model->delete();
        }
        return $this->redirect('/routes');
    }
}
