<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 28.09.2017
 * Time: 9:51
 */

namespace app\controllers;

use app\models\CampOrder;
use app\models\RouteOrder;
use app\models\Content;
use app\models\Camps;
use app\models\CampsShift;
use app\models\Vouchers;
use app\models\Pay;
use app\models\User;
use app\models\WaitingList;
use app\models\Blacklist;
use app\models\ShiftList;
use app\models\Routes;
use app\models\Addination;
use Faker\Provider\DateTime;
use Yii;
use yii\helpers\Json;
use yii\base\Model;
use kartik\mpdf\Pdf;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\Cors;

class ApiController extends Controller
{

    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => Cors::className(),
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://www.novoepokolenie.com', 'http://novoepokolenie.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers' => ['X-Wsse'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],

            ],
        ];
    }

    public function beforeAction($action)
    {
//        if(yii::$app->request->isAjax){
//            $this->enableCsrfValidation = false;
//            return true;
//        }else{
//            $this->redirect('https://novoepokolenie.com');
//        }
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionTest()
    {

    }

    public function actionInfo()
    {
        // $camp_shift = CampsShift::find()->select(['id', 'name', 'date_start', 'date_end', 'conditions', 'price', 'places', 'status', 'date_start_sale', 'camp_id'])->orderBy('sort')->all();
        $camp_shift = CampsShift::getCampsShift('sort');
        foreach ($camp_shift as $el) {
            $el->booking = CampOrder::getBooking($el->id);
            $el->payed = CampOrder::getPayed($el->id);
            $el->allowed = CampOrder::getAllowed($el->id);
            $el->booking = $el->BookingPlaces;
            $el->payed = $el->PayedPlaces;
            $el->allowed = $el->allowedplaces;
        }
        return Json::encode($camp_shift);
    }

    public function actionCheckbooking()
    {
        $camp_shift = new CampsShift();
        // $camp_shift = CampsShift::find()->where(['status' => CampsShift::STATUS_ACTIVE])->select(['id', 'name', 'date_start', 'date_end', 'conditions', 'price', 'places', 'status', 'date_start_sale', 'camp_id'])->orderBy('sort')->all();
        $camp_shift = CampsShift::getCampsShift('sort', ['status' => CampsShift::STATUS_ACTIVE]);
        $now = new \DateTime('now', new \DateTimeZone('Asia/Yekaterinburg'));
        $now = yii::$app->formatter->asTimestamp($now);
        foreach ($camp_shift as $key => $el) {
            $el->convertDate();
            if ($el->date_start_sale == null) {
                unset($camp_shift[$key]);
            }
            else {
                $dateStart = new \DateTime($el->date_start_sale_string, new \DateTimeZone('Asia/Yekaterinburg'));
                $dateStart->setTime('9', '0', '0');
                $dateStart = yii::$app->formatter->asTimestamp($dateStart);
                if ($dateStart <= $now) {
                    $el->booking = CampOrder::getBooking($el->id);
                    $el->payed = CampOrder::getPayed($el->id);
                    $el->allowed = CampOrder::getAllowed($el->id);
                }
                else {
                    unset($camp_shift[$key]);
                }
            }
        }
        if (count($camp_shift) > 0) {
            return Json::encode($camp_shift);
        }
        else {
            return Json::encode(['error' => '1']);
        }
    }

    public function actionCancelorder()
    {
        $order_id = Yii::$app->request->get('order_id', null);
        if ($order_id != null) {
            $token = Yii::$app->request->get('token', null);
            $order = CampOrder::findOne(['id' => $order_id, 'active' => true]);
            $pay = Pay::find()->where(['order_id' => $order_id, 'way' => Pay::TYPE_CAMP])->all();
            if (count($pay) > 0) {
                return Json::encode(['error' => '2']);
            }
            if ($token != null) {
                if ($order != null) {
                    if ($order->token === $token) {
                        $order->active = false;
                        if ($order->save()) {
                            $order->convertDate();
                            $date_create = date("d.m.y", strtotime($order->booking_end_string . "-15 days"));
                            Yii::$app->mailer->compose('@app/mail/Cancel', ['order' => $order, 'date_create' => $date_create])
                                ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                                ->setTo($order->email)
                                ->setSubject("Бронирование путёвки отменено")
                                ->send();
                            if ($order->campShift->AllowedPlaces > 0 && $order->campShift->AllowedPlaces >= $order->count) {
                                $now = new \DateTime('now', new \DateTimeZone('Asia/Yekaterinburg'));
                                $now = intval($now->format('H'));
                                $waitlist = WaitingList::find()
                                    ->where(['camp_shift_id' => $order->camp_shift_id])
                                    ->andWhere(['status' => WaitingList::STATUS_NO_ORDER])
                                    ->orderBy('id')
                                    ->limit($order->count)
                                    ->all();
                                foreach ($waitlist as $wl) {
                                    if ($now >= 9) {
                                        $wl->CreateOrder(2);
                                    }
                                    else {
                                        $wl->CreateOrder();
                                    }

                                }
                            }
                            header('Location: http://novoepokolenie.com/bronirovanie-putevki-otmeneno/');
                        }
                        else {
                            return Json::encode(['error' => '1']);
                        }
                    }
                }
                else {
                    return Json::encode(['error' => '1']);
                }
            }
            else {
                if ($order != null) {
                    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
                    $count = mb_strlen($chars);
                    for ($i = 0, $res = ''; $i < 30; $i++) {
                        $ii = rand(0, $count - 1);
                        $res .= mb_substr($chars, $ii, 1);
                    }
                    $token = $res;
                    $order->token = $token;
                    if ($order->save()) {
                        $order->convertDate();
                        $date_create = date("d.m.y", strtotime($order->booking_end_string . "-15 days"));
                        Yii::$app->mailer->compose('@app/mail/CancelConfirm', ['order' => $order, 'date_create' => $date_create])
                            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                            ->setTo($order->email)
                            ->setSubject("Подтвердите отмену бронирования путёвки")
                            ->send();
                        header('Location: http://www.novoepokolenie.com/');
                    }
                    else {
                        return Json::encode(['error' => '1']);
                    }
                }
                else {
                    return Json::encode(['error' => '1']);
                }
            }
        }
        header('Location: http://www.novoepokolenie.com/');
    }

    public function actionNewbooking()
    {
        $camp_shift = CampsShift::getCampsShift('sort', ['status' => CampsShift::STATUS_ACTIVE]);
        $now = new \DateTime('now', new \DateTimeZone('Asia/Yekaterinburg'));
        $now = yii::$app->formatter->asTimestamp($now);
        foreach ($camp_shift as $key => $el) {
            $el->convertDate();
            if ($el->date_start_sale == null) {
                unset($camp_shift[$key]);
            }
            else {
                $dateStart = new \DateTime($el->date_start_sale_string, new \DateTimeZone('Asia/Yekaterinburg'));
                $dateStart->setTime('9', '0', '0');
                $dateStart = yii::$app->formatter->asTimestamp($dateStart);
                if ($dateStart <= $now) {
                    $el->booking = CampOrder::getBooking($el->id);
                    $el->payed = CampOrder::getPayed($el->id);
                    $el->allowed = CampOrder::getAllowed($el->id);
                    if ($el->allowed <= 0) {
                        unset($camp_shift[$key]);
                    }
                }
                else {
                    unset($camp_shift[$key]);
                }
            }
        }
        if (count($camp_shift) <= 0) {
            return Json::encode(['error' => '1']);
        }

        if (yii::$app->request->isPost) {
            if ($_POST['key'] !== 'D\DTx6a2G3@]&*53') {
                Yii::$app->response->setStatusCode(403);
                return 'Не ну это бан!';
            }

            $order = new CampOrder();
            $order->load(Yii::$app->request->post());
            $order->discount = 0;
            $order->entity = CampOrder::ENTITY_FIZ;
            $count = Yii::$app->request->post('count', 1);
            $order->count = $count;
            $camp_shift = CampsShift::findOne($order->camp_shift_id);
            if ($camp_shift == null) {
                return Json::encode($order);
            }
            $order->price = CampsShift::getCurrentPrice($camp_shift->id);
            $camp_shift->booking = CampOrder::getBooking($order->camp_shift_id);
            $camp_shift->payed = CampOrder::getPayed($order->camp_shift_id);
            $camp_shift->allowed = CampOrder::getAllowed($order->camp_shift_id);

//            $dateStart = new \DateTime($camp_shift->date_start_sale_string, new \DateTimeZone('Asia/Yekaterinburg'));
//            if ($dateStart < time()) {
//                return Json::encode(['error' => 'time']);
//            }

            if ($camp_shift->allowed < $order->count) {

                return Json::encode(['error' => '2']);
            }
            if (!$order->save(false)) {
                return Json::encode(0);
            }

            for ($i = 0; $i < $count; $i++) {
                $vouchers[$i] = new Vouchers();
            }
            if (Model::loadMultiple($vouchers, Yii::$app->request->post())) {
                foreach ($vouchers as $voucher) {
                    $black = Blacklist::find()->where(['name' => $voucher->name, 'birth' => Yii::$app->formatter->asTimestamp($voucher->birth_string)])->count();
                    if ($black > 0) {
                        $order->delete();
                        foreach ($vouchers as $voucher) {
                            $voucher->delete();
                        }
                        return Json::encode(['error' => '3']);
                    }
                    $voucher->order_id = $order->id;
                    $voucher->way = Pay::TYPE_CAMP;
                    $voucher->save(false);
                    if (!$voucher->save(false)) {
                        $order->delete();
                        foreach ($vouchers as $voucher) {
                            $voucher->delete();
                        }
                        return Json::encode(0);
                    }
                }
            }
            else {
                $order->delete();
                foreach ($vouchers as $voucher) {
                    $voucher->delete();
                }
                return Json::encode(['error' => '2']);
            }
            $order->convertDate();

            $date_create = $order->create_date_string;
            $count = $order->count;
            $shift = $order->campShift;
            $shift->convertDate();
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'format' => Pdf::FORMAT_A4,
                'destination' => Pdf::DEST_STRING,
                'content' => $this->renderPartial('/api/index', ['data' => $order, 'createdate' => $date_create, 'count' => $count, 'shift' => $shift]),
                'options' => [
                    'title' => 'Счет на оплату',
                    'subject' => 'Счет на оплату'
                ],
            ]);
            $render = $pdf->render();

            $camp_shift = CampsShift::findOne($order->camp_shift_id);
            $camp_shift->convertDate();
            $data = array();
            $data['order_id'] = $order->id;
            $data['date_end'] = $order->booking_end_string;
            $data['date_create'] = date("d.m.y", strtotime($order->booking_end_string . "-15 days"));
            // $data['shift'][] = array('count' => $count, 'price' => $camp_shift->price, 'sum' => $camp_shift->price * $count, 'name' => $camp_shift->name, 'date_start_string' => $camp_shift->date_start_string, 'date_end_string' => $camp_shift->date_end_string);
            $data['shift'][] = array('count' => $count, 'price' => $order->price, 'sum' => $order->price * $count, 'name' => $camp_shift->name, 'date_start_string' => $camp_shift->date_start_string, 'date_end_string' => $camp_shift->date_end_string);
            $dogovor = '/mail/attach/договор_дневной_18.doc';
            if ($camp_shift->conditions == CampsShift::CONDITIONS_WELL) {
                $dogovor = '/mail/attach/договор_загородный_лагерь_20.doc';
            }
            elseif ($camp_shift->conditions == CampsShift::CONDITIONS_NOT_WELL) {
                $dogovor = '/mail/attach/договор_дневной_20.doc';
            }
            elseif ($camp_shift->conditions == CampsShift::CONDITIONS_TENTS) {
                $dogovor = '/mail/attach/договор_палатки_20.doc';
            }
            try {
                Yii::$app->mailer->compose('@app/mail/CreateOrder', ['data' => $data])
                    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                    ->setTo($order->email)
                    ->setSubject("Забронирована путёвка в лагерь \"Новое поколение\"")
//                    ->attach($_SERVER["DOCUMENT_ROOT"] . '/mail/attach/приложение_к_договору_№1_Медицинский_пакет.pdf')
                    ->attach($_SERVER["DOCUMENT_ROOT"] . '/mail/attach/свод_правил_пребывания_ребенка_в_лагере.doc')
                    ->attach($_SERVER["DOCUMENT_ROOT"] . '/mail/attach/med_pack.pdf')
                    ->attach($_SERVER["DOCUMENT_ROOT"] . $dogovor)
                    ->attachContent($render, ['fileName' => 'Счет на оплату.pdf', 'contentType' => 'application/pdf'])
                    ->send();
            }
            catch (\Exception $e) {
                return Json::encode($data);
            }
            return Json::encode($data);
        }

        //Получение списка смен
        $camp = Camps::find()->where(['status' => Camps::STATUS_ACTIVE])->asArray()->all();
        $data = array('camp' => $camp, 'camp_shift' => $camp_shift);
        return Json::encode($data);
    }

    public function actionCreateorder()
    {
        $order_id = yii::$app->request->get('order_id', null);
        if (yii::$app->request->isPost || $order_id != null) {
            if ($order_id == null) {
                $order_id = yii::$app->request->post('order_id', null);
            }
            if ($order_id != null) {
                $order = CampOrder::findOne($order_id);

                // Проверка, не аннулирован ли заказ
                if ($order->active == 0) {
                    if (yii::$app->request->isGet) {
                        return 'Ваш заказ аннулирован';
                    }
                    return Json::encode(['error' => '3']);
                }

                $pay = new Pay();
                $pay->order_id = $order_id;


                $pay->way = 1;

                $pay->sum = $order->count * $order->price;
                // $pay->sum = $order->count * $order->campShift->price;
                $pay->date_string = date("d.M.y");
                $pay->payed = false;
                $pay->save();


                $dat = array(
                    'userName' => Yii::$app->params['sberbank_login'],
                    'password' => Yii::$app->params['sberbank_password'],
                    'url' => Yii::$app->params['sberbank_url'] . '/payment/rest/register.do',
                );

                $data = array(
                    'userName' => $dat['userName'],
                    'password' => $dat['password'],
                    'orderNumber' => $pay->id,
                    'amount' => ($pay->sum * 100),
                    'returnUrl' => 'https://zakaz.novoepokolenie.com/api/paycheck',
                    'description' => 'Заказ номер №' . $pay->id
                );

                $params = array(
                    'amount' => $data['amount'],             // Сумма пополнения
                    'orderNumber' => $data['orderNumber'],            // Внутренний ID заказа
                    'password' => $data['password'],            // Здесь пароль от вашего API юзера в Сбербанке
                    'userName' => $data['userName'],                 // А тут его логин
                    'returnUrl' => $data['returnUrl'],      // URL куда вернуть пользователя после перечисления средств
                );

                $opts = array(                           // А здесь параметры для POST запроса
                    'http' => array(
                        'method' => 'POST',
                        'header' => "Content-type: application/x-www-form-urlencoded",
                        'content' => http_build_query($params),
                        'timeout' => 60
                    )
                );

                // И отправляем эти данные на сервер сбербанка
                $context = stream_context_create($opts);
                // Здесь должен быть URL тестового и боевого сервера Сбербанка
                $url = $dat['url'];
                $result = file_get_contents($url, false, $context);

                // Расшифруем полученные данные в массив $arSbrfResult
                $response = json_decode($result, true);

                if (isset($response['errorCode'])) {
                    $url = '/?pay_error=1';
                    return Json::encode(['error' => '3']);
                }
                else {
                    $pay->sberbank_id = $response['orderId'];
                    $pay->save();
                    $url = $response['formUrl'];
                    if (yii::$app->request->isGet && $order_id != null) {
                        header('Location: ' . $url);
                    }
                    return Json::encode($url);
                }
            }
            else {
                return Json::encode(['error' => '3']);
            }
        }
        else {
            return Json::encode(['error' => '3']);
        }
    }

    public function actionCreatemarsh()
    {
        $order_id = yii::$app->request->get('order_id', null);
        if (yii::$app->request->isPost || $order_id != null) {
            if ($order_id == null) {
                $order_id = yii::$app->request->post('order_id', null);
            }
            if ($order_id != null) {
                $order = RouteOrder::findOne($order_id);

                // Проверка, не аннулирован ли заказ
                if ($order->active == 0) {
                    if (yii::$app->request->isGet) {
                        return 'Ваш заказ аннулирован';
                    }
                    return Json::encode(['error' => '3']);
                }

                $pay = new Pay();
                $pay->order_id = $order_id;


                $pay->way = 0;

                $pay->sum = $order->count * $order->price;
                $pay->date_string = date("d.M.y");
                $pay->payed = false;
                $pay->save();

                $dat = array(
                    'userName' => Yii::$app->params['sberbank_login_marsh'],
                    'password' => Yii::$app->params['sberbank_password_marsh'],
                    'url' => Yii::$app->params['sberbank_url'] . '/payment/rest/register.do',
                );

                $data = array(
                    'userName' => $dat['userName'],
                    'password' => $dat['password'],
                    'orderNumber' => $pay->id,
                    'amount' => ($pay->sum * 100),
                    'returnUrl' => 'https://zakaz.novoepokolenie.com/api/paycheckmarsh',
                    'description' => 'Заказ номер №' . $pay->id
                );

                $params = array(
                    'amount' => $data['amount'],             // Сумма пополнения
                    'orderNumber' => $data['orderNumber'],            // Внутренний ID заказа
                    'password' => $data['password'],            // Здесь пароль от вашего API юзера в Сбербанке
                    'userName' => $data['userName'],                 // А тут его логин
                    'returnUrl' => $data['returnUrl'],      // URL куда вернуть пользователя после перечисления средств
                );

                $opts = array(                           // А здесь параметры для POST запроса
                    'http' => array(
                        'method' => 'POST',
                        'header' => "Content-type: application/x-www-form-urlencoded",
                        'content' => http_build_query($params),
                        'timeout' => 60
                    )
                );

                // И отправляем эти данные на сервер сбербанка
                $context = stream_context_create($opts);
                // Здесь должен быть URL тестового и боевого сервера Сбербанка
                $url = $dat['url'];
                $result = file_get_contents($url, false, $context);

                // Расшифруем полученные данные в массив $arSbrfResult
                $response = json_decode($result, true);

                if (isset($response['errorCode'])) {
                    $url = '/?pay_error=1';
                    return Json::encode(['error' => '3']);
                }
                else {
                    $pay->sberbank_id = $response['orderId'];
                    $pay->save();
                    $url = $response['formUrl'];
                    if (yii::$app->request->isGet && $order_id != null) {
                        header('Location: ' . $url);
                    }
                    return Json::encode($url);
                }
            }
            else {
                return Json::encode(['error' => '3']);
            }
        }
        else {
            return Json::encode(['error' => '3']);
        }
    }


    public function actionPaycheck()
    {
        $orderid = yii::$app->request->get('orderId', null);
        if (yii::$app->request->isPost || $orderid != null) {
            $order_id = yii::$app->request->post('order_id', null);
            if ($order_id != null || $orderid != null) {

                if ($orderid !== null) {
                    $pay = Pay::find()->where(['sberbank_id' => $orderid])->one();
                    if ($pay->way === 0) {
                        $data = array(
                            'userName' => Yii::$app->params['sberbank_login_marsh'],
                            'password' => Yii::$app->params['sberbank_password_marsh'],
                            'orderId' => $orderid
                        );
                    }
                    else {
                        $data = array(
                            'userName' => Yii::$app->params['sberbank_login'],
                            'password' => Yii::$app->params['sberbank_password'],
                            'orderId' => $orderid
                        );
                    }
                }
                elseif ($order_id !== null) {
                    $pay = Pay::find()->where(['order_id' => $order_id])->one();
                    if ($pay->way === 0) {
                        $data = array(
                            'userName' => Yii::$app->params['sberbank_login_marsh'],
                            'password' => Yii::$app->params['sberbank_password_marsh'],
                            'orderId' => $pay->sberbank_id
                        );
                    }
                    else {
                        $data = array(
                            'userName' => Yii::$app->params['sberbank_login'],
                            'password' => Yii::$app->params['sberbank_password'],
                            'orderId' => $pay->sberbank_id
                        );
                    }
                }
                else {
                    return json_encode(0);
                }
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => Yii::$app->params['sberbank_url'] . '/payment/rest/getOrderStatus.do',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true, //  POST
                    CURLOPT_POSTFIELDS => http_build_query($data),
                    CURLOPT_SSLVERSION => 6
                ));
                $response = curl_exec($curl);

                $response = json_decode($response, true); //   JSON

                curl_close($curl);
                $order = CampOrder::find()->where(['id' => $pay->order_id])->one();
                $order->convertDate();
                if (isset($response['ErrorCode']) && $response['ErrorCode'] > 0) {
                    $pay->payed = false;
                    $pay->save();
                    Yii::$app->mailer->compose('@app/mail/PayFailed', ['data' => $order])
                        ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                        ->setTo($order->email)
                        ->setSubject('Возникла ошибка при обработке платежа')
                        ->send();
                    header('Location: http://novoepokolenie.com/platezh-ne-proshel/');
                    return json_encode(0);
                }
                else {

                    if (isset($response['OrderStatus']) && $response['OrderStatus'] == 2) {
                        $pay->payed = true;
                        $pay->save();
                        $order->status = CampOrder::STATUS_PAYED;
                        $order->save();
                        Yii::$app->mailer->compose('@app/mail/PaySuccess', ['data' => $order])
                            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                            ->setTo($order->email)
                            ->setSubject('Оплачена путёвка в лагерь "Новое поколение"')
                            ->send();
                        header('Location: http://novoepokolenie.com/oplata-proshla-uspeshno/');
                        return json_encode(1);
                    }
                    else {
                        Yii::$app->mailer->compose('@app/mail/PayFailed', ['data' => $order])
                            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                            ->setTo($order->email)
                            ->setSubject('Возникла ошибка при обработке платежа')
                            ->send();
                        header('Location: http://novoepokolenie.com/platezh-ne-proshel/');
                        return json_encode($order);
                    }
                }
            }
        }
    }

    public function actionPaycheckmarsh()
    {
        $orderid = yii::$app->request->get('orderId', null);
        if (yii::$app->request->isPost || $orderid != null) {
            $order_id = yii::$app->request->post('order_id', null);
            if ($order_id != null || $orderid != null) {

                if ($orderid != null) {
                    $pay = Pay::find()->where(['sberbank_id' => $orderid])->one();
                    $data1 = array(
                        'orderId' => $orderid
                    );
                }
                elseif ($order_id != null) {
                    $pay = Pay::find()->where(['order_id' => $order_id])->one();
                    $data1 = array(
                        'orderId' => $pay->sberbank_id
                    );
                }
                else {
                    return json_encode(0);
                }

                $dat = array(
                    'url' => Yii::$app->params['sberbank_url_marsh'] . '/payment/rest/getOrderStatus.do',
                );

                $data = array(
                    'userName' => Yii::$app->params['sberbank_login_marsh'],
                    'password' => Yii::$app->params['sberbank_password_marsh'],
                    'orderId' => $data1['orderId'],
                );

                $opts = array(                           // А здесь параметры для POST запроса
                    'http' => array(
                        'method' => 'POST',
                        'header' => "Content-type: application/x-www-form-urlencoded",
                        'content' => http_build_query($data),
                        'timeout' => 60
                    )
                );

                // И отправляем эти данные на сервер сбербанка
                $context = stream_context_create($opts);
                // Здесь должен быть URL тестового и боевого сервера Сбербанка
                $url = Yii::$app->params['sberbank_url_marsh'] . '/payment/rest/getOrderStatus.do';
                $result = file_get_contents($url, false, $context);

                // Расшифруем полученные данные в массив $arSbrfResult
                $response = json_decode($result, true);


                $order = RouteOrder::find()->where(['id' => $pay->order_id])->one();
                $order->convertDate();
                if (isset($response['ErrorCode']) && $response['ErrorCode'] > 0) {
                    $pay->payed = false;
                    $pay->save();
                    Yii::$app->mailer->compose('@app/mail/PayFailed', ['data' => $order])
                        ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                        ->setTo($order->email)
                        ->setSubject('Возникла ошибка при обработке платежа')
                        ->send();
                    header('Location: http://novoepokolenie.com/platezh-ne-proshel/');
                    return json_encode(0);
                }
                else {

                    if (isset($response['OrderStatus']) && $response['OrderStatus'] == 2) {
                        $pay->payed = true;
                        $pay->save();
                        $order->status = RouteOrder::STATUS_PAYED;
                        $order->save();
                        Yii::$app->mailer->compose('@app/mail/PaySuccess', ['data' => $order])
                            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                            ->setTo($order->email)
                            ->setSubject('Оплачена путёвка в лагерь "Новое поколение"')
                            ->send();
                        header('Location: http://novoepokolenie.com/oplata-proshla-uspeshno/');
                        return json_encode(1);
                    }
                    else {
                        Yii::$app->mailer->compose('@app/mail/PayFailed', ['data' => $order])
                            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                            ->setTo($order->email)
                            ->setSubject('Возникла ошибка при обработке платежа')
                            ->send();
                        header('Location: http://novoepokolenie.com/platezh-ne-proshel/');
                        return json_encode($order);
                    }
                }
            }
        }
    }

    public function actionPdf()
    {
        $order_id = yii::$app->request->get('order_id', null);
        if ($order_id != null) {
            $order = CampOrder::findOne($order_id);

            // Проверка, не аннулирован ли заказ
            if ($order == null || $order->active == 0) {
                return '<h1>Ваш заказ аннулирован или не существует</h1>';
            }
            $order->convertDate();
            $date_create = date("d.m.Y", strtotime($order->booking_end_string . "-15 days"));
            $count = count(Vouchers::find()->where(['order_id' => $order_id])->all());
            $shift = CampsShift::findOne($order->camp_shift_id);
            $shift->convertDate();
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
                'format' => Pdf::FORMAT_A4,
                'content' => $this->renderPartial('index', ['data' => $order, 'createdate' => $date_create, 'count' => $count, 'shift' => $shift]),
                'options' => [
                    'title' => 'Счет на оплату',
                    'subject' => 'Счет на оплату'
                ],
            ]);
            return $pdf->render();
        }
        else {
            return '<h1>Ваш заказ аннулирован или не существует</h1>';
        }
    }

    public function actionCheckpays()
    {
        $pay = Pay::find()->where(['and', ['payed' => 0], ['not', ['sberbank_id' => null]]])->all();
        $datares = array();
        foreach ($pay as $el) {
            if ($el->sberbank_id) {
                $datares[] = $el;

                if ($el->way === 0) {
                    $data = array(
                        'userName' => Yii::$app->params['sberbank_login_marsh'],
                        'password' => Yii::$app->params['sberbank_password_marsh'],
                        'orderId' => $el->sberbank_id
                    );
                }
                else {
                    $data = array(
                        'userName' => Yii::$app->params['sberbank_login'],
                        'password' => Yii::$app->params['sberbank_password'],
                        'orderId' => $el->sberbank_id
                    );
                }

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => Yii::$app->params['sberbank_url'] . '/payment/rest/getOrderStatus.do',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true, //  POST
                    CURLOPT_POSTFIELDS => http_build_query($data),
                    CURLOPT_SSLVERSION => 6
                ));
                $response = curl_exec($curl);
                $response = json_decode($response, true); //   JSON
                curl_close($curl);
                $order = CampOrder::findOne($el->order_id);
                if (isset($response['OrderStatus']) && $response['OrderStatus'] == 2) {
                    $el->payed = true;
                    $el->save();
                    if ($order != null) {
                        $order->convertDate();
                        $order->status = CampOrder::STATUS_PAYED;
                        $order->save();
                        Yii::$app->mailer->compose('@app/mail/PaySuccess', ['data' => $order])
                            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                            ->setTo($order->email)
                            ->setSubject('Оплачена путёвка в лагерь "Новое поколение"')
                            ->send();
                    }
                }
            }
        }
        return Json::encode($datares);
    }

    public function actionRecordinwaitlist()
    {
        $shifts = CampsShift::find()->where(['status' => CampsShift::STATUS_ACTIVE])->all();
        foreach ($shifts as $el) {
            $el->convertDate();
        }
        if (yii::$app->request->isPost) {
            $model = new WaitingList();
            if ($model->load(Yii::$app->request->post())) {
                if (!preg_match('/((8|\+7) ?)?\(?\d{3,5}\)? ?\d{1}-?\d{1}-?\d{1}-?\d{1}-?\d{1}((-?\d{1})?-?\d{1})?/', $model->phone)) {
                    return Json::encode(['error' => '1']);
                }
                if (!filter_var($model->email, FILTER_VALIDATE_EMAIL)) {
                    return Json::encode(['error' => '2']);
                }
                if (!$model->name && $model->name !== '') {
                    return Json::encode(['error' => '3']);
                }
                $shift = CampsShift::findOne($model->camp_shift_id);
                if ($shift === null) {
                    return Json::encode(['error' => '4']);
                }
                $elem = WaitingList::find()->where(['name' => $model->name, 'phone' => $model->phone, 'email' => $model->email, 'camp_shift_id' => $model->camp_shift_id])->count();
                if ($elem == 0) {
                    $model->save(false);
                    return Json::encode(true);
                }
                return Json::encode(['error' => '5']);
            }
            return Json::encode($model);
        }
        return Json::encode($shifts);
    }

    public function actionCancelwaitlist($id)
    {
        $wl = WaitingList::findOne($id);
        $wl->delete();
        header('Location: http://novoepokolenie.com/');
    }

    public function actionContent()
    {
        $content_id = yii::$app->request->get('id', null);
        if ($content_id != null) {
            $content = Content::findOne($content_id);
            if ($content != null) {
                return Json::encode($content);
            }
            else {
                return Json::encode(['error' => 1]);
            }
        }
        else {
            return Json::encode(Content::find()->all());
        }
    }

    public function actionShiftlist()
    {
        return Json::encode(CampsShift::getVouchersParty());
    }

    public function actionGetroutes()
    {
        // $routes = Routes::getAllowed();
        $routes = Routes::find()->where(['status' => Routes::STATUS_ACTIVE])->all();
        $addination = Addination::find()->all();
        $data = array();
        $data['routes'] = $routes;
        $data['addination'] = $addination;
        return Json::encode($data);
    }

    public function actionCreaterouteorder()
    {
        $order = new RouteOrder();
        if ($order->load(yii::$app->request->Post())) {
            $order->addination = explode(',', $_POST['RouteOrder']['addination']);
            $order->active = true;
            $order->status = RouteOrder::STATUS_MODERATE;
            // if ($order->route->allowedplaces >= $order->places) {
            $order->save(false);
            /*
            $order->convertDate();
            $date_create = date("d.m.Y", strtotime($order->booking_end_string . "-15 days"));
            $count = $order->places;
            $shift = Routes::findOne($order->route_id);
            $shift->convertDate();
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
                'format' => Pdf::FORMAT_A4,
                'destination' => Pdf::DEST_STRING,
                'content' => $this->renderPartial('indexroute', ['data' => $order, 'createdate' => $date_create, 'count' => $count, 'shift' => $shift]),
                'options' => [
                    'title' => 'Счет на оплату',
                    'subject' => 'Счет на оплату'
                ],
            ]);
            $doc = $pdf->render();*/

            Yii::$app->mailer->compose('@app/mail/CreateRouteOrder', ['data' => $order])
                ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                ->setTo($order->email)
                ->setSubject("Заявка принята на маршрут выходного дня \"Новое поколение\"")
                ->attach($_SERVER["DOCUMENT_ROOT"] . '/web/data/правила_пребывания_мвд_19.doc')
                ->attach($_SERVER["DOCUMENT_ROOT"] . '/web/data/Договор_МВД.docx')
                // ->setSubject("Забронирован маршрут выходного дня \"Новое поколение\"")
                // ->attachContent($doc, ['fileName' => 'Счет на оплату.pdf', 'contentType' => 'application/pdf'])
                ->send();

            Yii::$app->mailer->compose('@app/mail/CreateRouteOrderManager', ['order' => $order])
                ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                ->setTo(Yii::$app->params['managerEmail'])
                ->setSubject("Поступила заявка маршрут выходного дня \"Новое поколение\"")
                ->send();
            // } else {
            // return Json::encode('Недостаточно свободных мест, осталось: ' . $order->route->allowedplaces);
            // }

            return Json::encode($order);
        }
    }

    public function actionPdfroute()
    {
        $order_id = yii::$app->request->get('order_id', null);
        if ($order_id != null) {
            $order = RouteOrder::findOne($order_id);
            if ($order != null) {
                $order->convertDate();
                $date_create = date("d.m.Y", strtotime($order->booking_end_string . "-15 days"));
                $count = $order->places;
                $shift = Routes::findOne($order->route_id);
                $shift->convertDate();
                $pdf = new Pdf([
                    'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
                    'format' => Pdf::FORMAT_A4,
                    'content' => $this->renderPartial('indexroute', ['data' => $order, 'createdate' => $date_create, 'count' => $count, 'shift' => $shift]),
                    'options' => [
                        'title' => 'Счет на оплату',
                        'subject' => 'Счет на оплату'
                    ],
                ]);
                return $pdf->render();
            }
            else {
                return '<h1> Такой брони не найдено</h1>';
            }
        }
    }

    function actionCreatevoucher()
    {
        $orderid = yii::$app->request->get('order_id', null);
        $token = yii::$app->request->get('token', null);
        $order = CampOrder::findOne($orderid);
        if ($order->active && $order->token == $token) {
            $voucher = $order->vouchers[0];
            if ($voucher->load(Yii::$app->request->post())) {
                $voucher->save();
                $order->token = null;
                $now = date("d.M.y");
                $add14 = date("d.M.y", strtotime($now . "+14 days"));
                $order->booking_end = Yii::$app->formatter->asTimestamp($add14);
                $order->save();
                header('Location: http://www.novoepokolenie.com/');
            }
            return $this->renderPartial('createvoucher', ['model' => $voucher]);
        }
        header('Location: http://www.novoepokolenie.com/');
    }
}