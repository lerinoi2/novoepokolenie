<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\User;
use Yii;
use yii\web\Controller;

class IndexController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }else{
            if(Yii::$app->user->identity->role == 4){
                $this->redirect('/content');
            }
        }
        return true;
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
