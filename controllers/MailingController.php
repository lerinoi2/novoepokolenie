<?php

namespace app\controllers;

use app\models\CampOrder;
use app\models\Camps;
use app\models\CampsShift;
use app\models\sendpulse\ApiClient;
use app\models\xlsx\XLSXWriter;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;

class MailingController extends Controller
{
    const API_USER_ID = '258f787667c73ff0130549d62ab9f32a';
    const API_SECRET = '9e0d6c275d3c53e6fcd1dee66246fa75';
    const TOKEN_STORAGE = 'file';

    public $SPApi = false;

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }

    public function getBookId($name)
    {
        $this->ApiProxy();
        $SPbooks = $this->SPApi->listAddressBooks();
        $booksArr = array();
        foreach ($SPbooks as $book) {
            $booksArr[$book->name] = $book->id;
        }
        return $booksArr[$name];
    }

    public function ApiProxy()
    {
        if (!$this->SPApi) {
            $this->SPApi = new ApiClient(self::API_USER_ID, self::API_SECRET);
        }
    }

    public function getTemplateId($name)
    {
        $this->ApiProxy();
        $SPtemplates = $this->SPApi->getTemplates();
        $temp_id = false;
        foreach ($SPtemplates as $key => $template) {
            $temp_id = ($template->name == $name) ? $template->id : $temp_id;
        }
        return $temp_id;
    }

    public function actionIndex()
    {

        return $this->render('index');
    }

    public function actionSendemail($status = "all", $entity = "all", $shift = "all", $theme = null, $temp_id = null)
    {
        $this->ApiProxy();
        if (yii::$app->request->isAjax) {
            $now = new \DateTime('now', new \DateTimeZone('Asia/Yekaterinburg'));
            $now = yii::$app->formatter->asDate($now, "php:d.m.y H:i:s");
            if ($theme == null || $theme === '') {
                return Json::encode(['ERROR' => 1]);//'Тема не должна быть пустой!';
            }
            if ($temp_id == null || $temp_id < 0) {
                return Json::encode(['ERROR' => 2]);//'Не правильно выбран шаблон!';
            }
            $orders = CampOrder::MultipleSearchAndFilter($status, $entity, $shift);
            foreach ($orders as $el) {
                $emails[] = array('email' => $el->email, 'variables' => array('phone' => $el->phone, 'name' => $el->name, 'order_id' => $el->id, 'create_date' => Yii::$app->formatter->asDate($el->create_date, "dd.MM.y"), 'booking_end' => Yii::$app->formatter->asDate($el->booking_end, "dd.MM.y")));
            }
            $book_id = $this->SPApi->createAddressBook('Рассылка от ' . $now)->id;
            if ($book_id == null) {
                return Json::encode(['ERROR' => 3]);//'Адресная книга в SendPulse не создалась!';
            }
            $this->SPApi->addEmails($book_id, $emails);

            sleep(5);

            $msg = $this->sendByTemplate($temp_id, $book_id, $theme);
            if ($msg->error_code == 709) {
                $msg->temp_id = $temp_id;
                $msg->book_id = $book_id;
                $msg->theme = $theme;
            }

            return Json::encode($msg);
        }
        $entity = CampOrder::getEntityesArray();
        $camps = ArrayHelper::map(Camps::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'name');
        // $shifts = CampsShift::findAll(['status' => Camps::STATUS_ACTIVE]);
        $shifts = CampsShift::find()->all();
        $status = CampOrder::getStatusArray();
        $conditions = CampsShift::getConditionsArray();
        $templates = $this->getTemplatesArray();
        return $this->render('sendmail', ['entity' => $entity, 'camps' => $camps, 'condition' => $conditions, 'status' => $status, 'shifts' => $shifts, 'templates' => $templates]);
    }

    public function sendByTemplate($temp, $book, $theme)
    {
        $this->ApiProxy();
        $temp_id = $temp;
        $book_id = $book;
        $SPtemplates = $this->SPApi->createCampaignByTemlate('Лагерь "Новое поколение"', 'office@npcamp.ru', $theme, $temp_id, $book_id);
        return $SPtemplates;
    }

    public function getTemplatesArray()
    {
        $this->ApiProxy();
        $SPtemplates = $this->SPApi->getTemplates();
        return $SPtemplates;
    }

    public function actionSendbytemplate()
    {
        $temp_id = Yii::$app->request->get('temp_id', null);
        $theme = Yii::$app->request->get('theme', null);
        $book_id = Yii::$app->request->get('book_id', null);

        if (!$temp_id || !$theme || !$book_id) {
            return Json::encode(['ERROR' => 0]);
        }

        $msg = $this->sendByTemplate($temp_id, $book_id, $theme);
        return Json::encode($msg);
    }

    public function actionStats()
    {
        $this->ApiProxy();
        $campaing = $this->SPApi->listCampaigns(10);
        $data = array();
        foreach ($campaing as $el) {
            $stat = $this->SPApi->getCampaignInfo($el->id);
            $data[] = array('id' => $el->id, 'name' => $el->name, 'statistic' => $stat->statistics);
        }
        return $this->render('stats', ['data' => $data]);
    }

    public function actionList($date_start = null, $date_end = null)
    {
        $this->ApiProxy();
        if (yii::$app->request->isAjax) {
            $campaing = $this->SPApi->getSmsCampaingLit($date_start, $date_end);
            $data = array();
            foreach ($campaing->data as $el) {
                $info = $this->SPApi->getBookInfo($el->address_book_id);
                $data[] = array('id' => $el->id, 'company_price' => $el->company_price, 'send_date' => $el->send_date, 'name' => $info[0]->name);
            }
            return $this->renderPartial('listPartical', ['data' => $data]);
        }
        $campaing = $this->SPApi->getSmsCampaingLit($date_start, $date_end);
        $data = array();
        foreach ($campaing->data as $el) {
            $info = $this->SPApi->getBookInfo($el->address_book_id);
            $data[] = array('id' => $el->id, 'company_price' => $el->company_price, 'send_date' => $el->send_date, 'name' => $info[0]->name);
        }
        return $this->render('list', ['data' => $data]);
    }

    public function actionSendsms($status = "all", $entity = "all", $shift = "all", $body = null)
    {
        $this->ApiProxy();
        if (yii::$app->request->isAjax) {
            $now = new \DateTime('now', new \DateTimeZone('Asia/Yekaterinburg'));
            $now = yii::$app->formatter->asDate($now, "php:d.m.y H:i:s");
            if ($body == null || $body === '') {
                return 'Введите текст сообщения';
            }
            $orders = CampOrder::MultipleSearchAndFilter($status, $entity, $shift);
            foreach ($orders as $el) {
                $emails[] = array('email' => $el->email, 'variables' => array('phone' => $el->phone, 'name' => $el->name, 'order_id' => $el->id, 'create_date' => Yii::$app->formatter->asDate($el->create_date, "dd.MM.y"), 'booking_end' => Yii::$app->formatter->asDate($el->booking_end, "dd.MM.y")));
            }
            $book_id = $this->SPApi->createAddressBook('Рассылка SMS от ' . $now)->id;
            if ($book_id == null) {
                return 'Адресная книга в SendPulse не создалась!';
            }
            $this->SPApi->addEmails($book_id, $emails);
            $msg = $this->SPApi->createPhoneCompany('office@npcamp.ru', $book_id, $body, $now);
            return 'Успешно создана компания и началась рассылка' . Json::encode($msg);
        }
        $entity = CampOrder::getEntityesArray();
        $camps = ArrayHelper::map(Camps::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'name');
        // $shifts = CampsShift::findAll(['status' => Camps::STATUS_ACTIVE]);
        $shifts = CampsShift::find()->all();
        $status = CampOrder::getStatusArray();
        $conditions = CampsShift::getConditionsArray();
        $templates = $this->getTemplatesArray();
        return $this->render('sendsms', ['entity' => $entity, 'camps' => $camps, 'condition' => $conditions, 'status' => $status, 'shifts' => $shifts, 'templates' => $templates]);
    }
}
