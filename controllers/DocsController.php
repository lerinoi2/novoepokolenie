<?php

namespace app\controllers;

use Yii;
use app\models\CampOrder;
use app\models\Camps;
use app\models\CampsShift;
use app\models\xlsx\XLSXWriter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use app\models\Upload;
use yii\web\UploadedFile;
use yii\web\Controller;

use app\models\sendpulse\ApiClient;

class DocsController extends Controller
{

    const ANKETA = 0;
    const MEDSPRAVKA = 1;
    const OBYAZATELSTVA = 2;
    const SOGL_MED = 3;
    const DOGOVOR_DNEVNOI = 4;
    const DOGOVOR_ZAGOROD = 5;
    const DOGOVOR_PALATKI = 6;

    public static function getDocsArray()
    {
        return array(
            self::ANKETA => 'Анкета для родителей',
            self::MEDSPRAVKA => 'Медицинская справка',
            self::OBYAZATELSTVA => 'Обязательсво родителей',
            self::SOGL_MED => 'Соглашение на медицинское вмешательство',
            self::DOGOVOR_DNEVNOI => 'Договор дневной',
            self::DOGOVOR_ZAGOROD => 'Договор загородный лагерь',
            self::DOGOVOR_PALATKI => 'Договор палатки',
        );
    }

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }

    public function actionIndex()
    {
        $dir = Yii::getAlias('@app/mail/attach');

        $model = new Upload();

        if ($model->load($_POST)) {
            $file = UploadedFile::getInstance($model, 'file');

            switch ($model->type) {
                case self::ANKETA:
                    $file->saveAs($dir . '/' . 'anketa_roditeli.doc');
                    break;
                case self::MEDSPRAVKA:
                    $file->saveAs($dir . '/' . 'medspravka.doc');
                    break;
                case self::OBYAZATELSTVA:
                    $file->saveAs($dir . '/' . 'objazatelstva_roditelej.doc');
                    break;
                case self::SOGL_MED:
                    $file->saveAs($dir . '/' . 'sogl_med_vmesh.doc');
                    break;
                case self::DOGOVOR_DNEVNOI:
                    $file->saveAs($dir . '/' . 'договор_дневной_18.doc');
                    break;
                case self::DOGOVOR_ZAGOROD:
                    $file->saveAs($dir . '/' . 'договор_загородный_лагерь_18.doc');
                    break;
                case self::DOGOVOR_PALATKI:
                    $file->saveAs($dir . '/' . 'договор_палатки_18.doc');
                    break;
            }
        }
        return $this->render('index', ['docs' => $this->getDocsArray(), 'model' => $model]);
    }

    public function actionDownload($url)
    {
        $dir = Yii::getAlias('@app/mail/attach');
        $filecont = file_get_contents($dir . '/' . $url);
        return \Yii::$app->response->sendContentAsFile($filecont, $url, ['mimeType' => 'application/doc']);
    }

}
