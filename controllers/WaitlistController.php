<?php

namespace app\controllers;

use app\models\WaitingList;
use app\models\CampsShift;
use yii\helpers\ArrayHelper;
use Yii;
use yii\base\Model;
use yii\web\Controller;

class WaitlistController extends Controller
{

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }

    public function actionIndex()
    {
        $list = Waitinglist::find()->all();
        $shifts = ArrayHelper::map(CampsShift::find()->all(), 'id', 'name');
        return $this->render('index', ['list' => $list, 'shift' => $shifts]);
    }

}