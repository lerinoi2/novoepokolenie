<?php


namespace app\controllers;

use app\models\CampOrder;
use app\models\Camps;
use app\models\CampsShift;
use app\models\RouteOrder;
use app\models\Vouchers;
use app\models\Pay;
use Yii;
use app\models\Blacklist;
use yii\base\Model;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use kartik\mpdf\Pdf;
use app\models\CampOrderSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class Camp_orderController extends Controller
{
    const PAGE_STEP = 20;

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }

    public function actionIndex()
    {
        $searchModel = new CampOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $campOrder = new CampOrder();
        $vouchers = [new Vouchers()];
        if ($campOrder->load(Yii::$app->request->post())) {
            $campOrder->price = CampsShift::getCurrentPrice($campOrder->camp_shift_id);
            $campOrder->save();
            $vouchers = array();
            for ($i = 0; $i < $campOrder->count; $i++) {
                $vouchers[] = new Vouchers();
            }
            if (Model::loadMultiple($vouchers, Yii::$app->request->post())) {
                foreach ($vouchers as $voucher) {
                $black = Blacklist::find()->where(['name' => $voucher->name, 'birth' => Yii::$app->formatter->asTimestamp($voucher->birth)])->count();
                if ($black > 0) {
                    $campOrder->delete();
                    $voucher->delete();
                    return Json::encode(['error' => '3']);
                }

                    $voucher->order_id = $campOrder->id;
                    $voucher->way = Pay::TYPE_CAMP;
                    $voucher->save(false);
                }
            }
            return $this->redirect(array('index/index'));
        }
        $entity = CampOrder::getEntityesArray();
        $camps = ArrayHelper::map(Camps::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'name');
        $camps_shift = ArrayHelper::map(CampsShift::find()->all(), 'id', 'name');
        return $this->render('create', ['model' => $campOrder, 'vouchers' => $vouchers, 'entity' => $entity, 'camps' => $camps, 'camps_shift' => $camps_shift]);
    }

    public function actionEdit($id)
    {
        $campOrder = CampOrder::findOne($id);
        $vouchers = Vouchers::find()->where(['order_id' => $campOrder->id])->all();
        $sum = $campOrder->count * $campOrder->price * ((100 - $campOrder->discount) / 100);
        $pays = Pay::find()->where(['order_id' => $id, 'way' => Pay::TYPE_CAMP])->all();
        $counts = 0;
        $payed = 0;
        foreach ($pays as $pay) {
            if ($campOrder != null && $payed) {
                $payed += $pay->sum;
                $counts = $payed / ($campOrder->price * ((100 - $campOrder->discount) / 100));
                if ($counts > $campOrder->count) {
                    $counts = $campOrder->count;
                }
            }
            else {
                $payed = $pay->sum;
                if ($campOrder->campShift) {
                    if ($campOrder->price == 0) {
                        $counts = 0;
                    }
                    else {
                        $counts = $payed / ($campOrder->price * ((100 - $campOrder->discount) / 100));
                    }
                    if ($counts > $campOrder->count) {
                        $counts = $campOrder->count;
                    }
                }
            }
        }
        foreach ($vouchers as $el) {
            $el->convertDate();
        }
        if ($campOrder == NULL) {
            Yii::$app->response->statusCode = 404;
        }
        $campOrder->convertDate();

        if ($campOrder->load(Yii::$app->request->post()) && $campOrder->save()) {
            if ($campOrder->count > count($vouchers)) {
                for ($i = count($vouchers); $i < $campOrder->count; $i++) {
                    $voucher = new Vouchers();
                    $voucher->order_id = $campOrder->id;
                    $voucher->way = Pay::TYPE_CAMP;
                    $voucher->save(false);
                }
            }
            elseif ($campOrder->count < count($vouchers)) {
                $dataVoucher = Vouchers::find()->where(['order_id' => $campOrder->id])->all();
                $deleteing = count($vouchers) - $campOrder->count;
                foreach ($dataVoucher as $value) {
                    if (($value->voucher_number == null || $value->voucher_number === '') && $deleteing > 0) {
                        $value->delete();
                        $deleteing--;
                    }
                }
                if ($deleteing > 0) {
                    $campOrder->count += $deleteing;
                    $campOrder->save();
                }
            }
            if (Model::loadMultiple($vouchers, Yii::$app->request->post())) {
                foreach ($vouchers as $el) {
                    $el->save(false);
                }
            }
        }
        $entity = CampOrder::getEntityesArray();
        $camps = ArrayHelper::map(Camps::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'name');
        $camps_shift = ArrayHelper::map(CampsShift::find()->all(), 'id', 'name');
        $status = CampOrder::getStatusArray();
        return $this->render('edit', ['model' => $campOrder, 'entity' => $entity, 'camps' => $camps, 'camps_shift' => $camps_shift, 'vouchers' => $vouchers, 'status' => $status, 'pays' => $pays, 'payed' => $payed, 'counts' => $counts, 'sum' => $sum]);
    }

    public function actionDelete($id)
    {
        $campOrder = CampOrder::findOne($id);
        if ($campOrder != null) {
            $campOrder->active = false;
            $campOrder->save();
        }
        return $this->redirect(array('index/index'));
    }

    public function actionPays($search = null, $stat = 'all', $entity = 'all', $shift = 'all')
    {
        $orders = CampOrder::SearchAndFilters($search, $stat, $entity, $shift, 1, 'notpayed');
        foreach ($orders as $order) {
            $order->convertDate();
            $order->campShift->convertDate();
            foreach ($order->vouchers as $voucher) {
                $voucher->convertDate();
            }
        }
        $entity = CampOrder::getEntityesArray();
        $camps = ArrayHelper::map(Camps::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'name');
        // $shifts = CampsShift::findAll(['status' => Camps::STATUS_ACTIVE]);
        $shifts = CampsShift::find()->all();
        $status = CampOrder::getStatusArray();
        $conditions = CampsShift::getConditionsArray();
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('paysPartical', ['orders' => $orders, 'entity' => $entity, 'camps' => $camps, 'condition' => $conditions, 'status' => $status]);
        }
        return $this->render('pays', ['orders' => $orders, 'entity' => $entity, 'camps' => $camps, 'shifts' => $shifts, 'status' => $status, 'conditions' => $conditions]);
    }

    public function actionCreatepays($id)
    {
        $pay = new Pay();
        $camp_orders = CampOrder::findOne($id);
        $camp_orders->convertDate();
        $vouchers = Vouchers::find()->where(['order_id' => $camp_orders->id])->all();
        if ($pay->load(Yii::$app->request->post())) {
            $arChildrens = $pay->frompaid;
            $arNames = '';
            if (count($vouchers) >= 1) {
                foreach ($vouchers as $val) {
                    if (in_array($val->id, $arChildrens)) {
                        $arNames .= $val->name . "\n";
                    }
                }
            }
            $pay->frompaid = $arNames;
            $pay->way = Pay::TYPE_CAMP;
            if (($pay->sum + $camp_orders->sumpayed) < $camp_orders->sum) {
                $pay->payed = false;
                $camp_orders->status = CampOrder::STATUS_PREPAYED;
            }
            elseif (($pay->sum + $camp_orders->sumpayed) >= $camp_orders->sum) {
                $pay->payed = true;
                $camp_orders->status = CampOrder::STATUS_PAYED;
                foreach ($camp_orders->pays as $el) {
                    if ($el->payed !== 0) {
                        $el->payed = true;
                        $el->save();
                    }
                }
            }
            $pay->save();
            $camp_orders->save();
            Yii::$app->mailer->compose('@app/mail/PaySuccess', ['data' => $camp_orders])
                ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                ->setTo($camp_orders->email)
                ->setSubject('Оплата прошла успешно')
                ->send();
            return $this->redirect(Yii::$app->request->referrer);
        }
        $payTypes = Pay::getPayTypesArray();
        return $this->render('payedit', ['model' => $pay, 'camp_orders' => $camp_orders, 'payTypes' => $payTypes, 'info' => $vouchers]);
    }

    public function actionVoucher($search = null, $stat = 'all', $entity = 'all', $shift = 'all', $onlyactive = 1)
    {
        $orders = CampOrder::SearchAndFilters($search, $stat, $entity, $shift, $onlyactive, 'payed');
        foreach ($orders as $order) {
            $order->convertDate();
            $order->campShift->convertDate();
            foreach ($order->vouchers as $voucher) {
                $voucher->convertDate();
            }
        }
        $entity = CampOrder::getEntityesArray();
        $camps = ArrayHelper::map(Camps::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'name');
        $shifts = CampsShift::find()->all();
        $status = CampOrder::getStatusArray();
        $conditions = CampsShift::getConditionsArray();
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('vouchersPartical', ['orders' => $orders, 'entity' => $entity, 'camps' => $camps, 'condition' => $conditions, 'status' => $status, 'shifts' => $shifts]);
        }
        return $this->render('vouchers', ['orders' => $orders, 'entity' => $entity, 'camps' => $camps, 'condition' => $conditions, 'status' => $status, 'shifts' => $shifts]);
    }

    public function actionEditpay($id)
    {
        $pay = new Pay();
        $camp_orders = CampOrder::findOne($id);
        $camp_orders->convertDate();
        $vouchers = Vouchers::find()->where(['order_id' => $camp_orders->id])->all();
        if ($pay->load(Yii::$app->request->post())) {
            $arChildrens = $pay->frompaid;
            $arNames = '';
            foreach ($vouchers as $val) {
                if (in_array($val->id, $arChildrens)) {
                    $arNames .= $val->name . "\n";
                }
            }
            $pay->frompaid = $arNames;
            $pay->way = Pay::TYPE_CAMP;
            if (($pay->sum + $camp_orders->sumpayed) < $camp_orders->sum) {
                $pay->payed = false;
                $camp_orders->status = CampOrder::STATUS_PREPAYED;
            }
            elseif (($pay->sum + $camp_orders->sumpayed) >= $camp_orders->sum) {
                $pay->payed = true;
                $camp_orders->status = CampOrder::STATUS_PAYED;
                foreach ($camp_orders->pays as $el) {
                    if (!$el->sberbank_id) {
                        $el->payed = true;
                        $el->save();
                    }
                }
            }
            $pay->save();
            $camp_orders->save();

            return $this->redirect(Yii::$app->request->referrer);
        }
        $payTypes = Pay::getPayTypesArray();
        return $this->render('payedit', ['model' => $pay, 'camp_orders' => $camp_orders, 'payTypes' => $payTypes, 'info' => $vouchers]);
    }

    public function actionVouchercreate($id)
    {
        $model = CampOrder::findOne($id);
        $model->convertDate();
        $voucher = $model->vouchers;
        $entity = CampOrder::getEntityesArray();
        $camps = ArrayHelper::map(Camps::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'name');
        $camps_shift = ArrayHelper::map(CampsShift::find()->all(), 'id', 'name');
        foreach ($voucher as $el) {
            $el->convertDate();
        }
        if (Yii::$app->request->isPost) {
            if (Model::loadMultiple($voucher, Yii::$app->request->post())) {
                $countnumber = 0;
                foreach ($voucher as $el) {
                    if ($el->voucher_number != null || $el->voucher_number !== '') {
                        $el->save(false);
                        $countnumber++;
                    }
                }

                if ($countnumber == $model->count && $model->status == CampOrder::STATUS_PAYED) {
                    $model->status = CampOrder::STATUS_COMPLETE;
                    $model->save();
                }

                $data['vouchers'] = $voucher;
                $model->campShift->convertDate();
                $model->convertDate();
                $data['order'] = $model;
                foreach ($voucher as $key => $el) {
                    $el->convertDate();
                }
            }
            return $this->render('vouchersCreate', ['model' => $model, 'vouchers' => $voucher, 'entity' => $entity, 'camps' => $camps, 'camps_shift' => $camps_shift]);
        }
        return $this->render('vouchersCreate', ['model' => $model, 'vouchers' => $voucher, 'entity' => $entity, 'camps' => $camps, 'camps_shift' => $camps_shift]);
    }

    public function actionSendvouchers($id)
    {
        set_time_limit(500);

        if ($id != null) {
            $order = CampOrder::findOne($id);
            if ($order != null) {
                $order->convertDate();
                $vouchers = Vouchers::find()->where(['order_id' => $id, 'way' => 1])->andWhere(['not', ['voucher_number' => null]])->all();
                $data['vouchers'] = $vouchers;
                $order->campShift->convertDate();
                $data['order'] = $order;

                $camps_shift = CampsShift::getCampsShift();
                $conditions = CampsShift::getConditionsArray();
                foreach ($camps_shift as $el) {
                    $el->convertDate();
                }

                $mail = Yii::$app->mailer->compose('@app/mail/SendVouchers', ['data' => $data])
                    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                    ->setTo($order->email)
                    ->setSubject('Ваша путёвка оформлена');

                foreach ($vouchers as $key => $el) {
                    $el->convertDate();
                    $pdf = new Pdf([
                        'mode' => Pdf::MODE_UTF8,
                        'format' => Pdf::FORMAT_A4,
                        'orientation' => Pdf::ORIENT_PORTRAIT,
                        'destination' => Pdf::DEST_STRING,
                        'content' => $this->renderPartial('voucherPdf', ['data' => $order, 'voucher' => $el, 'conditions' => $conditions]),
                        'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                        'marginLeft' => 0,
                        'marginRight' => 0,
                        'marginTop' => 0,
                        'marginBottom' => 0,
                        'marginHeader' => 0,
                        'marginFooter' => 0,
                        'options' => [
                            'title' => 'Путёвка',
                            'subject' => 'Путёвка'
                        ],
                    ]);
                    $pdf = $pdf->render();

                    $mail->attachContent($pdf, ['fileName' => 'Путёвка №' . ($key + 1) . '.pdf', 'contentType' => 'application/pdf']);
                }
                $mail->send();
            }
        }
        return $this->redirect('/camp_order/edit/' . $id);
    }

    public function actionGetpdf($id, $num_id)
    {
        if ($id != null) {
            $order = CampOrder::findOne($id);
            $order->convertDate();
            $vouchers = Vouchers::find()->where(['id' => $num_id, 'way' => 1])->andWhere(['not', ['voucher_number' => null]])->all();
            $data['vouchers'] = $vouchers;
            $order->campShift->convertDate();
            $data['order'] = $order;

            $camps_shift = CampsShift::getCampsShift();
            $conditions = CampsShift::getConditionsArray();
            foreach ($camps_shift as $el) {
                $el->convertDate();
            }


            foreach ($vouchers as $key => $el) {

                $query = "select * from np_pay where order_id = '" . $id . "' and frompaid like '%" . $el->name . "%'";
                $result = Pay::findBySql($query)->asArray()->one();

                $el->convertDate();
                $pdf = new Pdf([
                    'mode' => Pdf::MODE_UTF8,
                    'format' => Pdf::FORMAT_A4,
                    'orientation' => Pdf::ORIENT_PORTRAIT,
                    'destination' => Pdf::DEST_BROWSER, // DEST_DOWNLOAD
                    'content' => $this->renderPartial('voucherPdf', ['data' => $order, 'voucher' => $el, 'conditions' => $conditions, 'q' => $result]),
                    'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                    'marginLeft' => 0,
                    'marginRight' => 0,
                    'marginTop' => 0,
                    'marginBottom' => 0,
                    'marginHeader' => 0,
                    'marginFooter' => 0,
                    'options' => [
                        'title' => 'Путёвка',
                        'subject' => 'Путёвка'
                    ],
                ]);
            }
        }
        return $pdf->render();
    }

    public function actionGetzip($id)
    {
        set_time_limit(500);
        $model = CampOrder::findOne($id);
        $model->convertDate();
        $voucher = $model->vouchers;
        foreach ($voucher as $el) {
            $el->convertDate();
        }
        $data['vouchers'] = $voucher;
        $model->campShift->convertDate();
        $model->convertDate();
        $data['order'] = $model;
        $conditions = CampsShift::getConditionsArray();
        $file = tempnam("tmp", "zip");
        $zip = new \ZipArchive();
        $zip->open($file, \ZipArchive::OVERWRITE);
        foreach ($voucher as $key => $el) {
            $el->convertDate();
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_STRING,
                'content' => $this->renderPartial('voucherPdf', ['data' => $data['order'], 'voucher' => $el, 'conditions' => $conditions]),
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                'marginLeft' => 0,
                'marginRight' => 0,
                'marginTop' => 0,
                'marginBottom' => 0,
                'marginHeader' => 0,
                'marginFooter' => 0,
                'options' => [
                    'title' => 'Путёвка',
                    'subject' => 'Путёвка'
                ],
            ]);
            $pdf = $pdf->render();
            $name = time();
            $zip->addFromString($name . '.pdf', $pdf);
            $zip->addFile($name . '.pdf', $name . '.pdf');
        }
        $zip->close();
        header('Content-Type: application/zip');
        header('Content-Length: ' . filesize($file));
        header('Content-Disposition: attachment; filename="file.zip"');
        readfile($file);
        unlink($file);
        return $this->redirect('/camp_order/edit/' . $id);
    }


    public function actionSendvoucher($id, $num_id)
    {
        if ($id != null) {
            $order = CampOrder::findOne($id);
            $order->convertDate();
            $vouchers = Vouchers::find()->where(['id' => $num_id, 'way' => 1])->andWhere(['not', ['voucher_number' => null]])->all();
            $data['vouchers'] = $vouchers;
            $order->campShift->convertDate();
            $data['order'] = $order;

            $camps_shift = CampsShift::getCampsShift();
            $conditions = CampsShift::getConditionsArray();
            foreach ($camps_shift as $el) {
                $el->convertDate();
            }

            $mail = Yii::$app->mailer->compose('@app/mail/SendVouchers', ['data' => $data])
                ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                ->setTo($order->email)
                ->setSubject('Ваша путёвка оформлена');

            foreach ($vouchers as $key => $el) {
                $el->convertDate();
                $pdf = new Pdf([
                    'mode' => Pdf::MODE_UTF8,
                    'format' => Pdf::FORMAT_A4,
                    'orientation' => Pdf::ORIENT_PORTRAIT,
                    'destination' => Pdf::DEST_STRING,
                    'content' => $this->renderPartial('voucherPdf', ['data' => $order, 'voucher' => $el, 'conditions' => $conditions]),
                    'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                    'marginLeft' => 0,
                    'marginRight' => 0,
                    'marginTop' => 0,
                    'marginBottom' => 0,
                    'marginHeader' => 0,
                    'marginFooter' => 0,
                    'options' => [
                        'title' => 'Путёвка',
                        'subject' => 'Путёвка'
                    ],
                ]);
                $pdf = $pdf->render();

                $mail->attachContent($pdf, ['fileName' => 'Путёвка №' . ($key + 1) . '.pdf', 'contentType' => 'application/pdf']);
            }
            $mail->send();
        }
        return $this->redirect('/camp_order/edit/' . $id);
    }

    public function actionSendcheck($id)
    {
        if ($id != null) {
            $order = CampOrder::findOne($id);
            if ($order != null) {
                $order->convertDate();
                $date_create = $order->create_date_string;
                $count = $order->count;
                $shift = $order->campShift;
                $shift->convertDate();
                $pdf = new Pdf([
                    'mode' => Pdf::MODE_UTF8,
                    'format' => Pdf::FORMAT_A4,
                    'destination' => Pdf::DEST_STRING,
                    'content' => $this->renderPartial('/api/index', ['data' => $order, 'createdate' => $date_create, 'count' => $count, 'shift' => $shift]),
                    'options' => [
                        'title' => 'Счет на оплату',
                        'subject' => 'Счет на оплату'
                    ],
                ]);
                $render = $pdf->render();
                $data = array();
                $data['order_id'] = $order->id;
                $data['date_end'] = $order->booking_end_string;
                $data['date_create'] = $order->create_date_string;
                $data['shift'][] = array('count' => $order->count, 'price' => $order->price, 'sum' => ($order->price * $order->count) * ((100 - $order->discount) / 100), 'name' => $order->campShift->name, 'date_start_string' => $order->campShift->date_start_string, 'date_end_string' => $order->campShift->date_end_string);
                $dogovor = '/mail/attach/anketa_roditeli.doc';
                if ($order->campShift->conditions == CampsShift::CONDITIONS_WELL) {
                    $dogovor = '/mail/attach/договор_на_услуги_по_организации_отдыха_и_оздоровления_ребенка.doc';
                }
                elseif ($order->campShift->conditions == CampsShift::CONDITIONS_NOT_WELL) {
                    $dogovor = '/mail/attach/договор_загородный_лагерь_20.doc';
                }
                elseif ($order->campShift->conditions == CampsShift::CONDITIONS_TENTS) {
                    $dogovor = '/mail/attach/договор_палатки_20.doc';
                }

                Yii::$app->mailer->compose('@app/mail/CreateOrder', ['data' => $data])
                    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                    ->setTo($order->email)
                    ->setSubject("Забронирована путёвка в лагерь \"Новое поколение\"")
//                    ->attach($_SERVER["DOCUMENT_ROOT"] . '/mail/attach/приложение_к_договору_№1_Медицинский_пакет.pdf')
                    ->attach($_SERVER["DOCUMENT_ROOT"] . '/mail/attach/свод_правил_пребывания_ребенка_в_лагере.doc')
//                    ->attach($_SERVER["DOCUMENT_ROOT"] . '/mail/attach/sogl_med_vmesh.doc')
                    ->attach($_SERVER["DOCUMENT_ROOT"] . '/mail/attach/med_pack.pdf')
                    ->attach($_SERVER["DOCUMENT_ROOT"] . $dogovor)
                    ->attachContent($render, ['fileName' => 'Счет на оплату.pdf', 'contentType' => 'application/pdf'])
                    ->send();
            }
            else {
                return '<h1> Такой брони не найдено</h1>';
            }
        }
        return $this->redirect('/camp_order/edit/' . $id);
    }

    public function actionDeletepay($id)
    {
        $pay = Pay::findOne($id);
        if ($pay != null && !$pay->sberbank_id) {
            $pay->delete();
        }
        $order = CampOrder::findOne($pay->order_id);
        $order->save();
        // return $this->redirect('/camp_order/edit/' . $pay->order_id);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDeletevoucher($id)
    {
        $voucher = Vouchers::findOne($id);
        $voucher->order->count -= 1;
        $voucher->order->save();
        $voucher->delete();
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionSavevoucher($id)
    {
        // $voucher = Vouchers::findOne($id);
        $count = 1;

        while ($count > 0) {
            $voucherNumber = $this->randomString();
            $count = Vouchers::find()->where(['voucher_number' => $voucherNumber])->count();
        }

        return $voucherNumber;
    }

    public function randomString()
    {

        $seed = str_split('abcdefghijklmnopqrstuvwxyz'
            . 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
            . '0123456789');
        shuffle($seed);
        $rand = '';
        foreach (array_rand($seed, 6) as $k) {
            $rand .= $seed[$k];
        }

        $string = str_split($rand);

        $check = false;
        foreach ($string as $val) {
            if (is_numeric($val)) {
                $check = true;
            }
        }

        if ($check) {
            return $rand;
        }
        else {
            $this->randomString();
        }
    }

    public function actionRmpay($id)
    {
        $pay = Pay::findOne($id);
        $pay->delete();
    }

    function num2str($num)
    {
        $nul = 'ноль';
        $ten = array(
            array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
            array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        );
        $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
        $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
        $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
        $unit = array( // Units
            array('копейка', 'копейки', 'копеек', 1),
            array('рубль', 'рубля', 'рублей', 0),
            array('тысяча', 'тысячи', 'тысяч', 1),
            array('миллион', 'миллиона', 'миллионов', 0),
            array('миллиард', 'милиарда', 'миллиардов', 0),
        );
        //
        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
                else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk > 1) $out[] = $this->morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
            } //foreach
        }
        else $out[] = $nul;
        $out[] = $this->morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

    /**
     * Склоняем словоформу
     * @ author runcore
     */
    function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) return $f5;
        $n = $n % 10;
        if ($n > 1 && $n < 5) return $f2;
        if ($n == 1) return $f1;
        return $f5;
    }

    function ucfirst_utf8($stri)
    {
        if ($stri{0} >= "\xc3")
            return (($stri{1} >= "\xa0") ?
                    ($stri{0} . chr(ord($stri{1}) - 32)) :
                    ($stri{0} . $stri{1})) . substr($stri, 2);
        else return ucfirst($stri);
    }
}
