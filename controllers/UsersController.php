<?php

namespace app\controllers;

use app\models\User;
use app\models\CampsShift;
use yii\helpers\ArrayHelper;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\helpers\Json;

class UsersController extends Controller
{

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }

    public function actionIndex()
    {
        $users = User::find()->all();
        $roles = User::getRolesArray();
        return $this->render('index', ['users' => $users, 'roles' => $roles]);
    }

    public function actionEdit($id)
    {
        $user = User::findOne($id);
        $roles = User::getRolesArray();
        if ($user->load(Yii::$app->request->post())) {
            if($user->new_password){
                $user->setPassword($user->password);
            }
            $user->save();
            return $this->redirect('/users/');
        }
        return $this->render('edit', ['user' => $user, 'roles' => $roles]);
    }

    public function actionCreate()
    {
        $user = new User();
        $roles = User::getRolesArray();
        if ($user->load(Yii::$app->request->post())) {
            $user->setPassword($user->password);
            $user->save();
            return $this->redirect('/users/');
        }
        return $this->render('create', ['user' => $user, 'roles' => $roles]);
    }

}