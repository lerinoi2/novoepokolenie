<?php

namespace app\controllers;

use app\models\RouteOrder;
use app\models\Routes;
use app\models\Vouchers;
use app\models\Pay;
use app\models\ShiftList;
use app\models\CampsShift;
use app\models\Camps;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Shift_listController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }

    public function actionIndex($shift = 'all', $ageform = null, $ageto = null)
    {
        $shifts = ArrayHelper::map(CampsShift::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'namewithdate');
        $vouchers = Vouchers::getVouchersForShiftList($shift, $ageform, $ageto);
        if(yii::$app->request->isAjax){
            return $this->renderPartial('IndexPartial', ['shifts' => $shifts, 'vouchers' => $vouchers]);
        }
        return $this->render('index', ['shifts' => $shifts, 'vouchers' => $vouchers]);
    }

    public function actionSetparty($voucher_id)
    {
        $voucher = Vouchers::findOne($voucher_id);
        $parties = Vouchers::getParties();
        if (intval($voucher->party) > 0) {
            $voucher->party_tmp = intval($voucher->party);
        }
        if ($voucher->load(yii::$app->request->Post())) {
            if ($voucher->party_tmp > 0) {
                $shift_list = ShiftList::find()->where(['voucher_id'=>$voucher_id])->one();
                if($shift_list != null){
                    $shift_list->party = $voucher->party_tmp;
                    $shift_list->save();
                    return $this->redirect('/shift_list/');
                }
                $shift_list = new ShiftList();
                $shift_list->camp_shift_id = $voucher->shiftid;
                $shift_list->voucher_id = $voucher->id;
                $shift_list->party = $voucher->party_tmp;
                $shift_list->save();
            }
            return $this->redirect('/shift_list/');
        }
        return $this->render('update', ['voucher' => $voucher, 'parties' => $parties]);
    }

}
