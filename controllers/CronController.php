<?php

namespace app\controllers;

use app\models\CampOrder;
use app\models\Content;
use app\models\Camps;
use app\models\CampsShift;
use app\models\Vouchers;
use app\models\Pay;
use app\models\User;
use app\models\WaitingList;
use app\models\Blacklist;
use Faker\Provider\DateTime;
use Yii;
use yii\helpers\Json;
use yii\base\Model;
use kartik\mpdf\Pdf;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\models\sendpulse\ApiClient;

class CronController extends Controller
{

    const API_USER_ID = '258f787667c73ff0130549d62ab9f32a';
    const API_SECRET = '9e0d6c275d3c53e6fcd1dee66246fa75';
    const TOKEN_STORAGE = 'file';

    const ONE_DAY = 'Осталось 1 день до конца брони';
    const THREE_DAY = 'Осталось 3 деня до конца брони';
    const FIRE_DAY = 'Осталось 5 дней до конца брони';
    const END_BOOKING = 'Бронь закончилась';

    const TEMP_ONE_DAY = 'Остался 1 день до окончания бронирования';
    const TEMP_THREE_DAY = 'Осталось 3 дня до окончания бронирования';
    const TEMP_FIRE_DAY = 'Осталось 5 дней до окончания бронирования';
    const TEMP_END_BOOKING = 'Путёвка аннулирована';

    public $SPApi = false;


    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function ApiProxy()
    {
        if (!$this->SPApi) {
            $this->SPApi = new ApiClient(self::API_USER_ID, self::API_SECRET);
        }
    }

    public function getBookId($name)
    {
        $this->ApiProxy();
        $SPbooks = $this->SPApi->listAddressBooks();
        $booksArr = array();
        foreach ($SPbooks as $book) {
            $booksArr[$book->name] = $book->id;
        }
        return $booksArr[$name];
    }

    public function getTemplateId($name)
    {
        $this->ApiProxy();
        $SPtemplates = $this->SPApi->getTemplates();
        $temp_id = false;
        foreach ($SPtemplates as $key => $template) {
            $temp_id = ($template->name == $name) ? $template->id : $temp_id;
        }
        return $temp_id;
    }

    public function sendByTemplate($temp, $book, $theme)
    {
        $this->ApiProxy();
        $temp_id = $this->getTemplateId($temp);
        $book_id = $this->getBookId($book);
        $SPtemplates = $this->SPApi->createCampaignByTemlate('Лагерь "Новое поколение"', 'office@npcamp.ru', $theme, $temp_id, $book_id);
        return $SPtemplates;
    }

    public function actionOneday()
    {
        $now = Yii::$app->formatter->asTimestamp(date("d.M.y"));
        $this->ApiProxy();
        $id = $this->getBookId(self::ONE_DAY);
        $this->SPApi->removeAddressBook($id);
        $id = $this->getBookId(self::ONE_DAY);
        $orders = CampOrder::find()->where(['active' => true, 'status' => CampOrder::STATUS_NOT_PAYED, 'entity' => CampOrder::ENTITY_FIZ])->all();
        if ($id == null) {
            $this->SPApi->createAddressBook(self::ONE_DAY);
            $id = $this->getBookId(self::ONE_DAY);
        }
        $id = $this->getBookId(self::ONE_DAY);
        $emails = array();
        foreach ($orders as $el) {
            $el->convertDate();
            $daysforend = ($el->booking_end - $now) / (60 * 60 * 24);
            if ($daysforend == 1) {
                $emails[] = array('email' => $el->email, 'variables' => array('phone' => $el->phone, 'name' => $el->name, 'order_id' => $el->id, 'create_date' => Yii::$app->formatter->asDate($el->create_date, "dd.MM.y")));
            }
        }
        $this->SPApi->addEmails($id, $emails);
        $tmp = $this->sendByTemplate(self::TEMP_ONE_DAY, self::ONE_DAY, 'Через 1 день бронирование путёвки аннулируется');
        return Json::encode($tmp);
    }

    public function actionThreeday()
    {
        $now = Yii::$app->formatter->asTimestamp(date("d.M.y"));
        $this->ApiProxy();
        $id = $this->getBookId(self::THREE_DAY);
        $this->SPApi->removeAddressBook($id);
        $id = $this->getBookId(self::THREE_DAY);
        $orders = CampOrder::find()->where(['active' => true, 'status' => CampOrder::STATUS_NOT_PAYED, 'entity' => CampOrder::ENTITY_FIZ])->all();
        if ($id == null) {
            $this->SPApi->createAddressBook(self::THREE_DAY);
            $id = $this->getBookId(self::ONE_DAY);
        }
        $id = $this->getBookId(self::THREE_DAY);
        $emails = array();
        foreach ($orders as $el) {
            $el->convertDate();
            $daysforend = ($el->booking_end - $now) / (60 * 60 * 24);
            if ($daysforend == 3) {
                $emails[] = array('email' => $el->email, 'variables' => array('phone' => $el->phone, 'name' => $el->name, 'order_id' => $el->id, 'create_date' => Yii::$app->formatter->asDate($el->create_date, "dd.MM.y")));
            }
        }
        $this->SPApi->addEmails($id, $emails);
        $tmp = $this->sendByTemplate(self::TEMP_THREE_DAY, self::THREE_DAY, 'Через 3 дня бронирование путёвки аннулируется');
        return Json::encode($tmp);
    }

    public function actionFiveday()
    {
        $now = Yii::$app->formatter->asTimestamp(date("d.M.y"));
        $this->ApiProxy();
        $id = $this->getBookId(self::FIRE_DAY);
        $this->SPApi->removeAddressBook($id);
        $id = $this->getBookId(self::FIRE_DAY);
        $orders = CampOrder::find()->where(['active' => true, 'status' => CampOrder::STATUS_NOT_PAYED, 'entity' => CampOrder::ENTITY_FIZ])->all();
        if ($id == null) {
            $this->SPApi->createAddressBook(self::FIRE_DAY);
            $id = $this->getBookId(self::ONE_DAY);
        }
        $id = $this->getBookId(self::FIRE_DAY);
        $emails = array();
        foreach ($orders as $el) {
            $el->convertDate();
            $daysforend = ($el->booking_end - $now) / (60 * 60 * 24);
            if ($daysforend == 5) {
                $emails[] = array('email' => $el->email, 'variables' => array('phone' => $el->phone, 'name' => $el->name, 'order_id' => $el->id, 'create_date' => Yii::$app->formatter->asDate($el->create_date, "dd.MM.y")));
            }
        }
        $this->SPApi->addEmails($id, $emails);
        $tmp = $this->sendByTemplate(self::TEMP_FIRE_DAY, self::FIRE_DAY, 'Через 5 дней бронирование путёвки аннулируется');
        return Json::encode($tmp);
    }

    public function actionEndbooking()
    {
        $now = Yii::$app->formatter->asTimestamp(date("d.M.y"));
        $this->ApiProxy();
        $id = $this->getBookId(self::END_BOOKING);
        $this->SPApi->removeAddressBook($id);
        $id = $this->getBookId(self::END_BOOKING);
        $orders = CampOrder::find()->where(['active' => true, 'status' => CampOrder::STATUS_NOT_PAYED])->all();
        if ($id == null) {
            $this->SPApi->createAddressBook(self::END_BOOKING);
        }
        $id = $this->getBookId(self::END_BOOKING);
        $emails = array();
        foreach ($orders as $el) {
            $el->convertDate();
            $daysforend = ($el->booking_end - $now) / (60 * 60 * 24);
            if ($daysforend <= 0) {
                $el->active = false;
                $el->save();
                if ($el->campShift->status == 1) {
                    $emails[] = array('email' => $el->email, 'variables' => array('phone' => $el->phone, 'name' => $el->name, 'order_id' => $el->id, 'create_date' => Yii::$app->formatter->asDate($el->create_date, "dd.MM.y")));
                }

                $usl = !(($now > 1518235200 && $now < 1518339660) || ($now > 1519099200 && $now < 1519203660) || ($now > 1519790400 && $now < 1519894860));
                if ($el->campShift->AllowedPlaces > 0 && $el->campShift->AllowedPlaces >= $el->count && $usl && $el->campShift->status == 1) {
                    $waitlist = WaitingList::find()
                        ->where(['camp_shift_id' => $el->camp_shift_id])
                        ->andWhere(['status' => WaitingList::STATUS_NO_ORDER])
                        ->orderBy('id')
                        ->limit($el->count)
                        ->all();
                    foreach ($waitlist as $wl) {
                        $wl->CreateOrder();
                    }
                }
            }
        }
        $this->SPApi->addEmails($id, $emails);
        $tmp = $this->sendByTemplate(self::TEMP_END_BOOKING, self::END_BOOKING, 'Закончился срок бронирования путёвки');
        return Json::encode($tmp);
    }

    public function actionCheckpays()
    {
        $pay = Pay::find()->where(['and', ['payed' => 0], ['not', ['sberbank_id' => null]]])->all();
        $datares = array();
        foreach ($pay as $el) {
            if ($el->sberbank_id) {
                $datares[] = $el;

                if ($el->way === 0) {
                    $data = array(
                        'userName' => Yii::$app->params['sberbank_login_marsh'],
                        'password' => Yii::$app->params['sberbank_password_marsh'],
                        'orderId' => $el->sberbank_id
                    );
                }
                else {
                    $data = array(
                        'userName' => Yii::$app->params['sberbank_login'],
                        'password' => Yii::$app->params['sberbank_password'],
                        'orderId' => $el->sberbank_id
                    );
                }

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => Yii::$app->params['sberbank_url'] . '/payment/rest/getOrderStatus.do',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true, //  POST
                    CURLOPT_POSTFIELDS => http_build_query($data),
                    CURLOPT_SSLVERSION => 6
                ));
                $response = curl_exec($curl);
                $response = json_decode($response, true); //   JSON
                curl_close($curl);
                $order = CampOrder::findOne($el->order_id);
                if (isset($response['OrderStatus']) && $response['OrderStatus'] == 2) {
                    $el->payed = true;
                    $el->save();
                    if ($order != null) {
                        $order->convertDate();
                        $order->status = CampOrder::STATUS_PAYED;
                        $order->save();
                        Yii::$app->mailer->compose('@app/mail/PaySuccess', ['data' => $order])
                            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                            ->setTo($order->email)
                            ->setSubject('Оплачена путёвка в лагерь "Новое поколение"')
                            ->send();
                    }
                }
            }
        }
        return Json::encode($datares);
    }

    public function actionCheckwaitlist(){
        // $time = time();
        // ОТКЛЮЧЕНИЕ ЛИСТА ОЖИДАНИЯ!!!
        $time = Yii::$app->formatter->asTimestamp(date("d.M.y"));
        if (($time > 1518235200 && $time < 1518339660) || ($time > 1519099200 && $time < 1519203660) || ($time > 1519790400 && $time < 1519894860))
        {
            echo 'Лист ожидания отключён';
            return false;
        }
        $wlist = WaitingList::find()->where(['status'=>WaitingList::STATUS_ORDER_CREATE])->all();
        foreach ($wlist as $welem){
            if(!$welem->TodayCreate){
                $order = CampOrder::findOne($welem->order_id);
                if ($order->active) {
                    if ($order->Sumpayed > 0) {
                        $welem->status = WaitingList::STATUS_ORDER_PAYED;
                    }
                }
                else {
                    $welem->status = WaitingList::STATUS_NOT_ACTIVE;
                }
                $welem->save();
                echo $welem->id;
            }
        }
    }
}