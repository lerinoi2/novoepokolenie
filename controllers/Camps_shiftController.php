<?php

namespace app\controllers;

use Yii;
use app\models\Camps;
use app\models\CampsShift;
use app\models\CampPrice;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class Camps_shiftController extends Controller
{

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }

    public function actionIndex()
    {
        $camps_shift = CampsShift::getCampsShift();
        $status = CampsShift::getStatusesArray();
        $conditions = CampsShift::getConditionsArray();
        foreach ($camps_shift as $el) {
            $el->convertDate();
        }
        return $this->render('index', ['camps_shift' => $camps_shift, 'status' => $status, 'conditions' => $conditions]);
    }

    public function actionCreate()
    {
        $model = new CampsShift();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->price) {
                $prices = new CampPrice();
                $prices->price = $model->price;
                $prices->camp_id = $model->id;
                $prices->date_start = time();
                $prices->save();
            }
            return $this->redirect(array('camps_shift/index'));
        }
        $camps = ArrayHelper::map(Camps::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'name');
        $status = $model->getStatusesArray();
        $conditions = $model->getConditionsArray();
        return $this->render('create', ['model' => $model, 'camps' => $camps, 'status' => $status, 'conditions' => $conditions]);
    }

    public function actionEdit($id)
    {
        $model = CampsShift::findOne($id);
        $model->convertDate();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(array('camps_shift/index'));
        }
        $camps = ArrayHelper::map(Camps::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'name');
        $status = $model->getStatusesArray();
        $conditions = $model->getConditionsArray();

        $price = new CampPrice();
        $priceTable = $this->renderPartial('/camp_price/index', ['prices' => CampPrice::getByCampId($model->id), 'route_id' => $id]);

        return $this->render('edit', ['model' => $model, 'camps' => $camps, 'status' => $status, 'conditions' => $conditions, 'priceTable' => $priceTable, 'price'=>$price]);
    }

    public function actionDelete($id)
    {
        $model = CampsShift::findOne($id);
        if ($model != NULL) {
            $model->delete();
            Yii::$app->getResponse()->redirect(array('camps_shift/index'));
        } else {
            throw new HttpException(404, 'Not Found');
        }
    }
}
