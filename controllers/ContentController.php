<?php

namespace app\controllers;

use app\models\Content;
use Yii;
use yii\web\Controller;

class ContentController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }

    public function actionIndex()
    {
        $contents = Content::find()->all();
        return $this->render('index', ['contents' => $contents]);
    }

    public function actionCreate()
    {
        $content = new Content();
        if($content->load(Yii::$app->request->post()) && $content->save()){
            return $this->redirect('index');
        }
        return $this->render('create', ['content' => $content]);
    }

    public function actionEdit($id)
    {
        $content = Content::findOne($id);
        if($content->load(Yii::$app->request->post()) && $content->save()){
            return $this->redirect('/content/index');
        }
        return $this->render('create', ['content' => $content]);
    }
}
