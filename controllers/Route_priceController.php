<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\RoutePrice;

class Route_priceController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }
    
    public function actionCreate()
    {
        $model = new RoutePrice();
        if ($model->load(Yii::$app->request->post())) {
            $model->date_start = Yii::$app->formatter->asTimestamp($model->date_start);
            $model->save();
        }
        return $this->renderPartial('/route_price/index', ['prices' => RoutePrice::getByRouteId($model->route_id), 'price' => $model, 'route_id' => $model->route_id]);
    }

    public function actionDelete($id){
        $model = RoutePrice::findOne($id);
        $route_id = $model->route_id;
        if ($model != NULL) {
            $model->delete();
        }
        $price = new RoutePrice();
        return $this->renderPartial('/route_price/index', ['prices' => RoutePrice::getByRouteId($route_id), 'price' => $price, 'route_id' => $route_id]);
    }
}
