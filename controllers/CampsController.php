<?php

namespace app\controllers;

use app\models\Camps;
use Yii;
use yii\web\Controller;

class CampsController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }

    public function actionIndex()
    {
        $camps = Camps::find()->all();
        return $this->render('index', ['camps' => $camps]);
    }

    public function actionCreate()
    {
        $model = new Camps();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(array('camps/index'));
        }
        $status = $model->getStatusesArray();
        return $this->render('create', ['model' => $model, 'status' => $status]);
    }

    public function actionEdit($id){
        $model = new Camps();
        $model = Camps::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(array('camps/index'));
        }
        if ($model != NULL) {
            $status = $model->getStatusesArray();
            return $this->render('edit', ['model' => $model, 'status' => $status]);
        } else {
            throw new HttpException(404, 'Not Found');
        }
    }

    public function actionDelete($id)
    {
        $model = Camps::findOne($id);
        if ($model != NULL) {
            $model->delete();
            Yii::$app->getResponse()->redirect(array('camps/index'));
        } else {
            throw new HttpException(404, 'Not Found');
        }
    }

}
