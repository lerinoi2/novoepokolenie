<?php

namespace app\controllers;

use app\models\ContractInfo;
use app\models\RouteOrder;
use app\models\Routes;
use app\models\Vouchers;
use app\models\Pay;
use app\models\Addination;
use app\models\RouteOrderSearch;

use function is_null;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use kartik\mpdf\Pdf;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Autoloader;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\SimpleType\TblWidth;
use yii\web\UploadedFile;
use app\models\UploadForm;

class Route_orderController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }

    public function actionIndex()
    {
        $searchModel = new RouteOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $routeOrder = new RouteOrder();
        if ($routeOrder->load(Yii::$app->request->post())) {
            $routeOrder->save();

            $routeOrder->file = UploadedFile::getInstance($routeOrder, 'file');
            if ($routeOrder->validate()) {
                $routeOrder->save();

                $path = '/var/www/u0292935/data/www/zakaz.novoepokolenie.com/web/data/programs_marsh/';
                $routeOrder->file->saveAs($path . $routeOrder->file);
            }

            return $this->redirect(array('route_order/index'));
        }
        $status = RouteOrder::getStatusArray();
        $entity = RouteOrder::getEntityesArray();
        $route = ArrayHelper::map(Routes::find()->where(['status' => Routes::STATUS_ACTIVE])->all(), 'id', 'name');
        return $this->render('create', ['model' => $routeOrder, 'status' => $status, 'entity' => $entity, 'route' => $route]);
    }

    public function actionEdit($id)
    {
        $routeOrder = RouteOrder::findOne($id);
        $routeOrder->convertDate();
        $routeOrder->addination = $routeOrder->getAddination();
        if ($routeOrder->load(Yii::$app->request->post())) {
            $routeOrder->addination = $_POST['RouteOrder']['addination'];
            $routeOrder->save();


            return $this->redirect('/route_order/edit/' . $id);
        }
        $status = RouteOrder::getStatusArray();
        $entity = RouteOrder::getEntityesArray();
        $route = ArrayHelper::map(Routes::find()->where(['status' => Routes::STATUS_ACTIVE])->all(), 'id', 'name');
        $contract = ContractInfo::findOne(['route_order_id' => $id]);
        if ($contract == null) {
            $contract = new ContractInfo();
        }

        return $this->render('edit', [
            'model' => $routeOrder,
            'status' => $status,
            'entity' => $entity,
            'route' => $route,
            'contract' => $contract
        ]);
    }

    public function actionPays()
    {
        $searchModel = new RouteOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('pays', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

//        $routeOrders = RouteOrder::getNotPayed();
//        foreach ($routeOrders as $el) {
//            $el->convertDate();
////            $el->route->convertDate();
//        }
//        $status = RouteOrder::getStatusArray();
//        $entity = RouteOrder::getEntityesArray();
//
//        return $this->render('pays', ['routeOrders' => $routeOrders, 'status' => $status, 'entity' => $entity]);
    }

    public function actionCreatepays($id)
    {
        $pay = new Pay();
        $routeOrder = RouteOrder::findOne($id);
        $pay->order_id = $routeOrder->id;
        $pay->way = Pay::TYPE_ROUTE;
        if ($pay->load(Yii::$app->request->post())) {
            $pay->save();
            if ($routeOrder->endsum <= $routeOrder->sumpayed) {
                $routeOrder->status = RouteOrder::STATUS_PAYED;
            }
            else {
                $routeOrder->status = RouteOrder::STATUS_PREPAYED;
            }
            $routeOrder->save(false);
            $pay = new Pay();
            $pay->order_id = $routeOrder->id;
            $pay->way = Pay::TYPE_ROUTE;
        }

        $contract = ContractInfo::findOne(['route_order_id' => $id]);

        return $this->render('payCreate', ['routeOrder' => $routeOrder, 'model' => $pay, 'contacts' => $contract]);
    }

    public function actionEditpay($id)
    {
        $pay = Pay::findOne(['order_id' => $id]);
        $routeOrder = RouteOrder::findOne($id);
        $routeOrder->convertDate();
        if (!$pay) {
            return $this->redirect('/route_order/edit/' . $id);
        }
        if ($pay->load(Yii::$app->request->post())) {
            $pay->way = 0;
            $pay->save();
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->render('payedit', ['model' => $pay, 'routeOrder' => $routeOrder, 'payTypes' => Pay::getPayTypesArray()]);
    }

    public function actionDeletepay($id)
    {
        $pay = Pay::findOne($id);
        if ($pay != null && !$pay->sberbank_id) {
            $pay->delete();
        }
        $order = RouteOrder::findOne($pay->order_id);
        $order->status = 0;
        $order->save();
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionGetepays($id)
    {
        $pays = Pay::find()->where(['order_id' => $id])->asArray()->all();
    }

    public function actionAddination()
    {
        $addination = Addination::find()->all();
        return $this->render('addination', ['addination' => $addination]);

    }

    public function actionAddinationcreate()
    {
        $addination = new Addination();
        if ($addination->load(yii::$app->request->Post()) && $addination->save()) {
            return $this->redirect('/route_order/addination');
        }
        return $this->render('update', ['model' => $addination]);
    }

    public function actionAddinationedit($id)
    {
        $addination = Addination::findOne($id);
        if ($addination->load(yii::$app->request->Post()) && $addination->save()) {
            return $this->redirect('/route_order/addination');
        }
        return $this->render('update', ['model' => $addination]);
    }

    public function actionContract($id)
    {
        $routeOrder = RouteOrder::findOne($id);
        $adding = Addination::find()->all();
        $adding2 = $routeOrder->Addination;
        $contractInfo = ContractInfo::findOne(['route_order_id' => $id]);
        if ($contractInfo == null) {
            $contractInfo = new ContractInfo();
        }
        $contractInfo->route_order_id = $id;
        if ($routeOrder && $contractInfo->load(Yii::$app->request->post()) && $contractInfo->save()) {
            $templateWord = new TemplateProcessor('dog.docx');
            $templateWord->setValue('fio', $contractInfo->fio);
            $templateWord->setValue('today', date('d.m.Y'));
            $templateWord->setValue('passport', $contractInfo->pasportdata);
            $templateWord->setValue('per', $routeOrder->DateStartContact);
            $templateWord->setValue('per2', $routeOrder->DateEndContact);
            $templateWord->setValue('sum', $contractInfo->price);
            $templateWord->setValue('sumTEXT', $routeOrder->getPriceString($contractInfo->price));
            $templateWord->setValue('school', $routeOrder->school);
            $templateWord->setValue('det', $routeOrder->count);
            $templateWord->setValue('ruk', $routeOrder->count_managers);
            $templateWord->setValue('vzr', $routeOrder->count_adults);
            $templateWord->setValue('vsego', $routeOrder->places);
            $templateWord->setValue('vozr', $routeOrder->class);
            $templateWord->setValue('start', $routeOrder->DateStartContact);
            $templateWord->setValue('end', $routeOrder->DateEndContact);
            $templateWord->setValue('t_d', $routeOrder->DateStartContact);
            $templateWord->setValue('t_t', $routeOrder->TimeStartContact);
            $templateWord->setValue('v_o', $routeOrder->DateEndContact);
            $templateWord->setValue('o_t', $routeOrder->TimeEndContact);
            $templateWord->setValue('zavt_c', $contractInfo->breakfast);
            $templateWord->setValue('obed', $contractInfo->lunch);
            $templateWord->setValue('uzh', $contractInfo->dinner);
            $templateWord->setValue('transp', $contractInfo->transport);
            $templateWord->setValue('prozh', $contractInfo->abode);
            $templateWord->setValue('rukov', $routeOrder->name . ' ' . $routeOrder->phone);
            $templateWord->setValue('stoimost', $contractInfo->price);

            $table = new Table(array('borderSize' => 1, 'borderColor' => 'black', 'width' => 100 * 50, 'unit' => 'pct', 'cellMargin' => 160));
            $style = ['size' => 8];
            $table->addRow();
            $table->addCell(2000, ['valign' => 'center'])->addTextRun(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER])->addText('Услуга', $style);
            $table->addCell(2000, ['valign' => 'center'])->addTextRun(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER])->addText('Период', $style);
            $table->addCell(2000, ['valign' => 'center'])->addTextRun(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER])->addText('Включена', $style);
            foreach ($adding as $addination) {
                $table->addRow();
                $table->addCell(2000, ['valign' => 'center'])->addTextRun(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER])->addText($addination->name, $style);
                $table->addCell(2000, ['valign' => 'center'])->addTextRun(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER])->addText($addination->period, $style);
                if (in_array($addination->id, $routeOrder->Addination, true)) {
                    $table->addCell(2000, ['valign' => 'center'])->addTextRun(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER])->addText('Да', $style);
                }
                else {
                    $table->addCell(2000, ['valign' => 'center'])->addTextRun(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER])->addText('Нет', $style);
                }
            }
            $templateWord->setComplexBlock('table', $table);

            header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
            header('Content-Disposition: attachment;filename="dog.docx"');
            header('Cache-Control: max-age=0');
            $templateWord->saveAs('php://output');
            exit;
        }
    }

    public function actionSendcontract($id)
    {
        $routeOrder = RouteOrder::findOne($id);
        $adding = Addination::find()->all();
        $contractInfo = ContractInfo::findOne(['route_order_id' => $id]);
        if ($contractInfo == null) {
            $contractInfo = new ContractInfo();
        }
        $contractInfo->route_order_id = $id;
        if ($routeOrder && $contractInfo->load(Yii::$app->request->post()) && $contractInfo->save()) {
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_STRING,
                'content' => $this->renderPartial('contractPdf', ['order' => $routeOrder, 'contract' => $contractInfo, 'adding' => $adding]),
                'marginLeft' => 0,
                'marginRight' => 0,
                'marginTop' => 0,
                'marginBottom' => 0,
                'marginHeader' => 0,
                'marginFooter' => 0,
                'cssInline' => 'ol,p,ul{text-align:justify;line-height:1.5rem}.table td{border:1px solid #000}html{font-size:10px;font-family: Georgia, "Times New Roman", Times, serif;}h1,h2{text-transform:uppercase;font-weight:700;margin-bottom:0;font-size:1.2rem}p{margin:0}table{width:100%}.table td{border-spacing:0}ol,ul{list-style:none}.tab{width:4rem}.page{width:21cm;height:29.7cm;padding:1.25cm}',
                'options' => [
                    'title' => 'Договор',
                    'subject' => 'Договор'
                ],
            ]);
            $render = $pdf->render();
            Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                ->setTo($routeOrder->email)
                ->setSubject("Ваш договор на оказание услуг \"Новое поколение\"")
                ->attachContent($render, ['fileName' => 'Договор Новое поколение.pdf', 'contentType' => 'application/pdf'])
                ->send();
        }
    }

    public function actionSavecontract($id, $stay = false)
    {
        $routeOrder = RouteOrder::findOne($id);
        $contractInfo = ContractInfo::findOne(['route_order_id' => $id]);
        if ($contractInfo == null) {
            $contractInfo = new ContractInfo();
        }
        $contractInfo->route_order_id = $id;
        if ($routeOrder && $contractInfo->load(Yii::$app->request->post()) && $contractInfo->save(false)) {
            if ($stay) {
                return $this->redirect('/route_order/edit/' . $id);
            }
            else {
                return $this->redirect('/route_order/');
            }
        }
    }

    public function actionModerate($id, $res = false)
    {
        $routeOrder = RouteOrder::findOne($id);
        $comment = Yii::$app->request->post()['comment'];
        if ($res != 'true') {
            $routeOrder->status = RouteOrder::STATUS_NOT_CONFIRM;
            $routeOrder->active = 0;

            Yii::$app->mailer->compose('@app/mail/ModerateRouteOrderFail', ['data' => $routeOrder, 'comment' => $comment])
                ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                ->setTo($routeOrder->email)
                ->setSubject("Заявка отклонена на маршрут выходного дня \"Новое поколение\"")
                ->send();
        }
        else {
            $routeOrder->status = RouteOrder::STATUS_NOT_PAYED;
            $routeOrder->convertDate();
            $date_create = date("d.m.Y", strtotime($routeOrder->booking_end_string . "-15 days"));
            $count = $order->places;
            $shift = Routes::findOne($routeOrder->route_id);
            $shift->convertDate();
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
                'format' => Pdf::FORMAT_A4,
                'destination' => Pdf::DEST_STRING,
                'content' => $this->renderPartial('/api/indexroute', ['data' => $routeOrder, 'createdate' => $date_create, 'count' => $count, 'shift' => $shift]),
                'options' => [
                    'title' => 'Счет на оплату',
                    'subject' => 'Счет на оплату'
                ],
            ]);
            $doc = $pdf->render();
            $adding = Addination::find()->all();
            $contractInfo = ContractInfo::findOne(['route_order_id' => $id]);
            if ($contractInfo == null) {
                $contractInfo = new ContractInfo();
            }
            $contractInfo->route_order_id = $id;
            $pdf2 = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_STRING,
                'content' => $this->renderPartial('contractPdf', ['order' => $routeOrder, 'contract' => $contractInfo, 'adding' => $adding]),
                'marginLeft' => 0,
                'marginRight' => 0,
                'marginTop' => 0,
                'marginBottom' => 0,
                'marginHeader' => 0,
                'marginFooter' => 0,
                'cssInline' => 'ol,p,ul{text-align:justify;line-height:1.5rem}.table td{border:1px solid #000}html{font-size:10px;font-family: Georgia, "Times New Roman", Times, serif;}h1,h2{text-transform:uppercase;font-weight:700;margin-bottom:0;font-size:1.2rem}p{margin:0}table{width:100%}.table td{border-spacing:0}ol,ul{list-style:none}.tab{width:4rem}.page{width:21cm;height:29.7cm;padding:1.25cm}',
                'options' => [
                    'title' => 'Договор',
                    'subject' => 'Договор'
                ],
            ]);
            $doc2 = $pdf2->render();

            Yii::$app->mailer->compose('@app/mail/ModerateRouteOrderSuccess', ['data' => $routeOrder])
                ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                ->setTo($routeOrder->email)
                ->setSubject("Забронирован маршрут выходного дня \"Новое поколение\"")
                ->attachContent($doc, ['fileName' => 'Счет на оплату.pdf', 'contentType' => 'application/pdf'])
                ->attachContent($doc2, ['fileName' => 'Договор Новое поколение.pdf', 'contentType' => 'application/pdf'])
                ->send();
        }
        $routeOrder->save(false);


        $this->redirect('/route_order/');
    }

    public function actionDelete($id)
    {
        $order = RouteOrder::findOne($id);
        $order->delete();
        $this->redirect('/route_order/');
    }

    public function actionGetkurs($id)
    {
        $routeOrder = RouteOrder::findOne($id);
        $routeOrder->convertDate();
        $routeOrder->addination = $routeOrder->getAddination();
        $contract = ContractInfo::findOne(['route_order_id' => $id]);
        $add = Addination::find()->asArray()->all();

        $add_price = 0;
        foreach ($add as $item) {
            foreach ($routeOrder->addination as $value) {
                if ($value == $item['id']) {
                    $add_price += $item['price'];
                }
            }
        }


        if ($id != null) {

            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_BROWSER, // DEST_DOWNLOAD
                'content' => $this->renderPartial('KursPdf', [
                        'model' => $routeOrder,
                        'contract' => $contract,
                        'add_price' => $add_price
                    ]
                ),
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                'marginLeft' => 0,
                'marginRight' => 0,
                'marginTop' => 0,
                'marginBottom' => 0,
                'marginHeader' => 0,
                'marginFooter' => 0,
                'options' => [
                    'title' => 'Курсовка',
                    'subject' => 'Курсовка'
                ],
            ]);
            return $pdf->render();
        }
    }

    public function actionGetkursemail($id)
    {
        $routeOrder = RouteOrder::findOne($id);
        $routeOrder->convertDate();
        $routeOrder->addination = $routeOrder->getAddination();
        $contract = ContractInfo::findOne(['route_order_id' => $id]);

        $add = Addination::find()->asArray()->all();

        $add_price = 0;
        foreach ($add as $item) {
            foreach ($routeOrder->addination as $value) {
                if ($value == $item['id']) {
                    $add_price += $item['price'];
                }
            }
        }

        if ($id != null) {

            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_STRING, // DEST_DOWNLOAD
                'content' => $this->renderPartial('KursPdf', [
                        'model' => $routeOrder,
                        'contract' => $contract,
                        'add_price' => $add_price
                    ]
                ),
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                'marginLeft' => 0,
                'marginRight' => 0,
                'marginTop' => 0,
                'marginBottom' => 0,
                'marginHeader' => 0,
                'marginFooter' => 0,
                'options' => [
                    'title' => 'Курсовка',
                    'subject' => 'Курсовка'
                ],
            ]);
            $render = $pdf->render();
            Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
                ->setTo($routeOrder->email)
                ->setSubject("Курсовка \"Новое поколение\"")
                ->attachContent($render, ['fileName' => 'Курсовка.pdf', 'contentType' => 'application/pdf'])
                ->send();
        }

        return $this->redirect('/route_order/edit/' . $id);
    }

    public function actionSenddocs($id)
    {
        set_time_limit(500);
        $file = 'kurs.zip';
        $zip = new \ZipArchive();
        $zip->open($file, \ZipArchive::CREATE);

        $zip->addFile('правила_пребывания_мвд_19.doc');

        //dogovor
        $routeOrder = RouteOrder::findOne($id);
        $adding = Addination::find()->all();
        $contractInfo = ContractInfo::findOne(['route_order_id' => $id]);
        if ($contractInfo == null) {
            $contractInfo = new ContractInfo();
        }
        $contractInfo->route_order_id = $id;
        if ($routeOrder) {
            $templateWord = new TemplateProcessor('dog.docx');
            $templateWord->setValue('fio', $contractInfo->fio);
            $templateWord->setValue('today', date('d.m.Y'));
            $templateWord->setValue('passport', $contractInfo->pasportdata);
            $templateWord->setValue('per', $routeOrder->DateStartContact);
            $templateWord->setValue('per2', $routeOrder->DateEndContact);
            $templateWord->setValue('sum', $contractInfo->price);
            $templateWord->setValue('sumTEXT', $routeOrder->getPriceString($contractInfo->price));
            $templateWord->setValue('school', $routeOrder->school);
            $templateWord->setValue('det', $routeOrder->count);
            $templateWord->setValue('ruk', $routeOrder->count_managers);
            $templateWord->setValue('vzr', $routeOrder->count_adults);
            $templateWord->setValue('vsego', $routeOrder->places);
            $templateWord->setValue('vozr', $routeOrder->class);
            $templateWord->setValue('start', $routeOrder->DateStartContact);
            $templateWord->setValue('end', $routeOrder->DateEndContact);
            $templateWord->setValue('t_d', $routeOrder->DateStartContact);
            $templateWord->setValue('t_t', $routeOrder->TimeStartContact);
            $templateWord->setValue('v_o', $routeOrder->DateEndContact);
            $templateWord->setValue('o_t', $routeOrder->TimeEndContact);
            $templateWord->setValue('zavt_c', $contractInfo->breakfast);
            $templateWord->setValue('obed', $contractInfo->lunch);
            $templateWord->setValue('uzh', $contractInfo->dinner);
            $templateWord->setValue('transp', $contractInfo->transport);
            $templateWord->setValue('prozh', $contractInfo->abode);
            $templateWord->setValue('rukov', $routeOrder->name . ' ' . $routeOrder->phone);
            $templateWord->setValue('stoimost', $contractInfo->price);

            $table = new Table(array('borderSize' => 1, 'borderColor' => 'black', 'width' => 100 * 50, 'unit' => 'pct', 'cellMargin' => 160));
            $style = ['size' => 8];
            $table->addRow();
            $table->addCell(2000, ['valign' => 'center'])->addTextRun(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER])->addText('Услуга', $style);
            $table->addCell(2000, ['valign' => 'center'])->addTextRun(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER])->addText('Период', $style);
            $table->addCell(2000, ['valign' => 'center'])->addTextRun(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER])->addText('Включена', $style);
            foreach ($adding as $addination) {
                $table->addRow();
                $table->addCell(2000, ['valign' => 'center'])->addTextRun(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER])->addText($addination->name, $style);
                $table->addCell(2000, ['valign' => 'center'])->addTextRun(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER])->addText($addination->period, $style);
                if (in_array($addination->id, $routeOrder->Addination, true)) {
                    $table->addCell(2000, ['valign' => 'center'])->addTextRun(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER])->addText('Да', $style);
                }
                else {
                    $table->addCell(2000, ['valign' => 'center'])->addTextRun(['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER])->addText('Нет', $style);
                }
            }
            $templateWord->setComplexBlock('table', $table);
            $templateWord->saveAs('dogovor.docx');

            $zip->addFile('dogovor.docx', 'dogovor.docx');
        }

        //programma marshruta
        $routeOrder = RouteOrder::findOne($id);
        $routeOrder->convertDate();
        $routeOrder->addination = $routeOrder->getAddination();
        $contract = ContractInfo::findOne(['route_order_id' => $id]);
        $add = Addination::find()->asArray()->all();
        $add_price = 0;
        foreach ($add as $item) {
            foreach ($routeOrder->addination as $value) {
                if ($value == $item['id']) {
                    $add_price += $item['price'];
                }
            }
        }

        if ($id != null) {

            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_STRING,
                'content' => $this->renderPartial('KursPdf', [
                        'model' => $routeOrder,
                        'contract' => $contract,
                        'add_price' => $add_price
                    ]
                ),
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                'marginLeft' => 0,
                'marginRight' => 0,
                'marginTop' => 0,
                'marginBottom' => 0,
                'marginHeader' => 0,
                'marginFooter' => 0,
                'options' => [
                    'title' => 'Курсовка',
                    'subject' => 'Курсовка'
                ],
            ]);
            $pdf = $pdf->render();

            $zip->addFromString('kurs.pdf', $pdf);
            $zip->addFile('kurs.pdf', 'kurs.pdf');
        }

        //forma zayavki
        //zayavka na dop
        //pamyatka
        $shift = Routes::findOne($routeOrder->route_id);
        $zip->addFile('/data/programs_marsh/' . $shift->filename, $shift->filename);

        $zip->close();

        Yii::$app->mailer->compose()
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
            ->setTo($routeOrder->email)
            ->setSubject("Документы маршрута \"Новое поколение\"")
            ->attach($file)
            ->send();

        unlink($file);
        unlink('dogovor.docx');

        $this->redirect('/route_order/edit/' . $id);
    }

    public function actionGetpay($id)
    {
        if ($id != null) {
            $routeOrder = RouteOrder::findOne($id);
            $routeOrder->status = RouteOrder::STATUS_NOT_PAYED;
            $routeOrder->convertDate();
            $routeOrder->addination = $routeOrder->getAddination();
            $add = Addination::find()->asArray()->all();
            $add_price = 0;
            foreach ($add as $item) {
                foreach ($routeOrder->addination as $value) {
                    if ($value === $item['id']) {
                        $add_price += $item['price'];
                    }
                }
            }
            $contract = ContractInfo::findOne(['route_order_id' => $id]);

            $date_create = date("d.m.Y", strtotime($routeOrder->booking_end_string . "-15 days"));
            $count = $routeOrder->count + $routeOrder->count_adults;

            $shift = Routes::findOne($routeOrder->route_id);
            $shift->convertDate();
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
                'format' => Pdf::FORMAT_A4,
                'destination' => Pdf::DEST_BROWSER,//DEST_DOWNLOAD DEST_BROWSER
                'content' => $this->renderPartial('/api/indexroute', [
                    'data' => $routeOrder,
                    'createdate' => $date_create,
                    'count' => $count,
                    'shift' => $shift,
                    'addprice' => $add_price,
                    'contact' => $contract
                ]),
                'options' => [
                    'title' => 'Счет на оплату',
                    'subject' => 'Счет на оплату'
                ],
            ]);
            return $pdf->render();
        }
    }

    public function actionSendpay($id)
    {
        $routeOrder = RouteOrder::findOne($id);
        $routeOrder->status = RouteOrder::STATUS_NOT_PAYED;
        $routeOrder->convertDate();
        $routeOrder->addination = $routeOrder->getAddination();
        $date_create = date("d.m.Y", strtotime($routeOrder->booking_end_string . "-15 days"));
        $count = $routeOrder->places;
        $shift = Routes::findOne($routeOrder->route_id);
        $shift->convertDate();

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'destination' => Pdf::DEST_STRING,
            'content' => $this->renderPartial('/api/indexroute', ['data' => $routeOrder, 'createdate' => $date_create, 'count' => $count, 'shift' => $shift]),
            'options' => [
                'title' => 'Счет на оплату',
                'subject' => 'Счет на оплату'
            ],
        ]);
        $doc = $pdf->render();


        Yii::$app->mailer->compose()
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['projectName']])
            ->setTo($routeOrder->email)
            ->setSubject("Счет маршрут выходного дня \"Новое поколение\"")
            ->attachContent($doc, ['fileName' => 'Счет на оплату.pdf', 'contentType' => 'application/pdf'])
            ->send();

        $this->redirect('/route_order/edit/' . $id);
    }

}
