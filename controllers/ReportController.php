<?php

namespace app\controllers;

use app\models\CampOrder;
use app\models\Camps;
use app\models\CampsShift;
use app\models\RouteOrder;
use app\models\xlsx\XLSXWriter;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;

class ReportController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }

    public function actionIndex($status = "all", $entity = "all", $shift = "all", $date_start = null, $date_end = null)
    {
        if (yii::$app->request->isAjax) {
            $date_start = yii::$app->formatter->asTimestamp($date_start);
            $date_end = yii::$app->formatter->asTimestamp($date_end);
            $orders = CampOrder::MultipleSearchAndFilter($status, $entity, $shift, $date_start, $date_end);

            $data = array();
            foreach ($orders as $key => $el) {
                $el->campShift->convertDate();
                foreach ($el->vouchers as $voucher) {
                    $voucher->convertDate();
                    if ($voucher->voucher_number) {
                        $data['countVoucher']++;
                    }
                }
                $el->convertDate();
                $data['count'] += $el->count;
                $data['sum'] += $el->sum;
                $data['remainder'] += $el->remainder;
                $data['sumpayed'] += $el->sumpayed;
                if ($el->active == true) {
                    $data['payedPlaces'] += $el->CountPlacesPayed;
                    $data['bookingPlaces'] += ($el->count - $el->CountPlacesPayed);
                    $data['allPlaces'] += $el->count;
                }
            }
            $entity = CampOrder::getEntityesArray();
            $status = CampOrder::getStatusArray();
            $conditions = CampsShift::getConditionsArray();
            return $this->renderPartial('reportTable', ['orders' => $orders, 'entity' => $entity, 'condition' => $conditions, 'status' => $status, 'data' => $data]);
        }
        $entity = CampOrder::getEntityesArray();
        // $shifts = CampsShift::findAll(['status' => Camps::STATUS_ACTIVE]);
        $shifts = CampsShift::find()->all();
        $status = CampOrder::getStatusArray();
        return $this->render('index', ['entity' => $entity, 'status' => $status, 'shifts' => $shifts]);
    }

    public function actionGenerate($status = "all", $entity = "all", $shift = "all", $date_start = null, $date_end = null)
    {
        $orders = CampOrder::MultipleSearchAndFilter($status, $entity, $shift, $date_start, $date_end);
        $entity = CampOrder::getEntityesArray();
        $camps = ArrayHelper::map(Camps::findAll(['status' => Camps::STATUS_ACTIVE]), 'id', 'name');
        $status = CampOrder::getStatusArray();
        $conditions = CampsShift::getConditionsArray();
        $header = array(
            '#' => 'integer',
            'Дата заказа' => 'date',
            'Номер заказа' => 'integer',
            'Статус' => 'string',
            'ФИО\компания' => 'string',
            'Тип' => 'string',
            'Скидка' => '0%',
            'Телефон' => 'string',
            'E-mail' => 'string',
            'Лагерь' => 'string',
            'Смена' => 'string',
            'Дата начала смены' => 'DD.MM.YYYY',
            'Дата конца смены' => 'DD.MM.YYYY',
            'Проживание' => 'string',
            'Кол-во путёвок' => 'integer',
            'ФИО ребенка' => 'string',
            'Дата рождения' => 'DD.MM.YYYY',
            'Номер путёвки' => 'string',
            'Окончание брони' => 'DD.MM.YYYY',
            'Осталось дней до окончания' => 'integer',
            'Сумма заказа' => '#,##0.00',
            'Сумма оплаты' => '#,##0.00',
            'Дата оплаты' => 'DD.MM.YYYY',
            'Осталось заплатить' => '#,##0.00',
        );
        $writer = new XLSXWriter();
        $writer->writeSheetHeader('Отчет', $header);
        $q = 1;
        foreach ($orders as $key => $el) {
            $el->convertDateForXlsx();
            $el->campShift->convertDateForXlsx();
            foreach ($el->vouchers as $voucher) {
                $voucher->convertDateForXlsx();
            }
            for ($i = 0; $i < $el->count; $i++) {
                $rows[] = array(
                    $q,
                    $el->create_date_string,
                    $el->id,
                    $el->active ? $status[$el->status] : "Аннулирован",
                    $el->name, $entity[$el->entity],
                    $el->discount / 100, $el->phone,
                    $el->email,
                    $camps[$el->campShift->camp_id],
                    $el->campShift->name,
                    $el->campShift->date_start_string,
                    $el->campShift->date_end_string,
                    $conditions[$el->campShift->conditions],
                    1,
                    $el->vouchers[$i]->name,
                    $el->vouchers[$i]->birth_string,
                    $el->vouchers[$i]->voucher_number,
                    $el->booking_end_string,
                    $el->DayForEnd,
                    $el->sum / $el->count,
                    $i < count($el->paysPayed) ? $el->paysPayed[$i]->sum : 0,
                    $i < count($el->paysPayed) ? Yii::$app->formatter->asDate($el->paysPayed[$i]->date, "dd.MM.y") : 0,
                    $el->remainder / $el->count);
                $q++;
            }
            foreach ($rows as $row) {
                $writer->writeSheetRow('Отчет', $row);
            }
            unset($rows);

        }
        $writer->writeToString();
        $now = new \DateTime('now', new \DateTimeZone('Asia/Yekaterinburg'));
        $now = yii::$app->formatter->asDate($now, "php:d.m.y_H:i:s");
        return \Yii::$app->response->sendContentAsFile($writer->writeToString(), 'Отчет от ' . $now . '.xlsx', ['mimeType' => 'application/xlsx']);
    }
}
