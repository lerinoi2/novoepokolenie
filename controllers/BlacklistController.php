<?php

namespace app\controllers;

use app\models\Blacklist;
use Yii;
use yii\base\Model;
use yii\web\Controller;

class BlacklistController extends Controller
{

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/login');
            return false;
        }
        return true;
    }

    public function actionIndex()
    {
        $blacklist = Blacklist::find()->all();
        foreach ($blacklist as $el){
            $el->convertDate();
        }

        return $this->render('index', ['blacklist' => $blacklist]);
    }

    public function actionCreate()
    {
        $blacklist = new Blacklist();
        if($blacklist->load(Yii::$app->request->post()) && $blacklist->save()){
            return $this->redirect('/blacklist');
        }
        return $this->render('create', ['blacklist' => $blacklist]);
    }

    public function actionDelete($id){
        $model = Blacklist::findOne($id);
        if ($model != NULL) {
            $model->delete();
        }
        return $this->redirect('/blacklist');
    }
}