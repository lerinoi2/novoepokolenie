<?php

use yii\db\Migration;

/**
 * Handles the creation of table `routes`.
 */
class m170912_055902_create_routes_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%routes}}', [
            'id' => $this->primaryKey(),
            'date_start'=>$this->integer(),
            'date_end'=>$this->integer(),
            'description'=>$this->string(),
            'status'=>$this->integer(),
            'price'=>$this->integer(),
            'places'=>$this->integer(),
            'link'=>$this->string(),
            'name'=>$this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%routes}}');
    }
}
