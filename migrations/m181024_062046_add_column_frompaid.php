<?php

use yii\db\Migration;

class m181024_062046_add_column_frompaid extends Migration
{
    public function safeUp()
    {
        $this->addColumn('np_pay', 'frompaid', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('np_pay', 'frompaid');
    }

}
