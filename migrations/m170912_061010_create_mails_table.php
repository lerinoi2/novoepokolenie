<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mails`.
 */
class m170912_061010_create_mails_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%mails}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
            'content'=>$this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%mails}}');
    }
}
