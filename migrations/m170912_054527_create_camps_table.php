<?php

use yii\db\Migration;

/**
 * Handles the creation of table `camps`.
 */
class m170912_054527_create_camps_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%camps}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
            'status'=>$this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%camps}}');
    }
}
