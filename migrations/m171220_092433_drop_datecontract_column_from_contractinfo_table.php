<?php

use yii\db\Migration;

/**
 * Handles dropping datecontract from table `contractinfo`.
 */
class m171220_092433_drop_datecontract_column_from_contractinfo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('contractinfo', 'datestart');
        $this->dropColumn('contractinfo', 'dateend');
        $this->dropColumn('contractinfo', 'timestart');
        $this->dropColumn('contractinfo', 'timeend');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('contractinfo', 'datestart', $this->integer());
        $this->addColumn('contractinfo', 'dateend', $this->integer());
        $this->addColumn('contractinfo', 'timestart', $this->integer());
        $this->addColumn('contractinfo', 'timeend', $this->integer());
    }
}
