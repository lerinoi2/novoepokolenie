<?php

use yii\db\Migration;

/**
 * Handles adding transport to table `contractinfo`.
 */
class m171212_095948_add_transport_column_to_contractinfo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contractinfo', 'transport', $this->string()->defaultValue(null));
        $this->addColumn('contractinfo', 'abode', $this->string()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('contractinfo', 'transport');
        $this->dropColumn('contractinfo', 'abode');
    }
}
