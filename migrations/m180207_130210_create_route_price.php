<?php

use yii\db\Migration;

class m180207_130210_create_route_price extends Migration
{
    public function up()
    {
        $this->createTable('{{%route_price}}', [
            'id' => $this->primaryKey(),
            'route_id' => $this->integer()->notNull(),
            'price'=>$this->integer()->notNull(),
            'date_start'=>$this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-route_price-route_id',
            '{{%route_price}}',
            'route_id'
        );

        $this->addForeignKey(
            'fk-route_price-route_id',
            '{{%route_price}}',
            'route_id',
            '{{%routes}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-route_price-route_id',
            '{{%route_price}}'
        );

        $this->dropIndex(
            'idx-route_price-route_id',
            '{{%route_price}}'
        );

        $this->dropTable('{{%route_price}}');
    }
}
