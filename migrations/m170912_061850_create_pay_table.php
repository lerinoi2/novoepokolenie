<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pay`.
 */
class m170912_061850_create_pay_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%pay}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'way' => $this->integer(),
            'sum' => $this->integer(),
            'date' => $this->integer(),
            'sberbank_id' => $this->string(),
            'payed' => $this->boolean()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%pay}}');
    }
}
