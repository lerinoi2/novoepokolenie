<?php

use yii\db\Migration;

class m181011_123953_modify_vouchers_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('np_vouchers', 'representative_name', $this->string());
        $this->addColumn('np_vouchers', 'representative_phone', $this->string());
        $this->addColumn('np_vouchers', 'representative_address', $this->string());

    }

    public function safeDown()
    {
        $this->dropColumn('np_vouchers', 'representative_name');
        $this->dropColumn('np_vouchers', 'representative_phone');
        $this->dropColumn('np_vouchers', 'representative_address');
    }
}
