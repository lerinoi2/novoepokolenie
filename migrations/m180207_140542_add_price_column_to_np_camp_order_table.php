<?php

use yii\db\Migration;

/**
 * Handles adding price to table `np_camp_order`.
 */
class m180207_140542_add_price_column_to_np_camp_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('np_camp_order', 'price', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('np_camp_order', 'price');
    }
}
