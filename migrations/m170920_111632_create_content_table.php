<?php

use yii\db\Migration;

/**
 * Handles the creation of table `content`.
 */
class m170920_111632_create_content_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%content}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
            'content'=>$this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%content}}');
    }
}
