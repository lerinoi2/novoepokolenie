<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contractinfo`.
 * Has foreign keys to the tables:
 *
 * - `route_order`
 */
class m171206_133605_create_contractinfo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%contractinfo}}', [
            'id' => $this->primaryKey(),
            'route_order_id' => $this->integer()->notNull(),
            'fio' => $this->string(),
            'pasportdata' => $this->string(),
            'datestart' => $this->integer(),
            'dateend' => $this->integer(),
            'timestart' => $this->integer(),
            'timeend' => $this->integer(),
        ]);

        // creates index for column `route_order_id`
        $this->createIndex(
            'idx-contractinfo-route_order_id',
            '{{%contractinfo}}',
            'route_order_id'
        );

        // add foreign key for table `route_order`
        $this->addForeignKey(
            'fk-contractinfo-route_order_id',
            '{{%contractinfo}}',
            'route_order_id',
            '{{%route_order}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `route_order`
        $this->dropForeignKey(
            'fk-contractinfo-route_order_id',
            '{{%contractinfo}}'
        );

        // drops index for column `route_order_id`
        $this->dropIndex(
            'idx-contractinfo-route_order_id',
            '{{%contractinfo}}'
        );

        $this->dropTable('{{%contractinfo}}');
    }
}
