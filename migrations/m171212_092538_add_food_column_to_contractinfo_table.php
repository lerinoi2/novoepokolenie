<?php

use yii\db\Migration;

/**
 * Handles adding food to table `contractinfo`.
 */
class m171212_092538_add_food_column_to_contractinfo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contractinfo', 'breakfast', $this->integer()->defaultValue(0));
        $this->addColumn('contractinfo', 'lunch', $this->integer()->defaultValue(0));
        $this->addColumn('contractinfo', 'dinner', $this->integer()->defaultValue(0));
        $this->addColumn('contractinfo', 'price', $this->integer()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('contractinfo', 'breakfast');
        $this->dropColumn('contractinfo', 'lunch');
        $this->dropColumn('contractinfo', 'dinner');
        $this->dropColumn('contractinfo', 'price');
    }
}
