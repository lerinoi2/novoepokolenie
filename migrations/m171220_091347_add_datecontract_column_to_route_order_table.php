<?php

use yii\db\Migration;

/**
 * Handles adding datecontract to table `route_order`.
 */
class m171220_091347_add_datecontract_column_to_route_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%route_order}}', 'date_start_contract', $this->integer());
        $this->addColumn('{{%route_order}}', 'time_start_contract', $this->integer());
        $this->addColumn('{{%route_order}}', 'date_end_contract', $this->integer());
        $this->addColumn('{{%route_order}}', 'time_end_contract', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%route_order}}', 'date_start_contract');
    }
}
