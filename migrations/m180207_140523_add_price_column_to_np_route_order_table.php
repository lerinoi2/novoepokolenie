<?php

use yii\db\Migration;

/**
 * Handles adding price to table `np_route_order`.
 */
class m180207_140523_add_price_column_to_np_route_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('np_route_order', 'price', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('np_route_order', 'price');
    }
}
