<?php

use yii\db\Migration;

/**
 * Handles the creation of table `camp_order`.
 * Has foreign keys to the tables:
 *
 * - `camps`
 * - `camps_shift`
 */
class m170920_110823_create_camp_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%camp_order}}', [
            'id' => $this->primaryKey(),
            'camp_id' => $this->integer()->notNull(),
            'camp_shift_id' => $this->integer()->notNull(),
            'booking_end'=>$this->integer(),
            'entity'=>$this->integer(),
            'name'=>$this->string(),
            'phone'=>$this->string(),
            'email'=>$this->string(),
            'discount'=>$this->string(),
	        'active' => $this->boolean(),
	        'count'=>$this->integer(),
	        'token'=>$this->string(),
	        'create_date'=>$this->integer(),
            'status'=>$this->integer()
        ]);

        // creates index for column `camp_id`
        $this->createIndex(
            'idx-camp_order-camp_id',
            '{{%camp_order}}',
            'camp_id'
        );

        // add foreign key for table `camps`
        $this->addForeignKey(
            'fk-camp_order-camp_id',
            '{{%camp_order}}',
            'camp_id',
            '{{%camps}}',
            'id',
            'CASCADE'
        );

        // creates index for column `camp_shift_id`
        $this->createIndex(
            'idx-camp_order-camp_shift_id',
            '{{%camp_order}}',
            'camp_shift_id'
        );

        // add foreign key for table `camps_shift`
        $this->addForeignKey(
            'fk-camp_order-camp_shift_id',
            '{{%camp_order}}',
            'camp_shift_id',
            '{{%camps_shift}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `camps`
        $this->dropForeignKey(
            'fk-camp_order-camp_id',
            '{{%camp_order}}'
        );

        // drops index for column `camp_id`
        $this->dropIndex(
            'idx-camp_order-camp_id',
            '{{%camp_order}}'
        );

        // drops foreign key for table `camps_shift`
        $this->dropForeignKey(
            'fk-camp_order-camp_shift_id',
            '{{%camp_order}}'
        );

        // drops index for column `camp_shift_id`
        $this->dropIndex(
            'idx-camp_order-camp_shift_id',
            '{{%camp_order}}'
        );

        $this->dropTable('{{%camp_order}}');
    }
}
