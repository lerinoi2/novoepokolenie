<?php

use yii\db\Migration;

/**
 * Handles the creation of table `camp_price`.
 */
class m180208_145003_create_camp_price_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%camp_price}}', [
            'id' => $this->primaryKey(),
            'camp_id' => $this->integer()->notNull(),
            'price'=>$this->integer()->notNull(),
            'date_start'=>$this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-camp_price-camp_id',
            '{{%camp_price}}',
            'camp_id'
        );

        $this->addForeignKey(
            'fk-camp_price-camp_id',
            '{{%camp_price}}',
            'camp_id',
            '{{%camps_shift}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-camp_price-camp_id',
            '{{%camp_price}}'
        );

        $this->dropIndex(
            'idx-camp_price-camp_id',
            '{{%camp_price}}'
        );

        $this->dropTable('{{%camp_price}}');
    }
}
