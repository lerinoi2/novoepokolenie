<?php

use yii\db\Migration;

/**
 * Handles the creation of table `route_order`.
 * Has foreign keys to the tables:
 *
 * - `routes`
 */
class m170912_061402_create_route_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%route_order}}', [
            'id' => $this->primaryKey(),
            'route_id' => $this->integer()->notNull(),
            'booking_end' => $this->integer(),
            'entity' => $this->integer(),
            'name' => $this->string(),
            'phone' => $this->string(),
            'email' => $this->string(),
            'discount' => $this->string(),
            'count' =>$this->integer(),
            'status'=>$this->integer(),
            'active'=>$this->boolean(),
            'school'=>$this->string(),
            'class'=>$this->integer(),
            'count_adults'=>$this->integer(),
            'count_managers'=>$this->integer(),
            'transport'=>$this->boolean(),
            'abode'=>$this->boolean(),
            'create_date'=>$this->integer(),
        ]);

        // creates index for column `route_id`
        $this->createIndex(
            'idx-route_order-route_id',
            '{{%route_order}}',
            'route_id'
        );

        // add foreign key for table `routes`
        $this->addForeignKey(
            'fk-route_order-route_id',
            '{{%route_order}}',
            'route_id',
            '{{%routes}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `routes`
        $this->dropForeignKey(
            'fk-route_order-route_id',
            '{{%route_order}}'
        );

        // drops index for column `route_id`
        $this->dropIndex(
            'idx-route_order-route_id',
            '{{%route_order}}'
        );

        $this->dropTable('{{%route_order}}');
    }
}
