<?php

use yii\db\Migration;

/**
 * Class m200605_130151_add_file
 */
class m200605_130151_add_file extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('np_route_order', 'file', $this->string());
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200605_130151_add_file cannot be reverted.\n";

        return false;
    }
}
