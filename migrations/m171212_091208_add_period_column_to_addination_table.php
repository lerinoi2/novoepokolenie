<?php

use yii\db\Migration;

/**
 * Handles adding period to table `addination`.
 */
class m171212_091208_add_period_column_to_addination_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%addination}}', 'period', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%addination}}', 'period');
    }
}
