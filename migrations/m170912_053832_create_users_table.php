<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170912_053832_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull(),
            'password'=>$this->string()->notNull(),
            'role'=>$this->integer()->notNull(),
            'name'=>$this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%users}}');
    }
}
