<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shift_list`.
 * Has foreign keys to the tables:
 *
 * - `camps_shift`
 */
class m171016_061832_create_shift_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%shift_list}}', [
            'id' => $this->primaryKey(),
            'camp_shift_id' => $this->integer()->notNull(),
            'voucher_id'=>$this->integer()->notNull(),
            'party'=>$this->integer()->notNull(),
        ]);

        // creates index for column `camp_shift_id`
        $this->createIndex(
            'idx-shift_list-camp_shift_id',
            '{{%shift_list}}',
            'camp_shift_id'
        );

        // add foreign key for table `camps_shift`
        $this->addForeignKey(
            'fk-shift_list-camp_shift_id',
            '{{%shift_list}}',
            'camp_shift_id',
            '{{%camps_shift}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `camps_shift`
        $this->dropForeignKey(
            'fk-shift_list-camp_shift_id',
            '{{%shift_list}}'
        );

        // drops index for column `camp_shift_id`
        $this->dropIndex(
            'idx-shift_list-camp_shift_id',
            '{{%shift_list}}'
        );

        $this->dropTable('{{%shift_list}}');
    }
}
