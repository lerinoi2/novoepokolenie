<?php

use yii\db\Migration;

/**
 * Handles the creation of table `camps_shift`.
 * Has foreign keys to the tables:
 *
 * - `camps`
 */
class m170912_055644_create_camps_shift_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%camps_shift}}', [
            'id' => $this->primaryKey(),
            'camp_id' => $this->integer()->notNull(),
            'name'=>$this->string(),
            'date_start'=>$this->integer(),
            'date_end'=>$this->integer(),
            'sort'=>$this->integer(),
            'status'=>$this->integer(),
            'conditions'=>$this->integer(),
            'price'=>$this->integer(),
            'places'=>$this->integer(),
            'description'=>$this->string(),
            'date_start_sale'=>$this->integer(),
        ]);

        // creates index for column `camp_id`
        $this->createIndex(
            'idx-camps_shift-camp_id',
            '{{%camps_shift}}',
            'camp_id'
        );

        // add foreign key for table `camps`
        $this->addForeignKey(
            'fk-camps_shift-camp_id',
            '{{%camps_shift}}',
            'camp_id',
            '{{%camps}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `camps`
        $this->dropForeignKey(
            'fk-camps_shift-camp_id',
            '{{%camps_shift}}'
        );

        // drops index for column `camp_id`
        $this->dropIndex(
            'idx-camps_shift-camp_id',
            '{{%camps_shift}}'
        );

        $this->dropTable('{{%camps_shift}}');
    }
}
