<?php

use yii\db\Migration;

/**
 * Handles the creation of table `addinationM2M`.
 */
class m171017_055958_create_addinationM2M_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%addinationM2M}}', [
            'id' => $this->primaryKey(),
            'route_order_id' => $this->integer()->notNull(),
            'addination_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%addinationM2M}}');
    }
}
