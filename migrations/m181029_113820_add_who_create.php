<?php

use yii\db\Migration;

class m181029_113820_add_who_create extends Migration
{
    public function safeUp()
    {
        $this->addColumn('np_vouchers', 'who_created', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('np_vouchers', 'who_created');
    }


}
