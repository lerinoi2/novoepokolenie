<?php

use yii\db\Migration;

class m181012_084258_modify_pays_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('np_pay', 'person', $this->string());
        $this->addColumn('np_pay', 'type', $this->integer());

    }

    public function safeDown()
    {
        $this->dropColumn('np_pay', 'person');
        $this->dropColumn('np_pay', 'type');
    }
}
