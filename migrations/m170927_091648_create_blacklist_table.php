<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blacklist`.
 */
class m170927_091648_create_blacklist_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%blacklist}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'birth' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%blacklist}}');
    }
}
