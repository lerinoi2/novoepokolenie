<?php

use yii\db\Migration;

/**
 * Handles the creation of table `waiting_list`.
 * Has foreign keys to the tables:
 *
 * - `camp_shift`
 */
class m171009_073926_create_waiting_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%waiting_list}}', [
            'id' => $this->primaryKey(),
            'camp_shift_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'send_date' => $this->integer()->defaultValue(null),
            'status' => $this->string()->notNull()->defaultValue(0),
            'order_id' => $this->integer(),
            'date_order_create' => $this->integer(),

        ]);

        // creates index for column `camp_shift_id`
        $this->createIndex(
            'idx-waiting_list-camp_shift_id',
            '{{%waiting_list}}',
            'camp_shift_id'
        );

        // add foreign key for table `camp_shift`
        $this->addForeignKey(
            'fk-waiting_list-camp_shift_id',
            '{{%waiting_list}}',
            'camp_shift_id',
            '{{%camp_shift}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `camp_shift`
        $this->dropForeignKey(
            'fk-waiting_list-camp_shift_id',
            '{{%waiting_list}}'
        );

        // drops index for column `camp_shift_id`
        $this->dropIndex(
            'idx-waiting_list-camp_shift_id',
            '{{%waiting_list}}'
        );

        $this->dropTable('{{%waiting_list}}');
    }
}
