<?php

use yii\db\Migration;

/**
 * Handles the creation of table `couchers`.
 */
class m170912_063012_create_vouchers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%vouchers}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
            'birth'=>$this->integer(),
            'order_id'=>$this->integer(),
            'way'=>$this->integer(),
            'voucher_number'=>$this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%vouchers}}');
    }
}
