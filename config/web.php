<?php

use kartik\mpdf\Pdf;

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
if (YII_ENV_DEV) {
    $db = require(__DIR__ . '/db_dev.php');
}

$config = [
    'id' => 'novoepokolenie',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'homeUrl' => '/',
    'timeZone' => 'Asia/Yerevan',
    'components' => [
        'pdf' => [
            'class' => Pdf::classname(),
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            // refer settings section for all configuration options
        ],
        'request' => [
            'baseUrl' => '',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '_mtVdFErbfQ1yPjVB0TyP23P9xC8NFzi',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.mail.ru',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username' => 'zakaz2@npcamp.ru',
                'password' => 'fZ]6VIVnu1vv',
                'port' => '465', // Port 25 is a very common port too
                'encryption' => 'ssl', // It is often used, check your provider or mail server specs
            ],
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'index/index',
                '/login' => 'auth/login',
                '/logout' => 'auth/logout',
                '/gii' => 'index.php?r=gii',

                '/routes' => 'routes/index',
                '/routes/create' => 'routes/create',
                '/routes/edit/<id:\d+>' => 'routes/edit',
                '/routes/delete/<id:\d+>' => 'routes/delete',

                '/camps' => 'camps/index',
                '/camps/create' => 'camps/create',
                '/camps/edit/<id:\d+>' => 'camps/edit',
                '/camps/delete/<id:\d+>' => 'camps/delete',
                '/camps_shift' => 'camps_shift/index',
                '/camps_shift/create' => 'camps_shift/create',
                '/camps_shift/edit/<id:\d+>' => 'camps_shift/edit',
                '/camps_shift/delete/<id:\d+>' => 'camps_shift/delete',

                '/squad' => 'squad/index',
                '/squad/home' => 'squad/home',
                '/squad/home/edit/<id:\d+>' => 'squad/homeedit',
                '/squad/home/edit/<id:\d+>/save' => 'squad/savehome',
                '/squad/home/edit/<id:\d+>/remove' => 'squad/removehome',

                '/squad/room/edit/<id:\d+>/<home_id:\d+>' => 'squad/editroom',
                '/squad/room/edit/<id:\d+>/<home_id:\d+>/save' => 'squad/saveroom',
                '/squad/room/edit/<id:\d+>/<home_id:\d+>/remove' => 'squad/removeroom',

                '/squad/edit' => 'squad/squad',
                '/squad/edit/<id:\d+>' => 'squad/squadedit',
                '/squad/edit/<id:\d+>/getrooms' => 'squad/getrooms',
                '/squad/edit/<id:\d+>/remove' => 'squad/removesquad',
                '/squad/edit/<id:\d+>/reform' => 'squad/reformsquad',
                '/squad/edit/<id:\d+>/save' => 'squad/savesquad',
                '/squad/edit/<id:\d+>/saveone' => 'squad/savesquadone',
                '/squad/edit/<id:\d+>/get' => 'squad/getsquad',
                '/squad/edit/<id:\d+>/<squad_id:\d+>/form' => 'squad/fromsquad',
                '/squad/edit/<id:\d+>/<squad_id:\d+>/formview' => 'squad/fromsquadview',

                '/squad/gender' => 'squad/setgender',
                '/squad/gender/<id:\d+>' => 'squad/setgendersquad',
                '/squad/gender/<id:\d+>/save' => 'squad/savegender',

                '/squad/edit/<id:\d+>/bunch' => 'squad/bunch',
                '/squad/edit/<id:\d+>/bunchview' => 'squad/bunchview',
                '/squad/edit/<id:\d+>/bunchview/edit/<bunch_id:\d+>' => 'squad/bunchviewedit',
                '/squad/edit/<id:\d+>/bunchview/edit/<bunch_id:\d+>/remove' => 'squad/bunchviremove',
                '/squad/edit/<id:\d+>/bunch/save' => 'squad/bunchsave',

                '/squad/view' => 'squad/view',
                '/squad/view/<id:\d+>' => 'squad/viewall',
                '/squad/view/<id:\d+>/print' => 'squad/viewallprint',
                '/squad/view/<id:\d+>/printno' => 'squad/viewallprintno',
                '/squad/view/<id:\d+>/<squad_id:\d+>' => 'squad/viewsquad',
                '/squad/view/<id:\d+>/<squad_id:\d+>/print' => 'squad/viewsquadprint',
                '/squad/view/<id:\d+>/<squad_id:\d+>/print/<gender:\d+>' => 'squad/viewsquadprintgender',

                '/camp_order' => 'camp_order/index',
                '/test' => 'camp_order/test',
                '/camp_order/create' => 'camp_order/create',
                '/camp_order/edit/<id:\d+>' => 'camp_order/edit',
                '/camp_order/delete/<id:\d+>' => 'camp_order/delete',
                '/camp_order/pays' => 'camp_order/pays',
                '/camp_order/pays/create/<id:\d+>' => 'camp_order/createpays',
                '/camp_order/voucher' => 'camp_order/voucher',
                '/camp_order/vouchertest' => 'camp_order/vouchertest',
                '/camp_order/voucher/create/<id:\d+>' => 'camp_order/vouchercreate',
                '/camp_order/sendcheck/<id:\d+>' => 'camp_order/sendcheck',
                '/camp_order/sendvouchers/<id:\d+>' => 'camp_order/sendvouchers',
                '/camp_order/sendvoucher/<id:\d+>/<num_id:\d+>' => 'camp_order/sendvoucher',
                '/camp_order/getpdf/<id:\d+>/<num_id:\d+>' => 'camp_order/getpdf',
                '/camp_order/rmpay/<id:\d+>' => 'camp_order/rmpay',


                '/camp_order/getzip/<id:\d+>' => 'camp_order/getzip',
                '/camp_order/deletepay/<id:\d+>' => 'camp_order/deletepay',
                '/camp_order/deletevoucher/<id:\d+>' => 'camp_order/deletevoucher',
                '/camp_order/voucher_save/<id:\d+>' => 'camp_order/savevoucher',
                '/camp_order/editpay/<id:\d+>' => 'camp_order/editpay',

                '/route_order' => 'route_order/index',
                '/route_order/addination' => 'route_order/addination',
                '/route_order/addination/create' => 'route_order/addinationcreate',
                '/route_order/addination/edit/<id:\d+>' => 'route_order/addinationedit',
                '/route_order/create' => 'route_order/create',
                '/route_order/edit/<id:\d+>' => 'route_order/edit',
                '/route_order/delete/<id:\d+>' => 'route_order/delete',
                '/route_order/pays' => 'route_order/pays',
                '/route_order/pays/create/<id:\d+>' => 'route_order/createpays',
                '/route_order/voucher' => 'route_order/voucher',
                '/route_order/voucher/create/<id:\d+>' => 'route_order/vouchercreate',
                '/route_order/contract/<id:\d+>' => 'route_order/contract',
                '/route_order/sendcontract/<id:\d+>' => 'route_order/sendcontract',
                '/route_order/editpay/<id:\d+>' => 'route_order/editpay',
                '/route_order/deletepay/<id:\d+>' => 'route_order/deletepay',
                [
                    'pattern' => '/route_order/savecontract/<id:\d+>/<stay>',
                    'route' => 'route_order/savecontract',
                    'defaults' => ['stay' => false],
                ],
//                '/route_order/delete/<id:\d+>' => 'route_order/delete',
                [
                    'pattern' => '/route_order/moderate/<id:\d+>/<res:(true|false)>',
                    'route' => 'route_order/moderate',
                    'defaults' => ['res' => false],
                ],

                '/route_order/getkurs/<id:\d+>' => 'route_order/getkurs',
                '/route_order/getkursemail/<id:\d+>' => 'route_order/getkursemail',

                '/route_order/senddocs/<id:\d+>' => 'route_order/senddocs',
                '/route_order/getpay/<id:\d+>' => 'route_order/getpay',
                '/route_order/sendpay/<id:\d+>' => 'route_order/sendpay',


                '/route_order/getepays/<id:\d+>' => 'route_order/getepays',



                '/route_price/delete/<id:\d+>' => '/route_price/delete',
                '/camp_price/delete/<id:\d+>' => '/camp_price/delete',

                //API
                '/api/<_a:[\w\-]+>' => 'api/<_a>',
                '/cron/<_a:[\w\-]+>' => 'cron/<_a>',

                '/blacklist/' => 'blacklist/index',
                '/blacklist/create' => 'blacklist/create',
                '/blacklist/delete/<id:\d+>' => 'blacklist/delete',
                '/waitinglist/' => 'waitlist/index',

                '/content/' => 'content/index',
                '/content/create' => 'content/create',
                '/content/edit/<id:\d+>' => 'content/edit',

                '/report/' => 'report/index',
                '/report/generate' => 'report/generate',
                '/mailing/' => 'mailing/index',
                '/mailing/sendemail' => 'mailing/sendemail',
                '/mailing/sendsms' => 'mailing/sendsms',
                '/mailing/sendbytemplate' => 'mailing/sendbytemplate',
                '/mailing/list' => 'mailing/list',
                '/mailing/stats' => 'mailing/stats',

                '/docs/' => 'docs/index',

                '/users/' => 'users/index',
                '/users/edit/<id:\d+>' => 'users/edit',
                '/users/create/' => 'users/create',

                '/shift_list/' => 'shift_list/index',
                '/shift_list/update/<voucher_id:\d+>' => 'shift_list/setparty',

            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'basePath' => '@webroot',
                    'baseUrl' => '@web',
                    'js' => [
                        'js/libs/jquery.min.js',
                        'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js',
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => null,
                    'basePath' => '@webroot',
                    'baseUrl' => '@web',
                    'css' => [
                        'css/libs/bootstrap.min.css'
                    ],
                    'js' => [
                        'js/libs/bootstrap.min.js',
                    ],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => []
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '94.51.208.194'],
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '94.51.208.194'],
    ];
}

$config['modules']['adminer'] = [
    'class' => 'app\modules\adminer\Module',
    'as access' => [
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            [
                'allow' => true,
                'roles' => ['@'],
                'matchCallback' => function () {
                    // Here you define the conditions to make sure it's
                    // only accessible by an administrator, for example.
                    $canAccess = true;
                    return $canAccess;
                }
            ],
        ],
    ],
];

return $config;
