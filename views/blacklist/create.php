<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20.09.2017
 * Time: 9:45
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <?php $form = ActiveForm::begin([
            'id' => 'form',
            'options' => ['class' => 'form-content'],
            'fieldConfig' => [
                'template' => "{label}\n{input}\n{error}"
            ],
        ]); ?>
        <div class="row">
            <div class="col-md-6">

                <?= $form->field($blacklist, 'name')->textInput()->label('ФИО') ?>

                <?= $form->field($blacklist, 'birth_string')->input('text', ['id' => 'birth_string'])->label('Дата рождения') ?>

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>

