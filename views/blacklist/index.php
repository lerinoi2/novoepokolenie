<?php
/* @var $this yii\web\View */
?>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="label">Черный лист: </h2>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-3">
                <a class="btn btn-primary" href="/blacklist/create">Добавить</a>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover">
                    <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>ФИО</th>
                        <th>Дата рождения</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? if ($blacklist != null) { ?>
                        <? foreach ($blacklist as $key => $el) { ?>
                            <tr>
                                <th scope="row"><? echo $key + 1 ?></th>
                                <td><? echo $el->name ?></td>
                                <td><? echo $el->birth_string ?></td>
                                <td><a href="/blacklist/delete/<?=$el->id?>" class="btn btn-delete">Удалить</a></td>
                            </tr>
                        <? } ?>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


