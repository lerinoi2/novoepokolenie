<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20.09.2017
 * Time: 9:45
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                    <?php $form = ActiveForm::begin([
                        'id' => 'form',
                        'options' => ['class' => 'form-content'],
                        'fieldConfig' => [
                            'template' => "{label}\n{input}\n{error}"
                        ],
                    ]); ?>
                        <?= $form->field($model, 'name')->textInput()->label('Название') ?>
                        <?= $form->field($model, 'camp_id')->dropDownList(
                            $camps
                        )->label('Лагерь') ?>
                        <?= $form->field($model, 'date_start_string')->input('text', ['id' => 'date_start'])->label('Дата начала') ?>
                        <?= $form->field($model, 'date_end_string')->input('text', ['id' => 'date_end'])->label('Дата конца') ?>
                        <?= $form->field($model, 'status')->dropDownList(
                            $status
                        )->label('Статус') ?>

                        <?= $form->field($model, 'sort')->input('number')->label('Сортировка') ?>
                        <?= $form->field($model, 'date_start_sale_string')->input('text', ['id' => 'date_start_sale'])->label('Дата начала продаж') ?>
                        <?= $form->field($model, 'conditions')->dropDownList(
                            $conditions
                        )->label('Тип лагеря') ?>

                        <?//= $form->field($model, 'price')->input('number')->label('Цена') ?>
                        <?= $form->field($model, 'places')->input('number')->label('Мест') ?>
                        <?= $form->field($model, 'description')->textarea(['id' => 'cs_text'])->label('Описание') ?>
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                    <?php ActiveForm::end(); ?>
                    </div>
                    <div class="col-md-6">
                        <h2>История цен:</h2>
                        <div class="ajaxTable">
                            <?=$priceTable?>
                        </div>

                        <?$form2 = ActiveForm::begin([
                            'id' => 'addCampPrice',
                            'action' => '/camp_price/create/' . $model->id,
                            'options' => ['class' => 'form-content','data' => ['pjax' => true]],
                            'fieldConfig' => [
                                'template' => "{input}\n{error}"
                            ],
                        ]); ?>
                        <div class="row">
                            <div class="col-md-5">
                                <?= $form2->field($price, 'date_start')->input('text',['id'=>'date_start_price']) ?>
                            </div>
                            <div class="col-md-5">
                                <?= $form2->field($price, 'price')->input('double') ?>
                            </div>
                                <?= $form2->field($price, 'camp_id')->hiddenInput(['value' => $model->id]) ?>
                            <div class="col-md-3">
                                <?= Html::submitButton('Добавить', ['class' => 'btn btn-warning addCampPrice', 'name' => 'submit-button']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end();?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
