<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20.09.2017
 * Time: 9:45
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <?php $form = ActiveForm::begin([
                    'id' => 'form',
                    'options' => ['class' => 'form-content'],
                    'fieldConfig' => [
                        'template' => "{label}\n{input}\n{error}"
                    ],
                ]); ?>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'name')->textInput()->label('Название') ?>
                        <?= $form->field($model, 'camp_id')->dropDownList(
                            $camps
                        )->label('Лагерь') ?>
                        <?= $form->field($model, 'date_start_string')->input('text', ['id' => 'date_start'])->label('Дата начала') ?>
                        <?= $form->field($model, 'date_end_string')->input('text', ['id' => 'date_end'])->label('Дата конца') ?>
                        <?= $form->field($model, 'status')->dropDownList(
                            $status
                        )->label('Статус') ?>

                        <?= $form->field($model, 'sort')->input('number')->label('Сортировка') ?>
                        <?= $form->field($model, 'date_start_sale_string')->input('text', ['id' => 'date_start_sale'])->label('Дата начала продаж') ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'conditions')->dropDownList(
                            $conditions
                        )->label('Тип лагеря') ?>
                        <?= $form->field($model, 'price')->input('float')->label('Цена') ?>
                        <?= $form->field($model, 'places')->input('number')->label('Мест') ?>
                        <?= $form->field($model, 'description')->textarea(['id' => 'cs_text'])->label('Описание') ?>
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                    </div>

                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

