<?php
/* @var $this yii\web\View */
?>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <h2 class="label">Смены: </h2>
                <a class="btn btn-info btn-block btn-lg" href="/camps_shift/create">Новая смена</a>
            </div>
            <div class="col-md-3">
                <h2 class="label">Лагеря: </h2>
                <a class="btn btn-info btn-block btn-lg" href="/camps">Список лагерей</a>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover">
                    <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>Название</th>
                        <th>Лагерь</th>
                        <th>Дата начала</th>
                        <th>Дата конца</th>
                        <th>Статус</th>
                        <th>Цена</th>
                        <th>Мест</th>
                        <th>Тип лагеря</th>
                        <th>Сортировка</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($camps_shift as $key => $el) { ?>
                        <tr>
                            <th scope="row"><? echo $key + 1 ?></th>
                            <td><? echo $el->name ?></td>
                            <td><? echo $el->camp->name ?></td>
                            <td><? echo $el->date_start_string ?></td>
                            <td><? echo $el->date_end_string ?></td>
                            <td><? echo $status[$el->status] ?></td>
                            <td><? echo $el->price ?></td>
                            <td><? echo $el->places ?></td>
                            <td><? echo $conditions[$el->conditions] ?></td>
                            <td><? echo $el->sort ?></td>
                            <td><a href="/camps_shift/edit/<? echo $el->id ?>">Изменить</a></td>
                            <!--                            <td><a href="/camps_shift/delete/-->
                            <? // echo $el->id ?><!--">Удалить</a></td>-->
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


