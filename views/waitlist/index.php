<?php
/* @var $this yii\web\View */
?>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="label">Лист ожидания: </h2>
            </div>
        </div>
        <br>
<!--        <div class="row">-->
<!--            <div class="col-md-3">-->
<!--                <a class="btn btn-primary" href="/blacklist/create">Добавить</a>-->
<!--                <br>-->
<!--            </div>-->
<!--        </div>-->

        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover">
                    <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>ФИО</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                        <th>Смена</th>
                        <th>Номер заказа</th>
                        <th>Статус</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? if ($list != null) { ?>
                        <? foreach ($list as $key => $el) { ?>
                            <tr>
                                <th scope="row"><? echo $key + 1 ?></th>
                                <td><? echo $el->name ?></td>
                                <td><? echo $el->phone ?></td>
                                <td><? echo $el->email ?></td>
                                <td><? echo $el->campShift['name'] ?></td>
                                <td><? echo $el->order_id ?></td>
                                <td><? echo $el->statusstring ?></td>
                            </tr>
                        <? } ?>
                    <? }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


