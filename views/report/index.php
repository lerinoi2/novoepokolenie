<?php
/* @var $this yii\web\View */

use yii\helpers\Json;

?>
<style>
    .custom-select {
        height: 100%;
    }
</style>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form id="report" style="margin-bottom: 10px;">
                    <div class="form-row align-items-center">
                        <div class="col-auto">
                            <label class="mr-sm-2" for="filter_status">Статус</label>
                            <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="filter_status" multiple>
                                <option value="all" selected>Все</option>
                                <? foreach ($status as $key => $el) { ?>
                                    <option value="<?= $key ?>"><?= $el ?></option>
                                <? } ?>
                                <option value="-1">Аннулированые</option>
                            </select>
                        </div>
                        <div class="col-auto">
                            <label class="mr-sm-2" for="filter_entity">Тип плательщика</label>
                            <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="filter_entity" multiple>
                                <option value="all" selected>Все</option>
                                <? foreach ($entity as $key => $el) { ?>
                                    <option value="<?= $key ?>"><?= $el ?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="col-auto" style="margin-top: 50px">
                            <label class="mr-sm-2" for="filter_shift">Смена</label>
                            <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="filter_shift" multiple style="height: 350px">
                                <option value="all" selected>Все</option>
                                <? foreach ($shifts as $key => $el) { ?>
                                    <option value="<?= $el->id ?>"><?= $el->namewithdate ?></option>
                                <? } ?>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="form-row align-items-center">
                        <div class="col-auto">
                            <div class="input-group">
                                <span class="input-group-addon">от</span>
                                <input id="date_start" type="text" class="form-control"
                                       placeholder="Дата начала периода"
                                       aria-label="Дата начала периода" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="input-group">
                                <span class="input-group-addon">до</span>
                                <input id="date_end" type="text" class="form-control" placeholder="Дата конца переода"
                                       aria-label="Дата конца переода" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-auto">
                            <button type="submit" class="btn btn-primary">Показать</button>
                        </div>
                        <div class="col-auto">
                            <a id="generate" class="btn btn-info" style="color: white;">Сгенерировать документ</a>
                        </div>
                        <div class="col-auto">
                            <button type="reset" class="btn btn-warning" style="color: white;">Сбросить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" id="content">

            </div>
        </div>
    </div>
</div>


