<?php

use app\models\RouteOrder;
use yii\helpers\Json;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RouteOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$names = RouteOrder::getAllroute();


?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
    <h1><?= Html::encode($this->title) ?></h1>
<!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'create_date',
                'value' => function ($model, $key, $index, $grid) {
                    Yii::$app->formatter->locale = 'ru-RU';
                    return Yii::$app->formatter->asDate($model->create_date);
                },
                'filter' => kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from',
                    'attribute2' => 'date_to',
                    'type' => kartik\date\DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'weekStart' => 1, //неделя начинается с понедельника
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                    ],
                ]),
            ],
            'id',
            [
                'attribute' => 'status',
                'value' => function ($model, $key, $index, $grid) {
                    $stat = RouteOrder::getStatusArray();
                    return $model->active == 1 ? $stat[$model->status] : 'Аннулирован';
                },
                'filter' => array('Не оплачен', 'Предоплата', 'Оплачен', 'Завершен', 'На модерации', 'Не подтверждён'),
            ],
            'name',
            'phone',
            'email:email',
            'discount',
            [
                'attribute' => 'entity',
                'value' => function ($model, $key, $index, $grid) {
                    $stat = RouteOrder::getEntityesArray();
                    return $stat[$model->status];
                },
                'filter' => array('Физическое лицо', 'Юридическое лицо'),
            ],
            [
                'label' => 'Название маршрута',
                'attribute' => 'route',
                'value' => function ($model, $key, $index, $grid) {
                    return $model->route->name;
                },
                'filter' => $names,
            ],
            [
                'attribute' => 'BookingEnd',
                'value' => function ($model, $key, $index, $grid) {
                    Yii::$app->formatter->locale = 'ru-RU';
                    return Yii::$app->formatter->asDate($model->BookingEnd);
                },
                'filter' => kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_f',
                    'attribute2' => 'date_t',
                    'type' => kartik\date\DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'weekStart' => 1, //неделя начинается с понедельника
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                    ],
                ]),
            ],
            [
                'attribute' => 'date_start_contract',
                'value' => function ($model, $key, $index, $grid) {
                    Yii::$app->formatter->locale = 'ru-RU';
                    return Yii::$app->formatter->asDate($model->date_start_contract);
                },
                'filter' => kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_start_1',
                    'attribute2' => 'date_start_2',
                    'type' => kartik\date\DatePicker::TYPE_RANGE,
                    'separator' => '-',
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'weekStart' => 1, //неделя начинается с понедельника
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                    ],
                ]),
            ],
            'transportstring',
            'daterange',
            'SchoolClass',
            'places',
            'count',
            'count_adults',
            'count_managers',
            [
                'attribute' => 'addinations',
                'value' => function ($model, $key, $index, $grid) {
                    return $model->addinations->name ?: '';
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{clear}',
                'buttons' => [
                    'clear' => function ($url,$model,$key) {
                        return Html::a('Изменить', "/route_order/edit/".$model->id, ['class' => 'btn btn-success']);
                    },
                ],
            ],
        ],
    ]); ?>
    </div>
</div>






