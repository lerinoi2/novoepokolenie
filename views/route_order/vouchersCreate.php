<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20.09.2017
 * Time: 9:45
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <?php $form = ActiveForm::begin([
            'id' => 'form',
            'options' => ['class' => 'form-content'],
            'fieldConfig' => [
                'template' => "{label}\n{input}\n{error}"
            ],
        ]); ?>
        <div class="row">
            <div class="col-md-6">

                <b><label for="">Название</label></b>
                <p><?= $model->name ?></p>
                <b><label for="">Телефон</label></b>
                <p><?= $model->phone ?></p>
                <b><label for="">E-mail</label></b>
                <p><?= $model->email ?></p>
                <b><label for="">Скидка</label></b>
                <p><?= $model->discount ?></p>
                <b><label for="">Тип плательщика</label></b>
                <p><?= $entity[$model->entity] ?></p>
                <b><label for="">Лагерь</label></b>
                <p><?= $camps[$model->camp_id] ?></p>
                <b><label for="">Смена</label></b>
                <p><?= $camps[$model->camp_id] ?></p>
                <b><label for="">Дата окончания бронирования</label></b>
                <p><?= $model->booking_end_string ?></p>

            </div>
        </div>
        <h2>Дети</h2>

        <div id="voucher-container">
            <? foreach ($vouchers as $key => $voucher) { ?>
                <div class="row">
                    <?= $form->field($voucher, "[$key]id")->hiddenInput()->label(false) ?>
                    <div class="col-md-4">
                        <?= $form->field($voucher, "[$key]name")->textInput(['readonly' => true])->label('Имя ребенка') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($voucher, "[$key]birth_string")->textInput(['readonly' => true])->label('День рождение') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($voucher, "[$key]voucher_number")->textInput()->label('Номер путёвки') ?>
                    </div>
                </div>
            <? } ?>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                <br><br><br>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>

