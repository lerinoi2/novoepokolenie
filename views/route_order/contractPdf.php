<div class="page">
    <table cellpadding="0">
        <tr>
            <td>
                <table>
                    <tr>
                        <td colspan="3" align="center" style="font-size:2rem">
                            <h1>Договор</h1>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Г.Пермь
                        </td>
                        <td align="center">

                        </td>
                        <td align="right">
                            <?= date('d.m.Y') ?> г.
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td align="left">
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Некоммерческое партнерство «Новое поколение» в
                                лице исполнительного директора
                                Долгих Валерия Николаевича, действующего на основании Устава, именуемое в дальнейшем
                                «Исполнитель» и
                                <b><?= $contract->fio ?></b>, паспорт <b></b><?= $contract->pasportdata ?></b>, в
                                дальнейшем «Заказчик», заключили
                                настоящий
                                договор о нижеследующем.
                            </p>
                        </td>

                    </tr>
                    <tr>
                        <td height="10px"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <h2>1. ПРЕДМЕТ ДОГОВОРА.</h2>
                        </td>

                    </tr>
                    <tr>
                        <td align="left">
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;«Исполнитель» предоставляет территорию лагеря
                                (спальные комнаты, обеденный зал,
                                посуду, мебель, постельное белье) «Заказчику» в период
                                <b><?= $order->DateStartContact ?> – <?= $order->DateEndContact ?></b>
                                года на
                                общую сумму <b><?= $contract->price ?> рублей
                                    (<?= $order->getPriceString($contract->price) ?>)</b>. «Заказчик»
                                оплачивает услуги по согласованной стоимости в соответствии с Картой заказа (Приложение
                                №1 к Договору) не позднее, чем за два дня до начала заезда.
                            </p>
                        </td>

                    </tr>
                    <tr>
                        <td height="10px"></td>
                    </tr>
                    <tr>
                        <td align="center" style="vertical-align: middle">
                            <h2>2. ОБЯЗАННОСТИ СТОРОН.</h2>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <ol style="">
                                <li>2.1. «Исполнитель» обязуется:
                                    <ol style="list-style: none">
                                        <li>
                                            2.1.1. Предоставить «Заказчику» место для отдыха и проживания
                                        </li>
                                        <li>
                                            2.1.2. Соблюдать требования санитарно-гигиенических норм и правил
                                            противопожарной безопасности;
                                        </li>
                                        <li>
                                            2.1.3. Обеспечить заказчика питанием согласно карте заказа «Заказчик»
                                            обязуется:
                                        </li>
                                        <li>
                                            2.2.1. Своевременно оплатить работы по цене, определенной настоящим
                                            договором, путем перечисления денежных средств на расчетный
                                            счет «Исполнителя» или внесением в кассу;
                                        </li>
                                        <li>
                                            2.2.2. Соблюдать правила пребывания отдыхающих на территории лагеря
                                        </li>
                                        <li>
                                            2.2.3. По окончании заезда сдать арендуемые объекты: спальные комнаты,
                                            белье, спортивный инвентарь завхозу.
                                        </li>
                                        <li>
                                            2.2.4 Возместить затраты на восстановление мебели, приобретение посуды,
                                            инвентаря в случае утери или порчи таковых.
                                        </li>
                                    </ol>
                                </li>

                            </ol>
                            </p>
                        </td>

                    </tr>
                    <tr>
                        <td height="10px"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <h2>3. ОТВЕТСТВЕННОСТЬ СТОРОН.</h2>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <ol>
                                <li>
                                    3.1. Заказчик в полной мере несет ответственность за жизнь и здоровье своей группы
                                    во время пребывания по путевке на территории
                                    лагеря и во время доставки до лагеря и обратно.
                                </li>
                                <li>
                                    3.2. Исполнитель в полной мере несет ответственность за качественное оказание услуг
                                    по настоящему договору согласно действующего
                                    законодательства РФ (в случае если таковые услуги были заказаны и оплачены в
                                    установленном порядке).
                                </li>
                                <li>
                                    3.3. Стороны несут ответственность за неисполнение или ненадлежащее исполнение
                                    обязательств по настоящему договору в соответствии
                                    с действующим законодательством.
                                </li>


                                <li>
                                    3.4. Изменения и дополнения в настоящий договор вносятся по соглашению сторон в
                                    письменном виде.
                                </li>
                                <li>
                                    3.5. Все споры по настоящему договору разрешаются сторонами путем переговоров, а при
                                    невозможности достижения согласия передаются
                                    на рассмотрение арбитражного суда.
                                </li>
                                <li>
                                    3.6. «Исполнитель» создает условия и не препятствует осуществлению «Заказчиком»
                                    контроля за качеством обслуживания.
                                </li>
                                <li>
                                    3.7. Стороны не имеют права изменять сроки оказания услуг без предварительного
                                    согласования.
                                </li>
                                <li>
                                    3.8. Стороны освобождаются от ответственности за полное или частичное неисполнение
                                    обязательств по настоящему договору, если
                                    такое неисполнение явилось следствием возникновения обстоятельств непреодолимой
                                    силы (пожара, наводнения, землетрясения, резкого изменения законодательства,
                                    военных действий, народных волнений и т. п.) и если эти обстоятельства
                                    непосредственно
                                    повлияли на исполнение настоящего Договора.
                                </li>
                                <li>
                                    3.9. Меры ответственности сторон, не предусмотренные настоящим Договором,
                                    применяются в соответствии с нормами гражданского
                                    законодательства, действующего на территории Российской Федерации.
                                </li>
                            </ol>
                            </p>
                        </td>

                    </tr>
                    <tr>
                        <td height="10px"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <h2>4. СРОК ДЕЙСТВИЯ ДОГОВОРА.</h2>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <ol>
                                <li>
                                    4.1. Настоящий договор вступает в силу с момента подписания и действует до
                                    исполнения сторонами обязательств.
                                </li>
                                <li>
                                    4.2. Договор составлен в двух экземплярах, имеющих одинаковую юридическую силу и
                                    находятся по одному экземпляру у каждой
                                    из сторон.
                                </li>
                            </ol>
                            </p>
                        </td>

                    </tr>

                    <tr>
                        <td align="center">
                            <h2>5. ЮРИДИЧЕСКИЕ АДРЕСА И РЕКВИЗИТЫ СТОРОН.</h2>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <p style="text-align: center;">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <table class="table" cellspacing="0" cellpadding="5"
                                       style="border:solid 1px black; width: 100%;">
                                    <tr style="width: 100%;">
                                        <td style="width:50%;">Исполнитель</td>
                                        <td style="width:50%;">Заказчик</td>
                                    </tr>
                                    <tr>
                                        <td style="border-bottom:none;">
                            <p style="text-align: justify;  ">
                                <strong>
                                    <span style="">Некоммерческое партнерство</span>
                                </strong>
                            </p>
                            <p style="text-align: justify;  ">
                                <strong>
                                    <span style="">&nbsp;«Новое поколение»,</span>
                                </strong>
                            </p>
                            <p style=" ">
                                <span style="">г. Пермь, бульвар Гагарина, 44а т/ф 282-60-00</span>
                            </p>
                            <p style="text-align: justify;  ">
                                <span style="">ИНН 5904089876 КПП 590401001</span>
                            </p>
                            <p style="text-align: justify;  ">
                                <span style="">&nbsp;</span>
                            </p>
                            <p style="text-align: justify;  ">
                                <span style="">исполнительный директор</span>
                            </p>
                            <p style="text-align: justify;  ">
                                <span style="">&nbsp;В. Н. Долгих _______________________________</span>
                            </p>
                            <p style="text-align: justify;  ">
                                <span style="">&nbsp;</span>
                            </p>
                        </td>
                        <td style="border-bottom:none;">
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="border-top:none;">
                            мп
                        </td>
                        <td style="border-top:none;">
                            мп
                        </td>
                    </tr>
                </table>
                </p>
            </td>

        </tr>
    </table>
    </td>
    </tr>
    </table>
</div>


<div class="page">
    <table style="padding: 0; width: 100%;">
        <tr>
            <td align="left">

            </td>
            <td align="center">

            </td>
            <td align="right">
                Приложение №1
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" style="">
                <h1>Карта заказа</h1>
                <br/> путевка по маршруту выходного дня
            </td>
        </tr>
    </table>
    <table class="table" cellspacing="0" cellpadding="5"
           style="border:solid 1px black; width: 100%; font-size: 11px;">
        <tr>
            <td style="width:45%;">Группа
                <br/> (школа, ВУЗ, другое)
            </td>
            <td style="width:55%;" align="center">
                <b><?= $order->school ?></b>
            </td>
        </tr>
        <tr>
            <td>
                Количество отдыхающих
            </td>
            <td align="center">
                <b>Дети: <?= $order->count ?> чел., Рук. – <?= $order->count_managers ?> чел., взр.
                    – <?= $order->count_adults ?> Всего – <?= $order->places ?> чел.</b>

            </td>

        </tr>
        <tr>
            <td>
                Возраст, класс
            </td>
            <td align="center">
                <b><?= $order->class ?></b>
            </td>
        </tr>
        <tr>
            <td>
                Сроки заезда
            </td>
            <td align="center">
                <b><?= $order->DateStartContact ?> – <?= $order->DateEndContact ?></b>
            </td>
        </tr>
        <tr>
            <td>
                Время заезда
            </td>
            <td align="center">
                <b>
                    <b><?= $order->DateStartContact ?> – c <?= $order->TimeStartContact ?></b>
                </b>
            </td>
        </tr>
        <tr>
            <td>
                Время отъезда
            </td>
            <td align="center">
                <b><?= $order->DateEndContact ?> – до <?= $order->TimeEndContact ?></b>
            </td>
        </tr>
        <tr>
            <td>
                Питание (время)
            </td>
            <td align="center">
                <b>завтрак – <?= $contract->breakfast ?> чел., обед – <?= $contract->lunch ?> чел., ужин
                    – <?= $contract->dinner ?>
                    чел.,</b>
            </td>
        </tr>
        <tr>
            <td>
                Транспорт (доставка)
            </td>
            <td align="center">
                <b><?= $contract->transport ?></b>
            </td>
        </tr>
        <tr>
            <td>
                Проживание, размещение (корпус и № комнат)

            </td>
            <td align="center">
                <b><?= $contract->abode ?>
                </b>
            </td>
        </tr>
        <tr>
            <td>
                С правилами пребывания на территории лагеря ознакомлен (подпись и расшифровка)
            </td>
            <td align="center">
                <b></b>
            </td>
        </tr>
        <tr>
            <td>
                Фамилия и имя руководителя группы
            </td>
            <td align="center">
                <b><?= $order->name . ' ' . $order->phone ?></b>
            </td>
        </tr>
        <tr>
            <td>
                Стоимость заказа
            </td>
            <td align="center">
                <b><?= $contract->price ?></b>
            </td>
        </tr>
        <tr>
            <td>
                Заказ принял (подпись работника)
            </td>
            <td align="center">
                <b></b>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td colspan="3" align="center" style="">
                Дополнительные услуги
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </table>
    <table class="table" cellspacing="0" cellpadding="5"
           style="border:solid 1px black; width: 100%; font-size: 9px;">
        <tr>
            <td style="width:45%;" align="center">Услуга
                <br/>
            </td>
            <td style="width:35%;" align="center">
                Период
            </td>
            <td style="width:20%;" align="center">
                Включена
            </td>
        </tr>
        <? foreach ($adding as $addination) { ?>
            <tr>
                <td>
                    <?= $addination->name ?>
                </td>
                <td>
                    <?= $addination->period ?>
                </td>
                <td align="center">
                    <? if (in_array($addination->id, $order->Addination)) { ?>
                        Да
                    <? } else { ?>
                        Нет
                    <? } ?>
                </td>
            </tr>
        <? } ?>
    </table>

</div>