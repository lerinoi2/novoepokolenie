<?php
/* @var $this yii\web\View */
?>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="label">Оплаченные заказы: </h2>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover">
                    <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>Название</th>
                        <th>Телефон</th>
                        <th>Скидка</th>
                        <th>Конец бронирования</th>
                        <th>Лагерь</th>
                        <th>Смена</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? if ($data != null) { ?>
                        <? foreach ($data as $key => $campOrder) { ?>
                            <tr>
                                <th scope="row"><? echo $key + 1 ?></th>
                                <td><? echo $campOrder->name ?></td>
                                <td><? echo $campOrder->phone ?></td>
                                <td><? echo $campOrder->discount ?></td>
                                <td><? echo $campOrder->booking_end_string ?></td>
                                <td><? echo $campOrder->camp->name ?></td>
                                <td><? echo $campOrder->campShift->name ?></td>
                                <td><a href="/camp_order/voucher/create/<? echo $campOrder->id ?>">Выписать путёвку</a></td>
                            </tr>
                        <? } ?>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


