<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20.09.2017
 * Time: 9:45
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <?php $form = ActiveForm::begin([
                    'id' => 'form',
                    'options' => ['class' => 'form-content'],
                    'fieldConfig' => [
                        'template' => "{label}\n{input}\n{error}"
                    ],
                ]); ?>
                <b><label for="">Номер заказа</label></b>
                <p><?= $routeOrder->id ?></p>

                <?= $form->field($model, 'order_id')->hiddenInput(['value' => $routeOrder->id])->label(false); ?>
                <div class="form-group">
                    <label class="control-label" for="booking_end">Название заказа</label>
                    <input type="text" id="count_voucher" class="form-control" value="<?= $routeOrder->name ?>"
                           readonly>
                </div>

                <?= $form->field($model, 'sum')->input('double')->label('Сумма') ?>
                <?= $form->field($model, 'date_string')->textInput()->label('Дата оплаты') ?>
                <?= $form->field($model, 'person')->textInput()->label('Кем оплачено (ФИО)') ?>
                <!--                --><? //= $form->field($model, 'frompaid')->CheckBoxList($listNames)->label('За кого оплачено') ?>
                <?= $form->field($model, 'type')->dropDownList($payTypes)->label('Способ оплаты') ?>

                <div class="formSubmit">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-md-7">
                <span><b>Сумма заказа: </b><?= $routeOrder->sum ?> руб.</span>
                <h2>История платежей:</h2>
                <?
                $i = 1 ?>
                <? if (count($routeOrder->pays) > 0) { ?>
                    <table class="table table-bordered" style="font-size: 14px" valign="middle">
                        <thead class="thead-inverse">
                        <tr>
                            <th>Номер платежа</th>
                            <th>Дата платежа</th>
                            <th>Сумма</th>
                            <th>Способ оплаты</th>
                            <th>Платеж прошел</th>
                            <th>Кем оплачено</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($routeOrder->pays as $el) { ?>
                            <tr>
                                <td><?= $el->id ?></td>
                                <td><?= Yii::$app->formatter->asDate($el->date, "dd.MM.y"); ?></td>
                                <td><?= $el->sum . ' руб.' ?></td>
                                <td><?= $el->sberbank_id ? 'Сбербанк онлайн' : $el->payType ?></td>
                                <td><?
                                    if ($el->sberbank_id && $el->payed) {
                                        echo 'Да';
                                    }
                                    elseif (!$el->sberbank_id && $el->sum > 0) {
                                        echo 'Да';
                                    }
                                    else {
                                        echo 'Нет';
                                    } ?>
                                </td>
                                <td><?= $el->sberbank_id ? $routeOrder->name : $el->person ?></td>
                                <td><a href="/route_order/deletepay/<?= $el->id ?>" class="btn btn-danger">Удалить</a></td>
                            </tr>
                            <? $i++;
                        } ?>
                        </tbody>
                    </table>
                    <b>Итого: </b> <?= $routeOrder->sumpayed . ' руб.' ?>
                    <br>
                    <b>Осталось заплатить: </b> <?= $routeOrder->remainder . ' руб.' ?>
                    <br>
                    <b>Оплачено путёвок: </b> <?= $routeOrder->countplacespayed . ' из ' . $routeOrder->count ?>
                <? } else { ?>
                    <b>Платежей не было.</b>
                <? } ?>
            </div>
        </div>
    </div>
</div>

