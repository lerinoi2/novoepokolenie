<?
function num2str($num) {
    $nul='ноль';
    $ten=array(
        array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
        array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
    );
    $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
    $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
    $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
    $unit=array( // Units
        array('копейка' ,'копейки' ,'копеек',	 1),
        array('рубль'   ,'рубля'   ,'рублей'    ,0),
        array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
        array('миллион' ,'миллиона','миллионов' ,0),
        array('миллиард','милиарда','миллиардов',0),
    );

    list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
    $out = array();
    if (intval($rub)>0) {
        foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
            if (!intval($v)) continue;
            $uk = sizeof($unit)-$uk-1; // unit key
            $gender = $unit[$uk][3];
            list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
            // mega-logic
            $out[] = $hundred[$i1]; # 1xx-9xx
            if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
            else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
            // units without rub & kop
            if ($uk>1) $out[]= morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
        } //foreach
    }
    else $out[] = $nul;
    $out[] = morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
    $out[] = $kop.' '.morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
    return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
}

/**
 * Склоняем словоформу
 * @ author runcore
 */
function morph($n, $f1, $f2, $f5) {
    $n = abs(intval($n)) % 100;
    if ($n>10 && $n<20) return $f5;
    $n = $n % 10;
    if ($n>1 && $n<5) return $f2;
    if ($n==1) return $f1;
    return $f5;
}
?>

<?
$sum_pays = 0;
foreach ($model->pays as $pay) {
    $sum_pays+=$pay['sum'];
}
?>

<img src="http://zakaz.novoepokolenie.com/web/data/kurs.jpg" alt="">
<table style="margin-top: -1115px; width: 750px;font-weight: bold;font-size: 14px;" border="0">
    <tr style="height: 10px"></tr>
    <tr>
        <td valign="middle" style="padding-top: 495px;text-align: right;width: 160px;">
            <?= $model->id  ?>
        </td>
        <td valign="middle" style="padding-top: 495px;width: 170px;">
        </td>
        <td valign="middle" style="padding-top: 495px;width: 180px;">
            <?= Yii::$app->formatter->asDate($model->date_start_contract , "dd.MM") ?>
        </td>
        <td valign="middle" style="padding-top: 495px;width: 180px;">
            <?=Yii::$app->formatter->asDate($model->date_end_contract , "dd.MM")  ?>
        </td>
        <td valign="middle" style="padding-top: 495px;">
            <?=substr(Yii::$app->formatter->asDate($model->date_end_contract , "y"),-2)  ?>
        </td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="font-size: 14px;margin-left: 10px">
    <tr>
        <td width="365px" height="33px" align="center" valign="middle"><?= $model->count + $model->count_adults + $model->count_managers ?></td>
        <td width="300px" height="33px" align="center" valign="middle"><?= $contract->abode ?></td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="font-size: 14px;margin-top: -3px;margin-left: 20px">
    <tr>
        <td width="300px" height="10px" align="center" valign="middle"><?= $model->school ?></td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="font-size: 14px;margin-left: 150px">
    <tr>
        <td width="300px" height="28px" align="center" valign="middle"><?= $model->name ?></td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="font-size: 14px;margin-left: 170px">
    <tr>
        <td width="500px" height="18px" align="center" valign="middle"><?= num2str($contract->price) ?></td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="font-size: 14px;margin-left: 200px;">
    <tr>
        <td width="500px" height="30px" align="left" valign="middle"><?= $model->sum ?> руб.</td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="font-size: 14px;margin-left: 240px;margin-top: -3px">
    <tr>
        <td width="500px" height="20px" align="left" valign="middle"><?= $add_price ?> руб.</td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="font-size: 14px;margin-left: 80px">
    <tr>
        <td width="500px" height="18px" align="left" valign="middle"><?= $sum_pays ?> руб.</td>
    </tr>
</table>
