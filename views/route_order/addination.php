<?php
/* @var $this yii\web\View */
?>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <h2 class="label">Дополнительные услуги: </h2>
                <a class="btn btn-info btn-block btn-lg" href="/route_order/addination/create">Новая услуга</a>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover">
                    <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>Название</th>
                        <th>Цена</th>
                        <th>Период</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($addination as $key => $camp) { ?>
                        <tr>
                            <th scope="row"><? echo $key + 1 ?></th>
                            <td><? echo $camp->name ?></td>
                            <td><? echo $camp->price ?></td>
                            <td><? echo $camp->period ?></td>
                            <td><a href="/route_order/addination/edit/<? echo $camp->id ?>">Изменить</a></td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


