<?php
/* @var $this yii\web\View */

use yii\helpers\Json;
use app\models\RouteOrder;

?>
<!-- <style>
    .table td, .table th {
        vertical-align: middle;
    }
</style> -->
<table class="table table-bordered table-striped" valign="middle">
    <thead class="thead-inverse">
    <tr>
        <th data-col="r1">#</th>
        <th data-col="r26" style="min-width: 230px;">Дата заказа</th>
        <th data-col="r25">Номер заказа</th>
        <th data-col="r2" style="min-width: 280px;"><label for="sort_cd">Заказчик</label><input
                    type="checkbox" id="sort_cd" style="display: none;"></th>
        <th data-col="r3" style="min-width: 200px;"><label for="sort_nz">Телефон</label><input
                    type="checkbox" id="sort_nz" style="display: none;"></th>
        <th data-col="r4" style="min-width: 200px;"><label for="sort_status">E-mail</label><input
                    type="checkbox" id="sort_status" style="display: none;"></th>
        <th data-col="r5" style="min-width: 120px;"><label for="sort_name">Скидка (%)</label><input
                    type="checkbox" id="sort_name" style="display: none;"></th>
        <th data-col="r6" style="min-width: 200px;"><label for="sort_type">Тип плательщика</label><input
                    type="checkbox" id="sort_type" style="display: none;"></th>
        <th data-col="r7" style="min-width: 500px;"><label for="sort_discount">Маршрут</label><input
                    type="checkbox" id="sort_discount" style="display: none;"></th>
        <th data-col="r8" style="min-width: 110px;"><label for="sort_discount">Дата окончания
                брони</label><input
                    type="checkbox" id="sort_discount" style="display: none;"></th>
        <th data-col="r9" style="min-width: 150px;"><label for="sort_discount">Транспорт</label><input
                    type="checkbox" id="sort_discount" style="display: none;"></th>
        <th data-col="r10" style="min-width: 200px;"><label for="sort_camp">Дата
                заезда-выезда</label><input
                    type="checkbox" id="sort_camp" style="display: none;"></th>
        <th data-col="r11" style="min-width: 250px;"><label for="sort_datestartshift">Школа -
                класс</label><input
                    type="checkbox" id="sort_datestartshift" style="display: none;"></th>
        <th data-col="r12" style="min-width: 100px;"><label for="sort_dateendshift">Количество
                (общее)</label><input
                    type="checkbox" id="sort_dateendshift" style="display: none;"></th>
        <th data-col="r13" style="min-width: 100px;"><label for="sort_count">Дети</label><input
                    type="checkbox" id="sort_count" style="display: none;"></th>
        <th data-col="r14" style="min-width: 100px;"><label for="sort_childname">Взрослые</label><input
                    type="checkbox" id="sort_childname" style="display: none;"></th>
        <th data-col="r15" style="min-width: 100px;"><label for="sort_birth">Руководитель</label><input
                    type="checkbox" id="sort_birth" style="display: none;"></th>
        <th data-col="r16" style="min-width: 400px;"><label for="sort_vouchernumb">Доп.
                услуги</label><input
                    type="checkbox" id="sort_vouchernumb" style="display: none;"></th>
        <th data-col="r17" style="min-width: 150px;"><label for="sort_endbooking">Сумма
                заказа</label><input
                    type="checkbox" id="sort_endbooking" style="display: none;"></th>
        <th data-col="r18" style="min-width: 150px;"><label for="sort_endbooking">Сумма
                оплаты</label><input
                    type="checkbox" id="sort_endbooking" style="display: none;"></th>
        <th data-col="r19" style="min-width: 110px;"><label for="sort_endday">Дата оплаты</label><input
                    type="checkbox" id="sort_endday" style="display: none;"></th>
        <th data-col="r20" style="min-width: 150px;"><label for="sort_sum">Осталось
                заплатить</label><input
                    type="checkbox" id="sort_sum" style="display: none;"></th>
        <th data-col="r21" style="min-width: 200px;"><label for="sort_sumpayed">Статус
                заказа</label><input
                    type="checkbox" id="sort_sumpayed" style="display: none;"></th>
        <th data-col="r22" style="min-width: 200px;">Комментарий</th>
        <!-- <th data-col="r23" style="min-width: 180px;"></th> -->
        <th data-col="r23"></th>
    </tr>
    </thead>
    <tbody>
    <? if (count($routeOrders) > 0) { $time = time();?>
        <? foreach ($routeOrders as $key => $order) {
            if ($order->status == RouteOrder::STATUS_MODERATE) {
                $class = '';
                $responseTime = 86400 - ($time - $order->create_date);
                if ($responseTime <= 0) {
                    $class = 'table-danger';
                } else {
                    $class = 'table-warning';
                }
            }?>
            <tr data-id="<?= $key ?>">
                <input name="toggle" id="<?= $key ?>" type="checkbox">
                <td data-col="r1"><b><? echo $key + 1 ?></b></td>
                <td data-col="r26"><? echo $order->createdate ?></td>
                <td data-col="r25"><? echo $order->id ?></td>
                <td data-col="r2"><? echo $order->name ?></td>
                <td data-col="r3"><? echo $order->phone ?></td>
                <td data-col="r4"><? echo $order->email ?></td>
                <td data-col="r5"><? echo $order->discount ?></td>
                <td data-col="r6"><? echo $entity[$order->entity] ?></td>
                <td data-col="r7"><? echo $order->route->name ?></td>
                <td data-col="r8"><? echo $order->BookingEnd ?></td>
                <td data-col="r9"><? echo $order->transportstring ?></td>
                <td data-col="r10"><? echo $order->daterange ?></td>
                <td data-col="r11"><? echo $order->SchoolClass ?></td>
                <td data-col="r12"><? echo $order->places ?></td>
                <td data-col="r13"><? echo $order->count ?></td>
                <td data-col="r14"><? echo $order->count_adults ?></td>
                <td data-col="r15"><? echo $order->count_managers ?></td>
                <td data-col="r16" style="padding: 0;">
                    <table>
                        <tr>
                            <td>
                                <? if (count($order->addinations) > 0) { ?>
                                    <ol style="padding-left: 15px">
                                        <? foreach ($order->addinations as $addination) { ?>
                                            <li><? echo $addination->name ?></li>
                                        <? } ?>
                                    </ol>
                                <? } ?>
                            </td>

                        </tr>
                    </table>
                </td>
                <td data-col="r17"><? echo $order["contract"]->price ?></td>
                <td data-col="r18" <? echo count($order->pays) > 2 ? "class=\"lurking\"" : '' ?>
                    style="padding: 0;">
                    <table style="width: 100%;">
                        <? foreach ($order->pays as $pay) { ?>
                            <tr>
                                <td><? echo $pay->sum ?></td>
                            </tr>
                        <? } ?>
                    </table>
                </td>
                <td data-col="r19" <? echo count($order->pays) > 2 ? "class=\"lurking\"" : '' ?>
                    style="padding: 0;">
                    <table style="width: 100%;">
                        <? foreach ($order->pays as $pay) { ?>
                            <tr>
                                <td><? echo $pay->DateString ?></td>
                            </tr>
                        <? } ?>
                    </table>
                </td>
                <td data-col="r20"><? echo $order->remainder ?></td>
                <td data-col="r21"><? echo $order->active == 1 ? $status[$order->status] : 'Аннулирован' ?></td>
                <td data-col="r22"><? echo 'Комментарий' ?></td>
                <td data-col="r23"><a class="btn btn-info"
                                      href="/route_order/edit/<? echo $order->id ?>">Изменить</a>
                </td>
            </tr>
        <? } ?>
    <? } ?>
    </tbody>
</table>