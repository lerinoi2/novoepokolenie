<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20.09.2017
 * Time: 9:45
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            <span><b>Сумма заказа: </b><?= $model->sum ?> руб.</span>
            <h2>История платежей:</h2>
            <?
            $i = 1 ?>
            <? if (count($model->pays) > 0) { ?>
                <table class="table table-bordered" style="font-size: 14px" valign="middle">
                    <thead class="thead-inverse">
                    <tr>
                        <th>Номер платежа</th>
                        <th>Дата платежа</th>
                        <th>Сумма</th>
                        <th>Способ оплаты</th>
                        <th>Платеж прошел</th>
                        <th>Кем оплачено</th>
                        <th>За кого оплачено</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($model->pays as $el) { ?>
                        <tr>
                            <td><?= $el->id ?></td>
                            <td><?= Yii::$app->formatter->asDate($el->date, "dd.MM.y"); ?></td>
                            <td><?= $el->sum . ' руб.' ?></td>
                            <td><?= $el->sberbank_id ? 'Сбербанк онлайн' : $el->payType ?></td>
                            <td><?
                                if ($el->sberbank_id && $el->payed) {
                                    echo 'Да';
                                }
                                elseif (!$el->sberbank_id && $el->sum > 0) {
                                    echo 'Да';
                                }
                                else {
                                    echo 'Нет';
                                } ?>
                            </td>
                            <td><?= $el->sberbank_id ? $model->name : $el->person ?></td>
                            <td><?= $el->frompaid ?></td>
                            <td>
                                <a href="/route_order/editpay/<?= $model->id ?>" class="btn btn-success">Изменить</a>
                                    <br>
                                    <a href="/route_order/deletepay/<?= $el->id ?>" class="btn btn-danger rmpay"
                                       style="margin-top: 20px">Удалить</a>
                            </td>
                        </tr>
                        <? $i++;
                    } ?>
                    </tbody>
                </table>
                <b>Итого: </b> <?= $model->sumpayed . ' руб.' ?>
                <br>
                <b>Осталось заплатить: </b> <?= $model->remainder . ' руб.' ?>
                <br>
                <b>Оплачено путёвок: </b> <?= $model->countplacespayed . ' из ' . $model->count ?>
            <? } else { ?>
                <b>Платежей не было.</b>
            <? } ?>
            </div>


            <div class="col-md-6">
                <?php $form = ActiveForm::begin([
                    'id' => 'form',
                    'options' => ['class' => 'form-content', 'enctype' => 'multipart/form-data'],
                    'fieldConfig' => [
                        'template' => "{label}\n{input}\n{error}"
                    ],
                ]); ?>

                <div class="row">
                    <div class="col-md-6">
                        <b><label for="">Заказ № <span><?= $model->id ?></span></label></b><br>
                        <b>Статус: </b><? echo $model->active == 1 ? $status[$model->status] : 'Аннулирован' ?>
                    </div>
                    <div class="col-md-6 d-flex justify-content-end align-items-center">
                        <a class="btn btn-danger" href="/route_order/delete/<?= $model->id ?>">Удалить</a>
                    </div>
                </div>

                <br>

                <?= $form->field($model, 'name')->textInput()->label('Заказчик') ?>

                <?= $form->field($model, 'phone')->input('phone', ['id' => 'phone'])->label('Телефон') ?>

                <?= $form->field($model, 'email')->input('email', ['id' => 'email'])->label('E-mail') ?>

                <?= $form->field($model, 'discount')->input('number', ['value' => 0])->label('Скидка (%)') ?>

                <?= $form->field($model, 'entity')->dropDownList(
                    $entity
                )->label('Тип плательщика') ?>

                <?= $form->field($model, 'route_id')->dropDownList(
                    $route
                )->label('Маршрут') ?>

                <?= $form->field($model, 'BookingEnd')->input('text', ['id' => 'booking_end'])->label('Дата окончания бронирования') ?>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'DateStartContact')->textInput(['readonly' => 'true'])->label('Дата заезда') ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'TimeStartContact')->textInput()->label('Время заезда') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'DateEndContact')->textInput(['readonly' => 'true'])->label('Дата отъезда') ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'TimeEndContact')->textInput()->label('Время отъезда') ?>
                    </div>
                </div>

                <?= $form->field($model, 'transport')->dropdownList([
                    0 => 'Свой транспорт',
                    1 => 'Нужен транспорт'
                ],
                    ['prompt' => ' ']
                )->label('Транспорт') ?>

                <?= $form->field($model, 'abode')->dropdownList([
                    0 => 'Без проживания',
                    1 => 'С проживанием'
                ],
                    ['prompt' => ' ']
                )->label('Проживание') ?>

                <?= $form->field($model, 'school')->textInput()->label('Школа') ?>

                <?= $form->field($model, 'class')->textInput()->label('Класс') ?>

                <?= $form->field($model, 'count')->input('number')->label('Количество детей') ?>

                <?= $form->field($model, 'count_adults')->input('number')->label('Количество взрослых') ?>

                <?= $form->field($model, 'count_managers')->input('number')->label('Количество руководителей') ?>

                <p style="font-size: 21px;">Пункты выделенные красным - это старые услуги</p>

                <?= $form->field($model, 'addination')
                    ->checkboxList($model->AddinationList,
                        [
                            'multiple' => 'multiple',
                            'class' => 'chosen-select input-md required',
                        ]
                    )->label("Дополнительные услуги:"); ?>
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-inline', 'name' => 'login-button']) ?>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-md-6">
                <?php $form2 = ActiveForm::begin([
                    'id' => 'form2',
                    'action' => '/route_order/contract/' . $model->id,
                    'options' => ['class' => 'form-content'],
                    'fieldConfig' => [
                        'template' => "{label}\n{input}\n{error}"
                    ],
                ]); ?>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form2->field($contract, 'fio')->textInput(['placeholder' => 'Первунинская Светлана Сергеевна'])->label('ФИО заказчика') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form2->field($contract, 'pasportdata')->textInput(['placeholder' => '5711 № 840690 ОУФМС России по Пермскому краю в Мотовилихинском районе 11.02.2012'])->label('Паспортные данные') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><span>Кол-во людей на питание:</span></div>
                </div>
                <div class="row">

                    <div class="col-md-4">
                        <?= $form2->field($contract, 'breakfast')->input('number') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form2->field($contract, 'lunch')->input('number') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form2->field($contract, 'dinner')->input('number') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form2->field($contract, 'transport')->textInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form2->field($contract, 'abode')->textInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form2->field($contract, 'price')->input('double') ?>
                    </div>
                </div>
                <!--                <a id="saveContract" class="btn btn-warning" href="/route_order/savecontract/--><? //= $model->id ?><!--">Сохранить</a>-->


                <div class="inf_block">
                    <div class="row">
                        <div class="col-md-4">
                            <a id="saveContractAndStay" class="btn btn-warning" href="/route_order/savecontract/<?= $model->id ?>/stay">Сохранить</a>
                        </div>
                        <div class="col-md-4">
                            <?= Html::submitButton('Скачать договор', ['class' => 'btn btn-primary btn-inline', 'name' => 'login-button-423']) ?>
                        </div>
                        <div class="col-md-4">
                            <a id="sendContract" class="btn btn-info" href="/route_order/sendcontract/<?= $model->id ?>">Отправить на почту</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <a id="" class="btn btn-info" target="_blank" href="/route_order/getkurs/<?= $model->id ?>">Скачать курсовку</a>
                        </div>
                        <div class="col-md-4">
                            <a id="" class="btn btn-info" href="/route_order/getkursemail/<?= $model->id ?>">Отправить курсовку</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <a id="" class="btn btn-info" href="/route_order/senddocs/<?= $model->id ?>">Отправить документы</a>
                        </div>
                        <div class="col-md-4">
                            <a id="" class="btn btn-info" target="_blank" href="/route_order/getpay/<?= $model->id ?>">Скачать счет</a>
                        </div>
                        <div class="col-md-4">
                            <a id="" class="btn btn-info" href="/route_order/sendpay/<?= $model->id ?>">Отправить счет</a>
                        </div>
                    </div>
                </div>

                <style>
                    .inf_block .row {
                        margin-bottom: 15px;
                    }
                </style>


                <?php ActiveForm::end(); ?>

                <? if ($model->status == 4) { ?>
                    <br><br>
                    <h2>Модерация заказа:</h2>
                    <br>
                    <div class="form-group field-contractinfo-transport required has-error">
                        <label class="control-label" for="comment">Комментарий</label>
                        <input type="text" id="comment" class="form-control" name="comment">
                    </div>
                    <a class="moderateOrder btn btn-success"
                       href="/route_order/moderate/<?= $model->id ?>/true">Принять</a>
                    <a class="moderateOrder btn btn-danger" href="/route_order/moderate/<?= $model->id ?>/false">Отклонить</a>
                    <br><br>
                    <span>Срок ответа:</span>
                    <?
                    $responseTime = 86400 - (time() - $model->create_date);
                    if ($responseTime < 0) {
                        echo "Больше суток";
                    }
                    else {
                        function appenText($number, $type)
                        {
                            $textArr = array(
                                'm' => array('минута', 'минуты', 'минут'),
                                'h' => array('час', 'часа', 'часов'),
                            );
                            $number = (int)$number;
                            $result = $number . ' ';
                            if (isset($textArr[$type])) {
                                switch (($number >= 20) ? $number % 10 : $number) {
                                    case 1:
                                        $result .= $textArr[$type][0];
                                        break;
                                    case 2:
                                    case 3:
                                    case 4:
                                        $result .= $textArr[$type][1];
                                        break;
                                    default:
                                        $result .= $textArr[$type][2];
                                }
                            }
                            return $result;
                        }

                        $responseTime = $responseTime;
                        $hours = floor($responseTime / 3600);
                        $minutes = floor(($responseTime / 3600 - $hours) * 60);

                        echo appenText($hours, 'h') . " " . appenText($minutes, 'm');
                    } ?>
                <? } ?>
            </div>
        </div>
    </div>

    <style>
        .form-control:disabled, .form-control[readonly] {
            background-color: #fff;
        }
    </style>