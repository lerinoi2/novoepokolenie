<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20.09.2017
 * Time: 9:45
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <?php $form = ActiveForm::begin([
            'id' => 'form',
            'options' => ['class' => 'form-content', 'enctype' => 'multipart/form-data'],
            'fieldConfig' => [
                'template' => "{label}\n{input}\n{error}"
            ],
        ]); ?>
        <div class="row">
            <div class="col-md-6">

                <?= $form->field($model, 'name')->textInput()->label('Заказчик') ?>

                <?= $form->field($model, 'phone')->input('phone', ['id' => 'phone'])->label('Телефон') ?>

                <?= $form->field($model, 'email')->input('email', ['id' => 'email'])->label('E-mail') ?>

                <?= $form->field($model, 'discount')->input('number', ['value' => 0])->label('Скидка (%)') ?>

                <?= $form->field($model, 'entity')->dropDownList(
                    $entity
                )->label('Тип плательщика') ?>

                <?= $form->field($model, 'route_id')->dropDownList(
                    $route
                )->label('Маршрут') ?>

                <?= $form->field($model, 'BookingEnd')->input('text', ['id' => 'booking_end', 'readonly' => true])->label('Дата окончания бронирования') ?>

                <?= $form->field($model, 'transport')->dropdownList([
                        0 => 'Свой транспорт',
                        1 => 'Нужен транспорт'
                    ]
                )->label('Транспорт'); ?>

                <?= $form->field($model, 'abode')->dropdownList([
                        0 => 'Без проживания',
                        1 => 'С проживанием'
                    ]
                )->label('Проживание'); ?>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'DateStartContact')->textInput(['readonly'=>true]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'TimeStartContact')->textInput(); ?>
                    </div>
                </div>

<!--                <div class="row">-->
<!--                    <div class="col-md-12">-->
<!--                        --><?//= $form->field($model, 'imageFile')->fileInput() ?>
<!--                    </div>-->
<!--                </div>-->

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'DateEndContact')->textInput(['readonly'=>true]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'TimeEndContact')->textInput(); ?>
                    </div>
                </div>


            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'school')->textInput(['placeholder' => 'Наименование школы'])->label('Школа') ?>

                <?= $form->field($model, 'class')->textInput(['placeholder' => '11 "А"'])->label('Класс') ?>

                <?= $form->field($model, 'count')->input('number')->label('Количество детей') ?>

                <?= $form->field($model, 'count_adults')->input('number')->label('Количество взрослых') ?>

                <?= $form->field($model, 'count_managers')->input('number')->label('Количество руководителей') ?>


                <?= $form->field($model, 'addination')
                    ->checkboxList($model->AddinationList,
                        [
                            'multiple' => 'multiple',
                            'class' => 'chosen-select input-md required',
                        ]
                    )->label("Дополнительные услуги"); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                <br><br><br>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>

<style>
    .form-control:disabled, .form-control[readonly]{
        background-color: #fff;
    }
</style>