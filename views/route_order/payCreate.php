<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20.09.2017
 * Time: 9:45
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <?php $form = ActiveForm::begin([
                    'id' => 'form',
                    'options' => ['class' => 'form-content ropay'],
                    'fieldConfig' => [
                        'template' => "{label}\n{input}\n{error}"
                    ],
                ]); ?>

                <b><label for="">Номер заказа</label></b>
                <p><?= $routeOrder->id ?></p>

                <div class="form-group">
                    <label class="control-label" for="booking_end">Название заказа</label>
                    <input type="text" id="count_voucher" class="form-control" value="<?= $routeOrder->name ?>"
                           readonly>
                </div>

                <?= $form->field($model, 'sum')->input('double')->label('Сумма') ?>

                <?= $form->field($model, 'date_string')->textInput()->label('Дата оплаты') ?>

                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-md-6">
                <?if($routeOrder->endsum){?>
                <span><b>Сумма заказа: </b><?= $routeOrder->endsum ?> руб.</span>
                <?}else{?>
                    <span><b>Сумма заказа не установлена</b></span>
                <?}?>
                <? if (count($routeOrder->pays) > 0) { ?>
                    <h2>История платежей:</h2>
                    <?$i = 1 ?>
                    <table class="table table-bordered" style="font-size: 14px" valign="middle">
                        <thead class="thead-inverse">
                        <tr>
                            <th>Номер платежа</th>
                            <th>Дата платежа</th>
                            <th>Сумма</th>
                            <th>Сбербанк</th>
                            <th>Платеж прошел</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($routeOrder->pays as $el) { ?>
                            <tr>
                                <td><?= $el->id ?></td>
                                <td><?= Yii::$app->formatter->asDate($el->date, "dd.MM.y"); ?></td>
                                <td><?= $el->sum . ' руб.' ?></td>
                                <td><?= $el->sberbank_id ? 'Да' : 'Нет' ?></td>
                                <td><?
                                    if ($el->sberbank_id && $el->payed) {
                                        echo 'Да';
                                    } elseif (!$el->sberbank_id && $el->sum > 0) {
                                        echo 'Да';
                                    } else {
                                        echo 'Нет';
                                    } ?>
                                </td>
                            </tr>
                            <? $i++;
                        } ?>
                        </tbody>
                    </table>
                    <b>Итого: </b> <?= $routeOrder->sumpayed . ' руб.' ?>
                    <br>
                    </pre>
                    <b>Осталось заплатить: </b> <?= ($contacts->price - $routeOrder->sumpayed) . ' руб.' ?>
                <? } ?>
            </div>
        </div>
    </div>
</div>

