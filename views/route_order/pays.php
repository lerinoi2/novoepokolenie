<?php

use app\models\RouteOrder;
use yii\helpers\Json;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RouteOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <h1><?= Html::encode($this->title) ?></h1>
        <p>
            <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'create_date',
                    'value' => function ($model, $key, $index, $grid) {
                        Yii::$app->formatter->locale = 'ru-RU';
                        return Yii::$app->formatter->asDate($model->create_date);
                    },
                    'filter' => kartik\date\DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'date_from',
                        'attribute2' => 'date_to',
                        'type' => kartik\date\DatePicker::TYPE_RANGE,
                        'separator' => '-',
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'weekStart' => 1, //неделя начинается с понедельника
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                        ],
                    ]),
                ],
                'id',
                [
                    'attribute' => 'status',
                    'value' => function ($model, $key, $index, $grid) {
                        $stat = RouteOrder::getStatusArray();
                        return $model->active == 1 ? $stat[$model->status] : 'Аннулирован';
                    },
                    'filter' => array('Не оплачен', 'Предоплата', 'Оплачен', 'Завершен', 'На модерации', 'Не подтверждён'),
                ],
                'name',
                'discount',
                'phone',
                'email:email',
                [
                    'label' => 'Название маршрута',
                    'attribute' => 'route',
                    'value' => function ($model, $key, $index, $grid) {
                        return $model->route->name;
                    },
                    'filter' => RouteOrder::getAllroute(),
                ],
                [
                    'label' => 'Кол-во путёвок',
                    'attribute' => 'count',
                    'value' => function ($model, $key, $index, $grid) {
                        return $model->count;
                    },
                ],
                [
                    'attribute' => 'BookingEnd',
                    'value' => function ($model, $key, $index, $grid) {
                        Yii::$app->formatter->locale = 'ru-RU';
                        return Yii::$app->formatter->asDate($model->BookingEnd);
                    },
                    'filter' => kartik\date\DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'date_f',
                        'attribute2' => 'date_t',
                        'type' => kartik\date\DatePicker::TYPE_RANGE,
                        'separator' => '-',
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'weekStart' => 1, //неделя начинается с понедельника
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                        ],
                    ]),
                ],
                [
                    'label' => 'Сумма заказа',
                    'attribute' => 'sum',
                    'value' => function ($model, $key, $index, $grid) {
                        return $model->sum;
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{clear}',
                    'buttons' => [
                        'clear' => function ($url,$model,$key) {
                            return Html::a('Изменить', "/route_order/pays/create/".$model->id, ['class' => 'btn btn-success']);
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>














<!--<style>-->
<!--    .table td, .table th {-->
<!--        vertical-align: middle;-->
<!--    }-->
<!--</style>-->
<!--<div class="col-md-10 ml-sm-auto content">-->
<!--    <div class="container-fluid">-->
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <h2>Не оплаченные заказы: </h2>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <a id="editlist" href=""><b>Настройка списка</b></a>-->
<!--                <br>-->
<!--                <div class="items" style="display: none;">-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="1" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Порядковый номер</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="2" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Дата заказа</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="3" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Номер заказа</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="4" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Статус</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="5" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">ФИО\Компания</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="6" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Тип</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="7" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Скидка</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="8" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Телефон</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="9" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">E-mail</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="10" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Лагерь</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="11" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Смена</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="12" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Дата начала</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="13" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Дата конца</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="14" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Проживание</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="15" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Количество путёвок</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="16" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">ФИО ребенка</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="17" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Дата рождения</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="18" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Номер путёвки</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="19" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Окончание брони</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="20" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Осталось до окончания</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="21" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Сумма заказа</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="22" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Сумма оплаты</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="23" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Дата оплаты</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="24" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Осталось заплатить</span>-->
<!--                    </label>-->
<!--                    <label class="custom-control custom-checkbox">-->
<!--                        <input type="checkbox" class="custom-control-input" data-col-id="25" checked>-->
<!--                        <span class="custom-control-indicator"></span>-->
<!--                        <span class="custom-control-description">Комментарий</span>-->
<!--                    </label>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col-md-6">-->
<!--                <form id="search" style="margin-bottom: 10px;">-->
<!--                    <div class="form-row align-items-center">-->
<!--                        <div class="col-auto">-->
<!--                            <label class="sr-only" for="inlineFormInputGroup">Поиск</label>-->
<!--                            <div class="input-group mb-2 mb-sm-0">-->
<!--                                <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Поиск">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-auto">-->
<!--                            <button type="submit" class="btn btn-primary">Найти</button>-->
<!--                            <button type="reset" class="btn btn-danger">Сбросить</button>-->
<!--                        </div>-->
<!--                        <label class="custom-control custom-checkbox">-->
<!--                            <input type="checkbox" class="custom-control-input" id="active">-->
<!--                            <span class="custom-control-indicator"></span>-->
<!--                            <span class="custom-control-description">Только активные</span>-->
<!--                        </label>-->
<!--                    </div>-->
<!--                </form>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col-md-12" id="content">-->
<!--                <table class="table table-bordered table-striped" valign="middle">-->
<!--                    <thead class="thead-inverse">-->
<!--                    <tr>-->
<!--                        <th data-col="1">#</th>-->
<!--                        <th data-col="2" style="min-width: 180px;"><label for="sort_cd">Дата заказа</label><input-->
<!--                                    type="checkbox" id="sort_cd" style="display: none;"></th>-->
<!--                        <th data-col="3" style="min-width: 140px;"><label for="sort_nz">Номер заказа</label><input-->
<!--                                    type="checkbox" id="sort_nz" style="display: none;"></th>-->
<!--                        <th data-col="4" style="min-width: 80px;"><label for="sort_status">Статус</label><input-->
<!--                                    type="checkbox" id="sort_status" style="display: none;"></th>-->
<!--                        <th data-col="5" style="min-width: 300px;"><label for="sort_name">ФИО \ Компания</label><input-->
<!--                                    type="checkbox" id="sort_name" style="display: none;"></th>-->
<!--                        <th data-col="6"><label for="sort_type">Тип</label><input-->
<!--                                    type="checkbox" id="sort_type" style="display: none;"></th>-->
<!--                        <th data-col="7" style="min-width: 120px;"><label for="sort_discount">Скидка (%)</label><input-->
<!--                                    type="checkbox" id="sort_discount" style="display: none;"></th>-->
<!--                        <th data-col="8" style="min-width: 180px;"><label for="sort_discount">Телефон</label><input-->
<!--                                    type="checkbox" id="sort_discount" style="display: none;"></th>-->
<!--                        <th data-col="9"><label for="sort_discount">E-mail</label><input-->
<!--                                    type="checkbox" id="sort_discount" style="display: none;"></th>-->
<!--                        <th data-col="10" style="min-width: 180px;"><label for="sort_camp">Маршрут</label><input-->
<!--                                    type="checkbox" id="sort_camp" style="display: none;"></th>-->
<!--                        <th data-col="12" style="min-width: 130px;"><label for="sort_datestartshift">Дата начала</label><input-->
<!--                                    type="checkbox" id="sort_datestartshift" style="display: none;"></th>-->
<!--                        <th data-col="13" style="min-width: 130px;"><label for="sort_dateendshift">Дата-->
<!--                                конца</label><input-->
<!--                                    type="checkbox" id="sort_dateendshift" style="display: none;"></th>-->
<!--                        <th data-col="15" style="min-width: 150px;"><label for="sort_count">Кол-во путёвок</label><input-->
<!--                                    type="checkbox" id="sort_count" style="display: none;"></th>-->
<!--<!--                        <th data-col="16" style="min-width: 340px;"><label for="sort_childname">ФИО-->-->
<!--<!--                                ребенка</label><input-->-->
<!--<!--                                    type="checkbox" id="sort_childname" style="display: none;"></th>-->-->
<!--<!--                        <th data-col="17" style="min-width: 180px;"><label for="sort_birth">Дата рождения</label><input-->-->
<!--<!--                                    type="checkbox" id="sort_birth" style="display: none;"></th>-->-->
<!--<!--                        <th data-col="18" style="min-width: 180px;"><label for="sort_vouchernumb">Номер-->-->
<!--<!--                                путёвки</label><input-->-->
<!--<!--                                    type="checkbox" id="sort_vouchernumb" style="display: none;"></th>-->-->
<!--                        <th data-col="19" style="min-width: 170px;"><label for="sort_endbooking">Окончание брони</label><input-->
<!--                                    type="checkbox" id="sort_endbooking" style="display: none;"></th>-->
<!--                        <th data-col="20" style="min-width: 180px;"><label for="sort_endday">Осталось до окончания-->
<!--                                (дней)</label><input-->
<!--                                    type="checkbox" id="sort_endday" style="display: none;"></th>-->
<!--                        <th data-col="21" style="min-width: 140px;"><label for="sort_sum">Сумма заказа</label><input-->
<!--                                    type="checkbox" id="sort_sum" style="display: none;"></th>-->
<!--                        <th data-col="22" style="min-width: 180px;"><label for="sort_sumpayed">Сумма-->
<!--                                Оплаты</label><input-->
<!--                                    type="checkbox" id="sort_sumpayed" style="display: none;"></th>-->
<!--                        <th data-col="23" style="min-width: 180px;"><label for="sort_daypayed">Дата оплаты</label><input-->
<!--                                    type="checkbox" id="sort_daypayed" style="display: none;"></th>-->
<!--                        <th data-col="24" style="min-width: 180px;"><label for="sort_notpayed">Осталось-->
<!--                                заплатить</label><input-->
<!--                                    type="checkbox" id="sort_notpayed" style="display: none;"></th>-->
<!--                        <th data-col="25" style="min-width: 180px;">Комментарий</th>-->
<!--                        <th data-col="26" style="min-width: 180px;"></th>-->
<!--                    </tr>-->
<!--                    </thead>-->
<!--                    <tbody>-->
<!--                    --><?// if (count($routeOrders) > 0) { ?>
<!--                        --><?// foreach ($routeOrders as $key => $order) { ?>
<!--                            <tr data-id="--><?//= $key ?><!--">-->
<!--                                <input name="toggle" id="--><?//= $key ?><!--" type="checkbox">-->
<!--                                <th data-col="1">--><?// echo $key + 1 ?><!--</th>-->
<!--                                <td data-col="2">--><?// echo $order->create_date_string ?><!--</td>-->
<!--                                <td data-col="3">--><?// echo $order->id ?><!--</td>-->
<!--                                <td data-col="4">--><?// echo $order->active == 1 ? $status[$order->status] : 'Анулирован' ?><!--</td>-->
<!--                                <td data-col="5">--><?// echo $order->name ?><!--</td>-->
<!--                                <td data-col="6">--><?// echo $order->entity ?><!--</td>-->
<!--                                <td data-col="7">--><?// echo $order->discount ?><!--</td>-->
<!--                                <td data-col="8">--><?// echo $order->phone ?><!--</td>-->
<!--                                <td data-col="9">--><?// echo $order->email ?><!--</td>-->
<!--                                <td data-col="10">--><?// echo $order->route->name ?><!--</td>-->
<!--                                <td data-col="10">--><?// echo $order->route->date_start_string ?><!--</td>-->
<!--                                <td data-col="10">--><?// echo $order->route->date_end_string ?><!--</td>-->
<!--                                <td data-col="15">--><?// echo $order->count ?><!--</td>-->
<!--<!--                                <td data-col="16" -->--><?////= $order->count > 2 ? "class=\"lurking\"" : '' ?>
<!--<!--                                    style="padding: 0;">-->-->
<!--<!--                                    <table style="width: 100%;">-->-->
<!--<!--                                        -->--><?//// for ($i = 0; $i < $order->count; $i++) { ?>
<!--<!--                                            <tr>-->-->
<!--<!--                                                <td>-->--><?//// echo $order->vouchers[$i]->name ?><!--<!--</td>-->-->
<!--<!--                                            </tr>-->-->
<!--<!--                                        -->--><?//// } ?>
<!--<!--                                    </table>-->-->
<!--<!--                                </td>-->-->
<!--<!--                                <td data-col="17" -->--><?////= $order->count > 2 ? "class=\"lurking\"" : '' ?>
<!--<!--                                    style="padding: 0;">-->-->
<!--<!--                                    <table style="width: 100%;">-->-->
<!--<!--                                        -->--><?//// for ($i = 0; $i < $order->count; $i++) { ?>
<!--<!--                                            <tr>-->-->
<!--<!--                                                <td>-->--><?//// echo $order->vouchers[$i]->birth_string ?><!--<!--</td>-->-->
<!--<!--                                            </tr>-->-->
<!--<!--                                        -->--><?//// } ?>
<!--<!--                                    </table>-->-->
<!--<!--                                </td>-->-->
<!--<!--                                <td data-col="18" -->--><?////= $order->count > 2 ? "class=\"lurking\"" : '' ?>
<!--<!--                                    style="padding: 0;">-->-->
<!--<!--                                    <table style="width: 100%;">-->-->
<!--<!--                                        -->--><?//// for ($i = 0; $i < $order->count; $i++) { ?>
<!--<!--                                            <tr>-->-->
<!--<!--                                                <td>-->--><?//// echo $order->vouchers[$i]->voucher_number ?><!--<!--</td>-->-->
<!--<!--                                            </tr>-->-->
<!--<!--                                        -->--><?//// } ?>
<!--<!--                                    </table>-->-->
<!--<!--                                </td>-->-->
<!--                                <td data-col="19">--><?// echo $order->booking_end_string ?><!--</td>-->
<!--                                <td data-col="20">--><?// echo $order->DayForEnd ?><!--</td>-->
<!--                                <td data-col="21">--><?// echo $order->sum ?><!--</td>-->
<!--                                <td data-col="22" --><?//= count($order->pays) > 2 ? "class=\"lurking\"" : '' ?>
<!--                                    style="padding: 0;">-->
<!--                                    <table style="width: 100%;">-->
<!--                                        --><?// foreach ($order->pays as $el) { ?>
<!--                                            <tr>-->
<!--                                                <td>-->
<!--                                                    --><?//= $el->sum ?>
<!--                                                </td>-->
<!--                                            </tr>-->
<!--                                        --><?// } ?>
<!--                                    </table>-->
<!--                                </td>-->
<!--                                <td data-col="23" --><?//= count($order->pays) > 2 ? "class=\"lurking\"" : '' ?>
<!--                                    style="padding: 0;">-->
<!--                                    <table style="width: 100%;">-->
<!--                                        --><?// foreach ($order->pays as $el) { ?>
<!--                                            <tr>-->
<!--                                                <td>-->
<!--                                                    --><?//= Yii::$app->formatter->asDate($el->date, "dd.MM.y") ?>
<!--                                                </td>-->
<!--                                            </tr>-->
<!--                                        --><?// } ?>
<!--                                    </table>-->
<!--                                </td>-->
<!--                                <td data-col="24">--><?// echo $order->remainder ?><!--</td>-->
<!--                                <td data-col="25">--><?// echo 'Комментарий' ?><!--</td>-->
<!--                                <td data-col="26"><a-->
<!--                                            href="/route_order/pays/create/--><?// echo $order->id ?><!--">Поставить оплату</a>-->
<!--                                </td>-->
<!--                            </tr>-->
<!--                        --><?// } ?>
<!--                    --><?// } ?>
<!--                    </tbody>-->
<!--                </table>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->


