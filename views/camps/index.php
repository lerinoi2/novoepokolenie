<?php
/* @var $this yii\web\View */
?>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <h2 class="label">Лагеря: </h2>
                <a class="btn btn-info btn-block btn-lg" href="/camps/create">Новый лагерь</a>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover">
                    <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>Название</th>
                        <th>Статус</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($camps as $key => $camp) { ?>
                        <tr>
                            <th scope="row"><? echo $key + 1 ?></th>
                            <td><? echo $camp->name ?></td>
                            <td><? echo $camp->status ?></td>
                            <td><a href="/camps/edit/<? echo $camp->id ?>">Изменить</a></td>
                            <td><a href="/camps/delete/<? echo $camp->id ?>">Удалить</a></td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


