<?php
/* @var $this yii\web\View */
?>
<style>
    .table td, .table th {
        vertical-align: middle;
    }
</style>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <h2>Поставить пол - <?=$shift->name?> :</h2>
            <table class="table table-bordered table-striped" valign="middle">
                <thead class="thead-inverse">
                <tr>
                    <th><label>Имя</label>
                    <th><label>Пол</label>
                    <th><label></label>
                </tr>
                </thead>
                <tbody>
                <? foreach ($kids as $key => $kid) { ?>
                <tr>
                    <th data-col="1"><?= $kid['name'] ?></th>
                    <td data-col="3">
                        <select name="g" id="g">
                            <option value="0">Ж</option>
                            <option value="1">М</option>
                        </select>
                    </td>
                    <td data-col="3">
                        <a href="" class="btn btn-success save-gender" data-id="<?= $kid['id'] ?>">Сохранить</a>
                    </td>
                    <? } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>