<?php
/* @var $this yii\web\View */
?>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <h2>Список связок: </h2>
        <div class="row">
            <div class="col-md-12" id="content">
                <table class="table table-bordered table-striped" valign="middle">
                    <thead class="thead-inverse">
                    <tr>
                        <th><label>ФИО</label></th>
                        <th><label>Связь</label></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($kids as $k => $v) {
                        $arName[$v['bunch']][] = $v['name']."  - ".$v['age'];
                    }
                    ?>
                    <? if ($arName) { ?>
                        <? foreach ($arName as $key => $val) { ?>
                            <tr>
                                <td>
                                    <? foreach ($val as $n) { ?>
                                        <?= $n ?><br>
                                    <? } ?>
                                </td>
                                <td><?= $key ?></td>
                                <td style="vertical-align: inherit;"><a href="/squad/edit/<?=$id?>/bunchview/edit/<?=$key?>" class="btn btn-success">Изменить</a>
                                </td>
                            </tr>
                        <? }
                    } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>