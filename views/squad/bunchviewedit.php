<?php
/* @var $this yii\web\View */
?>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" id="content">
                <table class="table table-bordered table-striped" valign="middle">
                    <thead class="thead-inverse">
                    <tr>
                        <th><label>ФИО</label></th>
                        <th><label>Возраст</label></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($kids as $key => $val) { ?>
                        <tr>
                            <td> <?= $val['name'] ?> </td>
                            <td><?= $val['age'] ?></td>
                            <td style="vertical-align: inherit;">
                                <a href="/" class="btn btn-danger remove-kid" data-id="<?=$val['id']?>" data-bunch="<?=$bunch?>">Удалить</a>
                            </td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>