<style>
    .item {
        margin-bottom: 20px;
    }

    .item .inlime {
        display: inline-block;
        margin-bottom: 20px;
        min-width: 140px;
    }
</style>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <h2>Создать корпус - <?=$shift->name?> - создание комнат:</h2>
            <div class="col-md-12" id="content" data-id="<?=$id?>" data-home="<?=$home_id?>">
                <? foreach ($rooms as $key => $value) { ?>
                    <div class="item">
                        <div class="numbers">
                            <div class="inlime">Номер комнаты:</div>
                            <input type="text" value="<?= $value->number ?>" disabled>
                        </div>
                        <div class="counts">
                            <div class="inlime">Количество мест:</div>
                            <input type="text" value="<?= $value->count ?>">
                        </div>
                        <div class="genders">
                            <div class="inlime">Пол:</div>
                            <select name="gender" id="gender">
                                <? if ($value->gender == 0) {
                                    ?>
                                    <option value="0">Ж</option>
                                    <option value="1">М</option>
                                    <?
                                } else { ?>
                                    <option value="1">М</option>
                                    <option value="0">Ж</option>
                                <? } ?>
                            </select>
                        </div>
                        <div data-id="<?= $value->id ?>">
                            <a href="/" class="btn btn-danger remove-room">Удалить</a>
                        </div>
                    </div>
                <? } ?>
            </div>

            <div style="padding-left: 15px;">
                <a href="/" class="btn btn-success add-room">Добавить</a>
            </div>
            <br>
            <div style="padding-left: 25px">
                <a href="/" class="btn btn-success save-room">Сохранить</a>
            </div>

        </div>
    </div>
</div>