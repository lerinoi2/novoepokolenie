<?php
/* @var $this yii\web\View */
?>
<style>
    .table td, .table th {
        vertical-align: middle;
    }
</style>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <h2>Поставить пол: </h2>
            <table class="table table-bordered table-striped" valign="middle">
                <thead class="thead-inverse">
                <tr>
                    <th>#</th>
                    <th style="min-width: 300px;"><label for="sort_name">Название смены</label>
                    <th style="min-width: 300px;"><label></label>
                </tr>
                </thead>
                <tbody>
                <? foreach ($shifts as $key => $shift) { ?>
                <tr data-id="<?= $key ?>">
                    <th data-col="1"><?= $key ?></th>
                    <td data-col="3"> <?= $shift ?></td>
                    <td data-col="3"> <a href="/squad/gender/<?= $key ?>" class="btn btn-success">Изменить</a></td>
                    <? } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>