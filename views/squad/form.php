<style>
    .table td, .table th {
        vertical-align: middle;
    }
</style>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <h2>Отряд <?= $squad->number ?>: </h2>
        <div class="row">
            <div class="col-md-12" id="content">
                <? if (count($kids) > 0) { ?>
                    <table class="table table-bordered table-striped" valign="middle">
                        <thead class="thead-inverse">
                        <tr>
                            <th style="min-width: 300px;"><label>Имя</label>
                            <th style="min-width: 300px;"><label>Возраст</label>
                            <th style="min-width: 300px;"><label>Номер путевки</label>
                            <th style="min-width: 300px;"><label>Комната</label>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($kids as $key => $kid) { ?>
                        <tr>
                            <td data-col="3"><?= $kid['name'] ?></td>
                            <td data-col="3"><?= $kid['age'] ?></td>
                            <td data-col="3"><?= $kid['number'] ?></td>
                            <td data-col="3"><?= $kid['room'] ?></td>
                            <? } ?>
                        </tbody>
                    </table>
                <? } else { ?>
                    <div>Нет детей для формирования отряда</div>
                <? } ?>
            </div>
        </div>
    </div>
</div>
