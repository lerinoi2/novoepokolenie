<?php
/* @var $this yii\web\View */

$this->title = 'Распределение по отрядам';
?>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <h2 class="label">Распределение: </h2>
            <a class="btn btn-info btn-block btn-lg" href="/squad/gender">Поставить пол</a>
            <a class="btn btn-info btn-block btn-lg" href="/squad/home">Создать корпус</a>
            <a class="btn btn-info btn-block btn-lg" href="/squad/edit">Создать отряды</a>
            <a class="btn btn-info btn-block btn-lg" href="/squad/view">Отряды</a>
        </div>
    </div>
</div>