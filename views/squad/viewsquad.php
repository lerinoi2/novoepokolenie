<?php
/* @var $this yii\web\View */
?>
<style>
    .d-i {
        display: inline-block;
        margin-bottom: 20px;
    }

    .t-c {
        text-align: center;
    }
</style>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" id="content">
                <div class="all d-i" data-all="<?= $all ?>" data-currnet="<?= $current ?>">
                    <div class="t-c">Количество мест</div>
                </div>
                <div class="boys d-i" data-all="<?= $boys['count'] ?>" data-currnet="<?= $boys['current'] ?>">
                    <div class="t-c">Мальчики</div>
                </div>
                <div class="girls d-i" data-all="<?= $girls['count'] ?>" data-currnet="<?= $girls['current'] ?>">
                    <div class="t-c">Девочки</div>
                </div>
                <div class="d-i">
                    <a href="/squad/view/<?=$id?>/<?=$squad?>/print" class="btn btn-success" target="_blank">Напечатать все</a>
                    <a href="/squad/view/<?=$id?>/<?=$squad?>/print/1" class="btn btn-success" target="_blank">Напечатать мальчики</a>
                    <a href="/squad/view/<?=$id?>/<?=$squad?>/print/0" class="btn btn-success" target="_blank">Напечатать девочки</a>
                </div>
                <table class="table table-bordered table-striped" valign="middle">
                    <thead class="thead-inverse">
                    <tr>
                        <th><label>ФИО</label>
                        <th><label>Возраст</label>
                        <th><label>Комната</label>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($kids as $key => $value) { ?>
                        <tr>
                            <th><?= $value['name'] ?></th>
                            <td><?= $value['age'] ?></td>
                            <td><?= $value['room'] ?></td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>