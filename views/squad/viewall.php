<?php
/* @var $this yii\web\View */

$this->title = "Общий вид";

?>
<style>
    .d-i {
        display: inline-block;
        margin-bottom: 20px;
    }

    .t-c {
        text-align: center;
    }
</style>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" id="content">
                <div class="all d-i" data-all="<?= $all ?>" data-currnet="<?= $current ?>">
                    <div class="t-c">Количество мест</div>
                </div>
                <div class="boys d-i" data-all="<?= $boys['count'] ?>" data-currnet="<?= $boys['current'] ?>">
                    <div class="t-c">Мальчики</div>
                </div>
                <div class="girls d-i" data-all="<?= $girls['count'] ?>" data-currnet="<?= $girls['current'] ?>">
                    <div class="t-c">Девочки</div>
                </div>
                <? if ($q[0] > 0) { ?>
                    <div class="qqqq d-i" data-all="<?= $q[1] ?>" data-currnet="<?= $q[0] ?>">
                        <div class="t-c">Не распределенные дети</div>
                    </div>
                    <div class="n-b d-i" data-all="<?= $gen['boys'][1] ?>" data-currnet="<?= $gen['boys'][0] ?>">
                        <div class="t-c">Не распределенные мальчики</div>
                    </div>
                    <div class="n-g d-i" data-all="<?= $gen['girls'][1] ?>" data-currnet="<?= $gen['girls'][0] ?>">
                        <div class="t-c">Не распределенные девочки</div>
                    </div>
                <? } ?>
                <div class="d-i">
                    <a href="/squad/view/<?= $id ?>/print" class="btn btn-success" target="_blank">Напечатать</a>
                </div>
                <? if ($gen['boys'][1] ||  $gen['boys'][1]): ?>
                <div class="d-i">
                    <a href="/squad/view/<?= $id ?>/printno" class="btn btn-success" target="_blank">Напечатать не
                        распределенных</a>
                </div>
                <? endif; ?>

                <?
                foreach ($arrGirls as $k => $v) {
                    $zG[$k] = array_count_values(array_column($v, 'room'));
                }
                foreach ($arrBoys as $k => $v) {
                    $zB[$k] = array_count_values(array_column($v, 'room'));
                }
                ?>

                <table class="table table-bordered table-striped" valign="middle">
                    <thead class="thead-inverse">
                    <tr>
                        <th><label>Номер отряда</label>
                        <th><label>Корпус</label>
                        <th><label>Комнаты</label>
                        <th><label></label>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($squads as $key => $value) { ?>
                        <tr>
                            <th><?= $value->number ?></th>
                            <td>
                                <? foreach ($homes as $k => $val) { ?>
                                    <? if ($value->home == $val->id) { ?>
                                        <?= $val->home ?>
                                    <? } ?>
                                <? } ?>
                            </td>
                            <td>
                                Девочки: <?= count($zG[$value->number]) ?> / Мальчики: <?= count($zB[$value->number]) ?>
                            </td>
                            <td><a href="/squad/view/<?= $id ?>/<?= $value->id ?>" class="btn btn-success">Просмотр</a>
                            </td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>