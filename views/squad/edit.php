<style>
    .item{
        margin-bottom: 20px;
    }
</style>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <h2>Создать корпус - <?=$shift->name?> :</h2>
            <div class="col-md-12" id="content" data-id="<?=$id?>">
                <div class="item"></div>
                <? foreach ($homes as $key => $value) { ?>
                    <div class="item">
                        <div style="display: inline-block">Номер корпуса:</div>
                        <div style="display: inline-block">
                            <input type="text" value="<?= $value ?>" disabled>
                            <a href="/squad/room/edit/<?=$id?>/<?= $key ?>" class="btn btn-success">Изменить</a>
                            <a href="/" class="btn btn-danger remove-home" data-id="<?= $key ?>">Удалить</a>
                        </div>
                    </div>
                <? } ?>
                <div>
                    <a href="/" class="btn btn-success add-home">Добавить</a>
                </div>
                <br>
                <div>
                    <a href="/" class="btn btn-success save-home">Сохранить</a>
                </div>
            </div>
        </div>
    </div>
</div>