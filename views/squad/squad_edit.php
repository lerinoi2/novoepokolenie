<?php

$this->title = "Создать отряд";

?>

<style>
    .item {
        margin-bottom: 20px;
    }

    .item .text {
        display: inline-block;
        min-width: 130px;
        margin-bottom: 15px;
    }
</style>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <h2>Создать отряд - <?= $shift->name ?>:</h2>
        <div class="row">
            <div class="col-md-6" id="content" data-id="<?= $id ?>">
                <div class="item"></div>
                <? foreach ($squads as $key => $value) { ?>
                    <div class="item" data-squad="<?= $value->id ?>">
                        <div class="number">
                            <div class="text">Номер отряда:</div>
                            <div style="display: inline-block"><input type="text" class="form-control"
                                                                      value="<?= $value->number ?>"
                                                                      disabled></div>
                        </div>
                        <div class="home">
                            <div class="text">Корпус:</div>
                            <div style="display: inline-block">
                                <select class="select-home" style="min-width: 100px" disabled>
                                    <? foreach ($homes as $k => $val) { ?>
                                        <? if ($value->home == $val->id) { ?>
                                            <option value="<?= $val->id ?>" selected><?= $val->home ?></option>
                                            <? $w = $val->id ?>
                                        <? } else { ?>
                                            <option value="<?= $val->id ?>"><?= $val->home ?></option>
                                        <? } ?>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                        <div style="display: flex;align-items: center;margin-bottom: 20px" class="rooms">
                            <div class="text">Комнаты:</div>
                            <div style="display: inline-block">
                                <select size="5" multiple class="select-rooms" style="min-width: 100px" disabled>
                                    <? foreach ($rooms as $k => $val) { ?>
                                        <? if ($val->home == $value->home) {
                                            if (in_array($val->id, json_decode($value->rooms))) { ?>
                                                <option value="<?= $val->id ?>"
                                                        selected><?= $val->number ?></option>
                                            <? } else { ?>
                                                <option value="<?= $val->id ?>"><?= $val->number ?></option>
                                            <? }
                                        } ?>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                        <div class="age_from">
                            <div class="text">Возрат от:</div>
                            <div style="display: inline-block"><input type="text" class="form-control"
                                                                      value="<?= $value->age_from ?>" disabled></div>
                        </div>
                        <div class="age_to">
                            <div class="text">Возрат до:</div>
                            <div style="display: inline-block"><input type="text" class="form-control"
                                                                      value="<?= $value->age_to ?>" disabled></div>
                        </div>
                        <div class="home">
                            <div class="text">Количество мест:</div>
                                <span><?= $count[$value->id] ?: '0'?></span>
                        </div>
                        <div>
                            <? if ($value->form == 1) { ?>
                                <a href="/squad/edit/<?= $id ?>/<?= $value->id ?>/formview"
                                   class="btn btn-success">Просмотр</a>
                                <a href="/" class="btn btn-success edit" data-edit="0" data-squad="<?= $value->id ?>">Редактировать</a>
                            <? } ?>
                            <a href="/" class="btn btn-danger remove-squad">Удалить</a>
                        </div>
                    </div>
                <? } ?>
                <div>
                    <a href="/" class="btn btn-success add-squad">Добавить</a>
                    <a href="/" class="btn btn-success save-squad">Сохранить</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row flex-nowrap">
                    <a href="/squad/edit/<?= $id ?>/bunch" class="btn btn-success" style="margin-right: 15px">Создать
                        связку</a>
                    <a href="/squad/edit/<?= $id ?>/bunchview" class="btn btn-success" style="margin-right: 15px">Список
                        связок</a>
                    <a href="/" class="btn btn-success reform-all">Сформировать отряды</a>
                </div>

                <div class="row" style="margin-top: 60px">
                    <table class="table table-bordered table-striped" style="width: 300px" valign="middle">
                        <thead class="thead-inverse">
                        <tr>
                            <th>
                                <label>Мальчики</label>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($kids['boys'] as $key => $value) { ?>
                            <tr>
                                <td><?= $key ?> лет - <?= $value ?> чел.</td>
                            </tr>
                        <? } ?>

                        </tbody>
                    </table>
                    <table class="table table-bordered table-striped" style="width: 300px" valign="middle">
                        <thead class="thead-inverse">
                        <tr>
                            <th>
                                <label>Девочки</label>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($kids['girls'] as $key => $value) { ?>
                            <tr>
                                <td><?= $key ?> лет - <?= $value ?> чел.</td>
                            </tr>
                        <? } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

