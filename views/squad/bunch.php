<style>
    .dis-btn {
        pointer-events: none;
        background-color: #28a74533;
    }
</style>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <h2>Создать связку:</h2>
        <div class="row">
            <div class="col-md-12" id="content">
                <table class="table table-bordered table-striped" valign="middle">
                    <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th><label>Имя</label>
                        <th><label>Текущий отряд</label>
                        <th><label>Возраст</label>
                        <th><label></label>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($kids as $key => $kid) { ?>
                    <tr>
                        <th><?= $key + 1 ?></th>
                        <td> <?= $kid['name'] ?></td>
                        <td> <?= $kid['squad'] ?></td>
                        <td> <?= $kid['age'] ?></td>
                        <td><input type="checkbox" class="checkbox-bunch" data-id="<?= $kid['id'] ?>"></td>
                        <? } ?>
                    </tbody>
                </table>
                <div style="float: right">
                    <a href="" class="btn btn-success dis-btn create-bunch">Создать</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>