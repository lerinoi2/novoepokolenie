<style>
    .table td, .table th {
        vertical-align: middle;
    }
</style>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <h2>Отряд <?= $squad->number ?>: </h2>
        <div class="row">
            <div class="col-md-12" id="content">
                <table class="table table-bordered table-striped" valign="middle">
                    <thead class="thead-inverse">
                    <tr>
                        <th><label>Имя</label></th>
                        <th><label>Возраст</label></th>
                        <th><label>Номер путевки</label></th>
                        <th><label>Комната</label></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($kids as $key => $kid) { ?>
                    <tr>
                        <td data-col="3"><?= $kid['name'] ?></td>
                        <td data-col="3"><?= $kid['age'] ?></td>
                        <td data-col="3"><?= $kid['number'] ?></td>
                        <td data-col="3"><?= $kid['room'] ?></td>
                        <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
