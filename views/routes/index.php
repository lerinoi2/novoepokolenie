<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Routes;


/* @var $this yii\web\View */
/* @var $searchModel app\models\RoutesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <h2 class="label">Маршруты: </h2>
                <a class="btn btn-info btn-block btn-lg" href="/routes/create">Новый маршрут</a>
                <a class="btn btn-info btn-block btn-lg" href="/route_order/addination">Дополнительные услуги</a>
            </div>
        </div>
        <br>

                <div class="row">
            <div class="col-md-12">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name',
                    [
                        'attribute' => 'date_start',
                        'value' => function ($model, $key, $index, $grid) {
                            Yii::$app->formatter->locale = 'ru-RU';
                            return Yii::$app->formatter->asDate($model->date_start);
                        },
                        'filter' => kartik\date\DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'date_from',
                            'attribute2' => 'date_to',
                            'type' => kartik\date\DatePicker::TYPE_RANGE,
                            'separator' => '-',
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'weekStart' => 1, //неделя начинается с понедельника
                                'autoclose' => true,
                                'format' => 'dd.mm.yyyy',
                            ],
                        ]),
                    ],
                    [
                        'attribute' => 'date_end',
                        'value' => function ($model, $key, $index, $grid) {
                            Yii::$app->formatter->locale = 'ru-RU';
                            return Yii::$app->formatter->asDate($model->date_start);
                        },
                        'filter' => kartik\date\DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'd_f',
                            'attribute2' => 'd_t',
                            'type' => kartik\date\DatePicker::TYPE_RANGE,
                            'separator' => '-',
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'weekStart' => 1, //неделя начинается с понедельника
                                'autoclose' => true,
                                'format' => 'dd.mm.yyyy',
                            ],
                        ]),
                    ],
                    [
                        'attribute' => 'status',
                        'value' => function ($model, $key, $index, $grid) {
                            $stat = Routes::getStatusesArray();
                            return $stat[$model->status];
                        },
                        'filter' => array('Не активен', 'Активен'),
                    ],
                    'price',
                    'places',
                    [
                        'attribute' => 'allowedplaces',
                        'value' => function ($model, $key, $index, $grid) {
                            $stat = Routes::getAllowedplaces_new($model->id, $model->places);
                            return $stat > 0 ? $stat : 0;
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{clear}',
                        'buttons' => [
                            'clear' => function ($url,$model,$key) {
                                return Html::a($model->link, $model->link);
                            },
                        ],
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{clear}',
                        'buttons' => [
                            'clear' => function ($url,$model,$key) {
                                return Html::a('Изменить', "/routes/edit/".$model->id, ['class' => 'btn btn-success']);
                            },
                        ],
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{clear}',
                        'buttons' => [
                            'clear' => function ($url,$model,$key) {
                                return Html::a('Удалить', "/routes/delete/".$model->id, ['class' => 'btn btn-danger']);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
        </div>
    </div>
</div>

