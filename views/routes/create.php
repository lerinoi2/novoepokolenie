<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20.09.2017
 * Time: 9:45
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <?php $form = ActiveForm::begin([
                    'id' => 'form',
                    'options' => ['class' => 'form-content'],
                    'fieldConfig' => [
                        'template' => "{label}\n{input}\n{error}"
                    ],
                ]); ?>

                <?= $form->field($model, 'name')->input('text')->label('Название') ?>
                <?= $form->field($model, 'date_start_string')->input('text', ['id' => 'date_start'])->label('Дата начала') ?>
                <?= $form->field($model, 'date_end_string')->input('text', ['id' => 'date_end'])->label('Дата конца') ?>
                <?= $form->field($model, 'description')->textarea()->label('Описание') ?>
                <? /*= $form->field($model, 'status')->textInput()->label('Статус') */ ?>

                <?= $form->field($model, 'status')->dropDownList(
                    $status
                )->label('Статус') ?>

                <?= $form->field($model, 'price')->input('number')->label('Цена') ?>
                <?= $form->field($model, 'places')->input('number')->label('Мест') ?>
                <?= $form->field($model, 'link')->textInput()->label('Ссылка') ?>
                <?= $form->field($model, 'file')->fileInput() ?>


                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

