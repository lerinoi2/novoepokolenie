<?php
/* @var $this yii\web\View */

use yii\helpers\Json;

?>
<style>
    .table td, .table th {
        vertical-align: middle;
    }
</style>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a id="editlist" href=""><b>Настройка списка</b></a>
                <br>
                <div class="items" style="display: none;">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="1" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Порядковый номер</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="2" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Дата заказа</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="3" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Номер заказа</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="4" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Статус</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="5" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">ФИО\Компания</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="6" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Тип</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="7" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Скидка</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="8" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Телефон</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="9" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">E-mail</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="10" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Лагерь</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="11" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Смена</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="12" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Дата начала</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="13" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Дата конца</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="14" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Тип лагеря</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="15" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Количество путёвок</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="16" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">ФИО ребенка</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="17" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Дата рождения</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="18" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Номер путёвки</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="19" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Окончание брони</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="20" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Осталось до окончания</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="21" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Сумма заказа</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="22" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Сумма оплаты</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="23" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Дата оплаты</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="24" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Осталось заплатить</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" data-col-id="25" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Комментарий</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form id="search" style="margin-bottom: 10px;">
                    <div class="form-row align-items-center">
                        <div class="col-auto">
                            <label class="sr-only" for="filter_search">Поиск</label>
                            <div class="input-group mb-2 mb-sm-0">
                                <input type="text" class="form-control" id="filter_search" placeholder="Поиск">
                            </div>
                        </div>
                        <div class="col-auto">
                            <label class="mr-sm-2" for="filter_status">Статус</label>
                            <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="filter_status">
                                <option value="all" selected>Все</option>
                                <? foreach ($status as $key => $el) { ?>
                                    <? if ($key != 2 && $key != 3) { ?>
                                        <option value="<?= $key ?>"><?= $el ?></option>
                                    <? } ?>
                                <? } ?>
                            </select>
                        </div>
                        <div class="col-auto">
                            <label class="mr-sm-2" for="filter_entity">Тип плательщика</label>
                            <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="filter_entity">
                                <option value="all" selected>Все</option>
                                <? foreach ($entity as $key => $el) { ?>
                                    <option value="<?= $key ?>"><?= $el ?></option>
                                <? } ?>
                            </select>
                        </div>
<!--                        <div class="col-auto">-->
<!--                            <label class="custom-control custom-checkbox">-->
<!--                                <input id="filter_active" type="checkbox" class="custom-control-input" checked>-->
<!--                                <span class="custom-control-indicator"></span>-->
<!--                                <span class="custom-control-description">Не показывать аннулированные</span>-->
<!--                            </label>-->
<!--                        </div>-->
                    </div>
                    <br>
                    <div class="form-row">
                        <div class="col-auto">
                            <label class="mr-sm-2" for="filter_shift">Смена</label>
                            <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="filter_shift">
                                <option value="all" selected>Все</option>
                                <? foreach ($shifts as $key => $el) { ?>
                                    <option value="<?= $el->id ?>"><?= $el->namewithdate ?></option>
                                <? } ?>
                            </select>
                        </div>

                        <div class="col-auto">
                            <button type="submit" class="btn btn-primary">Найти</button>
                            <button type="reset" class="btn btn-danger">Сбросить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" id="content">
                <table class="table table-bordered table-striped" valign="middle">
                    <thead class="thead-inverse">
                    <tr>
                        <th data-col="1">#</th>
                        <th data-col="2" style="min-width: 180px;"><label for="sort_cd">Дата заказа</label><input
                                    type="checkbox" id="sort_cd" style="display: none;"></th>
                        <th data-col="3" style="min-width: 140px;"><label for="sort_nz">Номер заказа</label><input
                                    type="checkbox" id="sort_nz" style="display: none;"></th>
                        <th data-col="4" style="min-width: 80px;"><label for="sort_status">Статус</label><input
                                    type="checkbox" id="sort_status" style="display: none;"></th>
                        <th data-col="5" style="min-width: 300px;"><label for="sort_name">ФИО \ Компания</label><input
                                    type="checkbox" id="sort_name" style="display: none;"></th>
                        <th data-col="6"><label for="sort_type">Тип</label><input
                                    type="checkbox" id="sort_type" style="display: none;"></th>
                        <th data-col="7" style="min-width: 120px;"><label for="sort_discount">Скидка (%)</label><input
                                    type="checkbox" id="sort_discount" style="display: none;"></th>
                        <th data-col="8" style="min-width: 180px;"><label for="sort_discount">Телефон</label><input
                                    type="checkbox" id="sort_discount" style="display: none;"></th>
                        <th data-col="9"><label for="sort_discount">E-mail</label><input
                                    type="checkbox" id="sort_discount" style="display: none;"></th>
                        <th data-col="10" style="min-width: 180px;"><label for="sort_camp">Лагерь</label><input
                                    type="checkbox" id="sort_camp" style="display: none;"></th>
                        <th data-col="11" style="min-width: 180px;"><label for="sort_shift">Смена</label><input
                                    type="checkbox" id="sort_shift" style="display: none;"></th>
                        <th data-col="12" style="min-width: 130px;"><label for="sort_datestartshift">Дата начала</label><input
                                    type="checkbox" id="sort_datestartshift" style="display: none;"></th>
                        <th data-col="13" style="min-width: 130px;"><label for="sort_dateendshift">Дата
                                конца</label><input
                                    type="checkbox" id="sort_dateendshift" style="display: none;"></th>
                        <th data-col="14" style="min-width: 220px;"><label for="sort_condit">Тип лагеря</label><input
                                    type="checkbox" id="sort_condit" style="display: none;"></th>
                        <th data-col="15" style="min-width: 150px;"><label for="sort_count">Кол-во путёвок</label><input
                                    type="checkbox" id="sort_count" style="display: none;"></th>
                        <th data-col="16" style="min-width: 340px;"><label for="sort_childname">ФИО
                                ребенка</label><input
                                    type="checkbox" id="sort_childname" style="display: none;"></th>
                        <th data-col="17" style="min-width: 180px;"><label for="sort_birth">Дата рождения</label><input
                                    type="checkbox" id="sort_birth" style="display: none;"></th>
                        <th data-col="18" style="min-width: 180px;"><label for="sort_vouchernumb">Номер
                                путёвки</label><input
                                    type="checkbox" id="sort_vouchernumb" style="display: none;"></th>
                        <th data-col="19" style="min-width: 170px;"><label for="sort_endbooking">Окончание брони</label><input
                                    type="checkbox" id="sort_endbooking" style="display: none;"></th>
                        <th data-col="20" style="min-width: 180px;"><label for="sort_endday">Осталось до окончания
                                (дней)</label><input
                                    type="checkbox" id="sort_endday" style="display: none;"></th>
                        <th data-col="21" style="min-width: 140px;"><label for="sort_sum">Сумма заказа</label><input
                                    type="checkbox" id="sort_sum" style="display: none;"></th>
                        <th data-col="22" style="min-width: 180px;"><label for="sort_sumpayed">Сумма
                                Оплаты</label><input
                                    type="checkbox" id="sort_sumpayed" style="display: none;"></th>
                        <th data-col="23" style="min-width: 180px;"><label for="sort_daypayed">Дата оплаты</label><input
                                    type="checkbox" id="sort_daypayed" style="display: none;"></th>
                        <th data-col="24" style="min-width: 180px;"><label for="sort_notpayed">Осталось
                                заплатить</label><input
                                    type="checkbox" id="sort_notpayed" style="display: none;"></th>
                        <th data-col="25" style="min-width: 180px;">Комментарий</th>
                        <th data-col="26" style="min-width: 180px;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? if (count($orders) > 0) { ?>
                        <? foreach ($orders as $key => $campOrder) { ?>
                            <tr data-id="<?= $key ?>">
                                <input name="toggle" id="<?= $key ?>" type="checkbox">
                                <th data-col="1"><? echo $key + 1 ?></th>
                                <td data-col="2"><? echo $campOrder->create_date_string ?></td>
                                <td data-col="3"><? echo $campOrder->id ?></td>
                                <td data-col="4"><? echo $campOrder->active == 1 ? $status[$campOrder->status] : 'Аннулирован' ?></td>
                                <td data-col="5"><? echo $campOrder->name ?></td>
                                <td data-col="6"><? echo $entity[$campOrder->entity] ?></td>
                                <td data-col="7"><? echo $campOrder->discount ?></td>
                                <td data-col="8"><? echo $campOrder->phone ?></td>
                                <td data-col="9"><? echo $campOrder->email ?></td>
                                <td data-col="10"><? echo $campOrder->camp->name ?></td>
                                <td data-col="11"><? echo $campOrder->campShift->name ?></td>
                                <td data-col="12"><? echo $campOrder->campShift->date_start_string ?></td>
                                <td data-col="13"><? echo $campOrder->campShift->date_end_string ?></td>
                                <td data-col="14"><? echo $campOrder->campShift->conditions ?></td>
                                <td data-col="15"><? echo $campOrder->count ?></td>
                                <td data-col="16" <?= $campOrder->count > 2 ? "class=\"lurking\"" : '' ?>
                                    style="padding: 0;">
                                    <table style="width: 100%;">
                                        <? for ($i = 0; $i < $campOrder->count; $i++) { ?>
                                            <tr>
                                                <td><? echo $campOrder->vouchers[$i]->name ?></td>
                                            </tr>
                                        <? } ?>
                                    </table>
                                </td>
                                <td data-col="17" <?= $campOrder->count > 2 ? "class=\"lurking\"" : '' ?>
                                    style="padding: 0;">
                                    <table style="width: 100%;">
                                        <? for ($i = 0; $i < $campOrder->count; $i++) { ?>
                                            <tr>
                                                <td><? echo $campOrder->vouchers[$i]->birth_string ?></td>
                                            </tr>
                                        <? } ?>
                                    </table>
                                </td>
                                <td data-col="18" <?= $campOrder->count > 2 ? "class=\"lurking\"" : '' ?>
                                    style="padding: 0;">
                                    <table style="width: 100%;">
                                        <? for ($i = 0; $i < $campOrder->count; $i++) { ?>
                                            <tr>
                                                <td><? echo $campOrder->vouchers[$i]->voucher_number ?></td>
                                            </tr>
                                        <? } ?>
                                    </table>
                                </td>

                                <td data-col="19"><? echo $campOrder->booking_end_string ?></td>
                                <td data-col="20"><? echo $campOrder->dayforend ?></td>
                                <td data-col="21"><? echo $campOrder->sum ?></td>
                                <td data-col="22" <?= count($campOrder->pays) > 2 ? "class=\"lurking\"" : '' ?>
                                    style="padding: 0;">
                                    <table style="width: 100%;">
                                        <? foreach ($campOrder->pays as $el) { ?>
                                            <tr>
                                                <td>
                                                    <?= $el->sum ?>
                                                </td>
                                            </tr>
                                        <? } ?>
                                    </table>
                                </td>
                                <td data-col="23" <?= count($campOrder->pays) > 2 ? "class=\"lurking\"" : '' ?>
                                    style="padding: 0;">
                                    <table style="width: 100%;">
                                        <? foreach ($campOrder->pays as $el) { ?>
                                            <tr>
                                                <td>
                                                    <?= Yii::$app->formatter->asDate($el->date, "dd.MM.y") ?>
                                                </td>
                                            </tr>
                                        <? } ?>
                                    </table>
                                </td>
                                <td data-col="24"><? echo $campOrder->remainder ?></td>
                                <td data-col="25"><? echo 'Комментарий' ?></td>
                                <td data-col="26"><a
                                            href="/camp_order/pays/create/<? echo $campOrder->id ?>">Поставить оплату</a>
                                </td>
                            </tr>
                        <? } ?>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


