<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\CampOrder;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CampOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список заказов';
$this->params['breadcrumbs'][] = $this->title;

$campShifts = CampOrder::allShift();
?>


<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'create_date',
                        'value' => function ($model, $key, $index, $grid) {
                            Yii::$app->formatter->locale = 'ru-RU';
                            return Yii::$app->formatter->asDatetime($model->create_date);
                        },
                        'filter' => kartik\date\DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'date_from',
                            'attribute2' => 'date_to',
                            'type' => kartik\date\DatePicker::TYPE_RANGE,
                            'separator' => '-',
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'weekStart' => 1, //неделя начинается с понедельника
                                'autoclose' => true,
                                'format' => 'dd.mm.yyyy',
                            ],
                        ]),
                    ],
                    'id',
                    [
                        'attribute' => 'status',
                        'value' => function ($model, $key, $index, $grid) {
                            $stat = CampOrder::getStatusArray();
                            return $model->active == 1 ? $stat[$model->status] : 'Аннулирован';
                        },
                        'filter' => array('Не оплачен', 'Предоплата', 'Оплачен', 'Завершен', 'Аннулирован'),
                    ],
                    [
                        'attribute' => 'entity',
                        'value' => function ($model, $key, $index, $grid) {
                            $stat = CampOrder::getEntityesArray();
                            return $stat[$model->entity];
                        },
                        'filter' => array('Физическое лицо', 'Юридическое лицо'),
                    ],
                    'name',
                    'phone',
                    'email:email',
                    [
                        'label' => 'Смена',
                        'attribute' => 'camp_shift_id',
                        'value' => function ($model, $key, $index, $grid) {
                            return CampOrder::getCampShiftName($model->camp_shift_id);
                        },
                        'filter' => $campShifts,
                    ],
                    ['attribute' => 'fio','label' => 'ФИО ребенка', 'value'=>'fio.name'],
                    [
                        'attribute' => 'birth',
                        'label' => 'Дата рождения',
                        'value' => function ($model, $key, $index, $grid) {
                            Yii::$app->formatter->locale = 'ru-RU';
                            return Yii::$app->formatter->asDate($model->birth['birth']);
                        },
                        'filter' => kartik\date\DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'd_f',
                            'attribute2' => 'd_t',
                            'type' => kartik\date\DatePicker::TYPE_RANGE,
                            'separator' => '-',
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'weekStart' => 1, //неделя начинается с понедельника
                                'autoclose' => true,
                                'format' => 'dd.mm.yyyy',
                            ],
                        ]),
                    ],
                     'count',
                    [
                        'label' => 'Окончание брони',
                        'attribute' => 'booking_end',
                        'value' => function ($model, $key, $index, $grid) {
                            return CampOrder::getEnd($model->status, $model->booking_end)[1];
                        },
                    ],
                    [
                        'label' => 'Осталось до окончания (дней)',
                        'attribute' => 'booking_end',
                        'value' => function ($model, $key, $index, $grid) {
                            return CampOrder::getEnd($model->status, $model->booking_end)[0];
                        },
                    ],
                     'price',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{clear}',
                        'buttons' => [
                            'clear' => function ($url,$model,$key) {
                                return Html::a('Изменить', "/camp_order/edit/".$model->id, ['class' => 'btn btn-success']);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>