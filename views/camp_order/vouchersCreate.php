<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20.09.2017
 * Time: 9:45
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <?php $form = ActiveForm::begin([
            'id' => 'form',
            'options' => ['class' => 'form-content create-voucher'],
            'fieldConfig' => [
                'template' => "{label}\n{input}\n{error}"
            ],
        ]); ?>
        <div class="row">
            <div class="col-md-6">

                <b><label for="">Номер заказа</label></b>
                <p><?= $model->id ?></p>
                <b><label for="">Название</label></b>
                <p><?= $model->name ?></p>
                <b><label for="">Телефон</label></b>
                <p><?= $model->phone ?></p>
                <b><label for="">E-mail</label></b>
                <p><?= $model->email ?></p>
                <b><label for="">Скидка</label></b>
                <p><?= $model->discount ?></p>
                <b><label for="">Тип плательщика</label></b>
                <p><?= $entity[$model->entity] ?></p>
                <b><label for="">Лагерь</label></b>
                <p><?= $camps[$model->camp_id] ?></p>
                <b><label for="">Смена</label></b>
                <p><?= $camps_shift[$model->camp_shift_id] ?></p>
                <b><label for="">Дата окончания бронирования</label></b>
                <p><?= $model->booking_end_string ?></p>
                <b><label for="">Цена за одну путёвку</label></b>
                <!-- <p><?//= $model->campShift->price ?></p> -->
                <p><?= $model->price ?></p>

            </div>
            <div class="col-md-6">
                <span><b>Сумма заказа: </b><?= $model->sum ?> руб.</span>
                <h2>История платежей:</h2>
                <?
                $i = 1 ?>
                <? if (count($model->pays) > 0) { ?>
                    <table class="table table-bordered" style="font-size: 14px" valign="middle">
                        <thead class="thead-inverse">
                        <tr>
                            <th>Номер платежа</th>
                            <th>Дата платежа</th>
                            <th>Сумма</th>
                            <th>Сбербанк</th>
                            <th>Платеж прошел</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($model->pays as $el) { ?>
                            <tr>
                                <td><?= $el->id ?></td>
                                <td><?= Yii::$app->formatter->asDate($el->date, "dd.MM.y"); ?></td>
                                <td><?= $el->sum . ' руб.' ?></td>
                                <td><?= $el->sberbank_id ? 'Да' : 'Нет' ?></td>
                                <td><?
                                    if ($el->sberbank_id && $el->payed) {
                                        echo 'Да';
                                    } elseif (!$el->sberbank_id && $el->sum > 0) {
                                        echo 'Да';
                                    } else {
                                        echo 'Нет';
                                    } ?>
                                </td>
                            </tr>
                            <? $i++;
                        } ?>
                        </tbody>
                    </table>
                    <b>Итого: </b> <?= $model->sumpayed . ' руб.' ?>
                    <br>
                    <b>Осталось заплатить: </b> <?= $model->remainder . ' руб.' ?>
                    <br>
                    <b>Оплачено путёвок: </b> <?= $model->countplacespayed . ' из ' . $model->count ?>
                <? } else { ?>
                    <b>Платежей не было.</b>
                <? } ?>
            </div>
        </div>
        <h2>Дети</h2>

        <div id="voucher-container" class="col-md-9">
            <? foreach ($vouchers as $key => $voucher) { ?>
                <? if ($key > 0) {?>
                    <hr>
                <?}?>
                <div class="row">
                    <?= $form->field($voucher, "[$key]id")->hiddenInput()->label(false) ?>
                    <div class="col-md-4">
                        <?= $form->field($voucher, "[$key]name")->textInput()->label('Имя ребенка') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($voucher, "[$key]birth_string")->textInput()->label('День рождения') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($voucher, "[$key]voucher_number")->textInput(['style' => 'background-color: #d2d2d2', 'data-readonly' => true])->label('Номер путёвки') ?>
                    </div>
                    <div class="col-md-1">
                        <? if ((!$voucher->voucher_number) or (!$voucher->representative_name) or (!$voucher->representative_phone)) { ?>
                            <a class="btn voucher_btn" href="/camp_order/voucher_del/<?= $voucher->id ?>">Удалить</a>
                        <? } else {?>
                            <a href="/camp_order/getpdf/<?= $model->id ?>/<?= $voucher->id ?>" class="btn btn-success voucher_btn" target="_blank">Скачать путевку</a>
                        <? } ?>
                    </div>
                    <!-- Законный представитель -->
                    <div class="col-md-4">
                        <?= $form->field($voucher, "[$key]representative_name")->textInput()->label('Законный представитель ребенка (ФИО)') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($voucher, "[$key]representative_phone")->input('phone', ['class' => 'form-control phoneMask'])->label('Телефон') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($voucher, "[$key]representative_address")->textInput(['data-address' => 'andres'])->label('Адрес') ?>
                    </div>
                    <div class="col-md-1">
                        <? if ((!$voucher->voucher_number)) { ?>
                            <span class="btn btn-success voucher_btn _saveVoucher" data-id="<?=$voucher->id?>" data-key="<?=$key?>">Проверено</span>
                        <? } elseif (($voucher->representative_name) and ($voucher->representative_phone)) { ?>
                            <a href="/camp_order/sendvoucher/<?= $model->id ?>/<?= $voucher->id ?>" class="btn btn-success voucher_btn">Отправить путевку</a>
                        <? } ?>
                    </div>
                </div>
            <? } ?>
        </div>

        <div class="formSubmit">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>
<script>
    let countpayed = <?=$model->countplacespayed?>;
    let countwithnumber = <?=$model->CountVouchersWithNumber?>;
</script>
