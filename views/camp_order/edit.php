<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$check = 0;
foreach ($vouchers as $key => $voucher) {
    if (($voucher->voucher_number != "") and ($voucher->representative_name != "") and ($voucher->representative_phone != "")) {
        $check += 1;
    }
}

$count = count($vouchers);

?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="edit__pay">
                <span><b>Сумма заказа: </b><?= $model->sum ?> руб.</span>
                <h2>История платежей:</h2>
                <?
                $i = 1 ?>
                <? if (count($model->pays) > 0) { ?>
                    <table class="table table-bordered" style="font-size: 14px" valign="middle">
                        <thead class="thead-inverse">
                        <tr>
                            <th>Номер платежа</th>
                            <th>Дата платежа</th>
                            <th>Сумма</th>
                            <th>Способ оплаты</th>
                            <th>Платеж прошел</th>
                            <th>Кем оплачено</th>
                            <th>За кого оплачено</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($model->pays as $el) { ?>
                            <tr>
                                <td><?= $el->id ?></td>
                                <td><?= Yii::$app->formatter->asDate($el->date, "dd.MM.y"); ?></td>
                                <td><?= $el->sum . ' руб.' ?></td>
                                <td><?= $el->sberbank_id ? 'Сбербанк онлайн' : $el->payType ?></td>
                                <td><?
                                    if ($el->sberbank_id && $el->payed) {
                                        echo 'Да';
                                    }
                                    elseif (!$el->sberbank_id && $el->sum > 0) {
                                        echo 'Да';
                                    }
                                    else {
                                        echo 'Нет';
                                    } ?>
                                </td>
                                <td><?= $el->sberbank_id ? $model->name : $el->person ?></td>
                                <td><?= $el->frompaid ?></td>
                                <td>
                                    <a href="/camp_order/editpay/<?= $model->id ?>" class="btn btn-success">Изменить</a>
                                    <? if ((Yii::$app->user->identity->role == 1) and ($el->sberbank_id)) { ?>
                                        <br>
                                        <a href="/camp_order/rmpay/<?= $el->id ?>" class="btn btn-danger rmpay"
                                           style="margin-top: 20px">Удалить</a>
                                    <? } ?>
                                </td>
                            </tr>
                            <? $i++;
                        } ?>
                        </tbody>
                    </table>
                    <b>Итого: </b> <?= $model->sumpayed . ' руб.' ?>
                    <br>
                    <b>Осталось заплатить: </b> <?= $model->remainder . ' руб.' ?>
                    <br>
                    <b>Оплачено путёвок: </b> <?= $model->countplacespayed . ' из ' . $model->count ?>
                <? } else { ?>
                    <b>Платежей не было.</b>
                <? } ?>
                <br>
                <h2>Информация о брони:</h2>
                <b>Дата брони\заказа: </b> <?= Yii::$app->formatter->asDate($model->create_date, "dd.MM.y") ?>
                <br>
                <b>Дата окончания брони: </b><?= $model->booking_end_string ?><br>
                <b>Дней до окончания
                    брони: </b><?= round(($model->booking_end - Yii::$app->formatter->asTimestamp(date("d.M.y"))) / (60 * 60 * 24)) ?>
                <br>
                <br>
                <a href="/camp_order/sendcheck/<?= $model->id ?>" class="btn btn-info">Отправить счет</a>
                <?
                if ($check == $count) { ?>
                    <a href="/camp_order/sendvouchers/<?= $model->id ?>" class="btn btn-warning">Отправить путёвки</a>
                    <a href="/camp_order/getzip/<?= $model->id ?>" class="btn btn-warning" target="_blank">Скачать
                        путёвки</a>
                <? } ?>
            </div>
            <div class="edit__info">
                <?php $form = ActiveForm::begin([
                    'id' => 'form',
                    'options' => ['class' => 'form-content create-voucher'],
                    'fieldConfig' => [
                        'template' => "{label}{input}\n{error}"
                    ],
                ]); ?>
                <b><label for="">Заказ № <span><?= $model->id ?></span></label></b><br>
                <b>Статус: </b><? echo $model->active == 1 ? $status[$model->status] : 'Аннулирован' ?>

                <?= $form->field($model, 'name')->textInput()->label('Название') ?>

                <?= $form->field($model, 'create_date_string')->textInput(['readonly' => true])->label('Дата заказа') ?>

                <?= $form->field($model, 'phone')->input('phone', ['id' => 'phone'])->label('Телефон') ?>

                <?= $form->field($model, 'email')->input('email', ['id' => 'email'])->label('E-mail') ?>

                <?= $form->field($model, 'discount')->input('number')->label('Скидка') ?>

                <?= $form->field($model, 'entity')->dropDownList(
                    $entity
                )->label('Тип плательщика') ?>

                <?= $form->field($model, 'camp_id')->dropDownList(
                    $camps
                )->label('Лагерь') ?>

                <?= $form->field($model, 'camp_shift_id')->dropDownList(
                    $camps_shift
                )->label('Смена') ?>

                <?= $form->field($model, 'booking_end_string')->input('text', ['id' => 'booking_end'])->label('Дата окончания брони') ?>

                <?= $form->field($model, 'count')->input('number', ['id' => 'count_voucher'])->label('Количество детей') ?>

                <div id="voucher-container">
                    <hr>
                    <div class="row">
                        <? foreach ($vouchers as $key => $voucher): ?>
                            <div class="row">
                                <?= $form->field($voucher, "[$key]id")->hiddenInput()->label(false) ?>
                                <div class="col-md-4">
                                    <?= $form->field($voucher, "[$key]name")->textInput()->label('Имя ребенка') ?>
                                </div>
                                <div class="col-md-4">
                                    <?= $form->field($voucher, "[$key]birth_string")->textInput()->label('День рождения') ?>
                                </div>
                                <div class="col-md-4">
                                    <?= $form->field($voucher, "[$key]voucher_number")->textInput(['readonly' => true])->label('Номер путёвки') ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($voucher, "[$key]representative_phone")->input('phone', ['class' => 'form-control phoneMask'])->label('Телефон') ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($voucher, "[$key]gender")->dropDownList(['0' => 'Женский', '1' => 'Мужской'])->label('Пол ребенка'); ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($voucher, "[$key]representative_address")->textInput(['data-address' => 'andres'])->label('Адрес') ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($voucher, "[$key]representative_name")->textInput()->label('Законный представитель (ФИО)') ?>
                                </div>
                                <div class="col-md-6">
                                    <? if ((!$voucher->voucher_number)) { ?>
                                        <span class="btn btn-success voucher_btn _saveVoucher"
                                              data-id="<?= $voucher->id ?>"
                                              data-key="<?= $key ?>">Проверено</span>
                                    <? } elseif (($voucher->representative_name) and ($voucher->representative_phone)) { ?>
                                        <a href="/camp_order/sendvoucher/<?= $model->id ?>/<?= $voucher->id ?>"
                                           class="btn btn-success voucher_btn">Отправить путевку</a>
                                    <? } ?>
                                </div>
                                <div class="col-md-6">
                                    <? if (!$voucher->voucher_number) { ?>
                                        <a class="btn voucher_btn"
                                           href="/camp_order/deletevoucher/<?= $voucher->id ?>">Удалить</a>
                                    <? } elseif (($voucher->representative_name) and ($voucher->representative_phone)) { ?>
                                        <a class="btn voucher_btn"
                                           href="/camp_order/deletevoucher/<?= $voucher->id ?>">Удалить</a>
                                        <a href="/camp_order/getpdf/<?= $model->id ?>/<?= $voucher->id ?>"
                                           class="btn btn-success voucher_btn" target="_blank">Скачать путевку</a>
                                    <? } ?>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
                <div class="formSubmit">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    <a href="/camp_order/delete/<?= $model->id ?>" class="btn btn-danger">Аннулировать заказ</a>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>


