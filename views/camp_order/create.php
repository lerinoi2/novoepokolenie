<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <?php $form = ActiveForm::begin([
            'id' => 'form',
            'options' => ['class' => 'form-content'],
            'fieldConfig' => [
                'template' => "{label}\n{input}\n{error}"
            ],
        ]); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput()->label('Название') ?>

                <?= $form->field($model, 'phone')->input('phone', ['id' => 'phone'])->label('Телефон') ?>

                <?= $form->field($model, 'email')->input('email', ['id' => 'email'])->label('E-mail') ?>

                <?= $form->field($model, 'discount')->input('number', ['value' => 0])->label('Скидка (%)') ?>

                <?= $form->field($model, 'entity')->dropDownList(
                    $entity
                )->label('Тип плательщика') ?>

                <?= $form->field($model, 'camp_id')->dropDownList(
                    $camps
                )->label('Лагерь') ?>

                <?= $form->field($model, 'camp_shift_id')->dropDownList(
                    $camps_shift
                )->label('Смена') ?>

                <?= $form->field($model, 'booking_end_string')->input('text', ['id' => 'booking_end'])->label('Дата окончания бронирования') ?>

                <?= $form->field($model, 'count')->input('number', ['id' => 'count_voucher', 'value' => 1])->label('Количество детей') ?>
            </div>
        </div>
        <h2>Дети</h2>

        <div id="voucher-container">
            <? foreach ($vouchers as $key => $voucher) { ?>
                <hr>
                <div class="row">
                    <?= $form->field($voucher, "[$key]id")->hiddenInput()->label(false) ?>
                    <div class="col-md-5">
                        <?= $form->field($voucher, "[$key]name")->textInput()->label('Имя ребенка') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($voucher, "[$key]birth_string")->textInput()->label('День рождения') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($voucher, "[$key]voucher_number")->textInput(['readonly' => true])->label('Номер путёвки') ?>
                    </div>
                    <!-- Законный представитель -->
                    <div class="col-md-5">
                        <?= $form->field($voucher, "[$key]representative_name")->textInput()->label('Законный представитель (ФИО)') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($voucher, "[$key]representative_phone")->input('phone', ['class' => 'form-control phoneMask'])->label('Телефон') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($voucher, "[$key]representative_address")->textInput()->label('Адрес') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($voucher, "[$key]gender")->dropDownList(['0' => 'Женский', '1' => 'Мужской' ])->label('Пол ребенка'); ?>
                    </div>
                </div>
            <? } ?>
        </div>

        <div class="formSubmit">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>

