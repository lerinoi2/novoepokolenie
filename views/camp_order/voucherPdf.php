<?

use app\controllers\Camp_orderController;
use BigFish\PDF417\PDF417;
use BigFish\PDF417\Renderers\ImageRenderer;

$pdf417 = new PDF417();
$text = $voucher->name . " " . $voucher->birth_string . " " . $data->campShift->name;
$datas = $pdf417->encode($text);


$renderer = new ImageRenderer([
    'format' => 'data-url',
    'color' => '#000',
    'bgColor' => '#eee8c0',
    'scale' => 10,
]);
$img2 = $renderer->render($datas);


$renderer = new ImageRenderer([
    'format' => 'data-url',
    'color' => '#000',
    'bgColor' => '#f0e2b1',
    'scale' => 10,
]);
$img1 = $renderer->render($datas);

$pay_date = '';
foreach ($data->pays as $pay) {
    if ($pay->frompaid) {
        $need = trim($pay->frompaid);
    }
    else {
        $need = $pay->frompaid;
    }

    if ($need) {
        $pos1 = stristr($voucher->name, $need);
        if ($pos1 !== false) {
            $pay_date = Yii::$app->formatter->asDate($pay->date, "dd.MM.y");
        }
    }
}
?>
<img src="http://zakaz.novoepokolenie.com/web/data/bkgrnd.png" alt="">
<table style="margin-top: -1500px; width: 1300px;font-weight: bold;font-size: 25px" border="0">
    <tr style="height: 1px"></tr>

    <tr>
        <td valign="middle" style="padding-left: 60px;" class="qwe">
            НОМЕР ПУТЕВКИ: <?= $voucher->voucher_number ?>
        </td>

        <td align="left" valign="middle" rowspan="3" >
            <img src="<?= $img1->encoded ?>" style="margin-right: 350px" height="100px" width="250px" alt="">
        </td>
    </tr>

    <tr style="width: 10px;" width="10">
        <td valign="middle" style="padding-left: 60px;">
            НОМЕР ЗАКАЗА: <?= $voucher->order_id ?>
        </td>
    </tr>

    <tr style="width: 10px;" width="10">
        <td valign="middle" style="padding-left: 60px;">
            ДАТА ОФОРМЛЕНИЯ ПУТЕВКИ:
            <?= $pay_date ?: Yii::$app->formatter->asDate($data->pays[1]->date, "dd.MM.y") ?>
        </td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="350px"
       style="margin-top: -140px; margin-left: 1145px; font-weight: bold;font-size: 16px">
    <tr>
        <td valign="middle">НОМЕР ПУТЕВКИ: <?= $voucher->voucher_number ?></td>
    </tr>
    <tr>
        <td valign="middle">НОМЕР ЗАКАЗА: <?= $voucher->order_id ?></td>
    </tr>
    <tr>
        <td valign="middle">ДАТА ОФОРМЛЕНИЯ
            ПУТЕВКИ: <?= $pay_date ?: Yii::$app->formatter->asDate($data->pays[1]->date, "dd.MM.y") ?></td>
    </tr>
    <tr>
        <td height="120px" valign="middle"><img src="<?= $img2->encoded ?>" height="95px" width="285px" alt=""></td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="595px" style="margin-top: 45px;font-size: 14px">
    <tr>
        <td width="300px" height="72px" align="center" valign="middle"><?= $voucher->name ?></td>
        <td width="295px" align="center" valign="middle"><?= $voucher->birth_string ?></td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="590px" style="margin-top: 20px;font-size: 14px">
    <tr>
        <td width="210px" height="40px" align="center" valign="middle"><?= $data->campShift->name ?></td>
        <td width="180px" align="center" valign="middle"><?= substr($data->campShift->date_start_string, 0, 10) ?></td>
        <td width="200px" align="center" valign="middle"><?= substr($data->campShift->date_end_string, 0, 10) ?></td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="550px" style="font-size: 14px">
    <tr>
        <td colspan="2" width="290px" height="40px" align="center" valign="middle"></td>
        <td colspan="2" width="275px" align="center"
            valign="middle"><?= $conditions[$data->campShift->conditions] ?></td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="560px" style="margin-top: 80px;font-size: 9px">
    <tr>
        <td width="210px" height="40px" align="center" valign="middle"><?= $voucher->representative_name ?></td>
        <td width="190px" align="center" valign="middle"><?= $voucher->representative_phone ?></td>
        <td width="160px" align="center" valign="middle"><?= $voucher->representative_address ?></td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="500px"
       style="margin-top: 40px; margin-left: 50px;font-size: 14px">
    <tr>
        <td width="125px" height="40px" align="center" valign="middle"></td>
        <td width="125px" align="center" valign="middle"><?= $data->price ?> РУБ.</td>
        <td width="125px" align="center" valign="middle"></td>
        <?
        foreach ($data->pays as $val) {
            $payes = $val->sberbank_id ? 'Сбербанк онлайн' : $val->payType;
        }
        ?>
        <td width="125px" align="center" valign="middle"><?= $payes ?></td>
    </tr>
    <tr>
        <td width="125px" height="40px" align="center" valign="middle"></td>
        <td width="125px" align="center" valign="middle"
            style="font-size: 9px"><?= $q['person'] ?></td>
        <td width="125px" align="center" valign="middle"></td>
        <td width="125px" align="center" valign="middle"><?= $data->price ?> РУБ.</td>
    </tr>
    <tr>
        <td colspan="2" width="250px" height="40px" align="center" valign="middle"></td>
        <td colspan="2" width="250px" align="center"
            valign="middle"><?= $this->context->ucfirst_utf8($this->context->num2str($data->price)) ?></td>
    </tr>
    <tr>
        <td colspan="2" width="250px" height="40px" align="center" valign="middle"></td>
        <td colspan="2" width="250px" align="center" valign="middle"></td>
    </tr>
    <tr>
        <td width="125px" height="30px" align="center" valign="middle"></td>
        <td width="125px" align="center" valign="middle" style="font-size: 9px"><?= $voucher->who_created ?></td>
        <td width="125px" align="center" valign="middle"></td>
        <td width="125px" align="center" valign="middle"></td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="150px"
       style="margin-top: -550px; margin-left: 610px;font-size: 16px">
    <tr>
        <td colspan="2" width="75px" height="40px" align="center" valign="middle"></td>
    </tr>
    <tr>
        <td colspan="2" width="75px" height="40px" align="center" valign="middle"
            style="font-size: 9px"><?= $voucher->name ?></td>
    </tr>
    <tr>
        <td colspan="2" width="75px" height="40px" align="center" valign="middle"></td>
    </tr>
    <tr>
        <td colspan="2" width="75px" height="40px" align="center" valign="middle"><?= $voucher->birth_string ?></td>
    </tr>
    <tr>
        <td colspan="2" width="75px" height="40px" align="center" valign="middle"></td>
    </tr>
    <tr>
        <td colspan="2" width="75px" height="40px" align="center" valign="middle"
            style="font-size: 9px"><?= $voucher->representative_name ?></td>
    </tr>
    <tr>
        <td colspan="2" width="75px" height="40px" align="center" valign="middle"></td>
    </tr>
    <tr>
        <td colspan="2" width="75px" height="40px" align="center"
            valign="middle"><?= $voucher->representative_phone ?></td>
    </tr>
    <tr>
        <td colspan="2" width="75px" height="40px" align="center" valign="middle"></td>
    </tr>
    <tr>
        <td colspan="2" width="75px" height="40px" align="center" valign="middle"></td>
    </tr>
    <tr>
        <td colspan="2" width="75px" height="40px" align="center" valign="middle"></td>
    </tr>
    <tr>
        <td colspan="2" width="75px" height="40px" align="center" valign="middle"></td>
    </tr>
    <tr>
        <td colspan="2" width="75px" height="40px" align="center" valign="middle"></td>
    </tr>
    <tr>
        <td colspan="2" width="75px" height="40px" align="center" valign="middle"></td>
    </tr>
    <tr>
        <td width="75px" height="40px" align="center" valign="middle"></td>
        <td width="75px" height="40px" align="center" valign="middle" style="font-size: 11px"><?= $data->price ?>РУБ.
        </td>
    </tr>
    <tr>
        <td width="75px" height="40px" align="center" valign="middle"></td>
        <td width="75px" height="40px" align="center" valign="middle" style="font-size: 11px"><?= $data->price ?>РУБ.
        </td>
    </tr>
    <tr>
        <td width="150px" height="40px" colspan="2" style="font-size: 9px" align="center" valign="top"></td>
    </tr>
    <tr>
        <td width="150px" height="40px" colspan="2" style="font-size: 9px" align="center" valign="top"></td>
    </tr>
    <tr>
        <td width="150px" height="50px" colspan="2" style="font-size: 9px" align="center"
            valign="bottom"><?= $voucher->who_created ?></td>
    </tr>
</table>
