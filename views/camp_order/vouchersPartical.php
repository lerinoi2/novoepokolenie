<table class="table table-bordered table-striped" valign="middle">
    <thead class="thead-inverse">
    <tr>
        <th data-col="1">#</th>
        <th data-col="2" style="min-width: 180px;">Дата заказа</th>
        <th data-col="3" style="min-width: 140px;">Номер заказа</th>
        <th data-col="4" style="min-width: 80px;">Статус</th>
        <th data-col="5" style="min-width: 300px;">ФИО \ Компания</th>
        <th data-col="6">Тип</th>
        <th data-col="7" style="min-width: 120px;">Скидка (%)</th>
        <th data-col="8" style="min-width: 180px;">Телефон</th>
        <th data-col="9">E-mail</th>
        <th data-col="10" style="min-width: 180px;">Лагерь</th>
        <th data-col="11" style="min-width: 180px;">Смена</th>
        <th data-col="12" style="min-width: 130px;">Дата начала</th>
        <th data-col="13" style="min-width: 130px;">Дата конца</th>
        <th data-col="14" style="min-width: 220px;">Тип лагеря</th>
        <th data-col="15" style="min-width: 150px;">Кол-во путёвок</th>
        <th data-col="16" style="min-width: 340px;">ФИО ребенка</th>
        <th data-col="17" style="min-width: 180px;">Дата рождения</th>
        <th data-col="18" style="min-width: 180px;">Номер путёвки</th>
        <th data-col="19" style="min-width: 170px;">Окончание брони</th>
        <th data-col="20" style="min-width: 180px;">Осталось до окончания (дней)</th>
        <th data-col="21" style="min-width: 140px;">Сумма заказа</th>
        <th data-col="22" style="min-width: 180px;">Сумма Оплаты</th>
        <th data-col="23" style="min-width: 180px;">Дата оплаты</th>
        <th data-col="24" style="min-width: 180px;">Осталось заплатить</th>
        <th data-col="25" style="min-width: 180px;">Комментарий</th>
        <th data-col="26" style="min-width: 180px;"></th>
    </tr>
    </thead>
    <tbody>
    <? if (count($orders) > 0) { ?>
        <? foreach ($orders as $key => $campOrder) { ?>
            <tr data-id="<?= $key ?>">
                <input name="toggle" id="<?= $key ?>" type="checkbox">
                <th data-col="1"><? echo $key + 1 ?></th>
                <td data-col="2"><? echo $campOrder->create_date_string ?></td>
                <td data-col="3"><? echo $campOrder->id ?></td>
                <td data-col="4"><? echo $campOrder->active == 1 ? $status[$campOrder->status] : 'Аннулирован' ?></td>
                <td data-col="5"><? echo $campOrder->name ?></td>
                <td data-col="6"><? echo $entity[$campOrder->entity] ?></td>
                <td data-col="7"><? echo $campOrder->discount ?></td>
                <td data-col="8"><? echo $campOrder->phone ?></td>
                <td data-col="9"><? echo $campOrder->email ?></td>
                <td data-col="10"><? echo $campOrder->camp->name ?></td>
                <td data-col="11"><? echo $campOrder->campShift->name ?></td>
                <td data-col="12"><? echo $campOrder->campShift->date_start_string ?></td>
                <td data-col="13"><? echo $campOrder->campShift->date_end_string ?></td>
                <td data-col="14"><? echo $condition[$campOrder->campShift->conditions] ?></td>
                <td data-col="15"><? echo $campOrder->count ?></td>
                <td data-col="16" <?= $campOrder->count > 2 ? "class=\"lurking\"" : '' ?>
                    style="padding: 0;">
                    <table style="width: 100%;">
                        <? for ($i = 0; $i < $campOrder->count; $i++) { ?>
                            <tr>
                                <td><? echo $campOrder->vouchers[$i]->name ?></td>
                            </tr>
                        <? } ?>
                    </table>
                </td>
                <td data-col="17" <?= $campOrder->count > 2 ? "class=\"lurking\"" : '' ?>
                    style="padding: 0;">
                    <table style="width: 100%;">
                        <? for ($i = 0; $i < $campOrder->count; $i++) { ?>
                            <tr>
                                <td><? echo $campOrder->vouchers[$i]->birth_string ?></td>
                            </tr>
                        <? } ?>
                    </table>
                </td>
                <td data-col="18" <?= $campOrder->count > 2 ? "class=\"lurking\"" : '' ?>
                    style="padding: 0;">
                    <table style="width: 100%;">
                        <? for ($i = 0; $i < $campOrder->count; $i++) { ?>
                            <tr>
                                <td><? echo $campOrder->vouchers[$i]->voucher_number ?></td>
                            </tr>
                        <? } ?>
                    </table>
                </td>

                <td data-col="19"><? echo $campOrder->booking_end_string ?></td>
                <td data-col="20"><? echo $campOrder->dayforend ?></td>
                <td data-col="21"><? echo $campOrder->sum ?></td>
                <td data-col="22" <?= count($campOrder->pays) > 2 ? "class=\"lurking\"" : '' ?>
                    style="padding: 0;">
                    <table style="width: 100%;">
                        <? foreach ($campOrder->pays as $el) { ?>
                            <tr>
                                <td>
                                    <?= $el->sum ?>
                                </td>
                            </tr>
                        <? } ?>
                    </table>
                </td>
                <td data-col="23" <?= count($campOrder->pays) > 2 ? "class=\"lurking\"" : '' ?>
                    style="padding: 0;">
                    <table style="width: 100%;">
                        <? foreach ($campOrder->pays as $el) { ?>
                            <tr>
                                <td>
                                    <?= Yii::$app->formatter->asDate($el->date, "dd.MM.y") ?>
                                </td>
                            </tr>
                        <? } ?>
                    </table>
                </td>
                <td data-col="24"><? echo $campOrder->remainder ?></td>
                <td data-col="25"><? echo 'Комментарий' ?></td>
                <td data-col="26"><a
                            href="/camp_order/voucher/create/<? echo $campOrder->id ?>">Выписать путёвку</a>
                </td>
            </tr>
        <? } ?>
    <? } ?>
    </tbody>
</table>