<?php
/* @var $this yii\web\View */

use yii\helpers\Json;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<style>
    .table td, .table th {
        vertical-align: middle;
    }
</style>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <div class="form-row align-items-center">
                <div class="col-auto">
                    <?= $form->field($model, 'type')->dropDownList(
                        $docs
                    )->label('Тип документа') ?>
                </div>
                <?= $form->field($model, 'file')->fileInput() ?>
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary">Заменить</button>
                </div>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4">
            <a target="_blank" href="/docs/download/?url=anketa_roditeli.doc">Анкета для родителей</a>
        </div>
        <div class="col-md-4">
            <a target="_blank" href="/docs/download/?url=medspravka.doc">Медицинская справка</a>
        </div>
        <div class="col-md-4">
            <a target="_blank" href="/docs/download/?url=objazatelstva_roditelej.doc">Обязательсво родителей</a>
        </div>
        <div class="col-md-4">
            <a target="_blank" href="/docs/download/?url=sogl_med_vmesh.doc">Соглашение на медицинское вмешательство</a>
        </div>
        <div class="col-md-4">
            <a target="_blank" href="/docs/download/?url=договор_дневной_18.doc">Договор дневной</a>
        </div>
        <div class="col-md-4">
            <a target="_blank" href="/docs/download/?url=договор_загородный_лагерь_18.doc">Договор загородный лагерь</a>
        </div>
        <div class="col-md-4">
            <a target="_blank" href="/docs/download/?url=договор_палатки_18.doc">Договор палатки</a>
        </div>
    </div>
</div>
</div>


