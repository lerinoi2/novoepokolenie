<?php
/* @var $this yii\web\View */
?>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <h2 class="label">Контент: </h2>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover">
                    <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>Название</th>
                        <th>Контент</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($contents as $key => $el) { ?>
                        <tr>
                            <th scope="row"><? echo $key + 1 ?></th>
                            <td><? echo $el->name ?></td>
                            <td><? echo $el->content ?></td>
                            <td><a href="/content/edit/<? echo $el->id ?>">Изменить</a></td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


