<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20.09.2017
 * Time: 9:45
 */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/web/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/web/favicon.ico" type="image/x-icon">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <?php $form = ActiveForm::begin([
                    'id' => 'form',
                    'options' => ['class' => 'form-content'],
                    'fieldConfig' => [
                        'template' => "{label}{input}\n{error}"
                    ],
                ]); ?>
                <b><label for="">Заказ № <span><?= $model->id ?></span></label></b><br>
                <p>Для подтверждения вашей брони, необходимо указать данные Вашего ребенка.</p>

                <?= $form->field($model, "name")->textInput()->label('Имя ребенка') ?>

                <?= $form->field($model, "birth_string")->textInput(['placeholder'=>'дд.мм.гггг'])->label('День рождение') ?>


                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>


                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

</body>
</html>
<?php $this->endPage() ?>