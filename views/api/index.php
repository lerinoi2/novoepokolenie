<?
function num2str($num)
{
    $nul = 'ноль';
    $ten = array(
        array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
    );
    $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
    $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
    $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
    $unit = array( // Units
        array('копейка', 'копейки', 'копеек', 1),
        array('рубль', 'рубля', 'рублей', 0),
        array('тысяча', 'тысячи', 'тысяч', 1),
        array('миллион', 'миллиона', 'миллионов', 0),
        array('миллиард', 'милиарда', 'миллиардов', 0),
    );
    //
    list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
    $out = array();
    if (intval($rub) > 0) {
        foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
            if (!intval($v)) continue;
            $uk = sizeof($unit) - $uk - 1; // unit key
            $gender = $unit[$uk][3];
            list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
            // mega-logic
            $out[] = $hundred[$i1]; # 1xx-9xx
            if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
            else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
            // units without rub & kop
            if ($uk > 1) $out[] = morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
        } //foreach
    } else $out[] = $nul;
    $out[] = morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
    $out[] = $kop . ' ' . morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
    return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
}

/**
 * Склоняем словоформу
 * @ author runcore
 */
function morph($n, $f1, $f2, $f5)
{
    $n = abs(intval($n)) % 100;
    if ($n > 10 && $n < 20) return $f5;
    $n = $n % 10;
    if ($n > 1 && $n < 5) return $f2;
    if ($n == 1) return $f1;
    return $f5;
}

function ucfirst_utf8($stri)
{
    if ($stri{0} >= "\xc3")
        return (($stri{1} >= "\xa0") ?
                ($stri{0} . chr(ord($stri{1}) - 32)) :
                ($stri{0} . $stri{1})) . substr($stri, 2);
    else return ucfirst($stri);
}

?>

<table border="0" cellpadding="0" cellspacing="0" style="font-size: 8pt;">
    <tr>
        <td align="center" style="padding: 10px 20px;">
            Внимание! Оплата данного счета означает согласие с условиями поставки товара. Уведомление об оплате
            обязательно, в противном случае не гарантируется наличие товара на складе. Товар отпускается по факту
            прихода денег на р/с Поставщика, самовывозом, при наличии доверенности и паспорта.
        </td>
    </tr>
    <tr>
        <td style="padding-bottom: 10px;">
            <table border="1px" bordercolor="#000000" cellpadding="3px" cellspacing="0"
                   style="font-size: 8pt; border: 1px solid; border-collapse: collapse;" width="100%">
                <tr style="border: 1px solid">
                    <td colspan="2" rowspan="2" style="border: 1px solid">
                        <span>ФИЛИАЛ ПАО "БАНК УРАЛСИБ" В Г.УФА Г. УФА</span><br><br>
                        <span>Банк получателя</span>
                    </td>
                    <td valign="top" style="border: 1px solid">БИК</td>
                    <td valign="top" style="border: 1px solid">048073770</td>
                </tr>
                <tr style="border: 1px solid">
                    <td valign="top" style="border: 1px solid">Сч. №</td>
                    <td valign="top" style="border: 1px solid">30101810600000000770</td>
                </tr>
                <tr style="border: 1px solid">
                    <td width="30%" style="border: 1px solid">
                        <span>ИНН</span>
                        <span style="padding-left: 10px;">5904089876</span>
                    </td>
                    <td width="30%" style="border: 1px solid">
                        <span>КПП</span>
                        <span style="padding-left: 10px;">590401001</span>
                    </td>
                    <td rowspan="2" valign="top" style="border: 1px solid">Сч. №</td>
                    <td rowspan="2" valign="top" style="border: 1px solid">40703810601240000147</td>
                </tr>
                <tr style="border: 1px solid">
                    <td colspan="2">
                        <span>Некоммерческое партнерство "НОВОЕ ПОКОЛЕНИЕ"</span><br><br>
                        <span>Получатель</span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="right" style="font-size: 10pt;"><b>Счёт действителен к оплате в течение 15 календарных дней</b></td>
    </tr>
    <tr>
        <td style="font-size: 14pt; padding-bottom: 10px; border-bottom: 2px solid;">
            <b>Счет на оплату № <?= $data->id ?> от <?= $createdate ?> г.</b>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" style="font-size: 9pt;">
                <tr>
                    <td width="15%" valign="top" style="padding-bottom: 10px; padding-top: 10px;">Поставщик:</td>
                    <td style="padding-bottom: 10px; padding-top: 10px;"><b>Некоммерческое партнерство "НОВОЕ
                            ПОКОЛЕНИЕ", ИНН 5904089876, КПП 590401001, 614010,
                            Пермский край, Пермь г, Куйбышева ул, дом № 114,
                            офис 25В, тел.: 2484801</b></td>
                </tr>
                <tr>
                    <td valign="top" style="padding-bottom: 10px;">Покупатель:</td>
                    <td style="padding-bottom: 10px;"><b><?= $data->name ?></b></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="1px" bordercolor="#000000" cellpadding="3px" cellspacing="0" width="100%"
                   style="font-size: 8pt; border: 2px solid; border-collapse: collapse;">
                <tr style="font-weight: bold; font-size: 9pt;">
                    <td align="center" width="4%" style="border: 1px solid">№</td>
                    <td align="center" width="50%" style="border: 1px solid">Товары (работы, услуги)</td>
                    <td align="center" width="10%" style="border: 1px solid">Кол-во</td>
                    <td align="center" width="6%" style="border: 1px solid">Ед.</td>
                    <td align="center" width="15%" style="border: 1px solid">Цена (руб.)</td>
                    <td align="center" width="15%" style="border: 1px solid">Сумма (руб.)</td>
                </tr>
                <tr>
                    <td align="center" style="border: 1px solid">1</td>
                    <td style="border: 1px solid"><?= $shift->name . ' (c ' . $shift->date_start_string . ' - ' . $shift->date_end_string . ')' ?></td>
                    <td align="right" style="border: 1px solid"><?= $count ?></td>
                    <td align="left" style="border: 1px solid">шт</td>
                    <td align="right" style="border: 1px solid"><?= $data->price ?>,00</td>
                    <td align="right" style="border: 1px solid"><?= $data->price * $count ?>,00</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size: 9pt;font-weight: bold;">
                <tr>
                    <td align="right" width="85%" style="padding-top: 10px;">Итого:</td>
                    <td align="right" style="padding-top: 10px;"><?= $data->price * $count ?>,00</td>
                </tr>
                <tr>
                    <td align="right">Без налога (НДС)</td>
                    <td align="right">-</td>
                </tr>
                <tr>
                    <td align="right">Всего к оплате:</td>
                    <td align="right"><?= $data->price * $count ?>,00</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="font-size: 9pt;">Всего наименований 1, на сумму <?= $data->price * $count ?>,00 руб.</td>
    </tr>
    <tr>
        <td style="font-size: 9pt; padding-bottom: 10px; border-bottom: 2px solid;">
            <b><?= ucfirst_utf8(num2str($data->price * $count)) ?></b></td>
    </tr>
    <tr>
        <td style="padding-top: 15px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size: 8pt;">
                <tr>
                    <td style="font-size: 9pt;" width="15%"><b>Руководитель</b></td>
                    <td align="right" style="border-bottom: 1px solid;">Долгих В.Н.</td>
                    <td align="center" style="font-size: 9pt;" width="15%"><b>Бухгалтер</b></td>
                    <td align="right" style="border-bottom: 1px solid;" width="30%">Вишневецкая В.А.</td>
                </tr>
            </table>
        </td>
    </tr>
</table>