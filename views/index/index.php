<?php
/* @var $this yii\web\View */
?>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h2 class="label">Путёвки: </h2>
                <? if (Yii::$app->user->identity->role == 2) { ?>
                    <a class="btn btn-info btn-block btn-lg" href="/camp_order">Список заказов</a>
                    <a class="btn btn-info btn-block btn-lg" href="/camp_order/pays">Поставить оплату</a>
                <? } ?>
                <? if (Yii::$app->user->identity->role == 3) { ?>
                    <a class="btn btn-info btn-block btn-lg" href="/camp_order/create">Новый заказ</a>
                    <a class="btn btn-info btn-block btn-lg" href="/camp_order">Список заказов</a>
                    <a class="btn btn-info btn-block btn-lg" href="/camp_order/voucher">Выписать путёвку</a>
                <? } ?>
                <? if (Yii::$app->user->identity->role == 1) { ?>
                    <a class="btn btn-info btn-block btn-lg" href="/camp_order/create">Новый заказ</a>
                    <a class="btn btn-info btn-block btn-lg" href="/camp_order">Список заказов</a>
                    <a class="btn btn-info btn-block btn-lg" href="/camp_order/voucher">Выписать путёвку</a>
                    <a class="btn btn-info btn-block btn-lg" href="/camp_order/pays">Поставить оплату</a>
                <? } ?>
            </div>
            <div class="col-md-6">
                <h2 class="label">Маршруты: </h2>
                <? if (Yii::$app->user->identity->role == 2) { ?>
                    <a class="btn btn-info btn-block btn-lg" href="/route_order">Список заказов</a>
                    <a class="btn btn-info btn-block btn-lg" href="/route_order/pays">Поставить оплату</a>
                <? } ?>
                <? if (Yii::$app->user->identity->role == 3) { ?>
                    <a class="btn btn-info btn-block btn-lg" href="/route_order/create">Новый заказ</a>
                    <a class="btn btn-info btn-block btn-lg" href="/route_order/">Список заказов</a>
                    <a class="btn btn-info btn-block btn-lg" href="">Выписать путёвку</a>
                <? } ?>
                <? if (Yii::$app->user->identity->role == 1) { ?>
                    <a class="btn btn-info btn-block btn-lg" href="/route_order/create">Новый заказ</a>
                    <a class="btn btn-info btn-block btn-lg" href="/route_order/">Список заказов</a>
                    <a class="btn btn-info btn-block btn-lg" href="">Выписать путёвку</a>
                    <a class="btn btn-info btn-block btn-lg" href="/route_order/pays">Поставить оплату</a>
                <? } ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h2 class="label">ОПЛАТА МАРШРУТА ПО НОМЕРУ ЗАКАЗА: </h2>
                <div class="reserv-modal__pays">
                    <div class="left" style="width:30%">
                        <form class="contact-form">
                            <input id="number_order" class="form-control" type="number" name="number_order" placeholder="Ввидите номер заказа">
                        </form>
                    </div>
                    <div class="right">
                        <a id="pay" href="" class="btn btn-primary">Оплатить сейчас</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

    </div>
</div>