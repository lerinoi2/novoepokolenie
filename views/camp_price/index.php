<? 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

if ($prices) {?>
    <table class="table table-bordered" style="font-size: 14px" valign="middle">
        <thead class="thead-inverse">
        <tr>
            <th>Дата начала действия стоимости</th>
            <th>Стоимость путёвки</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        <? foreach ($prices as $el) { ?>
            <tr>
                <td width="45%"><?= Yii::$app->formatter->asDate($el->date_start, "dd.MM.y"); ?></td>
                <td width="45%"><?= $el->price?> руб.</td>
                <td width="10%">
                    <?= Html::a('Удалить', ['/camp_price/delete/' . $el->id], ['class' => 'deleteCampPrice']) ?>
                </td>
            </tr>
            <?
        } ?>
        </tbody>
    </table>
<?}