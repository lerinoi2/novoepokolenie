<?php
/* @var $this yii\web\View */

use yii\helpers\Json;

?>
<style>
    .table td, .table th {
        vertical-align: middle;
    }
</style>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" id="content">
                <table class="table table-bordered table-striped" valign="middle">
                    <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th style="min-width: 140px;"><label for="sort_nz">Логин</label><input
                                type="checkbox" id="sort_nz" style="display: none;"></th>
                        <th style="min-width: 80px;"><label for="sort_status">Имя</label><input
                                type="checkbox" id="sort_status" style="display: none;"></th>
                        <th style="min-width: 80px;"><label for="sort_status">Роль</label><input
                                type="checkbox" id="sort_status" style="display: none;"></th>
                        <th style="min-width: 80px;"><label for="sort_status"></label><input
                                type="checkbox" id="sort_status" style="display: none;"></th>
                        <th style="min-width: 80px;"><label for="sort_status"></label><input
                                type="checkbox" id="sort_status" style="display: none;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? if (count($users) > 0) { ?>
                        <? foreach ($users as $key => $el) { ?>
                            <tr>
                                <th><? echo $key + 1 ?></th>
                                <td><? echo $el->login ?></td>
                                <td><? echo $el->name ?></td>
                                <td><? echo $el->role ?></td>
                                <td><a
                                        href="/users/edit/<? echo $el->id ?>">Изменить</a></td>
                                <td>Удалить</td>
                            </tr>
                        <? } ?>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


