<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20.09.2017
 * Time: 9:45
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <?php $form = ActiveForm::begin([
                    'id' => 'form',
                    'options' => ['class' => 'form-content'],
                    'fieldConfig' => [
                        'template' => "{label}\n{input}\n{error}"
                    ],
                ]); ?>

                <?= $form->field($user, 'login')->textInput()->label('Логин') ?>
                <?= $form->field($user, 'name')->textInput()->label('Имя') ?>
                <?= $form->field($user, 'role')->dropDownList(
                    $roles
                )->label('Роль') ?>
                <?= $form->field($user, 'password')->textInput()->label('Пароль') ?>
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

