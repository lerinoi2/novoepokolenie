<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 20.09.2017
 * Time: 9:45
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <?php $form = ActiveForm::begin([
            'id' => 'form',
            'options' => ['class' => 'form-content'],
            'fieldConfig' => [
                'template' => "{label}\n{input}\n{error}"
            ],
        ]); ?>

        <div id="voucher-container">
            <div class="row">
                <div class="col-md-4">
                    <b><label for="">Имя</label></b><br>
                    <span><?=$voucher->name?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <b><label for="">Дата рождения</label></b><br>
                    <span><?=$voucher->birthstring . ' (Возраст: '.$voucher->yearold.')'?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($voucher, 'party_tmp')->dropDownList(
                        $parties
                    )->label('Отряд') ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                <br><br><br>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>

