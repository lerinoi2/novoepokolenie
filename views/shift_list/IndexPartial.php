<table class="table table-bordered table-striped" valign="middle">
    <thead class="thead-inverse">
    <tr>
        <th>#</th>
        <th style="min-width: 140px;"><label for="sort_nz">Номер заказа</label><input
                type="checkbox" id="sort_nz" style="display: none;"></th>
        <th style="min-width: 300px;"><label for="sort_name">ФИО \ Компания</label>
        <th style="min-width: 180px;"><label for="sort_shift">Смена</label>
        <th style="min-width: 340px;"><label for="sort_childname">ФИО
                ребенка</label></th>
        <th style="min-width: 180px;"><label for="sort_birth">Дата рождения</label><input
                type="checkbox" id="sort_birth" style="display: none;"></th>
        <th style="min-width: 180px;"><label for="sort_birth">Лет</label><input
                type="checkbox" id="sort_birth" style="display: none;"></th>
        <th style="min-width: 180px;"><label for="sort_vouchernumb">Номер
                путёвки</label>
        <th style="min-width: 180px;"><label for="sort_vouchernumb">Отряд</label>
        <th style="min-width: 180px;"></th>
    </tr>
    </thead>
    <tbody>
    <? if (count($vouchers) > 0) { ?>
        <? foreach ($vouchers as $key => $voucher) { ?>
            <tr data-id="<?= $key ?>">
                <input name="toggle" id="<?= $key ?>" type="checkbox">
                <th data-col="1"><? echo $key + 1 ?></th>
                <td data-col="3"><? echo $voucher->order_id ?></td>
                <td data-col="3"><? echo $voucher->orderName ?></td>
                <td data-col="10"><? echo $shifts[$voucher->shiftId] ?></td>
                <td data-col="11"><? echo $voucher->name ?></td>
                <td data-col="15"><? echo $voucher->birthstring ?></td>
                <td data-col="15"><? echo $voucher->YearOld ?></td>
                <td data-col="15"><? echo $voucher->voucher_number ?></td>
                <td data-col="15"><? echo $voucher->party ?></td>
                <td data-col="26"><a
                        href="/shift_list/update/<? echo $voucher->id ?>">Поставить в отряд</a>
                </td>
            </tr>
        <? } ?>
    <? } ?>
    </tbody>
</table>