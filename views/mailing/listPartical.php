
<table class="table table-bordered table-striped" valign="middle">
    <thead class="thead-inverse">
    <tr>
        <th>#</th>
        <th style="min-width: 180px;"><label for="sort_cd">ID</label><input
                type="checkbox" id="sort_cd" style="display: none;"></th>
        <th style="min-width: 140px;"><label for="sort_nz">Название рассылки</label><input
                type="checkbox" id="sort_nz" style="display: none;"></th>
        <th style="min-width: 80px;"><label for="sort_status">Цена расслыки</label><input
                type="checkbox" id="sort_status" style="display: none;"></th>
        <th style="min-width: 80px;"><label for="sort_status">Время отправки</label><input
                type="checkbox" id="sort_status" style="display: none;"></th>
    </tr>
    </thead>
    <tbody>
    <? if (count($data) > 0) { ?>
        <? foreach ($data as $key => $el) { ?>
            <tr data-id="<?= $key ?>">
                <th><? echo $key + 1 ?></th>
                <td><? echo $el['id'] ?></td>
                <td><? echo $el['name'] ?></td>
                <td><? echo $el['company_price'] ?></td>
                <td><? echo $el['send_date'] ?></td>
            </tr>
        <? } ?>
    <? } ?>
    </tbody>
</table>