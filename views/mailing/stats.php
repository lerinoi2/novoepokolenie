<?php
/* @var $this yii\web\View */

use yii\helpers\Json;

?>
<style>
    .table td, .table th {
        vertical-align: middle;
    }
</style>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12" id="content">
                <table class="table table-bordered table-striped" valign="middle">
                    <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th style="min-width: 180px;"><label for="sort_cd">ID</label><input
                                type="checkbox" id="sort_cd" style="display: none;"></th>
                        <th style="min-width: 140px;"><label for="sort_nz">Тема рассылки</label><input
                                type="checkbox" id="sort_nz" style="display: none;"></th>
                        <th style="min-width: 80px;"><label for="sort_status">Отправлено</label><input
                                type="checkbox" id="sort_status" style="display: none;"></th>
                        <th style="min-width: 80px;"><label for="sort_status">Доставлено</label><input
                                type="checkbox" id="sort_status" style="display: none;"></th>
                        <th style="min-width: 80px;"><label for="sort_status">Открыто</label><input
                                type="checkbox" id="sort_status" style="display: none;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? if (count($data) > 0) { ?>
                        <? foreach ($data as $key => $el) { ?>
                            <tr data-id="<?= $key ?>">
                                <th><? echo $key + 1 ?></th>
                                <td><? echo $el['id'] ?></td>
                                <td><? echo $el['name'] ?></td>
                                <td><? echo $el['statistic'][0]->count ?></td>
                                <td><? echo $el['statistic'][1]->count ?></td>
                                <td><? echo $el['statistic'][2]->count ?></td>
                            </tr>
                        <? } ?>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


