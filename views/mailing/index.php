<?php
/* @var $this yii\web\View */
?>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <h2 class="label">Рассылки: </h2>
        <div class="row">
            <div class="col-md-6">
                <a class="btn btn-info btn-block btn-lg" href="/mailing/sendemail">Создать E-mail рассылку</a>
                <a class="btn btn-info btn-block btn-lg" href="/mailing/stats">Статистика E-mail рассылок</a>
            </div>
            <div class="col-md-6">
                <a class="btn btn-info btn-block btn-lg" href="/mailing/sendsms">Создать SMS рассылку</a>
                <a class="btn btn-info btn-block btn-lg" href="/mailing/list">Статистика SMS рассылок</a>
            </div>
        </div>
    </div>
</div>