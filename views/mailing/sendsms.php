<?php
/* @var $this yii\web\View */

use yii\helpers\Json;

?>
<style>
    .custom-select {
        height: 100%;
    }
</style>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form id="sendsms" style="margin-bottom: 10px;">
                    <div class="form-row align-items-center">
                        <div class="col-auto">
                            <label class="mr-sm-2" for="filter_status">Статус</label>
                            <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="filter_status" multiple>
                                <option value="all" selected>Все</option>
                                <? foreach ($status as $key => $el) { ?>
                                    <option value="<?= $key ?>"><?= $el ?></option>
                                <? } ?>
                                <option value="-1">Аннулированые</option>
                            </select>
                        </div>
                        <div class="col-auto">
                            <label class="mr-sm-2" for="filter_entity">Тип плательщика</label>
                            <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="filter_entity" multiple>
                                <option value="all" selected>Все</option>
                                <? foreach ($entity as $key => $el) { ?>
                                    <option value="<?= $key ?>"><?= $el ?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="col-auto">
                            <label class="mr-sm-2" for="filter_shift">Смена</label>
                            <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="filter_shift" multiple>
                                <option value="all" selected>Все</option>
                                <? foreach ($shifts as $key => $el) { ?>
                                    <option value="<?= $el->id ?>"><?= $el->namewithdate ?></option>
                                <? } ?>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="form-row align-items-center">
                        <div class="col-auto">
                            <div class="form-group mb-2 mr-sm-2 mb-sm-0">
                                <label for="sms_body">Сообщение</label>
                                <input type="text" id="sms_body" class="form-control" placeholder="Введите текст SMS оообщения" required style="display: inline-block;width: auto;">
                            </div>
                        </div>
                        <div class="col-auto">
                            <button type="submit" class="btn btn-primary">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" id="content">

            </div>
        </div>
    </div>
</div>


