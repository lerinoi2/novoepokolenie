<?php
/* @var $this yii\web\View */

use yii\helpers\Json;

?>
<style>
    .table td, .table th {
        vertical-align: middle;
    }
</style>
<div class="col-md-10 ml-sm-auto content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-auto">
                <form id="report" style="margin-bottom: 10px;">
                    <div class="form-row align-items-center">
                        <div class="col-auto">
                            <div class="input-group">
                                <span class="input-group-addon">от</span>
                                <input id="date_start" type="text" class="form-control"
                                       placeholder="Дата начала периода"
                                       aria-label="Дата начала периода" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="input-group">
                                <span class="input-group-addon">до</span>
                                <input id="date_end" type="text" class="form-control" placeholder="Дата конца переода"
                                       aria-label="Дата конца переода" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-auto">
                            <button type="submit" class="btn btn-primary">Показать</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12" id="content">
                <table class="table table-bordered table-striped" valign="middle">
                    <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th style="min-width: 180px;"><label for="sort_cd">ID</label><input
                                type="checkbox" id="sort_cd" style="display: none;"></th>
                        <th style="min-width: 140px;"><label for="sort_nz">Название рассылки</label><input
                                type="checkbox" id="sort_nz" style="display: none;"></th>
                        <th style="min-width: 80px;"><label for="sort_status">Цена расслыки</label><input
                                type="checkbox" id="sort_status" style="display: none;"></th>
                        <th style="min-width: 80px;"><label for="sort_status">Время отправки</label><input
                                type="checkbox" id="sort_status" style="display: none;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? if (count($data) > 0) { ?>
                        <? foreach ($data as $key => $el) { ?>
                            <tr data-id="<?= $key ?>">
                                <th><? echo $key + 1 ?></th>
                                <td><? echo $el['id'] ?></td>
                                <td><? echo $el['name'] ?></td>
                                <td><? echo $el['company_price'] ?></td>
                                <td><? echo $el['send_date'] ?></td>
                            </tr>
                        <? } ?>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


