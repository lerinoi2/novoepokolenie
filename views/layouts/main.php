<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/web/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/web/favicon.ico" type="image/x-icon">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="container-fluid">
<?php if (!Yii::$app->user->isGuest):?>
    <div class="row">
        <nav class="col-md-2 sidebar d-flex flex-column justify-content-between">
            <ul class="nav nav-pills flex-column">
                <?if(Yii::$app->user->identity->role == 1){?>
                    <li class="nav-item">
                        <a href="/" class="btn <?=$this->context->route == "index/index"?'btn-primary':'btn-secondary'?>  nav-link">Заказы</a>
                    </li>
                    <li class="nav-item">
                        <a href="/camps_shift" class="btn <?=$this->context->route == "camps_shift/index"?'btn-primary':'btn-secondary'?>  nav-link">Смены</a>
                    </li>
                    <li class="nav-item">
                        <a href="/routes" class="btn <?=$this->context->route == "routes/index"?'btn-primary':'btn-secondary'?>  nav-link">Маршруты</a>
                    </li>
                    <li class="nav-item">
                        <a href="/blacklist" class="btn <?=$this->context->route == "blacklist/index"?'btn-primary':'btn-secondary'?>  nav-link">Черный список</a>
                    </li>
                    <li class="nav-item">
                        <a href="/waitinglist" class="btn <?=$this->context->route == "waitlist/index"?'btn-primary':'btn-secondary'?>  nav-link">Лист ожидания</a>
                    </li>
                    <li class="nav-item">
                        <a href="/report" class="btn <?=$this->context->route == "report/index"?'btn-primary':'btn-secondary'?>  nav-link">Отчёт</a>
                    </li>
                    <li class="nav-item">
                        <a href="/content" class="btn <?=$this->context->route == "content/index"?'btn-primary':'btn-secondary'?>  nav-link">Управление контентом</a>
                    </li>
                    <li class="nav-item">
                        <a href="/mailing" class="btn <?=$this->context->route == "mailing/index"?'btn-primary':'btn-secondary'?>  nav-link">Рассылки</a>
                    </li>
                    <li class="nav-item">
                        <a href="/docs/" class="btn <?=$this->context->route == "docs/index"?'btn-primary':'btn-secondary'?>  nav-link">Управление документами</a>
                    </li>
                    <li class="nav-item">
                        <a href="/users/" class="btn  <?=$this->context->route == "users/index"?'btn-primary':'btn-secondary'?>  nav-link">Управление пользователями</a>
                    </li>
                    <li class="nav-item">
                        <a href="/shift_list" class="btn <?=$this->context->route == "shift_list/index"?'btn-primary':'btn-secondary'?>  nav-link">Отряды</a>
                    </li>
                    <li class="nav-item">
                        <a href="/squad" class="btn <?=$this->context->route == "squad/index"?'btn-primary':'btn-secondary'?>  nav-link">Распределение по отрядам</a>
                    </li>
                <?}?>
                <?if(Yii::$app->user->identity->role == 2){?>
                    <li class="nav-item">
                        <a href="/" class="btn <?=$this->context->route == "index/index"?'btn-primary':'btn-secondary'?>  nav-link">Заказы</a>
                    </li>
                    <li class="nav-item">
                        <a href="/camps_shift" class="btn <?=$this->context->route == "camps_shift/index"?'btn-primary':'btn-secondary'?>  nav-link">Смены</a>
                    </li>
                    <li class="nav-item">
                        <a href="/routes" class="btn <?=$this->context->route == "routes/index"?'btn-primary':'btn-secondary'?>  nav-link">Маршруты</a>
                    </li>
                    <li class="nav-item">
                        <a href="/report" class="btn <?=$this->context->route == "report/index"?'btn-primary':'btn-secondary'?>  nav-link">Отчёт</a>
                    </li>
                    <li class="nav-item">
                        <a href="/shift_list" class="btn <?=$this->context->route == "shift_list/index"?'btn-primary':'btn-secondary'?>  nav-link">Отряды</a>
                    </li>
                <?}?>
                <?if(Yii::$app->user->identity->role == 3){?>
                    <li class="nav-item">
                        <a href="/" class="btn <?=$this->context->route == "index/index"?'btn-primary':'btn-secondary'?>  nav-link">Заказы</a>
                    </li>
                    <li class="nav-item">
                        <a href="/camps_shift" class="btn <?=$this->context->route == "camps_shift/index"?'btn-primary':'btn-secondary'?>  nav-link">Смены</a>
                    </li>
                    <li class="nav-item">
                        <a href="/routes" class="btn <?=$this->context->route == "routes/index"?'btn-primary':'btn-secondary'?>  nav-link">Маршруты</a>
                    </li>
                    <li class="nav-item">
                        <a href="/shift_list" class="btn <?=$this->context->route == "shift_list/index"?'btn-primary':'btn-secondary'?>  nav-link">Отряды</a>
                    </li>
                    <li class="nav-item">
                        <a href="/waitinglist" class="btn <?=$this->context->route == "waitlist/index"?'btn-primary':'btn-secondary'?>  nav-link">Лист ожидания</a>
                    </li>
                    <li class="nav-item">
                        <a href="/report" class="btn <?=$this->context->route == "report/index"?'btn-primary':'btn-secondary'?>  nav-link">Отчёт</a>
                    </li>
                    <li class="nav-item">
                        <a href="/squad" class="btn <?=$this->context->route == "squad/index"?'btn-primary':'btn-secondary'?>  nav-link">Распределение по отрядам</a>
                    </li>
                <?}?>
                <?if(Yii::$app->user->identity->role == 5){?>
                    <li class="nav-item">
                        <a href="/squad" class="btn <?=$this->context->route == "squad/index"?'btn-primary':'btn-secondary'?>  nav-link">Распределение по отрядам</a>
                    </li>
                <?}?>
                <?if(Yii::$app->user->identity->role == 4){?>
                    <li class="nav-item">
                        <a href="/content" class="btn <?=$this->context->route == "content/index"?'btn-primary':'btn-secondary'?>  nav-link">Управление контентом</a>
                    </li>
                    <li class="nav-item">
                        <a href="/mailing" class="btn <?=$this->context->route == "mailing/index"?'btn-primary':'btn-secondary'?>  nav-link">Рассылки</a>
                    </li>

                    <li class="nav-item">
                        <a href="/report" class="btn <?=$this->context->route == "report/index"?'btn-primary':'btn-secondary'?>  nav-link">Отчёт</a>
                    </li>
                    <li class="nav-item">
                        <a href="/shift_list" class="btn <?=$this->context->route == "shift_list/index"?'btn-primary':'btn-secondary'?>  nav-link">Отряды</a>
                    </li>
                    <li class="nav-item">
                        <a href="/docs/" class="btn <?=$this->context->route == "docs/index"?'btn-primary':'btn-secondary'?>  nav-link">Управление документами</a>
                    </li>
                <?}?>
            </ul>
            <ul class="nav nav-pills flex-column logout">
                <li class="nav-item">
                    <a href="/logout" class="nav-link btn btn-light ">Выход</a>
                </li>
            </ul>
        </nav>
    </div>
    <?php endif;?>
    <?php Yii::trace($this->context->route);?>
    <?= $content ?>
</div>
</div>

<div class='container_loading loading'>
    <div class='loading-overlay'></div>
    <div style="position: absolute;top: 40%;left: 50%;transform: translate(-50%,-50%)">
        <div class='loading-anim'>
            <div class='border out'></div>
            <div class='border in'></div>
            <div class='border mid'></div>
            <div class='circle'>
                <span class='dot'></span>
                <span class='dot'></span>
                <span class='dot'></span>
                <span class='dot'></span>
                <span class='dot'></span>
                <span class='dot'></span>
                <span class='dot'></span>
                <span class='dot'></span>
                <span class='dot'></span>
                <span class='dot'></span>
                <span class='dot'></span>
                <span class='dot'></span>
            </div>
        </div>
        <div>Пожалуйста, не закрывайте окно, идет процесс отправки писем!</div>
    </div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
