<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>

<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-signin'],
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}"
    ],
]); ?>

<?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Логин') ?>

<?= $form->field($model, 'password')->passwordInput(['value' => '', 'empty'=>''])->label('Пароль') ?>

<?= $form->field($model, 'rememberMe')->checkbox([
    'template' => "{input} {label}\n{error}",
])->label('Запомнить') ?>

<?= Html::submitButton('Войти', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>

<?php ActiveForm::end(); ?>

