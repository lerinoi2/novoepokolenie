<?
//$date_create = date("d.m.y", strtotime($data['order']->booking_end_string . "-15 days"));
$date_create = stristr($data['order']->create_date_string, ',', true);

?>
<table border="0" cellpadding="0" cellspacing="0" style="font-size: 14px; width: 650px; margin: auto;" bgcolor="#f3f2e7">
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff" style="font-size: 14px;">
                <tr style="height: 50px;">
                    <td align="center" style="background-color:#ffffff;">
                        <a href="https://novoepokolenie.com/" style="padding: 15px 20px;color: #d57ead;text-decoration: none;text-transform: uppercase;">На сайт</a>
                    </td>
                    <td align="center" style="background-color:#ffffff;">
                        <a href="https://novoepokolenie.com/about/" style="padding: 15px 20px;color: #d57ead;text-decoration: none;text-transform: uppercase;">О лагере</a>
                    </td>
                    <td valign="top" rowspan="2" width="125px">
                        <a href="https://novoepokolenie.com/" style="display: block;height: 110px;width: 125px;background-color: #ffffff;color: #9b466c;text-decoration: none;text-transform: uppercase;border-radius: 0 0 100% 100%; text-align: center; position: relative; z-index: 100;">
                            <img src="http://velozames.com/local/static/imgNew/big_logo.gif" alt="Logo" style="padding: 7px 0;"><br>
                            <span>Новое<br>поколение</span>
                        </a>
                    </td>
                    <td align="center" style="background-color:#ffffff;">
                        <a href="https://novoepokolenie.com/" style="padding: 15px 20px;color: #d57ead;text-decoration: none;text-transform: uppercase;">Телефоны</a>
                    </td>
                    <td align="center" style="background-color:#ffffff;">
                        <a href="https://novoepokolenie.com/contacts/" style="padding: 15px 20px;color: #d57ead;text-decoration: none;text-transform: uppercase;">Контакты</a>
                    </td>
                </tr>
                <tr style="height: 60px;">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" style="font-size: 14px; width: 650px; margin: auto; background-color: #f3f2e7; margin-top: -60px ">
                <tr style="height: 225px">
                    <td style="background-image: url(http://velozames.com/local/static/imgNew/top_background.png);">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" style="font-size: 11px; width: 560px; margin: auto; background-color: #ffffff; margin-top: -60px; border-radius: 5px; padding: 40px; color: #333333">
                <tr>
                    <td style="font-size: 20px;text-transform: uppercase;" align="center">
                        <span>ПОЗДРАВЛЯЕМ!</span><br>
                        <span style="color: #269532; line-height: 40px;">Ваша путевка оформлена!</span>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding-top: 10px">
                        <span style="display: inline-block; background-color: #f3f3f3; height: 40px; line-height: 40px; padding: 0 20px; border-radius: 5px;font-size: 13px;">
                            <span>Бронь <b>№ <?=$data['order']->id?></b> от <b><?=$date_create?></b></span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 20px 0; line-height: 20px;">
                        <span>
                            Поздравляем вас с приобретением путевки в детский загородный оздоровительно- образовательный лагерь круглогодичного действия «Ребячий лагерь «Новое поколение»!
                        </span>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding-bottom: 20px;font-size: 13px;">
                        <b>Ваша путевка:</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="1" cellpadding="0" cellspacing="0" style="border-color: #f1f1f1;font-size: 11px;">
                            <tr style="height: 40px;">
                                <td align="center" width="40px">№</td>
                                <td style="padding: 10px 15px" width="250px">ФИО ребенка</td>
                                <td align="center" width="95px">Дата рождения</td>
                                <td align="center" width="95px">Номер путевки</td>
                            </tr>
                            <?$i = 1?>
                            <?foreach ($data['vouchers'] as $el){?>
                            <tr style="height: 40px;">
                                <td align="center"><?=$i?></td>
                                <td style="padding: 10px 15px"><?=$el['name']?></td>
                                <td align="center"><?=date('j.m.Y', $el['birth'])?></td>
                                <td align="center"><?=$el['voucher_number']?></td>
                            </tr>
                                <?$i++?>
                            <?}?>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 20px 0 0; line-height: 20px;">
                        <span>
                            Вам выслана электронная путевка в лагерь. Распечатайте ее и все необходимые документы для лагеря, которые были высланы вам на электронную почту. Заполните их.
                        </span>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding: 35px 0 20px;font-size: 15px;">
                        <span>Если у вас возникли трудности или появился вопрос, то вы можете связаться с нашим менеджером</span>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="font-size: 20px;">
                        <span>+7 (342) 248-48-01</span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" style="font-size: 14px; width: 650px; margin: auto; background-color: #f3f2e7; ">
                <tr style="height: 570px; background-image: url(http://velozames.com/local/static/imgNew/footer_img.png); background-size: cover; background-position: center 50px; background-repeat: no-repeat;">
                    <td style="padding-left: 45px; line-height: 26px; color: #3c3b2e; vertical-align: top; padding-top: 55px">
                        <h1 style="color: #724956; font-size: 30px;">Контакты</h1>Пермь,<br>
                        ул. Бульвар Гагарина 44а, 3 этаж<br>
                        часы работы: с 9.00 до 18.00 пн-пт <br>
                        e-mail: office@npcamp.ru</a>
                    </td>
                    <td style="padding-right: 45px; color: #724956; font-size: 22px; line-height: 45px; vertical-align: top; padding-top: 55px; text-align: right;">
                        <span class="buy_show">
                            <br>
                            <br>
                            +7 (342) 248-48-01
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>